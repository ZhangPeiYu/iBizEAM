import { Subject } from 'rxjs';
import { UIActionTool, ViewTool } from '@/utils';
import { PickupViewBase } from '@/studio-core';
import EMStockService from '@/service/emstock/emstock-service';
import EMStockAuthService from '@/authservice/emstock/emstock-auth-service';
import PickupViewEngine from '@engine/view/pickup-view-engine';
import EMStockUIService from '@/uiservice/emstock/emstock-ui-service';

/**
 * 库存数据选择视图视图基类
 *
 * @export
 * @class EMSTOCKPickupViewBase
 * @extends {PickupViewBase}
 */
export class EMSTOCKPickupViewBase extends PickupViewBase {
    /**
     * 视图对应应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOCKPickupViewBase
     */
    protected appDeName: string = 'emstock';

    /**
     * 应用实体主键
     *
     * @protected
     * @type {string}
     * @memberof EMSTOCKPickupViewBase
     */
    protected appDeKey: string = 'emstockid';

    /**
     * 应用实体主信息
     *
     * @protected
     * @type {string}
     * @memberof EMSTOCKPickupViewBase
     */
    protected appDeMajor: string = 'emstockname';

    /**
     * 实体服务对象
     *
     * @type {EMStockService}
     * @memberof EMSTOCKPickupViewBase
     */
    protected appEntityService: EMStockService = new EMStockService;

    /**
     * 实体权限服务对象
     *
     * @type EMStockUIService
     * @memberof EMSTOCKPickupViewBase
     */
    public appUIService: EMStockUIService = new EMStockUIService(this.$store);

    /**
     * 视图模型数据
     *
     * @protected
     * @type {*}
     * @memberof EMSTOCKPickupViewBase
     */
    protected model: any = {
        srfCaption: 'entities.emstock.views.pickupview.caption',
        srfTitle: 'entities.emstock.views.pickupview.title',
        srfSubTitle: 'entities.emstock.views.pickupview.subtitle',
        dataInfo: '',
    };

    /**
     * 容器模型
     *
     * @protected
     * @type {*}
     * @memberof EMSTOCKPickupViewBase
     */
    protected containerModel: any = {
        view_pickupviewpanel: {
            name: 'pickupviewpanel',
            type: 'PICKUPVIEWPANEL',
        },
        view_okbtn: {
            name: 'okbtn',
            type: 'button',
            text: '确定',
            disabled: true,
        },
        view_cancelbtn: {
            name: 'cancelbtn',
            type: 'button',
            text: '取消',
            disabled: false,
        },
        view_leftbtn: {
            name: 'leftbtn',
            type: 'button',
            text: '左移',
            disabled: true,
        },
        view_rightbtn: {
            name: 'rightbtn',
            type: 'button',
            text: '右移',
            disabled: true,},
        view_allleftbtn: {
            name: 'allleftbtn',
            type: 'button',
            text: '全部左移',
            disabled: true,
        },
        view_allrightbtn: {
            name: 'allrightbtn',
            type: 'button',
            text: '全部右移',
            disabled: true,
        },
    };


	/**
     * 视图唯一标识
     *
     * @protected
     * @type {string}
     * @memberof EMSTOCKPickupViewBase
     */
	protected viewtag: string = '60f1d13bd2af148643eda5a6feb02fbb';

    /**
     * 视图名称
     *
     * @protected
     * @type {string}
     * @memberof EMSTOCKPickupViewBase
     */ 
    protected viewName: string = 'EMSTOCKPickupView';


    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof EMSTOCKPickupViewBase
     */
    public engine: PickupViewEngine = new PickupViewEngine();


    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof EMSTOCKPickupViewBase
     */    
    public counterServiceArray: Array<any> = [
        
    ];

    /**
     * 引擎初始化
     *
     * @public
     * @memberof EMSTOCKPickupViewBase
     */
    public engineInit(): void {
        this.engine.init({
            view: this,
            pickupviewpanel: this.$refs.pickupviewpanel,
            keyPSDEField: 'emstock',
            majorPSDEField: 'emstockname',
            isLoadDefault: true,
        });
    }

    /**
     * pickupviewpanel 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOCKPickupViewBase
     */
    public pickupviewpanel_selectionchange($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'selectionchange', $event);
    }

    /**
     * pickupviewpanel 部件 activated 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOCKPickupViewBase
     */
    public pickupviewpanel_activated($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'activated', $event);
    }

    /**
     * pickupviewpanel 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof EMSTOCKPickupViewBase
     */
    public pickupviewpanel_load($event: any, $event2?: any): void {
        this.engine.onCtrlEvent('pickupviewpanel', 'load', $event);
    }


}