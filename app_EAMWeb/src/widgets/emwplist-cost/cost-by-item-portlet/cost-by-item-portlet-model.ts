/**
 * CostByItem 部件模型
 *
 * @export
 * @class CostByItemModel
 */
export default class CostByItemModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof CostByItemModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'wplistcostinfo',
      },
      {
        name: 'createdate',
      },
      {
        name: 'price',
      },
      {
        name: 'rempname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'adate',
      },
      {
        name: 'emwplistcostname',
      },
      {
        name: 'discnt',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'itemdesc',
      },
      {
        name: 'unitrate',
      },
      {
        name: 'begindate',
      },
      {
        name: 'listprice',
      },
      {
        name: 'items',
      },
      {
        name: 'orgid',
      },
      {
        name: 'pricecdt',
      },
      {
        name: 'intunitflag',
      },
      {
        name: 'description',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'emwplistcost',
        prop: 'emwplistcostid',
      },
      {
        name: 'wplistcosteval',
      },
      {
        name: 'unitdesc',
      },
      {
        name: 'enddate',
      },
      {
        name: 'wplistcostresult',
      },
      {
        name: 'enable',
      },
      {
        name: 'taxrate',
      },
      {
        name: 'sunitprice',
      },
      {
        name: 'sunitid',
      },
      {
        name: 'unitname',
      },
      {
        name: 'sunitname',
      },
      {
        name: 'itemname',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'avgprice',
      },
      {
        name: 'wplistid',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'itemid',
      },
      {
        name: 'unitid',
      },
    ]
  }


}
