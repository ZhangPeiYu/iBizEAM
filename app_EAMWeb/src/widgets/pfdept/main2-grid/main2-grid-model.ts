/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'deptcode',
          prop: 'deptcode',
          dataType: 'TEXT',
        },
        {
          name: 'pfdeptname',
          prop: 'pfdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'deptpname',
          prop: 'deptpname',
          dataType: 'TEXT',
        },
        {
          name: 'sdept',
          prop: 'sdept',
          dataType: 'NSCODELIST',
        },
        {
          name: 'orgid',
          prop: 'orgid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'deptfn',
          prop: 'deptfn',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'maindeptcode',
          prop: 'maindeptcode',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'pfdeptname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'pfdeptid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'pfdeptid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'pfdept',
          prop: 'pfdeptid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}