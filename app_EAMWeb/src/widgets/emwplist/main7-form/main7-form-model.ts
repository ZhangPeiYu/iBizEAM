/**
 * Main7 部件模型
 *
 * @export
 * @class Main7Model
 */
export default class Main7Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main7Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emwplistid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emwplistname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emwplistid',
        prop: 'emwplistid',
        dataType: 'GUID',
      },
      {
        name: 'wplisttype',
        prop: 'wplisttype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'adate',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'useto',
        prop: 'useto',
        dataType: 'SSCODELIST',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equips',
        prop: 'equips',
        dataType: 'TEXT',
      },
      {
        name: 'aempid',
        prop: 'aempid',
        dataType: 'PICKUP',
      },
      {
        name: 'aempname',
        prop: 'aempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'deptid',
        prop: 'deptid',
        dataType: 'PICKUP',
      },
      {
        name: 'deptname',
        prop: 'deptname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'teamid',
        prop: 'teamid',
        dataType: 'PICKUP',
      },
      {
        name: 'teamname',
        prop: 'teamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'lastdate',
        prop: 'lastdate',
        dataType: 'DATETIME',
      },
      {
        name: 'hdate',
        prop: 'hdate',
        dataType: 'DATETIME',
      },
      {
        name: 'need3q',
        prop: 'need3q',
        dataType: 'YESNO',
      },
      {
        name: 'wpliststate',
        prop: 'wpliststate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'm3q',
        prop: 'm3q',
        dataType: 'NSCODELIST',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'rempid',
        prop: 'rempid',
        dataType: 'PICKUP',
      },
      {
        name: 'rempname',
        prop: 'rempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'asum',
        prop: 'asum',
        dataType: 'FLOAT',
      },
      {
        name: 'itemdesc',
        prop: 'itemdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'emwplist',
        prop: 'emwplistid',
        dataType: 'FONTKEY',
      },
    ]
  }

}