/**
 * WpAmountFunnel 部件模型
 *
 * @export
 * @class WpAmountFunnelModel
 */
export default class WpAmountFunnelModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof WpAmountFunnelPortlet_WpAmountFunnel_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}