import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Nearest30DayByEQ 部件服务对象
 *
 * @export
 * @class Nearest30DayByEQService
 */
export default class Nearest30DayByEQService extends ControlService {
}
