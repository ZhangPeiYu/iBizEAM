import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * MainKanBan 部件服务对象
 *
 * @export
 * @class MainKanBanService
 */
export default class MainKanBanService extends ControlService {
}