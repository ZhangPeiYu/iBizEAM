import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMENConsumService from '@/service/emenconsum/emenconsum-service';
import EqEnByYearLineModel from './eq-en-by-year-line-chart-model';


/**
 * EqEnByYearLine 部件服务对象
 *
 * @export
 * @class EqEnByYearLineService
 */
export default class EqEnByYearLineService extends ControlService {

    /**
     * 能耗服务对象
     *
     * @type {EMENConsumService}
     * @memberof EqEnByYearLineService
     */
    public appEntityService: EMENConsumService = new EMENConsumService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof EqEnByYearLineService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of EqEnByYearLineService.
     * 
     * @param {*} [opts={}]
     * @memberof EqEnByYearLineService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new EqEnByYearLineModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EqEnByYearLineService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}