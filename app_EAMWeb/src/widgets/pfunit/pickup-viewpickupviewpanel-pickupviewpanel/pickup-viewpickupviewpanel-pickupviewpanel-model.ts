/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'pfunitname',
      },
      {
        name: 'unitcode',
      },
      {
        name: 'unitinfo',
      },
      {
        name: 'orgid',
      },
      {
        name: 'createman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'pfunit',
        prop: 'pfunitid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
    ]
  }


}