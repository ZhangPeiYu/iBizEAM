/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'emrfomoname',
      },
      {
        name: 'objid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'rfomoinfo',
      },
      {
        name: 'rfomocode',
      },
      {
        name: 'createman',
      },
      {
        name: 'enable',
      },
      {
        name: 'description',
      },
      {
        name: 'orgid',
      },
      {
        name: 'emrfomo',
        prop: 'emrfomoid',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'rfodename',
      },
      {
        name: 'rfodeid',
      },
    ]
  }


}