/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'objectinfo',
      },
      {
        name: 'emobjecttype',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'enable',
      },
      {
        name: 'emobject',
        prop: 'emobjectid',
      },
      {
        name: 'orgid',
      },
      {
        name: 'objectcode',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'emobjectname',
      },
      {
        name: 'majorequipname',
      },
      {
        name: 'majorequipid',
      },
    ]
  }


}