import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * Supply 部件服务对象
 *
 * @export
 * @class SupplyService
 */
export default class SupplyService extends ControlService {
}
