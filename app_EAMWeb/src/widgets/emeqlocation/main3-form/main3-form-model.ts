/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqlocationid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqlocationname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'eqlocationcode',
        prop: 'eqlocationcode',
        dataType: 'TEXT',
      },
      {
        name: 'emeqlocationname',
        prop: 'emeqlocationname',
        dataType: 'TEXT',
      },
      {
        name: 'eqlocationtype',
        prop: 'eqlocationtype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'emeqlocationid',
        prop: 'emeqlocationid',
        dataType: 'GUID',
      },
      {
        name: 'emeqlocation',
        prop: 'emeqlocationid',
        dataType: 'FONTKEY',
      },
    ]
  }

}