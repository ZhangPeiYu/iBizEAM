import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * RunInfo 部件服务对象
 *
 * @export
 * @class RunInfoService
 */
export default class RunInfoService extends ControlService {
}
