/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emequipname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipcode',
        prop: 'equipcode',
        dataType: 'TEXT',
      },
      {
        name: 'emequipname',
        prop: 'emequipname',
        dataType: 'TEXT',
      },
      {
        name: 'eqtypename',
        prop: 'eqtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqlocationname',
        prop: 'eqlocationname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipgroup',
        prop: 'equipgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'deptname',
        prop: 'deptname',
        dataType: 'TEXT',
      },
      {
        name: 'eqlocationid',
        prop: 'eqlocationid',
        dataType: 'PICKUP',
      },
      {
        name: 'emequipid',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'eqtypeid',
        prop: 'eqtypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emequip',
        prop: 'emequipid',
        dataType: 'FONTKEY',
      },
    ]
  }

}