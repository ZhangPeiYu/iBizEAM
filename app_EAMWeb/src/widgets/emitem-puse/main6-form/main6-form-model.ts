/**
 * Main6 部件模型
 *
 * @export
 * @class Main6Model
 */
export default class Main6Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main6Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitempuseid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'itempuseinfo',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'emitempuseid',
        prop: 'emitempuseid',
        dataType: 'GUID',
      },
      {
        name: 'pusetype',
        prop: 'pusetype',
        dataType: 'SSCODELIST',
      },
      {
        name: 'itemname',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'itemid',
        prop: 'itemid',
        dataType: 'PICKUP',
      },
      {
        name: 'mservicename',
        prop: 'mservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'labservicename',
        prop: 'labservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'asum',
        prop: 'asum',
        dataType: 'FLOAT',
      },
      {
        name: 'purplanname',
        prop: 'purplanname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'aempid',
        prop: 'aempid',
        dataType: 'PICKUP',
      },
      {
        name: 'aempname',
        prop: 'aempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'adate',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'useto',
        prop: 'useto',
        dataType: 'SSCODELIST',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'deptname',
        prop: 'deptname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equips',
        prop: 'equips',
        dataType: 'TEXT',
      },
      {
        name: 'teamname',
        prop: 'teamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'pusestate',
        prop: 'pusestate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'apprdesc',
        prop: 'apprdesc',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'sapcbzx',
        prop: 'sapcbzx',
        dataType: 'SSCODELIST',
      },
      {
        name: 'sapllyt',
        prop: 'sapllyt',
        dataType: 'SSCODELIST',
      },
      {
        name: 'remark',
        prop: 'remark',
        dataType: 'LONGTEXT_1000',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'psum',
        prop: 'psum',
        dataType: 'FLOAT',
      },
      {
        name: 'price',
        prop: 'price',
        dataType: 'FLOAT',
      },
      {
        name: 'amount',
        prop: 'amount',
        dataType: 'FLOAT',
      },
      {
        name: 'batcode',
        prop: 'batcode',
        dataType: 'TEXT',
      },
      {
        name: 'sempid',
        prop: 'sempid',
        dataType: 'PICKUP',
      },
      {
        name: 'sempname',
        prop: 'sempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'sdate',
        prop: 'sdate',
        dataType: 'DATETIME',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'PICKUP',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'apprempid',
        prop: 'apprempid',
        dataType: 'PICKUP',
      },
      {
        name: 'apprempname',
        prop: 'apprempname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'apprdate',
        prop: 'apprdate',
        dataType: 'DATETIME',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'createman',
        prop: 'createman',
        dataType: 'TEXT',
      },
      {
        name: 'createdate',
        prop: 'createdate',
        dataType: 'DATETIME',
      },
      {
        name: 'updateman',
        prop: 'updateman',
        dataType: 'TEXT',
      },
      {
        name: 'updatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'mserviceid',
        prop: 'mserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'teamid',
        prop: 'teamid',
        dataType: 'PICKUP',
      },
      {
        name: 'deptid',
        prop: 'deptid',
        dataType: 'PICKUP',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'woid',
        prop: 'woid',
        dataType: 'PICKUP',
      },
      {
        name: 'labserviceid',
        prop: 'labserviceid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'purplanid',
        prop: 'purplanid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitempuse',
        prop: 'emitempuseid',
        dataType: 'FONTKEY',
      },
    ]
  }

}