import EMEQType_en_US_Base from './emeqtype_en_US_base';

function getLocaleResource(){
    const EMEQType_en_US_OwnData = {};
    const targetData = Object.assign(EMEQType_en_US_Base(), EMEQType_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
