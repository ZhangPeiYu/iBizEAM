import EMApply_zh_CN_Base from './emapply_zh_CN_base';

function getLocaleResource(){
    const EMApply_zh_CN_OwnData = {};
    const targetData = Object.assign(EMApply_zh_CN_Base(), EMApply_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;