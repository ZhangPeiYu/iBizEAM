import EMApply_en_US_Base from './emapply_en_US_base';

function getLocaleResource(){
    const EMApply_en_US_OwnData = {};
    const targetData = Object.assign(EMApply_en_US_Base(), EMApply_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
