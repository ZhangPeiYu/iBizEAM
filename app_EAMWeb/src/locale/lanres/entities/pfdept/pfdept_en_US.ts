import PFDept_en_US_Base from './pfdept_en_US_base';

function getLocaleResource(){
    const PFDept_en_US_OwnData = {};
    const targetData = Object.assign(PFDept_en_US_Base(), PFDept_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
