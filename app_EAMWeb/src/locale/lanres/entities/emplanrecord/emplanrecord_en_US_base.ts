import commonLogic from '@/locale/logic/common/common-logic';

function getLocaleResourceBase(){
	const data:any = {
		fields: {
			emplanrecordid: commonLogic.appcommonhandle("触发记录标识",null),
			emplanrecordname: commonLogic.appcommonhandle("触发记录名称",null),
			createman: commonLogic.appcommonhandle("建立人",null),
			createdate: commonLogic.appcommonhandle("建立时间",null),
			updatedate: commonLogic.appcommonhandle("更新时间",null),
			updateman: commonLogic.appcommonhandle("更新人",null),
			istrigger: commonLogic.appcommonhandle("是否触发",null),
			emplanid: commonLogic.appcommonhandle("计划编号",null),
			emplanname: commonLogic.appcommonhandle("计划名称",null),
			triggerca: commonLogic.appcommonhandle("原因",null),
			triggerre: commonLogic.appcommonhandle("结果",null),
			remark: commonLogic.appcommonhandle("备注",null),
			triggerdate: commonLogic.appcommonhandle("触发时间",null),
		},
			views: {
				gridview: {
					caption: commonLogic.appcommonhandle("触发记录",null),
					title: commonLogic.appcommonhandle("触发记录表格视图",null),
				},
				editview: {
					caption: commonLogic.appcommonhandle("触发记录",null),
					title: commonLogic.appcommonhandle("触发记录编辑视图",null),
				},
			},
			main_form: {
				details: {
					group1: commonLogic.appcommonhandle("触发记录基本信息",null), 
					formpage1: commonLogic.appcommonhandle("基本信息",null), 
					srfupdatedate: commonLogic.appcommonhandle("更新时间",null), 
					srforikey: commonLogic.appcommonhandle("",null), 
					srfkey: commonLogic.appcommonhandle("触发记录标识",null), 
					srfmajortext: commonLogic.appcommonhandle("触发记录名称",null), 
					srftempmode: commonLogic.appcommonhandle("",null), 
					srfuf: commonLogic.appcommonhandle("",null), 
					srfdeid: commonLogic.appcommonhandle("",null), 
					srfsourcekey: commonLogic.appcommonhandle("",null), 
					emplanname: commonLogic.appcommonhandle("计划",null), 
					triggerdate: commonLogic.appcommonhandle("触发时间",null), 
					istrigger: commonLogic.appcommonhandle("是否触发",null), 
					triggerca: commonLogic.appcommonhandle("原因",null), 
					triggerre: commonLogic.appcommonhandle("结果",null), 
					remark: commonLogic.appcommonhandle("备注",null), 
					emplanid: commonLogic.appcommonhandle("计划编号",null), 
					emplanrecordid: commonLogic.appcommonhandle("触发记录标识",null), 
				},
				uiactions: {
				},
			},
			main_grid: {
				columns: {
					emplanname: commonLogic.appcommonhandle("计划名称",null),
					triggerdate: commonLogic.appcommonhandle("触发时间",null),
					istrigger: commonLogic.appcommonhandle("是否触发",null),
				},
				nodata:commonLogic.appcommonhandle("",null),
				uiactions: {
				},
			},
			default_searchform: {
				details: {
					formpage1: commonLogic.appcommonhandle("常规条件",null), 
				},
				uiactions: {
				},
			},
			editviewtoolbar_toolbar: {
				tbitem2: {
					caption: commonLogic.appcommonhandle("关闭",null),
					tip: commonLogic.appcommonhandle("关闭",null),
				},
			},
		};
		return data;
}

export default getLocaleResourceBase;