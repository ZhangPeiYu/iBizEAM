import EMEQSetup_en_US_Base from './emeqsetup_en_US_base';

function getLocaleResource(){
    const EMEQSetup_en_US_OwnData = {};
    const targetData = Object.assign(EMEQSetup_en_US_Base(), EMEQSetup_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
