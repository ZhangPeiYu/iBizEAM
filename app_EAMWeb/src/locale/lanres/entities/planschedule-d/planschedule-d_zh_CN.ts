import PLANSCHEDULE_D_zh_CN_Base from './planschedule-d_zh_CN_base';

function getLocaleResource(){
    const PLANSCHEDULE_D_zh_CN_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_D_zh_CN_Base(), PLANSCHEDULE_D_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;