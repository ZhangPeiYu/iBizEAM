import PFUnit_en_US_Base from './pfunit_en_US_base';

function getLocaleResource(){
    const PFUnit_en_US_OwnData = {};
    const targetData = Object.assign(PFUnit_en_US_Base(), PFUnit_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
