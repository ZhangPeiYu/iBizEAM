import EMDRWGMap_zh_CN_Base from './emdrwgmap_zh_CN_base';

function getLocaleResource(){
    const EMDRWGMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMDRWGMap_zh_CN_Base(), EMDRWGMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;