import EMEN_en_US_Base from './emen_en_US_base';

function getLocaleResource(){
    const EMEN_en_US_OwnData = {};
    const targetData = Object.assign(EMEN_en_US_Base(), EMEN_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
