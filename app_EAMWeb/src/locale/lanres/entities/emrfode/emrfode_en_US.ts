import EMRFODE_en_US_Base from './emrfode_en_US_base';

function getLocaleResource(){
    const EMRFODE_en_US_OwnData = {};
    const targetData = Object.assign(EMRFODE_en_US_Base(), EMRFODE_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
