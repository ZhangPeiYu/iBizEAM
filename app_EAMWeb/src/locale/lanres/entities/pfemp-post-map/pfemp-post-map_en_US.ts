import PFEmpPostMap_en_US_Base from './pfemp-post-map_en_US_base';

function getLocaleResource(){
    const PFEmpPostMap_en_US_OwnData = {};
    const targetData = Object.assign(PFEmpPostMap_en_US_Base(), PFEmpPostMap_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
