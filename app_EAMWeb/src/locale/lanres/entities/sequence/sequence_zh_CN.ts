import SEQUENCE_zh_CN_Base from './sequence_zh_CN_base';

function getLocaleResource(){
    const SEQUENCE_zh_CN_OwnData = {};
    const targetData = Object.assign(SEQUENCE_zh_CN_Base(), SEQUENCE_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;