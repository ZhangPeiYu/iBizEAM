import SEQUENCE_en_US_Base from './sequence_en_US_base';

function getLocaleResource(){
    const SEQUENCE_en_US_OwnData = {};
    const targetData = Object.assign(SEQUENCE_en_US_Base(), SEQUENCE_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
