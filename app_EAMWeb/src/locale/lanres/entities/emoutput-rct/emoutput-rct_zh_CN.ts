import EMOutputRct_zh_CN_Base from './emoutput-rct_zh_CN_base';

function getLocaleResource(){
    const EMOutputRct_zh_CN_OwnData = {};
    const targetData = Object.assign(EMOutputRct_zh_CN_Base(), EMOutputRct_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;