import EMOutputRct_en_US_Base from './emoutput-rct_en_US_base';

function getLocaleResource(){
    const EMOutputRct_en_US_OwnData = {};
    const targetData = Object.assign(EMOutputRct_en_US_Base(), EMOutputRct_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
