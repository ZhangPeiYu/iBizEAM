import EMStock_en_US_Base from './emstock_en_US_base';

function getLocaleResource(){
    const EMStock_en_US_OwnData = {};
    const targetData = Object.assign(EMStock_en_US_Base(), EMStock_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
