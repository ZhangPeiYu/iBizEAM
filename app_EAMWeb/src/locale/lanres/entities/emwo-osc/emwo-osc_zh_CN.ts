import EMWO_OSC_zh_CN_Base from './emwo-osc_zh_CN_base';

function getLocaleResource(){
    const EMWO_OSC_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWO_OSC_zh_CN_Base(), EMWO_OSC_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;