import EMObjMap_zh_CN_Base from './emobj-map_zh_CN_base';

function getLocaleResource(){
    const EMObjMap_zh_CN_OwnData = {};
    const targetData = Object.assign(EMObjMap_zh_CN_Base(), EMObjMap_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;