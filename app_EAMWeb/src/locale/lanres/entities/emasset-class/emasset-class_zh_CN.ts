import EMAssetClass_zh_CN_Base from './emasset-class_zh_CN_base';

function getLocaleResource(){
    const EMAssetClass_zh_CN_OwnData = {};
    const targetData = Object.assign(EMAssetClass_zh_CN_Base(), EMAssetClass_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;