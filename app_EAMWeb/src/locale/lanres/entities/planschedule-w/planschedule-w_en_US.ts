import PLANSCHEDULE_W_en_US_Base from './planschedule-w_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_W_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_W_en_US_Base(), PLANSCHEDULE_W_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
