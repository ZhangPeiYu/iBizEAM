import EMRFODEType_en_US_Base from './emrfodetype_en_US_base';

function getLocaleResource(){
    const EMRFODEType_en_US_OwnData = {};
    const targetData = Object.assign(EMRFODEType_en_US_Base(), EMRFODEType_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
