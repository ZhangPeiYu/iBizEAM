import EMEQSpareDetail_zh_CN_Base from './emeqspare-detail_zh_CN_base';

function getLocaleResource(){
    const EMEQSpareDetail_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQSpareDetail_zh_CN_Base(), EMEQSpareDetail_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;