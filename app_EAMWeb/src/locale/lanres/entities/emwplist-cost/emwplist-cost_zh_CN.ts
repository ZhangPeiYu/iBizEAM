import EMWPListCost_zh_CN_Base from './emwplist-cost_zh_CN_base';

function getLocaleResource(){
    const EMWPListCost_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWPListCost_zh_CN_Base(), EMWPListCost_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;