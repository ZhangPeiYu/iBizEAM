import EMItemPL_zh_CN_Base from './emitem-pl_zh_CN_base';

function getLocaleResource(){
    const EMItemPL_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemPL_zh_CN_Base(), EMItemPL_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;