import EMEQMP_zh_CN_Base from './emeqmp_zh_CN_base';

function getLocaleResource(){
    const EMEQMP_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQMP_zh_CN_Base(), EMEQMP_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;