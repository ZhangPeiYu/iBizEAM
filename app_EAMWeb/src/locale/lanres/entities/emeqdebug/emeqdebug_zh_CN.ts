import EMEQDebug_zh_CN_Base from './emeqdebug_zh_CN_base';

function getLocaleResource(){
    const EMEQDebug_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQDebug_zh_CN_Base(), EMEQDebug_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;