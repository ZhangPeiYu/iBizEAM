import EMObject_en_US_Base from './emobject_en_US_base';

function getLocaleResource(){
    const EMObject_en_US_OwnData = {};
    const targetData = Object.assign(EMObject_en_US_Base(), EMObject_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
