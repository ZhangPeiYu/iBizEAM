import EMItemPUseUIServiceBase from './emitem-puse-ui-service-base';

/**
 * 领料单UI服务对象
 *
 * @export
 * @class EMItemPUseUIService
 */
export default class EMItemPUseUIService extends EMItemPUseUIServiceBase {

    /**
     * Creates an instance of  EMItemPUseUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemPUseUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}