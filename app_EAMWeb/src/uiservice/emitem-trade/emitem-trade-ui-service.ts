import EMItemTradeUIServiceBase from './emitem-trade-ui-service-base';

/**
 * 物品交易UI服务对象
 *
 * @export
 * @class EMItemTradeUIService
 */
export default class EMItemTradeUIService extends EMItemTradeUIServiceBase {

    /**
     * Creates an instance of  EMItemTradeUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemTradeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}