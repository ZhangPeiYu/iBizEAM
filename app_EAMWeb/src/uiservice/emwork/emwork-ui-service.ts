import EMWorkUIServiceBase from './emwork-ui-service-base';

/**
 * 加班工单UI服务对象
 *
 * @export
 * @class EMWorkUIService
 */
export default class EMWorkUIService extends EMWorkUIServiceBase {

    /**
     * Creates an instance of  EMWorkUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWorkUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}