import PFDeptUIServiceBase from './pfdept-ui-service-base';

/**
 * 部门UI服务对象
 *
 * @export
 * @class PFDeptUIService
 */
export default class PFDeptUIService extends PFDeptUIServiceBase {

    /**
     * Creates an instance of  PFDeptUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  PFDeptUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}