import EMENConsumUIServiceBase from './emenconsum-ui-service-base';

/**
 * 能耗UI服务对象
 *
 * @export
 * @class EMENConsumUIService
 */
export default class EMENConsumUIService extends EMENConsumUIServiceBase {

    /**
     * Creates an instance of  EMENConsumUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENConsumUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}