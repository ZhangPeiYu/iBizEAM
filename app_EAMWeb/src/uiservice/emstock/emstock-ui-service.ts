import EMStockUIServiceBase from './emstock-ui-service-base';

/**
 * 库存UI服务对象
 *
 * @export
 * @class EMStockUIService
 */
export default class EMStockUIService extends EMStockUIServiceBase {

    /**
     * Creates an instance of  EMStockUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStockUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}