import SEQUENCEUIServiceBase from './sequence-ui-service-base';

/**
 * 序列号UI服务对象
 *
 * @export
 * @class SEQUENCEUIService
 */
export default class SEQUENCEUIService extends SEQUENCEUIServiceBase {

    /**
     * Creates an instance of  SEQUENCEUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  SEQUENCEUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}