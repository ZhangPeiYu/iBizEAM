import EMItemUIServiceBase from './emitem-ui-service-base';

/**
 * 物品UI服务对象
 *
 * @export
 * @class EMItemUIService
 */
export default class EMItemUIService extends EMItemUIServiceBase {

    /**
     * Creates an instance of  EMItemUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}