import EMEQKeepUIServiceBase from './emeqkeep-ui-service-base';

/**
 * 维护保养UI服务对象
 *
 * @export
 * @class EMEQKeepUIService
 */
export default class EMEQKeepUIService extends EMEQKeepUIServiceBase {

    /**
     * Creates an instance of  EMEQKeepUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQKeepUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}