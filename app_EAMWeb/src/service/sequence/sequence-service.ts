import { Http } from '@/utils';
import { Util } from '@/utils';
import SEQUENCEServiceBase from './sequence-service-base';


/**
 * 序列号服务对象
 *
 * @export
 * @class SEQUENCEService
 * @extends {SEQUENCEServiceBase}
 */
export default class SEQUENCEService extends SEQUENCEServiceBase {

    /**
     * Creates an instance of  SEQUENCEService.
     * 
     * @param {*} [opts={}]
     * @memberof  SEQUENCEService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}