import { Http } from '@/utils';
import { Util } from '@/utils';
import SubmitLogicBase from './submit-logic-base';

/**
 * 提交
 *
 * @export
 * @class SubmitLogic
 */
export default class SubmitLogic extends SubmitLogicBase{

    /**
     * Creates an instance of  SubmitLogic
     * 
     * @param {*} [opts={}]
     * @memberof  SubmitLogic
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}