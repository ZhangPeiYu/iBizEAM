import { Http } from '@/utils';
import { Util } from '@/utils';
import EMStoreServiceBase from './emstore-service-base';


/**
 * 仓库服务对象
 *
 * @export
 * @class EMStoreService
 * @extends {EMStoreServiceBase}
 */
export default class EMStoreService extends EMStoreServiceBase {

    /**
     * Creates an instance of  EMStoreService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStoreService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}