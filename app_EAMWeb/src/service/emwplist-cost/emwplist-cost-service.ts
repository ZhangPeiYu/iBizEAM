import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWPListCostServiceBase from './emwplist-cost-service-base';


/**
 * 询价单服务对象
 *
 * @export
 * @class EMWPListCostService
 * @extends {EMWPListCostServiceBase}
 */
export default class EMWPListCostService extends EMWPListCostServiceBase {

    /**
     * Creates an instance of  EMWPListCostService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListCostService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}