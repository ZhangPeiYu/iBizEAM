import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULE_DServiceBase from './planschedule-d-service-base';


/**
 * 计划_按天服务对象
 *
 * @export
 * @class PLANSCHEDULE_DService
 * @extends {PLANSCHEDULE_DServiceBase}
 */
export default class PLANSCHEDULE_DService extends PLANSCHEDULE_DServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULE_DService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULE_DService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}