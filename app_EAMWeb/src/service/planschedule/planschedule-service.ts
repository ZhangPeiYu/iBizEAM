import { Http } from '@/utils';
import { Util } from '@/utils';
import PLANSCHEDULEServiceBase from './planschedule-service-base';


/**
 * 计划时刻设置服务对象
 *
 * @export
 * @class PLANSCHEDULEService
 * @extends {PLANSCHEDULEServiceBase}
 */
export default class PLANSCHEDULEService extends PLANSCHEDULEServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULEService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULEService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}