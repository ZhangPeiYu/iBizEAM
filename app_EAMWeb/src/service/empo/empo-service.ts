import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPOServiceBase from './empo-service-base';


/**
 * 订单服务对象
 *
 * @export
 * @class EMPOService
 * @extends {EMPOServiceBase}
 */
export default class EMPOService extends EMPOServiceBase {

    /**
     * Creates an instance of  EMPOService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPOService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}