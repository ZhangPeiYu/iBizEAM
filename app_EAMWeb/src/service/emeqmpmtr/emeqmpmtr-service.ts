import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQMPMTRServiceBase from './emeqmpmtr-service-base';


/**
 * 设备仪表读数服务对象
 *
 * @export
 * @class EMEQMPMTRService
 * @extends {EMEQMPMTRServiceBase}
 */
export default class EMEQMPMTRService extends EMEQMPMTRServiceBase {

    /**
     * Creates an instance of  EMEQMPMTRService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMPMTRService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}