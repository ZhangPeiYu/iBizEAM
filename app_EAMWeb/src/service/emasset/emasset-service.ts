import { Http } from '@/utils';
import { Util } from '@/utils';
import EMAssetServiceBase from './emasset-service-base';


/**
 * 资产服务对象
 *
 * @export
 * @class EMAssetService
 * @extends {EMAssetServiceBase}
 */
export default class EMAssetService extends EMAssetServiceBase {

    /**
     * Creates an instance of  EMAssetService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}