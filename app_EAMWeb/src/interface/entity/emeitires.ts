/**
 * 轮胎清单
 *
 * @export
 * @interface EMEITIRes
 */
export interface EMEITIRes {

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    empid?: any;

    /**
     * 轮胎信息
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    eicaminfo?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    description?: any;

    /**
     * 地址
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    macaddr?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    createdate?: any;

    /**
     * 编码/出场号
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    emeitiresname?: any;

    /**
     * 状态
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    eistate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    updatedate?: any;

    /**
     * 报废原因
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    disdesc?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    empname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    updateman?: any;

    /**
     * 轮胎编号
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    emeitiresid?: any;

    /**
     * 报废日期
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    disdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    orgid?: any;

    /**
     * 型号
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    eqmodelcode?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    enable?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    equipname?: any;

    /**
     * 领料单
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    itempusename?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    eqlocationname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    eqlocationid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    equipid?: any;

    /**
     * 领料单
     *
     * @returns {*}
     * @memberof EMEITIRes
     */
    itempuseid?: any;
}