/**
 * 能力
 *
 * @export
 * @interface EMOutput
 */
export interface EMOutput {

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMOutput
     */
    updatedate?: any;

    /**
     * 能力标识
     *
     * @returns {*}
     * @memberof EMOutput
     */
    emoutputid?: any;

    /**
     * 能力代码
     *
     * @returns {*}
     * @memberof EMOutput
     */
    outputcode?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMOutput
     */
    description?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMOutput
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMOutput
     */
    createman?: any;

    /**
     * 能力名称
     *
     * @returns {*}
     * @memberof EMOutput
     */
    emoutputname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMOutput
     */
    updateman?: any;

    /**
     * 能力信息
     *
     * @returns {*}
     * @memberof EMOutput
     */
    outputinfo?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMOutput
     */
    createdate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMOutput
     */
    enable?: any;
}