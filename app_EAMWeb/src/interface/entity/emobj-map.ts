/**
 * 对象关系
 *
 * @export
 * @interface EMObjMap
 */
export interface EMObjMap {

    /**
     * 对象关系标识
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    emobjmapid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    updatedate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    updateman?: any;

    /**
     * 设备编号
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    equipid?: any;

    /**
     * 对象关系类型
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    emobjmaptype?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    createdate?: any;

    /**
     * 对象关系名称
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    emobjmapname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    createman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    enable?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    orgid?: any;

    /**
     * 上级对象类型
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objptype?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    majorequipname?: any;

    /**
     * 主设备
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    majorequipid?: any;

    /**
     * 上级对象
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objpname?: any;

    /**
     * 对象类型
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objtype?: any;

    /**
     * 对象
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objname?: any;

    /**
     * 对象
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objid?: any;

    /**
     * 上级对象
     *
     * @returns {*}
     * @memberof EMObjMap
     */
    objpid?: any;
}