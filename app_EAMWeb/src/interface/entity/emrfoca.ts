/**
 * 原因
 *
 * @export
 * @interface EMRFOCA
 */
export interface EMRFOCA {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    enable?: any;

    /**
     * 原因标识
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    emrfocaid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    createdate?: any;

    /**
     * 原因代码
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfocacode?: any;

    /**
     * 原因名称
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    emrfocaname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    createman?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    orgid?: any;

    /**
     * 信息
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfocainfo?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    updatedate?: any;

    /**
     * 对象编号
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    objid?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfodename?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfomoname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfodeid?: any;

    /**
     * 模式
     *
     * @returns {*}
     * @memberof EMRFOCA
     */
    rfomoid?: any;
}