/**
 * 能耗
 *
 * @export
 * @interface EMENConsum
 */
export interface EMENConsum {

    /**
     * 单价
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    price?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    deptid?: any;

    /**
     * 能耗标识
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    emenconsumid?: any;

    /**
     * 领料分类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    pusetype?: any;

    /**
     * 倍率
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    vrate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    orgid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    updatedate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    description?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    createdate?: any;

    /**
     * 总金额
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    amount?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    createman?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    updateman?: any;

    /**
     * 能耗值
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    nval?: any;

    /**
     * 上次记录值
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    lastval?: any;

    /**
     * 部门
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    deptname?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    enable?: any;

    /**
     * 本次记录值
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    curval?: any;

    /**
     * 能耗名称
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    emenconsumname?: any;

    /**
     * 上次采集时间
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    bdate?: any;

    /**
     * 采集时间
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    edate?: any;

    /**
     * 能源单价
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    enprice?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    objname?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemmtypename?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemname?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    woname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    unitname?: any;

    /**
     * 能源库存单价
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemprice?: any;

    /**
     * 能源类型
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    energytypeid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    equipcode?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    equipname?: any;

    /**
     * 能源
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    enname?: any;

    /**
     * 设备统计归类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    sname?: any;

    /**
     * 物品类型
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemtypeid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itembtypeid?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itembtypename?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemid?: any;

    /**
     * 物品二级类
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    itemmtypeid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    objid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    equipid?: any;

    /**
     * 工单
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    woid?: any;

    /**
     * 能源
     *
     * @returns {*}
     * @memberof EMENConsum
     */
    enid?: any;
}