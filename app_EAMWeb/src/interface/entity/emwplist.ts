/**
 * 采购申请
 *
 * @export
 * @interface EMWPList
 */
export interface EMWPList {

    /**
     * 批准日期
     *
     * @returns {*}
     * @memberof EMWPList
     */
    apprdate?: any;

    /**
     * 申请日期
     *
     * @returns {*}
     * @memberof EMWPList
     */
    adate?: any;

    /**
     * 采购申请
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplistids?: any;

    /**
     * 经理指定询价数
     *
     * @returns {*}
     * @memberof EMWPList
     */
    m3q?: any;

    /**
     * 需要3次询价
     *
     * @returns {*}
     * @memberof EMWPList
     */
    need3q?: any;

    /**
     * 采购申请名称
     *
     * @returns {*}
     * @memberof EMWPList
     */
    emwplistname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWPList
     */
    description?: any;

    /**
     * 希望到货日期
     *
     * @returns {*}
     * @memberof EMWPList
     */
    hdate?: any;

    /**
     * 设备集合
     *
     * @returns {*}
     * @memberof EMWPList
     */
    equips?: any;

    /**
     * 询价记录数
     *
     * @returns {*}
     * @memberof EMWPList
     */
    qcnt?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWPList
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWPList
     */
    orgid?: any;

    /**
     * 物品备注
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemdesc?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    createman?: any;

    /**
     * 采购申请号
     *
     * @returns {*}
     * @memberof EMWPList
     */
    emwplistid?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWPList
     */
    enable?: any;

    /**
     * 请购类型
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplisttype?: any;

    /**
     * 物品和备注
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemanditemdesc?: any;

    /**
     * 询价总金额
     *
     * @returns {*}
     * @memberof EMWPList
     */
    costamount?: any;

    /**
     * 用途
     *
     * @returns {*}
     * @memberof EMWPList
     */
    useto?: any;

    /**
     * 工作流状态
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wfstate?: any;

    /**
     * 是否为取消标志
     *
     * @returns {*}
     * @memberof EMWPList
     */
    iscancel?: any;

    /**
     * 工作流实例
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wfinstanceid?: any;

    /**
     * 上次请购时间
     *
     * @returns {*}
     * @memberof EMWPList
     */
    lastdate?: any;

    /**
     * 审核意见
     *
     * @returns {*}
     * @memberof EMWPList
     */
    apprdesc?: any;

    /**
     * 请购状态
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wpliststate?: any;

    /**
     * 流程步骤
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wfstep?: any;

    /**
     * 处理结果
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplistdp?: any;

    /**
     * 请购数
     *
     * @returns {*}
     * @memberof EMWPList
     */
    asum?: any;

    /**
     * 采购申请信息
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplistinfo?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWPList
     */
    updatedate?: any;

    /**
     * 预计总金额
     *
     * @returns {*}
     * @memberof EMWPList
     */
    pamount?: any;

    /**
     * 删除标识
     *
     * @returns {*}
     * @memberof EMWPList
     */
    deltype?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMWPList
     */
    labservicename?: any;

    /**
     * 不足3家供应商
     *
     * @returns {*}
     * @memberof EMWPList
     */
    no3q?: any;

    /**
     * 物品代码
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemcode?: any;

    /**
     * 询价结果
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplistcostname?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMWPList
     */
    unitname?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemname?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWPList
     */
    emservicename?: any;

    /**
     * 物品大类
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itembtypeid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemname_show?: any;

    /**
     * 物品均价
     *
     * @returns {*}
     * @memberof EMWPList
     */
    avgprice?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWPList
     */
    equipname?: any;

    /**
     * 申请班组
     *
     * @returns {*}
     * @memberof EMWPList
     */
    teamname?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWPList
     */
    objname?: any;

    /**
     * 产品供应商
     *
     * @returns {*}
     * @memberof EMWPList
     */
    labserviceid?: any;

    /**
     * 单位
     *
     * @returns {*}
     * @memberof EMWPList
     */
    unitid?: any;

    /**
     * 申请班组
     *
     * @returns {*}
     * @memberof EMWPList
     */
    teamid?: any;

    /**
     * 位置
     *
     * @returns {*}
     * @memberof EMWPList
     */
    objid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMWPList
     */
    emserviceid?: any;

    /**
     * 询价结果
     *
     * @returns {*}
     * @memberof EMWPList
     */
    wplistcostid?: any;

    /**
     * 设备
     *
     * @returns {*}
     * @memberof EMWPList
     */
    equipid?: any;

    /**
     * 物品
     *
     * @returns {*}
     * @memberof EMWPList
     */
    itemid?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    aempid?: any;

    /**
     * 申请人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    aempname?: any;

    /**
     * 申请部门
     *
     * @returns {*}
     * @memberof EMWPList
     */
    deptid?: any;

    /**
     * 申请部门
     *
     * @returns {*}
     * @memberof EMWPList
     */
    deptname?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    apprempid?: any;

    /**
     * 批准人
     *
     * @returns {*}
     * @memberof EMWPList
     */
    apprempname?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMWPList
     */
    rempid?: any;

    /**
     * 采购员
     *
     * @returns {*}
     * @memberof EMWPList
     */
    rempname?: any;
}