/**
 * 现象引用
 *
 * @export
 * @interface EMRFODEMap
 */
export interface EMRFODEMap {

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    updateman?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    createdate?: any;

    /**
     * 现象引用标识
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    emrfodemapid?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    description?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    orgid?: any;

    /**
     * 现象引用名称
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    emrfodemapname?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    createman?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    rfodename?: any;

    /**
     * 现象引用
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    refobjname?: any;

    /**
     * 现象
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    rfodeid?: any;

    /**
     * 现象引用
     *
     * @returns {*}
     * @memberof EMRFODEMap
     */
    refobjid?: any;
}