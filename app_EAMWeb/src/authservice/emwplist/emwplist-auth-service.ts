import EMWPListAuthServiceBase from './emwplist-auth-service-base';


/**
 * 采购申请权限服务对象
 *
 * @export
 * @class EMWPListAuthService
 * @extends {EMWPListAuthServiceBase}
 */
export default class EMWPListAuthService extends EMWPListAuthServiceBase {

    /**
     * Creates an instance of  EMWPListAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}