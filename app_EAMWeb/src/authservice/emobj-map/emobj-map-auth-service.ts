import EMObjMapAuthServiceBase from './emobj-map-auth-service-base';


/**
 * 对象关系权限服务对象
 *
 * @export
 * @class EMObjMapAuthService
 * @extends {EMObjMapAuthServiceBase}
 */
export default class EMObjMapAuthService extends EMObjMapAuthServiceBase {

    /**
     * Creates an instance of  EMObjMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}