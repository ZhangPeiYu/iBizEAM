import EMPOAuthServiceBase from './empo-auth-service-base';


/**
 * 订单权限服务对象
 *
 * @export
 * @class EMPOAuthService
 * @extends {EMPOAuthServiceBase}
 */
export default class EMPOAuthService extends EMPOAuthServiceBase {

    /**
     * Creates an instance of  EMPOAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPOAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}