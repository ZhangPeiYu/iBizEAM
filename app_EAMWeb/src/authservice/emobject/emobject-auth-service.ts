import EMObjectAuthServiceBase from './emobject-auth-service-base';


/**
 * 对象权限服务对象
 *
 * @export
 * @class EMObjectAuthService
 * @extends {EMObjectAuthServiceBase}
 */
export default class EMObjectAuthService extends EMObjectAuthServiceBase {

    /**
     * Creates an instance of  EMObjectAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMObjectAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}