import PLANSCHEDULEAuthServiceBase from './planschedule-auth-service-base';


/**
 * 计划时刻设置权限服务对象
 *
 * @export
 * @class PLANSCHEDULEAuthService
 * @extends {PLANSCHEDULEAuthServiceBase}
 */
export default class PLANSCHEDULEAuthService extends PLANSCHEDULEAuthServiceBase {

    /**
     * Creates an instance of  PLANSCHEDULEAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  PLANSCHEDULEAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}