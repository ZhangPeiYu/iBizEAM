import EMWO_DPAuthServiceBase from './emwo-dp-auth-service-base';


/**
 * 点检工单权限服务对象
 *
 * @export
 * @class EMWO_DPAuthService
 * @extends {EMWO_DPAuthServiceBase}
 */
export default class EMWO_DPAuthService extends EMWO_DPAuthServiceBase {

    /**
     * Creates an instance of  EMWO_DPAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWO_DPAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}