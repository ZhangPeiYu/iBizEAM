import EMItemAuthServiceBase from './emitem-auth-service-base';


/**
 * 物品权限服务对象
 *
 * @export
 * @class EMItemAuthService
 * @extends {EMItemAuthServiceBase}
 */
export default class EMItemAuthService extends EMItemAuthServiceBase {

    /**
     * Creates an instance of  EMItemAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMItemAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}