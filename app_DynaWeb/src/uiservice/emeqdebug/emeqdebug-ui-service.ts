import EMEQDebugUIServiceBase from './emeqdebug-ui-service-base';

/**
 * 事故记录UI服务对象
 *
 * @export
 * @class EMEQDebugUIService
 */
export default class EMEQDebugUIService extends EMEQDebugUIServiceBase {

    /**
     * Creates an instance of  EMEQDebugUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQDebugUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}