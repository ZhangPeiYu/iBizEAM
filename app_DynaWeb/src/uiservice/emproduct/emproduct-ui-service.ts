import EMProductUIServiceBase from './emproduct-ui-service-base';

/**
 * 试用品UI服务对象
 *
 * @export
 * @class EMProductUIService
 */
export default class EMProductUIService extends EMProductUIServiceBase {

    /**
     * Creates an instance of  EMProductUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMProductUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}