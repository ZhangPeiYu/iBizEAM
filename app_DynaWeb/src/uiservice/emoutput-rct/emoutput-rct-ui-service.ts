import EMOutputRctUIServiceBase from './emoutput-rct-ui-service-base';

/**
 * 产能UI服务对象
 *
 * @export
 * @class EMOutputRctUIService
 */
export default class EMOutputRctUIService extends EMOutputRctUIServiceBase {

    /**
     * Creates an instance of  EMOutputRctUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMOutputRctUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}