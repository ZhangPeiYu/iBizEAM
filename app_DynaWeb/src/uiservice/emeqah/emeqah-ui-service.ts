import EMEQAHUIServiceBase from './emeqah-ui-service-base';

/**
 * 活动历史UI服务对象
 *
 * @export
 * @class EMEQAHUIService
 */
export default class EMEQAHUIService extends EMEQAHUIServiceBase {

    /**
     * Creates an instance of  EMEQAHUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQAHUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}