import EMEQMonitorUIServiceBase from './emeqmonitor-ui-service-base';

/**
 * 设备状态监控UI服务对象
 *
 * @export
 * @class EMEQMonitorUIService
 */
export default class EMEQMonitorUIService extends EMEQMonitorUIServiceBase {

    /**
     * Creates an instance of  EMEQMonitorUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQMonitorUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}