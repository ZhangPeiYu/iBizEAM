import EMServiceEvlUIServiceBase from './emservice-evl-ui-service-base';

/**
 * 服务商评估UI服务对象
 *
 * @export
 * @class EMServiceEvlUIService
 */
export default class EMServiceEvlUIService extends EMServiceEvlUIServiceBase {

    /**
     * Creates an instance of  EMServiceEvlUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMServiceEvlUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}