import EMDPRCTUIServiceBase from './emdprct-ui-service-base';

/**
 * 测点记录UI服务对象
 *
 * @export
 * @class EMDPRCTUIService
 */
export default class EMDPRCTUIService extends EMDPRCTUIServiceBase {

    /**
     * Creates an instance of  EMDPRCTUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDPRCTUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}