import { Http } from '@/utils';
import { Util } from '@/utils';
import EMStorePartServiceBase from './emstore-part-service-base';


/**
 * 仓库库位服务对象
 *
 * @export
 * @class EMStorePartService
 * @extends {EMStorePartServiceBase}
 */
export default class EMStorePartService extends EMStorePartServiceBase {

    /**
     * Creates an instance of  EMStorePartService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStorePartService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}