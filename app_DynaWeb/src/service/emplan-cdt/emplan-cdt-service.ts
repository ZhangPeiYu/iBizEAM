import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPlanCDTServiceBase from './emplan-cdt-service-base';


/**
 * 计划条件服务对象
 *
 * @export
 * @class EMPlanCDTService
 * @extends {EMPlanCDTServiceBase}
 */
export default class EMPlanCDTService extends EMPlanCDTServiceBase {

    /**
     * Creates an instance of  EMPlanCDTService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanCDTService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}