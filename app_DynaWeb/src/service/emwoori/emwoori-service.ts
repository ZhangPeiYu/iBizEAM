import { Http } from '@/utils';
import { Util } from '@/utils';
import EMWOORIServiceBase from './emwoori-service-base';


/**
 * 工单来源服务对象
 *
 * @export
 * @class EMWOORIService
 * @extends {EMWOORIServiceBase}
 */
export default class EMWOORIService extends EMWOORIServiceBase {

    /**
     * Creates an instance of  EMWOORIService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWOORIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}