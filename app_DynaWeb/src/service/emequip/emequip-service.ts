import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEquipServiceBase from './emequip-service-base';


/**
 * 设备档案服务对象
 *
 * @export
 * @class EMEquipService
 * @extends {EMEquipServiceBase}
 */
export default class EMEquipService extends EMEquipServiceBase {

    /**
     * Creates an instance of  EMEquipService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEquipService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}