import { Http } from '@/utils';
import { Util } from '@/utils';
import EMEQLCTTIResServiceBase from './emeqlcttires-service-base';


/**
 * 轮胎位置服务对象
 *
 * @export
 * @class EMEQLCTTIResService
 * @extends {EMEQLCTTIResServiceBase}
 */
export default class EMEQLCTTIResService extends EMEQLCTTIResServiceBase {

    /**
     * Creates an instance of  EMEQLCTTIResService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTTIResService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}