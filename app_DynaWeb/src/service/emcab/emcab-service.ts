import { Http } from '@/utils';
import { Util } from '@/utils';
import EMCabServiceBase from './emcab-service-base';


/**
 * 货架服务对象
 *
 * @export
 * @class EMCabService
 * @extends {EMCabServiceBase}
 */
export default class EMCabService extends EMCabServiceBase {

    /**
     * Creates an instance of  EMCabService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMCabService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}