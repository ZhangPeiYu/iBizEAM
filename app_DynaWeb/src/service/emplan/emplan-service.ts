import { Http } from '@/utils';
import { Util } from '@/utils';
import EMPlanServiceBase from './emplan-service-base';


/**
 * 计划服务对象
 *
 * @export
 * @class EMPlanService
 * @extends {EMPlanServiceBase}
 */
export default class EMPlanService extends EMPlanServiceBase {

    /**
     * Creates an instance of  EMPlanService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMPlanService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}