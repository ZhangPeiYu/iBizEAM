import { MockAdapter } from '@/mock/mock-adapter';
const mock = MockAdapter.getInstance();

import Mock from 'mockjs'
const Random = Mock.Random;

// 获取应用数据
mock.onGet('v7/index-viewappmenu').reply((config: any) => {
    let status = MockAdapter.mockStatus(config);
    return [status, {
        name: 'appmenu',
        items:  [
            {
	id: '37fd952d8c32526edff0882a0a9d78ca',
	name: 'menuitem61',
	text: '工作台',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '工作台',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: true,
	iconcls: 'fa fa-desktop',
	icon: '',
	textcls: '',
	appfunctag: 'Auto37',
	resourcetag: '',
},
            {
	id: '3ec0da79dd7bcea9bc5d05ff54c04763',
	name: 'menuitem3',
	text: '设备',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '设备',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-align-right',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '8fb1698740519ae5a976b02060a6623f',
	name: 'menuitem1',
	text: '设备类型',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '设备类型',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto21',
	resourcetag: '',
},
		{
	id: '4bd608bc63e9a0af6d928f30b263e222',
	name: 'menuitem8',
	text: '设备档案',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '设备档案',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto1',
	resourcetag: '',
},
		{
	id: '29205094c86e6dc025eb6d64573f8364',
	name: 'menuitem5',
	text: '位置',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '位置',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto12',
	resourcetag: '',
},
		{
	id: '121fc39c740b0fbc234d814fd64519fa',
	name: 'menuitem6',
	text: '文档',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '文档',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto20',
	resourcetag: '',
},
		{
	id: '7389049eb8f54e5afa10a5e43ad78833',
	name: 'menuitem7',
	text: '备件包',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '备件包',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto10',
	resourcetag: '',
},
	],
},
            {
	id: '5ae70009515d3cd9254ccd9ea82e3603',
	name: 'menuitem72',
	text: '运行',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '运行',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-bug',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '24f6e5b49926ed807eb4aa7998c63271',
	name: 'menuitem73',
	text: '运行日志',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '运行日志',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto65',
	resourcetag: '',
},
		{
	id: 'f684fdd0fa576fe0d80532cd7c44fb45',
	name: 'menuitem74',
	text: '运行监控',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '运行监控',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto60',
	resourcetag: '',
},
		{
	id: 'b87d33ea50b9df7844a2bbdfe0f2ee1a',
	name: 'menuitem77',
	text: '仪表',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '仪表',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto62',
	resourcetag: '',
},
		{
	id: '9e6a5debcd59f2af2ab2f09db4ca71a8',
	name: 'menuitem78',
	text: '仪表读数',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '仪表读数',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto59',
	resourcetag: '',
},
		{
	id: 'ad87f1ecd6635217713beeb5f7c44826',
	name: 'menuitem75',
	text: '关键点',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '关键点',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto63',
	resourcetag: '',
},
		{
	id: '20644bf8d9eb440ed7ba6118cc84452a',
	name: 'menuitem76',
	text: '关键点记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '关键点记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto67',
	resourcetag: '',
},
	],
},
            {
	id: 'cf5cfa363a254851813316eb37531201',
	name: 'menuitem16',
	text: '计划',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '计划',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-pencil-square-o',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'd3dbc12952f14dd7c85dd524bd2d2cdb',
	name: 'menuitem17',
	text: '计划',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '计划',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto15',
	resourcetag: '',
},
		{
	id: '1ef49b322792bd13fe7fc3a70ff7ea41',
	name: 'menuitem18',
	text: '计划模板',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '计划模板',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto6',
	resourcetag: '',
},
	],
},
            {
	id: '3e11e371a4af5f7d4d70a847a536ebc2',
	name: 'menuitem33',
	text: '能耗',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '能耗',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-area-chart',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'd0fca3fa09602c25f3a394613009df16',
	name: 'menuitem50',
	text: '能源',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '能源',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto35',
	resourcetag: '',
},
		{
	id: '02e5fca23f7cf03e21335aa549a21467',
	name: 'menuitem55',
	text: '能耗',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '能耗',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto32',
	resourcetag: '',
},
	],
},
            {
	id: '78109e416b8c7ded362ba72a3992577e',
	name: 'menuitem9',
	text: '工单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '工单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-align-justify',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '6bbbf88535359bc9d2cd155321636611',
	name: 'menuitem69',
	text: '工单日历',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '工单日历',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto58',
	resourcetag: '',
},
		{
	id: '9540f06224ef6a8e957e56d7348264fd',
	name: 'menuitem11',
	text: '内部工单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '内部工单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto9',
	resourcetag: '',
},
		{
	id: '33b2f5087ea7b7e4ddcf022d3da8e97c',
	name: 'menuitem12',
	text: '外委工单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '外委工单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto17',
	resourcetag: '',
},
		{
	id: '89b15b2a0643a5098ea611604117945d',
	name: 'menuitem13',
	text: '能耗工单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '能耗工单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto5',
	resourcetag: '',
},
		{
	id: 'c03a06db65083236452350d71def886c',
	name: 'menuitem14',
	text: '点检工单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '点检工单',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto3',
	resourcetag: '',
},
		{
	id: 'eed829a2251f771aa38499f9d98a9642',
	name: 'menuitem15',
	text: '外委申请',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '外委申请',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto16',
	resourcetag: '',
},
	],
},
            {
	id: 'e60fb4de4b3a0f208d46bbbfc1d49270',
	name: 'menuitem32',
	text: '活动',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '活动',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-university',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '55f27bd30001bdd4ca191ca5a6b44c82',
	name: 'menuitem70',
	text: '活动日历',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '活动日历',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto64',
	resourcetag: '',
},
		{
	id: '6a7904d3a443791b2d1d57b37bdee9e4',
	name: 'menuitem56',
	text: '更换安装',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '更换安装',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto27',
	resourcetag: '',
},
		{
	id: 'fd88f292ede0e93f6314f07466e9e5a5',
	name: 'menuitem57',
	text: '事故记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '事故记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto33',
	resourcetag: '',
},
		{
	id: '49480bfa806839949feee646828774f5',
	name: 'menuitem58',
	text: '维修记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '维修记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto38',
	resourcetag: '',
},
		{
	id: '409cb76a7c72d39dd8353b5ffca64cd0',
	name: 'menuitem59',
	text: '抢修记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '抢修记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto31',
	resourcetag: '',
},
		{
	id: '708a9133510bfd5f208024a2a38c9778',
	name: 'menuitem60',
	text: '保养记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '保养记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto28',
	resourcetag: '',
},
	],
},
            {
	id: '9cdce61b823e4a48dc4d6daf651c09b3',
	name: 'menuitem31',
	text: '故障',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '故障',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-exclamation-triangle',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '45f40dfc253e3d8b8a972ccf39beb293',
	name: 'menuitem79',
	text: '故障知识库',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '故障知识库',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto66',
	resourcetag: '',
},
		{
	id: '96c9770e0586e0bcfc0abbf88b2f0dd4',
	name: 'menuitem48',
	text: '现象',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '现象',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto50',
	resourcetag: '',
},
		{
	id: 'e04feb96801b8a00ef7e1e20ce379805',
	name: 'menuitem47',
	text: '现象分类',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '现象分类',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto26',
	resourcetag: '',
},
		{
	id: '7024b3bf0327d872c985e5bdd0062f08',
	name: 'menuitem49',
	text: '模式',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '模式',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto41',
	resourcetag: '',
},
		{
	id: '9860345c46654263518e6ec2cc2ab477',
	name: 'menuitem51',
	text: '原因',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '原因',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto48',
	resourcetag: '',
},
		{
	id: 'd8c0914a4329002fcfdb1f14ec5af46f',
	name: 'menuitem52',
	text: '方案',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '方案',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto30',
	resourcetag: '',
},
	],
},
            {
	id: '4b5ca063c863d1245995416f7bf9dc0f',
	name: 'menuitem30',
	text: '资产',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '资产',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-navicon',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'd57e22d65f02b9617cb455220b17a4da',
	name: 'menuitem43',
	text: '资产科目',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '资产科目',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto51',
	resourcetag: '',
},
		{
	id: 'af92eaec6fc90b5e78268394df28adef',
	name: 'menuitem44',
	text: '固定资产台账',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '固定资产台账',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto29',
	resourcetag: '',
},
		{
	id: '74614bf276d779dfce1551ef25497a98',
	name: 'menuitem45',
	text: '报废资产',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '报废资产',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto46',
	resourcetag: '',
},
		{
	id: '922a73276d3545064eaaba188993b5af',
	name: 'menuitem46',
	text: '资产盘点记录',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '资产盘点记录',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto39',
	resourcetag: '',
},
	],
},
            {
	id: '84c53f5d971a19ae8f13974a9289e196',
	name: 'menuitem4',
	text: '材料',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '材料',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-file-o',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'ee89463e4646185c11a44cd37f215f8d',
	name: 'menuitem2',
	text: '物品类型',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '物品类型',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto22',
	resourcetag: '',
},
		{
	id: '3de4f2b7a3a9cb6e771e9db393d6d173',
	name: 'menuitem19',
	text: '物品',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '物品',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto14',
	resourcetag: '',
},
		{
	id: 'f9c649e99cf89eebaf1626084de1f574',
	name: 'menuitem21',
	text: '库存管理',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '库存管理',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto2',
	resourcetag: '',
},
		{
	id: '82e39aa8b49501c370ce911740b5229a',
	name: 'menuitem24',
	text: '损溢单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '损溢单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto18',
	resourcetag: '',
},
		{
	id: 'a052b0fec08c028ed9e32a0e9c79a84e',
	name: 'menuitem25',
	text: '调整单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '调整单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto4',
	resourcetag: '',
},
		{
	id: '0c9c86b54fef20aab81630a7a1a70406',
	name: 'menuitem27',
	text: '出库单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '出库单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto13',
	resourcetag: '',
},
		{
	id: '2c571cfb22a8129ff3468d4c18cf4732',
	name: 'menuitem23',
	text: '领料单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '领料单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto7',
	resourcetag: '',
},
		{
	id: 'a3ae9eb467554e0ebcbddd9f346d4a52',
	name: 'menuitem28',
	text: '还料单',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '还料单',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto8',
	resourcetag: '',
},
	],
},
            {
	id: 'cb19675fe2dc30c6879d7e8a2c79c1ca',
	name: 'menuitem29',
	text: '采购',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-leanpub',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: 'b7dcc7407a3eba740e5012a59b77722e',
	name: 'menuitem64',
	text: '采购流程',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '采购流程',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto54',
	resourcetag: '',
},
		{
	id: '6f503c3d1d9289047a051662603b4f92',
	name: 'menuitem38',
	text: '服务商',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '服务商',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto36',
	resourcetag: '',
},
		{
	id: '0b99f1325a6d3aaf767e523253a64683',
	name: 'menuitem39',
	text: '服务商评估',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '服务商评估',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto34',
	resourcetag: '',
},
	],
},
            {
	id: 'a824689bb7919218f0cb9d0fd8b6b054',
	name: 'menuitem62',
	text: '预警',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '预警',
	expanded: true,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: 'fa fa-bell-o',
	icon: '',
	textcls: '',
	appfunctag: '',
	resourcetag: '',
	items: [
		{
	id: '7bb7a29339a10653ec4f9496865d90f0',
	name: 'menuitem63',
	text: '钢丝绳位置超期预警',
	type: 'MENUITEM',
	counterid: '',
	tooltip: '钢丝绳位置超期预警',
	expanded: false,
	separator: false,
	hidden: false,
	hidesidebar: false,
	opendefault: false,
	iconcls: '',
	icon: '',
	textcls: '',
	appfunctag: 'Auto24',
	resourcetag: '',
},
	],
},
        ],
    }];
});

