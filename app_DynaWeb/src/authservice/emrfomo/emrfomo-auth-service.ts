import EMRFOMOAuthServiceBase from './emrfomo-auth-service-base';


/**
 * 模式权限服务对象
 *
 * @export
 * @class EMRFOMOAuthService
 * @extends {EMRFOMOAuthServiceBase}
 */
export default class EMRFOMOAuthService extends EMRFOMOAuthServiceBase {

    /**
     * Creates an instance of  EMRFOMOAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMRFOMOAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}