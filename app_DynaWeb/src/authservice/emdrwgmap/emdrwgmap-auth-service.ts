import EMDRWGMapAuthServiceBase from './emdrwgmap-auth-service-base';


/**
 * 文档引用权限服务对象
 *
 * @export
 * @class EMDRWGMapAuthService
 * @extends {EMDRWGMapAuthServiceBase}
 */
export default class EMDRWGMapAuthService extends EMDRWGMapAuthServiceBase {

    /**
     * Creates an instance of  EMDRWGMapAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMDRWGMapAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}