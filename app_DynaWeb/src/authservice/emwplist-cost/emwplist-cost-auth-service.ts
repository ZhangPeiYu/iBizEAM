import EMWPListCostAuthServiceBase from './emwplist-cost-auth-service-base';


/**
 * 询价单权限服务对象
 *
 * @export
 * @class EMWPListCostAuthService
 * @extends {EMWPListCostAuthServiceBase}
 */
export default class EMWPListCostAuthService extends EMWPListCostAuthServiceBase {

    /**
     * Creates an instance of  EMWPListCostAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMWPListCostAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}