import EMStorePartAuthServiceBase from './emstore-part-auth-service-base';


/**
 * 仓库库位权限服务对象
 *
 * @export
 * @class EMStorePartAuthService
 * @extends {EMStorePartAuthServiceBase}
 */
export default class EMStorePartAuthService extends EMStorePartAuthServiceBase {

    /**
     * Creates an instance of  EMStorePartAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMStorePartAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}