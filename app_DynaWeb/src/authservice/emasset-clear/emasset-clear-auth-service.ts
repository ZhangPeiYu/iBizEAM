import EMAssetClearAuthServiceBase from './emasset-clear-auth-service-base';


/**
 * 资产清盘记录权限服务对象
 *
 * @export
 * @class EMAssetClearAuthService
 * @extends {EMAssetClearAuthServiceBase}
 */
export default class EMAssetClearAuthService extends EMAssetClearAuthServiceBase {

    /**
     * Creates an instance of  EMAssetClearAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMAssetClearAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}