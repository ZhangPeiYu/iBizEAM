import EMEQLCTRHYAuthServiceBase from './emeqlctrhy-auth-service-base';


/**
 * 润滑油位置权限服务对象
 *
 * @export
 * @class EMEQLCTRHYAuthService
 * @extends {EMEQLCTRHYAuthServiceBase}
 */
export default class EMEQLCTRHYAuthService extends EMEQLCTRHYAuthServiceBase {

    /**
     * Creates an instance of  EMEQLCTRHYAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMEQLCTRHYAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}