import EMApplyAuthServiceBase from './emapply-auth-service-base';


/**
 * 外委申请权限服务对象
 *
 * @export
 * @class EMApplyAuthService
 * @extends {EMApplyAuthServiceBase}
 */
export default class EMApplyAuthService extends EMApplyAuthServiceBase {

    /**
     * Creates an instance of  EMApplyAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMApplyAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}