import EMENAuthServiceBase from './emen-auth-service-base';


/**
 * 能源权限服务对象
 *
 * @export
 * @class EMENAuthService
 * @extends {EMENAuthServiceBase}
 */
export default class EMENAuthService extends EMENAuthServiceBase {

    /**
     * Creates an instance of  EMENAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  EMENAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }


}