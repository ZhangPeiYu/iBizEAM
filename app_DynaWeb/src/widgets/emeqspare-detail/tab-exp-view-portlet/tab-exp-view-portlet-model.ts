/**
 * TabExpView 部件模型
 *
 * @export
 * @class TabExpViewModel
 */
export default class TabExpViewModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'orgid',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createman',
      },
      {
        name: 'emeqsparedetail',
        prop: 'emeqsparedetailid',
      },
      {
        name: 'emeqsparedetailname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'itemname',
      },
      {
        name: 'eqsparename',
      },
      {
        name: 'itemid',
      },
      {
        name: 'eqspareid',
      },
    ]
  }


}
