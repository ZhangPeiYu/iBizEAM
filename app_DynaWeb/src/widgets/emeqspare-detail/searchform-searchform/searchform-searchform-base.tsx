import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMEQSpareDetailService from '@/service/emeqspare-detail/emeqspare-detail-service';
import SearchFormService from './searchform-searchform-service';
import EMEQSpareDetailUIService from '@/uiservice/emeqspare-detail/emeqspare-detail-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {SearchFormSearchFormBase}
 */
export class SearchFormSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof SearchFormSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {SearchFormService}
     * @memberof SearchFormSearchFormBase
     */
    public service: SearchFormService = new SearchFormService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQSpareDetailService}
     * @memberof SearchFormSearchFormBase
     */
    public appEntityService: EMEQSpareDetailService = new EMEQSpareDetailService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof SearchFormSearchFormBase
     */
    protected appDeName: string = 'emeqsparedetail';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof SearchFormSearchFormBase
     */
    protected appDeLogicName: string = '备件包明细';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQSpareDetailUIService}
     * @memberof SearchFormBase
     */  
    public appUIService: EMEQSpareDetailUIService = new EMEQSpareDetailUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof SearchFormSearchFormBase
     */
    public data: any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof SearchFormSearchFormBase
     */
    public detailsModel: any = {
        searchform: new FormTabPanelModel({ caption: 'searchform', detailType: 'TABPANEL', name: 'searchform', visible: true, isShowCaption: true, form: this, tabPages: [] }),
    };

    /**
     * 新建默认值
     * @memberof SearchFormBase
     */
    public createDefault(){                    
    }
}