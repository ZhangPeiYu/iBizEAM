/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main3GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'eqlocationinfo',
          prop: 'eqlocationinfo',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'equipname',
          prop: 'equipname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'eqmodelcode',
          prop: 'eqmodelcode',
          dataType: 'TEXT',
        },
        {
          name: 'len',
          prop: 'len',
          dataType: 'FLOAT',
        },
        {
          name: 'zj',
          prop: 'zj',
          dataType: 'FLOAT',
        },
        {
          name: 'valve',
          prop: 'valve',
          dataType: 'INT',
        },
        {
          name: 'emeqlocationid',
          prop: 'emeqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'equipid',
          prop: 'equipid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'eqlocationinfo',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emeqlocationid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfkey',
          prop: 'emeqlocationid',
          dataType: 'PICKUP',
          isEditable:true
        },
        {
          name: 'emeqlctgss',
          prop: 'emeqlocationid',
        },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}