/**
 * Default 部件模型
 *
 * @export
 * @class DefaultModel
 */
export default class DefaultModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof DefaultModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'n_itemname_like',
        prop: 'itemname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_adate_gtandeq',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'n_adate_ltandeq',
        prop: 'adate',
        dataType: 'DATETIME',
      },
      {
        name: 'n_emservicename_eq',
        prop: 'emservicename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'n_wpliststate_eq',
        prop: 'wpliststate',
        dataType: 'NSCODELIST',
      },
      {
        name: 'n_emserviceid_eq',
      },
    ]
  }

}