import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMServiceEvlService from '@/service/emservice-evl/emservice-evl-service';
import Main4Service from './main4-form-service';
import EMServiceEvlUIService from '@/uiservice/emservice-evl/emservice-evl-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main4EditFormBase}
 */
export class Main4EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main4Service}
     * @memberof Main4EditFormBase
     */
    public service: Main4Service = new Main4Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMServiceEvlService}
     * @memberof Main4EditFormBase
     */
    public appEntityService: EMServiceEvlService = new EMServiceEvlService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeName: string = 'emserviceevl';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main4EditFormBase
     */
    protected appDeLogicName: string = '服务商评估';

    /**
     * 界面UI服务对象
     *
     * @type {EMServiceEvlUIService}
     * @memberof Main4Base
     */  
    public appUIService: EMServiceEvlUIService = new EMServiceEvlUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        servicename: null,
        empname: null,
        empid: null,
        evlregion: null,
        evldate: null,
        evlresult3: null,
        evlresult7: null,
        evlresult2: null,
        evlresult8: null,
        evlresult5: null,
        evlresult6: null,
        evlresult9: null,
        evlresult4: null,
        evlresult1: null,
        evlresult: null,
        serviceevlstate: null,
        evlmark: null,
        emserviceevlid: null,
        serviceid: null,
        emserviceevl: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public rules(): any{
        return {
            servicename: [
                {
                    required: this.detailsModel.servicename.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.servicename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.servicename.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.servicename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            empname: [
                {
                    required: this.detailsModel.empname.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.empname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.empname.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.empname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            evlregion: [
                {
                    required: this.detailsModel.evlregion.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.evlregion')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.evlregion.required,
                    type: 'string',
                    message: `${this.$t('entities.emserviceevl.main4_form.details.evlregion')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main4Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main4EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '服务商评估信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emserviceevl.main4_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '服务商评估标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '服务商评估名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        servicename: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'servicename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        empname: new FormItemModel({
    caption: '评估人', detailType: 'FORMITEM', name: 'empname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        empid: new FormItemModel({
    caption: '评估人', detailType: 'FORMITEM', name: 'empid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlregion: new FormItemModel({
    caption: '评估区间', detailType: 'FORMITEM', name: 'evlregion', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        evldate: new FormItemModel({
    caption: '评估时间', detailType: 'FORMITEM', name: 'evldate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult3: new FormItemModel({
    caption: '价格(100分,20%)', detailType: 'FORMITEM', name: 'evlresult3', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult7: new FormItemModel({
    caption: '距离(100分,5%)', detailType: 'FORMITEM', name: 'evlresult7', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult2: new FormItemModel({
    caption: '质量(100分,25%)', detailType: 'FORMITEM', name: 'evlresult2', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult8: new FormItemModel({
    caption: '质量管理体系(100分,5%)', detailType: 'FORMITEM', name: 'evlresult8', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult5: new FormItemModel({
    caption: '供货能力(100分,10%)', detailType: 'FORMITEM', name: 'evlresult5', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult6: new FormItemModel({
    caption: '及时性(100分,10%)', detailType: 'FORMITEM', name: 'evlresult6', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult9: new FormItemModel({
    caption: '售后(100分,10%)', detailType: 'FORMITEM', name: 'evlresult9', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult4: new FormItemModel({
    caption: '安全性能(100分,10%)', detailType: 'FORMITEM', name: 'evlresult4', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult1: new FormItemModel({
    caption: '资质(100分,5%)', detailType: 'FORMITEM', name: 'evlresult1', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        evlresult: new FormItemModel({
    caption: '总分数', detailType: 'FORMITEM', name: 'evlresult', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        serviceevlstate: new FormItemModel({
    caption: '评估状态', detailType: 'FORMITEM', name: 'serviceevlstate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        evlmark: new FormItemModel({
    caption: '评价', detailType: 'FORMITEM', name: 'evlmark', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emserviceevlid: new FormItemModel({
    caption: '服务商评估标识', detailType: 'FORMITEM', name: 'emserviceevlid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        serviceid: new FormItemModel({
    caption: '服务商', detailType: 'FORMITEM', name: 'serviceid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

    };

    /**
     * 新建默认值
     * @memberof Main4EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('empname')) {
            this.data['empname'] = this.context['srfusername'];
        }
        if (this.data.hasOwnProperty('empid')) {
            this.data['empid'] = this.context['srfuserid'];
        }
        if (this.data.hasOwnProperty('evldate')) {
            this.data['evldate'] = this.$util.dateFormat(new Date());
        }
        if (this.data.hasOwnProperty('serviceevlstate')) {
            this.data['serviceevlstate'] = '0';
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main4Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}