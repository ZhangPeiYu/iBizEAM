/**
 * QuickCreate 部件模型
 *
 * @export
 * @class QuickCreateModel
 */
export default class QuickCreateModel {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof QuickCreateModel
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'itemcode',
        prop: 'itemcode',
        dataType: 'TEXT',
      },
      {
        name: 'emitemname',
        prop: 'emitemname',
        dataType: 'TEXT',
      },
      {
        name: 'itemgroup',
        prop: 'itemgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'itemtypename',
        prop: 'itemtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'unitname',
        prop: 'unitname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storename',
        prop: 'storename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartname',
        prop: 'storepartname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'storepartid',
        prop: 'storepartid',
        dataType: 'PICKUP',
      },
      {
        name: 'storeid',
        prop: 'storeid',
        dataType: 'PICKUP',
      },
      {
        name: 'unitid',
        prop: 'unitid',
        dataType: 'PICKUP',
      },
      {
        name: 'itemtypeid',
        prop: 'itemtypeid',
        dataType: 'PICKUP',
      },
      {
        name: 'emitemid',
        prop: 'emitemid',
        dataType: 'GUID',
      },
      {
        name: 'emitem',
        prop: 'emitemid',
        dataType: 'FONTKEY',
      },
    ]
  }

}