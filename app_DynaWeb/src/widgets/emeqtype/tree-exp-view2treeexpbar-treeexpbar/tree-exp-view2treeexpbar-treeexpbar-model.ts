/**
 * TreeExpView2treeexpbar 部件模型
 *
 * @export
 * @class TreeExpView2treeexpbarModel
 */
export default class TreeExpView2treeexpbarModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TreeExpView2treeexpbarModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'eqtypegroup',
      },
      {
        name: 'createman',
      },
      {
        name: 'eqstateinfo',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'eqtypecode',
      },
      {
        name: 'stype',
      },
      {
        name: 'eqtypeinfo',
      },
      {
        name: 'arg',
      },
      {
        name: 'orgid',
      },
      {
        name: 'emeqtype',
        prop: 'emeqtypeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'sname',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqtypename',
      },
      {
        name: 'description',
      },
      {
        name: 'eqtypepcode',
      },
      {
        name: 'eqtypepname',
      },
      {
        name: 'eqtypepid',
      },
    ]
  }


}