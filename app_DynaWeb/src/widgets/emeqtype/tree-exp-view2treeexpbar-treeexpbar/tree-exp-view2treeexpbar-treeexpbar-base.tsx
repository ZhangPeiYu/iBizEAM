import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, TreeExpBarControlBase } from '@/studio-core';
import EMEQTypeService from '@/service/emeqtype/emeqtype-service';
import TreeExpView2treeexpbarService from './tree-exp-view2treeexpbar-treeexpbar-service';
import EMEQTypeUIService from '@/uiservice/emeqtype/emeqtype-ui-service';

/**
 * treeexpbar部件基类
 *
 * @export
 * @class TreeExpBarControlBase
 * @extends {TreeExpView2treeexpbarTreeExpBarBase}
 */
export class TreeExpView2treeexpbarTreeExpBarBase extends TreeExpBarControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    protected controlType: string = 'TREEEXPBAR';

    /**
     * 建构部件服务对象
     *
     * @type {TreeExpView2treeexpbarService}
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    public service: TreeExpView2treeexpbarService = new TreeExpView2treeexpbarService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEQTypeService}
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    public appEntityService: EMEQTypeService = new EMEQTypeService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    protected appDeName: string = 'emeqtype';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    protected appDeLogicName: string = '设备类型';

    /**
     * 界面UI服务对象
     *
     * @type {EMEQTypeUIService}
     * @memberof TreeExpView2treeexpbarBase
     */  
    public appUIService: EMEQTypeUIService = new EMEQTypeUIService(this.$store);

    /**
     * treeexpbar_tree 部件 selectionchange 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_selectionchange($event: any, $event2?: any) {
        this.treeexpbar_selectionchange($event, 'treeexpbar_tree', $event2);
    }

    /**
     * treeexpbar_tree 部件 load 事件
     *
     * @param {*} [args={}]
     * @param {*} $event
     * @memberof TreeExpView2treeexpbarTreeExpBarBase
     */
    public treeexpbar_tree_load($event: any, $event2?: any) {
        this.treeexpbar_load($event, 'treeexpbar_tree', $event2);
    }


    /**
     * 控件宽度
     *
     * @type {number}
     * @memberof TreeExpView2treeexpbarBase
     */
    public ctrlWidth:number = 250;

    /**
     * 获取关系项视图
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof TreeExpView2treeexpbarBase
     */
    public getExpItemView(arg: any = {}): any {
        let expmode = arg.nodetype.toUpperCase();
        if (!expmode) {
            expmode = '';
        }
        if (Object.is(expmode, 'YJTYPE')) {
            return {  
                viewname: 'emeqtype-grid-exp-view', 
                parentdata: {"srfparentdefname":"eqtypeid"},
                deKeyField:'emeqtype'
			};
        }
        if (Object.is(expmode, 'EJTYPE')) {
            return {  
                viewname: 'emeqtype-grid-exp-view', 
                parentdata: {"srfparentdefname":"eqtypeid"},
                deKeyField:'emeqtype'
			};
        }
        if (Object.is(expmode, 'EQTYPE')) {
            return {  
                viewname: 'emeqtype-grid-view', 
                parentdata: {},
                deKeyField:'emeqtype'
			};
        }
        return null;
    }

    /**
    * 执行mounted后的逻辑
    *
    * @memberof TreeExpView2treeexpbarBase
    */
    public ctrlMounted(){ 
        if(this.$store.getters.getViewSplit(this.viewUID)){
            this.split = this.$store.getters.getViewSplit(this.viewUID);
        }else{
            let containerWidth:number = (document.getElementById("treeexpview2treeexpbar") as any).offsetWidth;
            if(this.ctrlWidth){
                    this.split = this.ctrlWidth/containerWidth;
            }
            this.$store.commit("setViewSplit",{viewUID:this.viewUID,viewSplit:this.split}); 
        }  
    }

    /**
     * 视图数据加载完成
     *
     * @param {*} $event
     * @memberof TreeExpView2treeexpbarBase
     */
    public onViewLoad($event: any): void {
        this.$emit('load', $event);
    }
}