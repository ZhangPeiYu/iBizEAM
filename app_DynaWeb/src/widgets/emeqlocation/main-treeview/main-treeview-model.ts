/**
 * Main 部件模型
 *
 * @export
 * @class MainModel
 */
export default class MainModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof MainModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'orgid',
      },
      {
        name: 'createman',
      },
      {
        name: 'eqlocationinfo',
      },
      {
        name: 'enable',
      },
      {
        name: 'emeqlocationname',
      },
      {
        name: 'updateman',
      },
      {
        name: 'createdate',
      },
      {
        name: 'description',
      },
      {
        name: 'emeqlocation',
        prop: 'emeqlocationid',
      },
      {
        name: 'eqlocationcode',
      },
      {
        name: 'eqlocationtype',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'majorequipname',
      },
      {
        name: 'majorequipid',
      },
    ]
  }


}