/**
 * PickupViewpickupviewpanel 部件模型
 *
 * @export
 * @class PickupViewpickupviewpanelModel
 */
export default class PickupViewpickupviewpanelModel {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof PickupViewpickupviewpanelModel
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'content',
      },
      {
        name: 'poamount',
      },
      {
        name: 'eadate',
      },
      {
        name: 'empo',
        prop: 'empoid',
      },
      {
        name: 'postate',
      },
      {
        name: 'pdate',
      },
      {
        name: 'civo',
      },
      {
        name: 'labservicedesc',
      },
      {
        name: 'description',
      },
      {
        name: 'emponame',
      },
      {
        name: 'createdate',
      },
      {
        name: 'tsfee',
      },
      {
        name: 'orgid',
      },
      {
        name: 'taxfee',
      },
      {
        name: 'htjy',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'apprdate',
      },
      {
        name: 'taxivo',
      },
      {
        name: 'att',
      },
      {
        name: 'createman',
      },
      {
        name: 'maxprice',
      },
      {
        name: 'wfinstanceid',
      },
      {
        name: 'wfstep',
      },
      {
        name: 'enable',
      },
      {
        name: 'wfstate',
      },
      {
        name: 'updateman',
      },
      {
        name: 'payway',
      },
      {
        name: 'poinfo',
      },
      {
        name: 'tsivo',
      },
      {
        name: 'labservicetypeid',
      },
      {
        name: 'labservicename',
      },
      {
        name: 'labserviceid',
      },
      {
        name: 'zjlempid',
      },
      {
        name: 'zjlempname',
      },
      {
        name: 'fgempid',
      },
      {
        name: 'fgempname',
      },
      {
        name: 'apprempid',
      },
      {
        name: 'apprempname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
    ]
  }


}