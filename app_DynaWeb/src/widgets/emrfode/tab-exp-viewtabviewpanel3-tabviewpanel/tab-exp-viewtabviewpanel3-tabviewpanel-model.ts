/**
 * TabExpViewtabviewpanel3 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel3Model
 */
export default class TabExpViewtabviewpanel3Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel3Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'createman',
      },
      {
        name: 'orgid',
      },
      {
        name: 'description',
      },
      {
        name: 'rfodecode',
      },
      {
        name: 'objid',
      },
      {
        name: 'rfodeinfo',
      },
      {
        name: 'emrfode',
        prop: 'emrfodeid',
      },
      {
        name: 'createdate',
      },
      {
        name: 'emrfodename',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'updateman',
      },
    ]
  }


}