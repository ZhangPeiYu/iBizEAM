/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main3Gridexpbar_gridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main3Gridexpbar_gridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'rfodecode',
          prop: 'rfodecode',
          dataType: 'TEXT',
        },
        {
          name: 'emrfodename',
          prop: 'emrfodename',
          dataType: 'TEXT',
        },
        {
          name: 'srfmajortext',
          prop: 'emrfodename',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emrfodeid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emrfodeid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emrfode',
          prop: 'emrfodeid',
        },
        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}