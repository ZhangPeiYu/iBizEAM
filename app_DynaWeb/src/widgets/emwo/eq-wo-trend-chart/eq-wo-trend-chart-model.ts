/**
 * EqWoTrend 部件模型
 *
 * @export
 * @class EqWoTrendModel
 */
export default class EqWoTrendModel {

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof EqWoTrendDashboard_sysportlet7_chartMode
	 */
	public getDataItems(): any[] {
		return [
			{
			name:'size',
			prop:'size'
			},
			{
			name:'query',
			prop:'query'
			},
			{
			name:'page',
			prop:'page'
			},
			{
			name:'sort',
			prop:'sort'
			}
		]
	}

}