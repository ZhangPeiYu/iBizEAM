import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMWOService from '@/service/emwo/emwo-service';
import YearWoTrendModel from './year-wo-trend-chart-model';


/**
 * YearWoTrend 部件服务对象
 *
 * @export
 * @class YearWoTrendService
 */
export default class YearWoTrendService extends ControlService {

    /**
     * 工单服务对象
     *
     * @type {EMWOService}
     * @memberof YearWoTrendService
     */
    public appEntityService: EMWOService = new EMWOService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof YearWoTrendService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of YearWoTrendService.
     * 
     * @param {*} [opts={}]
     * @memberof YearWoTrendService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new YearWoTrendModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof YearWoTrendService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}