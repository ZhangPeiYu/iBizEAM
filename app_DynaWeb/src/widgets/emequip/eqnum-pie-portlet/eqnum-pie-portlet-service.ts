import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * EQNumPie 部件服务对象
 *
 * @export
 * @class EQNumPieService
 */
export default class EQNumPieService extends ControlService {
}
