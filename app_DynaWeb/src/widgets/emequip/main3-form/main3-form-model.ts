/**
 * Main3 部件模型
 *
 * @export
 * @class Main3Model
 */
export default class Main3Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main3Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emequipname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'equipcode',
        prop: 'equipcode',
        dataType: 'TEXT',
      },
      {
        name: 'emequipname',
        prop: 'emequipname',
        dataType: 'TEXT',
      },
      {
        name: 'equippname',
        prop: 'equippname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqtypename',
        prop: 'eqtypename',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'eqlocationname',
        prop: 'eqlocationname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'deptname',
        prop: 'deptname',
        dataType: 'TEXT',
      },
      {
        name: 'empname',
        prop: 'empname',
        dataType: 'TEXT',
      },
      {
        name: 'rteamname',
        prop: 'rteamname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipgroup',
        prop: 'equipgroup',
        dataType: 'NMCODELIST',
      },
      {
        name: 'empid',
        prop: 'empid',
        dataType: 'TEXT',
      },
      {
        name: 'emequipid',
        prop: 'emequipid',
        dataType: 'GUID',
      },
      {
        name: 'emequip',
        prop: 'emequipid',
        dataType: 'FONTKEY',
      },
    ]
  }

}