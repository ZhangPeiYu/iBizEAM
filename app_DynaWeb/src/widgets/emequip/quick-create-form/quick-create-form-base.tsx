import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMEquipService from '@/service/emequip/emequip-service';
import QuickCreateService from './quick-create-form-service';
import EMEquipUIService from '@/uiservice/emequip/emequip-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {QuickCreateEditFormBase}
 */
export class QuickCreateEditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateEditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {QuickCreateService}
     * @memberof QuickCreateEditFormBase
     */
    public service: QuickCreateService = new QuickCreateService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMEquipService}
     * @memberof QuickCreateEditFormBase
     */
    public appEntityService: EMEquipService = new EMEquipService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateEditFormBase
     */
    protected appDeName: string = 'emequip';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof QuickCreateEditFormBase
     */
    protected appDeLogicName: string = '设备档案';

    /**
     * 界面UI服务对象
     *
     * @type {EMEquipUIService}
     * @memberof QuickCreateBase
     */  
    public appUIService: EMEquipUIService = new EMEquipUIService(this.$store);

    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof QuickCreateEditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        equipcode: null,
        emequipname: null,
        eqtypename: null,
        eqlocationname: null,
        equipgroup: null,
        empid: null,
        empname: null,
        deptname: null,
        eqlocationid: null,
        emequipid: null,
        eqtypeid: null,
        emequip: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof QuickCreateEditFormBase
     */
    public majorMessageField: string = 'emequipname';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QuickCreateEditFormBase
     */
    public rules(): any{
        return {
            equipcode: [
                {
                    required: this.detailsModel.equipcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.equipcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equipcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.equipcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            eqtypename: [
                {
                    required: this.detailsModel.eqtypename.required,
                    type: 'string',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.eqtypename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.eqtypename.required,
                    type: 'string',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.eqtypename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            equipgroup: [
                {
                    required: this.detailsModel.equipgroup.required,
                    type: 'number',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.equipgroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.equipgroup.required,
                    type: 'number',
                    message: `${this.$t('entities.emequip.quickcreate_form.details.equipgroup')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof QuickCreateBase
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof QuickCreateEditFormBase
     */
    public detailsModel: any = {
        group1: new FormGroupPanelModel({ caption: '设备档案基本信息', detailType: 'GROUPPANEL', name: 'group1', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emequip.quickcreate_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '设备标识', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '设备名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipcode: new FormItemModel({
    caption: '设备代码', detailType: 'FORMITEM', name: 'equipcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 1,
}),

        emequipname: new FormItemModel({
    caption: '设备名称', detailType: 'FORMITEM', name: 'emequipname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqtypename: new FormItemModel({
    caption: '设备类型', detailType: 'FORMITEM', name: 'eqtypename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        eqlocationname: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'eqlocationname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        equipgroup: new FormItemModel({
    caption: '设备分组', detailType: 'FORMITEM', name: 'equipgroup', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        empid: new FormItemModel({
    caption: '专责人', detailType: 'FORMITEM', name: 'empid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        empname: new FormItemModel({
    caption: '专责人', detailType: 'FORMITEM', name: 'empname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        deptname: new FormItemModel({
    caption: '专责部门', detailType: 'FORMITEM', name: 'deptname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        eqlocationid: new FormItemModel({
    caption: '位置', detailType: 'FORMITEM', name: 'eqlocationid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emequipid: new FormItemModel({
    caption: '设备标识', detailType: 'FORMITEM', name: 'emequipid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        eqtypeid: new FormItemModel({
    caption: '设备类型', detailType: 'FORMITEM', name: 'eqtypeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

    };

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof QuickCreateBase
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}