/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

  /**
  * 获取数据项集合
  *
  * @returns {any[]}
  * @memberof Main2Model
  */
  public getDataItems(): any[] {
    return [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      // 前端新增修改标识，新增为"0",修改为"1"或未设值
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
      {
        name: 'srfupdatedate',
        prop: 'updatedate',
        dataType: 'DATETIME',
      },
      {
        name: 'srforikey',
      },
      {
        name: 'srfkey',
        prop: 'emeqmpmtrid',
        dataType: 'GUID',
      },
      {
        name: 'srfmajortext',
        prop: 'emeqmpmtrname',
        dataType: 'TEXT',
      },
      {
        name: 'srftempmode',
      },
      {
        name: 'srfuf',
      },
      {
        name: 'srfdeid',
      },
      {
        name: 'srfsourcekey',
      },
      {
        name: 'mpname',
        prop: 'mpname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'woname',
        prop: 'woname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'equipname',
        prop: 'equipname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'objname',
        prop: 'objname',
        dataType: 'PICKUPTEXT',
      },
      {
        name: 'edate',
        prop: 'edate',
        dataType: 'DATETIME',
      },
      {
        name: 'val',
        prop: 'val',
        dataType: 'TEXT',
      },
      {
        name: 'orgid',
        prop: 'orgid',
        dataType: 'SSCODELIST',
      },
      {
        name: 'description',
        prop: 'description',
        dataType: 'TEXT',
      },
      {
        name: 'emeqmpmtrid',
        prop: 'emeqmpmtrid',
        dataType: 'GUID',
      },
      {
        name: 'objid',
        prop: 'objid',
        dataType: 'PICKUP',
      },
      {
        name: 'woid',
        prop: 'woid',
        dataType: 'PICKUP',
      },
      {
        name: 'equipid',
        prop: 'equipid',
        dataType: 'PICKUP',
      },
      {
        name: 'mpid',
        prop: 'mpid',
        dataType: 'PICKUP',
      },
      {
        name: 'emeqmpmtr',
        prop: 'emeqmpmtrid',
        dataType: 'FONTKEY',
      },
    ]
  }

}