/**
 * Main2 部件模型
 *
 * @export
 * @class Main2Model
 */
export default class Main2Model {

	/**
	 * 是否是实体数据导出
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public isDEExport: boolean = false;

	/**
	 * 获取数据项集合
	 *
	 * @returns {any[]}
	 * @memberof Main2GridMode
	 */
	public getDataItems(): any[] {
    if(this.isDEExport){
		  return [
      ]
    }else{
		  return [
        {
          name: 'energycode',
          prop: 'energycode',
          dataType: 'TEXT',
        },
        {
          name: 'emenname',
          prop: 'emenname',
          dataType: 'TEXT',
        },
        {
          name: 'energytypeid',
          prop: 'energytypeid',
          dataType: 'SSCODELIST',
        },
        {
          name: 'vrate',
          prop: 'vrate',
          dataType: 'FLOAT',
        },
        {
          name: 'energydesc',
          prop: 'energydesc',
          dataType: 'LONGTEXT_1000',
        },
        {
          name: 'price',
          prop: 'price',
          dataType: 'FLOAT',
        },
        {
          name: 'itemname',
          prop: 'itemname',
          dataType: 'PICKUPTEXT',
        },
        {
          name: 'description',
          prop: 'description',
          dataType: 'TEXT',
        },
        {
          name: 'itemid',
          prop: 'itemid',
          dataType: 'PICKUP',
        },
        {
          name: 'srfmajortext',
          prop: 'emenname',
          dataType: 'TEXT',
        },
        {
          name: 'srfdataaccaction',
          prop: 'emenid',
          dataType: 'GUID',
        },
        {
          name: 'srfkey',
          prop: 'emenid',
          dataType: 'GUID',
          isEditable:true
        },
        {
          name: 'emen',
          prop: 'emenid',
        },
      {
        name: 'n_emenname_like',
        prop: 'n_emenname_like',
        dataType: 'TEXT',
      },
      {
        name: 'n_energytypeid_eq',
        prop: 'n_energytypeid_eq',
        dataType: 'SSCODELIST',
      },

        {
          name:'size',
          prop:'size'
        },
        {
          name:'query',
          prop:'query'
        },
        {
          name:'filter',
          prop:'filter'
        },
        {
          name:'page',
          prop:'page'
        },
        {
          name:'sort',
          prop:'sort'
        },
        {
          name:'srfparentdata',
          prop:'srfparentdata'
        },
        // 前端新增修改标识，新增为"0",修改为"1"或未设值
        {
          name: 'srffrontuf',
          prop: 'srffrontuf',
          dataType: 'TEXT',
        },
      ]
    }
  }

}