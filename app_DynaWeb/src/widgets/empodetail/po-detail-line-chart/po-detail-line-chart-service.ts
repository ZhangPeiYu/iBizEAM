import { Http } from '@/utils';
import { Util, Errorlog } from '@/utils';
import ControlService from '@/widgets/control-service';
import EMPODetailService from '@/service/empodetail/empodetail-service';
import PoDetailLineModel from './po-detail-line-chart-model';


/**
 * PoDetailLine 部件服务对象
 *
 * @export
 * @class PoDetailLineService
 */
export default class PoDetailLineService extends ControlService {

    /**
     * 订单条目服务对象
     *
     * @type {EMPODetailService}
     * @memberof PoDetailLineService
     */
    public appEntityService: EMPODetailService = new EMPODetailService({ $store: this.getStore() });

    /**
     * 设置从数据模式
     *
     * @type {boolean}
     * @memberof PoDetailLineService
     */
    public setTempMode(){
        this.isTempMode = false;
    }

    /**
     * Creates an instance of PoDetailLineService.
     * 
     * @param {*} [opts={}]
     * @memberof PoDetailLineService
     */
    constructor(opts: any = {}) {
        super(opts);
        this.model = new PoDetailLineModel();
    }

    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof PoDetailLineService
     */
    @Errorlog
    public search(action: string,context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
        const {data:Data,context:Context} = this.handleRequestData(action,context,data,true);
        return new Promise((resolve: any, reject: any) => {
            const _appEntityService: any = this.appEntityService;
            let result: Promise<any>;
            if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
                result = _appEntityService[action](Context,Data, isloading);
            }else{
                result =_appEntityService.FetchDefault(Context,Data, isloading);
            }
            result.then((response) => {
                resolve(response);
            }).catch(response => {
                reject(response);
            });      
        });
    }
}