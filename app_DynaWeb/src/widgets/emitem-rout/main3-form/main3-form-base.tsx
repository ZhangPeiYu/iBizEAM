import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, EditFormControlBase } from '@/studio-core';
import EMItemROutService from '@/service/emitem-rout/emitem-rout-service';
import Main3Service from './main3-form-service';
import EMItemROutUIService from '@/uiservice/emitem-rout/emitem-rout-ui-service';
import {
    FormButtonModel,
    FormPageModel,
    FormItemModel,
    FormDRUIPartModel,
    FormPartModel,
    FormGroupPanelModel,
    FormIFrameModel,
    FormRowItemModel,
    FormTabPageModel,
    FormTabPanelModel,
    FormUserControlModel,
} from '@/model/form-detail';

/**
 * form部件基类
 *
 * @export
 * @class EditFormControlBase
 * @extends {Main3EditFormBase}
 */
export class Main3EditFormBase extends EditFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected controlType: string = 'FORM';

    /**
     * 建构部件服务对象
     *
     * @type {Main3Service}
     * @memberof Main3EditFormBase
     */
    public service: Main3Service = new Main3Service({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMItemROutService}
     * @memberof Main3EditFormBase
     */
    public appEntityService: EMItemROutService = new EMItemROutService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeName: string = 'emitemrout';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof Main3EditFormBase
     */
    protected appDeLogicName: string = '退货单';

    /**
     * 界面UI服务对象
     *
     * @type {EMItemROutUIService}
     * @memberof Main3Base
     */  
    public appUIService: EMItemROutUIService = new EMItemROutUIService(this.$store);


    /**
     * 主键表单项名称
     *
     * @protected
     * @type {number}
     * @memberof Main3EditFormBase
     */
    protected formKeyItemName: string = 'emitemroutid';
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public data: any = {
        srfupdatedate: null,
        srforikey: null,
        srfkey: null,
        srfmajortext: null,
        srftempmode: null,
        srfuf: null,
        srfdeid: null,
        srfsourcekey: null,
        emitemroutid: null,
        rid: null,
        rname: null,
        itemid: null,
        itemname: null,
        storeid: null,
        storename: null,
        storepartid: null,
        storepartname: null,
        psum: null,
        price: null,
        amount: null,
        shf: null,
        batcode: null,
        sempname: null,
        sempid: null,
        emitemroutname: null,
        sdate: null,
        orgid: null,
        description: null,
        createman: null,
        createdate: null,
        updateman: null,
        updatedate: null,
        emitemrout: null,
    };

    /**
     * 主信息属性映射表单项名称
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public majorMessageField: string = '';

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public rules(): any{
        return {
            itemname: [
                {
                    required: this.detailsModel.itemname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.itemname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.itemname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.itemname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            storename: [
                {
                    required: this.detailsModel.storename.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.storename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.storename.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.storename')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            storepartname: [
                {
                    required: this.detailsModel.storepartname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.storepartname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.storepartname.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.storepartname')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            psum: [
                {
                    required: this.detailsModel.psum.required,
                    type: 'number',
                    message: `${this.$t('entities.emitemrout.main3_form.details.psum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.psum.required,
                    type: 'number',
                    message: `${this.$t('entities.emitemrout.main3_form.details.psum')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            price: [
                {
                    required: this.detailsModel.price.required,
                    type: 'number',
                    message: `${this.$t('entities.emitemrout.main3_form.details.price')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.price.required,
                    type: 'number',
                    message: `${this.$t('entities.emitemrout.main3_form.details.price')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
            batcode: [
                {
                    required: this.detailsModel.batcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.batcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'change',
                },
                {
                    required: this.detailsModel.batcode.required,
                    type: 'string',
                    message: `${this.$t('entities.emitemrout.main3_form.details.batcode')}  ${this.$t('app.commonWords.valueNotEmpty')}`,
                    trigger: 'blur',
                },
        ],
        }
    }

    /**
     * 属性值规则
     *
     * @type {*}
     * @memberof Main3Base
     */
    public deRules:any = {
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof Main3EditFormBase
     */
    public detailsModel: any = {
        grouppanel2: new FormGroupPanelModel({ caption: '退货单信息', detailType: 'GROUPPANEL', name: 'grouppanel2', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitemrout.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage1: new FormPageModel({ caption: '基本信息', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        grouppanel16: new FormGroupPanelModel({ caption: '操作信息', detailType: 'GROUPPANEL', name: 'grouppanel16', visible: true, isShowCaption: false, form: this, showMoreMode: 0, uiActionGroup: { caption: '', langbase: 'entities.emitemrout.main3_form', extractMode: 'ITEM', details: [] } }),

        formpage15: new FormPageModel({ caption: '其它', detailType: 'FORMPAGE', name: 'formpage15', visible: true, isShowCaption: true, form: this, showMoreMode: 0 }),

        srfupdatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'srfupdatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srforikey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srforikey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfkey: new FormItemModel({
    caption: '退货单号', detailType: 'FORMITEM', name: 'srfkey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        srfmajortext: new FormItemModel({
    caption: '退货单名称', detailType: 'FORMITEM', name: 'srfmajortext', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srftempmode: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srftempmode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfuf: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfuf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfdeid: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfdeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        srfsourcekey: new FormItemModel({
    caption: '', detailType: 'FORMITEM', name: 'srfsourcekey', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emitemroutid: new FormItemModel({
    caption: '退货单号(自动)', detailType: 'FORMITEM', name: 'emitemroutid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        rid: new FormItemModel({
    caption: '入库单', detailType: 'FORMITEM', name: 'rid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        rname: new FormItemModel({
    caption: '入库单', detailType: 'FORMITEM', name: 'rname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemid: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        itemname: new FormItemModel({
    caption: '物品', detailType: 'FORMITEM', name: 'itemname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        storeid: new FormItemModel({
    caption: '仓库', detailType: 'FORMITEM', name: 'storeid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storename: new FormItemModel({
    caption: '仓库', detailType: 'FORMITEM', name: 'storename', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        storepartid: new FormItemModel({
    caption: '库位', detailType: 'FORMITEM', name: 'storepartid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        storepartname: new FormItemModel({
    caption: '库位', detailType: 'FORMITEM', name: 'storepartname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        psum: new FormItemModel({
    caption: '数量', detailType: 'FORMITEM', name: 'psum', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        price: new FormItemModel({
    caption: '单价', detailType: 'FORMITEM', name: 'price', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        amount: new FormItemModel({
    caption: '总金额', detailType: 'FORMITEM', name: 'amount', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        shf: new FormItemModel({
    caption: '税费', detailType: 'FORMITEM', name: 'shf', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        batcode: new FormItemModel({
    caption: '批次', detailType: 'FORMITEM', name: 'batcode', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:true,
    disabled: false,
    enableCond: 3,
}),

        sempname: new FormItemModel({
    caption: '退货人', detailType: 'FORMITEM', name: 'sempname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sempid: new FormItemModel({
    caption: '退货人', detailType: 'FORMITEM', name: 'sempid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        emitemroutname: new FormItemModel({
    caption: '退货单名称', detailType: 'FORMITEM', name: 'emitemroutname', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        sdate: new FormItemModel({
    caption: '出库日期', detailType: 'FORMITEM', name: 'sdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        orgid: new FormItemModel({
    caption: '组织', detailType: 'FORMITEM', name: 'orgid', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        description: new FormItemModel({
    caption: '描述', detailType: 'FORMITEM', name: 'description', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 3,
}),

        createman: new FormItemModel({
    caption: '建立人', detailType: 'FORMITEM', name: 'createman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        createdate: new FormItemModel({
    caption: '建立时间', detailType: 'FORMITEM', name: 'createdate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updateman: new FormItemModel({
    caption: '更新人', detailType: 'FORMITEM', name: 'updateman', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        updatedate: new FormItemModel({
    caption: '更新时间', detailType: 'FORMITEM', name: 'updatedate', visible: true, isShowCaption: true, form: this, showMoreMode: 0,
    required:false,
    disabled: false,
    enableCond: 0,
}),

        form: new FormTabPanelModel({
            caption: 'form',
            detailType: 'TABPANEL',
            name: 'form',
            visible: true,
            isShowCaption: true,
            form: this,
            tabPages: [
                {
                    name: 'formpage1',
                    index: 0,
                    visible: true,
                },
                {
                    name: 'formpage15',
                    index: 1,
                    visible: true,
                },
            ]
        }),
    };

    /**
     * 表单项逻辑
     *
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @returns {Promise<void>}
     * @memberof Main3EditFormBase
     */
    public async formLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): Promise<void> {
                




































        if (Object.is(name, 'rname')) {
            const details: string[] = ['itemname', 'storepartname', 'price', 'itemid', 'rid', 'rname', 'amount', 'storename', 'storeid', 'storepartid', 'psum', 'batcode'];
            this.updateFormItems('FormUpdateByRID', this.data, details, true);
        }
    }

    /**
     * 新建默认值
     * @memberof Main3EditFormBase
     */
    public createDefault() {                    
        if (this.data.hasOwnProperty('sdate')) {
            this.data['sdate'] = this.$util.dateFormat(new Date());
        }
    }

    /**
     * 面板数据变化处理事件
     * @param {any} item 当前数据
     * @param {any} $event 面板事件数据
     *
     * @memberof Main3Base
     */
    public onPanelDataChange(item:any,$event:any) {
        Object.assign(item, $event, {rowDataState:'update'});
    }
}