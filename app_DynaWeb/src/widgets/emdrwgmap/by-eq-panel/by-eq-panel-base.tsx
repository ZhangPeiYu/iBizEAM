import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, PanelControlBase } from '@/studio-core';
import EMDRWGMapService from '@/service/emdrwgmap/emdrwgmap-service';
import ByEQService from './by-eq-panel-service';
import EMDRWGMapUIService from '@/uiservice/emdrwgmap/emdrwgmap-ui-service';
import { PanelDetailModel,PanelRawitemModel,PanelTabPanelModel,PanelTabPageModel,PanelFieldModel,PanelContainerModel,PanelControlModel,PanelUserControlModel,PanelButtonModel } from '@/model/panel-detail';
import ByEQModel from './by-eq-panel-model';
import CodeListService from "@service/app/codelist-service";

/**
 * dashboard_sysportlet3_list_itempanel部件基类
 *
 * @export
 * @class PanelControlBase
 * @extends {ByEQPanelBase}
 */
export class ByEQPanelBase extends PanelControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof ByEQPanelBase
     */
    protected controlType: string = 'PANEL';

    /**
     * 建构部件服务对象
     *
     * @type {ByEQService}
     * @memberof ByEQPanelBase
     */
    public service: ByEQService = new ByEQService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMDRWGMapService}
     * @memberof ByEQPanelBase
     */
    public appEntityService: EMDRWGMapService = new EMDRWGMapService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQPanelBase
     */
    protected appDeName: string = 'emdrwgmap';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof ByEQPanelBase
     */
    protected appDeLogicName: string = '文档引用';

    /**
     * 界面UI服务对象
     *
     * @type {EMDRWGMapUIService}
     * @memberof ByEQBase
     */  
    public appUIService: EMDRWGMapUIService = new EMDRWGMapUIService(this.$store);


    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof ByEQ
     */
    public detailsModel: any = {
        drwgname: new PanelFieldModel({ caption: '', itemType: 'FIELD',visible: true, disabled: false, name: 'drwgname', panel: this })
,
        container1: new PanelContainerModel({ caption: '', itemType: 'CONTAINER',visible: true, disabled: false, name: 'container1', panel: this })
,
    };

    /**
     * 面板逻辑
     *
     * @public
     * @param {{ name: string, newVal: any, oldVal: any }} { name, newVal, oldVal }
     * @memberof ByEQ
     */
    public panelLogic({ name, newVal, oldVal }: { name: string, newVal: any, oldVal: any }): void {
                


    }

    /**
     * 数据模型对象
     *
     * @type {ByEQModel}
     * @memberof ByEQ
     */
    public dataModel: ByEQModel = new ByEQModel();

    /**
     * 界面行为标识数组
     *
     * @type {Array<any>}
     * @memberof ByEQ
     */
    public actionList:Array<any> = [];

    /**
     * 界面行为
     *
     * @param {*} row
     * @param {*} tag
     * @param {*} $event
     * @memberof ByEQ
     */
    public uiAction(row: any, tag: any, $event: any) {
    }
}