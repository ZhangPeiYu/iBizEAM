/**
 * TabExpViewtabviewpanel2 部件模型
 *
 * @export
 * @class TabExpViewtabviewpanel2Model
 */
export default class TabExpViewtabviewpanel2Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof TabExpViewtabviewpanel2Model
    */
  public getDataItems(): any[] {
    return [
      {
        name: 'description',
      },
      {
        name: 'emplan',
        prop: 'emplanid',
      },
      {
        name: 'mdate',
      },
      {
        name: 'createman',
      },
      {
        name: 'updateman',
      },
      {
        name: 'prefee',
      },
      {
        name: 'plandesc',
      },
      {
        name: 'mtflag',
      },
      {
        name: 'plancvl',
      },
      {
        name: 'emplanname',
      },
      {
        name: 'createdate',
      },
      {
        name: 'orgid',
      },
      {
        name: 'emwotype',
      },
      {
        name: 'plantype',
      },
      {
        name: 'content',
      },
      {
        name: 'planinfo',
      },
      {
        name: 'eqstoplength',
      },
      {
        name: 'recvpersonid',
      },
      {
        name: 'archive',
      },
      {
        name: 'activelengths',
      },
      {
        name: 'planstate',
      },
      {
        name: 'recvpersonname',
      },
      {
        name: 'updatedate',
      },
      {
        name: 'enable',
      },
      {
        name: 'dpname',
      },
      {
        name: 'acclassname',
      },
      {
        name: 'dptype',
      },
      {
        name: 'plantemplname',
      },
      {
        name: 'equipname',
      },
      {
        name: 'objname',
      },
      {
        name: 'rteamname',
      },
      {
        name: 'rservicename',
      },
      {
        name: 'rserviceid',
      },
      {
        name: 'objid',
      },
      {
        name: 'dpid',
      },
      {
        name: 'equipid',
      },
      {
        name: 'acclassid',
      },
      {
        name: 'plantemplid',
      },
      {
        name: 'rteamid',
      },
      {
        name: 'cron',
      },
      {
        name: 'mpersonid',
      },
      {
        name: 'mpersonname',
      },
      {
        name: 'rempid',
      },
      {
        name: 'rempname',
      },
      {
        name: 'rdeptid',
      },
      {
        name: 'rdeptname',
      },
    ]
  }


}