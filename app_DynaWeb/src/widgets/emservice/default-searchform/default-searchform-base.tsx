import { Prop, Provide, Emit, Model } from 'vue-property-decorator';
import { Subject, Subscription } from 'rxjs';
import { UIActionTool, Util, ViewTool } from '@/utils';
import { Watch, SearchFormControlBase } from '@/studio-core';
import EMServiceService from '@/service/emservice/emservice-service';
import DefaultService from './default-searchform-service';
import EMServiceUIService from '@/uiservice/emservice/emservice-ui-service';
import { FormButtonModel, FormPageModel, FormItemModel, FormDRUIPartModel, FormPartModel, FormGroupPanelModel, FormIFrameModel, FormRowItemModel, FormTabPageModel, FormTabPanelModel, FormUserControlModel } from '@/model/form-detail';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

/**
 * searchform部件基类
 *
 * @export
 * @class SearchFormControlBase
 * @extends {DefaultSearchFormBase}
 */
export class DefaultSearchFormBase extends SearchFormControlBase {
    /**
     * 获取部件类型
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected controlType: string = 'SEARCHFORM';

    /**
     * 建构部件服务对象
     *
     * @type {DefaultService}
     * @memberof DefaultSearchFormBase
     */
    public service: DefaultService = new DefaultService({ $store: this.$store });

    /**
     * 实体服务对象
     *
     * @type {EMServiceService}
     * @memberof DefaultSearchFormBase
     */
    public appEntityService: EMServiceService = new EMServiceService({ $store: this.$store });

    /**
     * 应用实体名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeName: string = 'emservice';

    /**
     * 应用实体中文名称
     *
     * @protected
     * @type {string}
     * @memberof DefaultSearchFormBase
     */
    protected appDeLogicName: string = '服务商';

    /**
     * 界面UI服务对象
     *
     * @type {EMServiceUIService}
     * @memberof DefaultBase
     */  
    public appUIService: EMServiceUIService = new EMServiceUIService(this.$store);


    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public data: any = {
        n_emservicename_like: null,
        n_labservicetypeid_eq: null,
        n_labservicelevelid_eq: null,
        n_servicestate_eq: null,
    };

    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof DefaultSearchFormBase
     */
    public detailsModel: any = {
        formpage1: new FormPageModel({ caption: '常规条件', detailType: 'FORMPAGE', name: 'formpage1', visible: true, isShowCaption: true, form: this })
, 
        n_emservicename_like: new FormItemModel({ caption: '服务商名称(文本包含(%))', detailType: 'FORMITEM', name: 'n_emservicename_like', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_labservicetypeid_eq: new FormItemModel({ caption: '服务商类型(等于(=))', detailType: 'FORMITEM', name: 'n_labservicetypeid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_labservicelevelid_eq: new FormItemModel({ caption: '级别(等于(=))', detailType: 'FORMITEM', name: 'n_labservicelevelid_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
        n_servicestate_eq: new FormItemModel({ caption: '服务商状态(等于(=))', detailType: 'FORMITEM', name: 'n_servicestate_eq', visible: true, isShowCaption: true, form: this, disabled: false, enableCond: 3 })
, 
    };

    /**
     * 新建默认值
     * @memberof DefaultBase
     */
    public createDefault(){                    
    }
}