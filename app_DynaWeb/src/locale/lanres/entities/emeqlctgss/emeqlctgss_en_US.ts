import EMEQLCTGSS_en_US_Base from './emeqlctgss_en_US_base';

function getLocaleResource(){
    const EMEQLCTGSS_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLCTGSS_en_US_Base(), EMEQLCTGSS_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
