import EMEQCheck_zh_CN_Base from './emeqcheck_zh_CN_base';

function getLocaleResource(){
    const EMEQCheck_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQCheck_zh_CN_Base(), EMEQCheck_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;