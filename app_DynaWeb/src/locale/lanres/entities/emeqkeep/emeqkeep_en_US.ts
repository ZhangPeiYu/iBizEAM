import EMEQKeep_en_US_Base from './emeqkeep_en_US_base';

function getLocaleResource(){
    const EMEQKeep_en_US_OwnData = {};
    const targetData = Object.assign(EMEQKeep_en_US_Base(), EMEQKeep_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
