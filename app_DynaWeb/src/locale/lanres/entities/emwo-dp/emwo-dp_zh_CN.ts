import EMWO_DP_zh_CN_Base from './emwo-dp_zh_CN_base';

function getLocaleResource(){
    const EMWO_DP_zh_CN_OwnData = {};
    const targetData = Object.assign(EMWO_DP_zh_CN_Base(), EMWO_DP_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;