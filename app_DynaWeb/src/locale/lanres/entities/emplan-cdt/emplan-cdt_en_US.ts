import EMPlanCDT_en_US_Base from './emplan-cdt_en_US_base';

function getLocaleResource(){
    const EMPlanCDT_en_US_OwnData = {};
    const targetData = Object.assign(EMPlanCDT_en_US_Base(), EMPlanCDT_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
