import EMOutput_zh_CN_Base from './emoutput_zh_CN_base';

function getLocaleResource(){
    const EMOutput_zh_CN_OwnData = {};
    const targetData = Object.assign(EMOutput_zh_CN_Base(), EMOutput_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;