import EMProduct_en_US_Base from './emproduct_en_US_base';

function getLocaleResource(){
    const EMProduct_en_US_OwnData = {};
    const targetData = Object.assign(EMProduct_en_US_Base(), EMProduct_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
