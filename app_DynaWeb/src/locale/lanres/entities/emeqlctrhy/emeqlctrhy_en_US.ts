import EMEQLCTRHY_en_US_Base from './emeqlctrhy_en_US_base';

function getLocaleResource(){
    const EMEQLCTRHY_en_US_OwnData = {};
    const targetData = Object.assign(EMEQLCTRHY_en_US_Base(), EMEQLCTRHY_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
