import EMEQMaintance_zh_CN_Base from './emeqmaintance_zh_CN_base';

function getLocaleResource(){
    const EMEQMaintance_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQMaintance_zh_CN_Base(), EMEQMaintance_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;