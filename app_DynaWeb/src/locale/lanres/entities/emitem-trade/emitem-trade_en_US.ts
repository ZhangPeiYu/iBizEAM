import EMItemTrade_en_US_Base from './emitem-trade_en_US_base';

function getLocaleResource(){
    const EMItemTrade_en_US_OwnData = {};
    const targetData = Object.assign(EMItemTrade_en_US_Base(), EMItemTrade_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
