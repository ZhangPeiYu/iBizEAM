import EMEQAH_zh_CN_Base from './emeqah_zh_CN_base';

function getLocaleResource(){
    const EMEQAH_zh_CN_OwnData = {};
    const targetData = Object.assign(EMEQAH_zh_CN_Base(), EMEQAH_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;