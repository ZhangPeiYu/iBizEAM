import EMItemPUse_zh_CN_Base from './emitem-puse_zh_CN_base';

function getLocaleResource(){
    const EMItemPUse_zh_CN_OwnData = {};
    const targetData = Object.assign(EMItemPUse_zh_CN_Base(), EMItemPUse_zh_CN_OwnData);
    return targetData;
}
export default getLocaleResource;