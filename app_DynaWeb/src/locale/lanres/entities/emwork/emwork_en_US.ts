import EMWork_en_US_Base from './emwork_en_US_base';

function getLocaleResource(){
    const EMWork_en_US_OwnData = {};
    const targetData = Object.assign(EMWork_en_US_Base(), EMWork_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
