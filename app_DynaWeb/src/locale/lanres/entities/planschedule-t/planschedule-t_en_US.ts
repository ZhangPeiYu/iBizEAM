import PLANSCHEDULE_T_en_US_Base from './planschedule-t_en_US_base';

function getLocaleResource(){
    const PLANSCHEDULE_T_en_US_OwnData = {};
    const targetData = Object.assign(PLANSCHEDULE_T_en_US_Base(), PLANSCHEDULE_T_en_US_OwnData);
    return targetData;
}
export default getLocaleResource;
