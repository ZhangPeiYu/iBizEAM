/**
 * 设备关键点关系
 *
 * @export
 * @interface EMEQKPMap
 */
export interface EMEQKPMap {

    /**
     * 设备关键点关系
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    emeqkpmapname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    updateman?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    description?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    orgid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    updatedate?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    enable?: any;

    /**
     * 设备关键点关系标识
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    emeqkpmapid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    createman?: any;

    /**
     * 设备关键点归属
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    refobjname?: any;

    /**
     * 设备关键点
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    kpname?: any;

    /**
     * 设备关键点
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    kpid?: any;

    /**
     * 设备关键点归属
     *
     * @returns {*}
     * @memberof EMEQKPMap
     */
    refobjid?: any;
}