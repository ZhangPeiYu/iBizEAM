/**
 * 工单来源
 *
 * @export
 * @interface EMWOORI
 */
export interface EMWOORI {

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    description?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    enable?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    createdate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    orgid?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    updatedate?: any;

    /**
     * 工单来源名称
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    emwooriname?: any;

    /**
     * 工单来源信息
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    wooriinfo?: any;

    /**
     * 工单来源标识
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    emwooriid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    updateman?: any;

    /**
     * 来源类型
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    emwooritype?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMWOORI
     */
    createman?: any;
}