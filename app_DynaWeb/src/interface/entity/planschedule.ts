/**
 * 计划时刻设置
 *
 * @export
 * @interface PLANSCHEDULE
 */
export interface PLANSCHEDULE {

    /**
     * 计划时刻设置标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    planscheduleid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    createman?: any;

    /**
     * 计划时刻设置名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    planschedulename?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    updatedate?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    scheduletype?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    cyclestarttime?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    cycleendtime?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    description?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    intervalminute?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    rundate?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    runtime?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    scheduleparam?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    scheduleparam2?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    scheduleparam3?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    scheduleparam4?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    schedulestate?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    emplanid?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    emplanname?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    lastminute?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE
     */
    taskid?: any;
}