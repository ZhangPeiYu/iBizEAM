/**
 * 计划_定时
 *
 * @export
 * @interface PLANSCHEDULE_T
 */
export interface PLANSCHEDULE_T {

    /**
     * 定时标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    planschedule_tid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    createman?: any;

    /**
     * 定时名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    planschedule_tname?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    updateman?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    updatedate?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    emplanid?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    intervalminute?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    cyclestarttime?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    cycleendtime?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    scheduleparam?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    emplanname?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    scheduletype?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    scheduleparam4?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    description?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    schedulestate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    scheduleparam2?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    rundate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    scheduleparam3?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    runtime?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    lastminute?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_T
     */
    taskid?: any;
}