/**
 * 计划模板
 *
 * @export
 * @interface EMPlanTempl
 */
export interface EMPlanTempl {

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    orgid?: any;

    /**
     * 计划模板信息
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    plantemplinfo?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    mpersonname?: any;

    /**
     * 计划模板名称
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    emplantemplname?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rempname?: any;

    /**
     * 预算(￥)
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    prefee?: any;

    /**
     * 持续时间(H)
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    activelengths?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    updateman?: any;

    /**
     * 计划内容
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    plandesc?: any;

    /**
     * 制定人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    mpersonid?: any;

    /**
     * 计划类型
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    plantype?: any;

    /**
     * 制定时间
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    mdate?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    recvpersonid?: any;

    /**
     * 停运时间(分)
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    eqstoplength?: any;

    /**
     * 计划模板编号
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    emplantemplid?: any;

    /**
     * 多任务?
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    mtflag?: any;

    /**
     * 详细内容
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    content?: any;

    /**
     * 生成工单种类
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    emwotype?: any;

    /**
     * 计划周期(天)
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    plancvl?: any;

    /**
     * 归档
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    archive?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    enable?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    createman?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rdeptname?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    description?: any;

    /**
     * 责任人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rempid?: any;

    /**
     * 责任部门
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rdeptid?: any;

    /**
     * 接收人
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    recvpersonname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    updatedate?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    createdate?: any;

    /**
     * 计划状态
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    planstate?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rservicename?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rteamname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    acclassname?: any;

    /**
     * 总帐科目
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    acclassid?: any;

    /**
     * 服务商
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rserviceid?: any;

    /**
     * 责任班组
     *
     * @returns {*}
     * @memberof EMPlanTempl
     */
    rteamid?: any;
}