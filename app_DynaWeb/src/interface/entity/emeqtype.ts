/**
 * 设备类型
 *
 * @export
 * @interface EMEQType
 */
export interface EMEQType {

    /**
     * 类型分组
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypegroup?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMEQType
     */
    createman?: any;

    /**
     * 设备状态情况
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqstateinfo?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMEQType
     */
    updatedate?: any;

    /**
     * 类型代码
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypecode?: any;

    /**
     * 统计归口分组
     *
     * @returns {*}
     * @memberof EMEQType
     */
    stype?: any;

    /**
     * 设备类型信息
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypeinfo?: any;

    /**
     * 辅助设施故障/维护
     *
     * @returns {*}
     * @memberof EMEQType
     */
    arg?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof EMEQType
     */
    orgid?: any;

    /**
     * 设备类型标识
     *
     * @returns {*}
     * @memberof EMEQType
     */
    emeqtypeid?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMEQType
     */
    createdate?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMEQType
     */
    updateman?: any;

    /**
     * 统计归口名称
     *
     * @returns {*}
     * @memberof EMEQType
     */
    sname?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMEQType
     */
    enable?: any;

    /**
     * 设备类型名称
     *
     * @returns {*}
     * @memberof EMEQType
     */
    emeqtypename?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof EMEQType
     */
    description?: any;

    /**
     * 上级设备类型代码
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypepcode?: any;

    /**
     * 上级设备类型
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypepname?: any;

    /**
     * 上级设备类型
     *
     * @returns {*}
     * @memberof EMEQType
     */
    eqtypepid?: any;
}