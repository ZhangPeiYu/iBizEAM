/**
 * 自定义间隔天数
 *
 * @export
 * @interface PLANSCHEDULE_O
 */
export interface PLANSCHEDULE_O {

    /**
     * 自定义间隔天数标识
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    planschedule_oid?: any;

    /**
     * 自定义间隔天数名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    planschedule_oname?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    updateman?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    createman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    createdate?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    updatedate?: any;

    /**
     * 间隔时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    intervalminute?: any;

    /**
     * 计划编号
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    emplanid?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    scheduleparam?: any;

    /**
     * 循环开始时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    cyclestarttime?: any;

    /**
     * 时刻类型
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    scheduletype?: any;

    /**
     * 循环结束时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    cycleendtime?: any;

    /**
     * 计划名称
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    emplanname?: any;

    /**
     * 时刻设置状态
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    schedulestate?: any;

    /**
     * 持续时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    lastminute?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    scheduleparam4?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    scheduleparam2?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    description?: any;

    /**
     * 执行时间
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    runtime?: any;

    /**
     * 运行日期
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    rundate?: any;

    /**
     * 时刻参数
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    scheduleparam3?: any;

    /**
     * 定时任务
     *
     * @returns {*}
     * @memberof PLANSCHEDULE_O
     */
    taskid?: any;
}