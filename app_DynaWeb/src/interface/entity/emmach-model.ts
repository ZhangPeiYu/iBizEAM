/**
 * 机型
 *
 * @export
 * @interface EMMachModel
 */
export interface EMMachModel {

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    createman?: any;

    /**
     * 机种编码
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    machtypecode?: any;

    /**
     * 流水号
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    emmachmodelid?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    updateman?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    createdate?: any;

    /**
     * 备注
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    remarks?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    enable?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    updatedate?: any;

    /**
     * 机型名称
     *
     * @returns {*}
     * @memberof EMMachModel
     */
    emmachmodelname?: any;
}