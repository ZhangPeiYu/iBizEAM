/**
 * 部门
 *
 * @export
 * @interface PFDept
 */
export interface PFDept {

    /**
     * 部门代码
     *
     * @returns {*}
     * @memberof PFDept
     */
    deptcode?: any;

    /**
     * 主管
     *
     * @returns {*}
     * @memberof PFDept
     */
    mgrempname?: any;

    /**
     * 更新时间
     *
     * @returns {*}
     * @memberof PFDept
     */
    updatedate?: any;

    /**
     * 组织
     *
     * @returns {*}
     * @memberof PFDept
     */
    orgid?: any;

    /**
     * 建立人
     *
     * @returns {*}
     * @memberof PFDept
     */
    createman?: any;

    /**
     * 部门信息
     *
     * @returns {*}
     * @memberof PFDept
     */
    deptinfo?: any;

    /**
     * 职能
     *
     * @returns {*}
     * @memberof PFDept
     */
    deptfn?: any;

    /**
     * 更新人
     *
     * @returns {*}
     * @memberof PFDept
     */
    updateman?: any;

    /**
     * 主部门编码
     *
     * @returns {*}
     * @memberof PFDept
     */
    maindeptcode?: any;

    /**
     * 逻辑有效标志
     *
     * @returns {*}
     * @memberof PFDept
     */
    enable?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof PFDept
     */
    deptpid?: any;

    /**
     * 部门标识
     *
     * @returns {*}
     * @memberof PFDept
     */
    pfdeptid?: any;

    /**
     * 主管
     *
     * @returns {*}
     * @memberof PFDept
     */
    mgrempid?: any;

    /**
     * 部门名称
     *
     * @returns {*}
     * @memberof PFDept
     */
    pfdeptname?: any;

    /**
     * 统计归口部门
     *
     * @returns {*}
     * @memberof PFDept
     */
    sdept?: any;

    /**
     * 建立时间
     *
     * @returns {*}
     * @memberof PFDept
     */
    createdate?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof PFDept
     */
    description?: any;

    /**
     * 上级部门
     *
     * @returns {*}
     * @memberof PFDept
     */
    deptpname?: any;
}