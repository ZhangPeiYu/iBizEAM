package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMMachineCategory;
/**
 * 关系型数据实体[EMMachineCategory] 查询条件对象
 */
@Slf4j
@Data
public class EMMachineCategorySearchContext extends QueryWrapperContext<EMMachineCategory> {

	private String n_emmachinecategoryname_like;//[机种名称]
	public void setN_emmachinecategoryname_like(String n_emmachinecategoryname_like) {
        this.n_emmachinecategoryname_like = n_emmachinecategoryname_like;
        if(!ObjectUtils.isEmpty(this.n_emmachinecategoryname_like)){
            this.getSearchCond().like("emmachinecategoryname", n_emmachinecategoryname_like);
        }
    }
	private String n_jzinfo_like;//[机种信息]
	public void setN_jzinfo_like(String n_jzinfo_like) {
        this.n_jzinfo_like = n_jzinfo_like;
        if(!ObjectUtils.isEmpty(this.n_jzinfo_like)){
            this.getSearchCond().like("jzinfo", n_jzinfo_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emmachinecategoryname", query)
            );
		 }
	}
}



