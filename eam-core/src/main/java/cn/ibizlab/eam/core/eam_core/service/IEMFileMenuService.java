package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMFileMenu;
import cn.ibizlab.eam.core.eam_core.filter.EMFileMenuSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMFileMenu] 服务对象接口
 */
public interface IEMFileMenuService extends IService<EMFileMenu> {

    boolean create(EMFileMenu et);
    void createBatch(List<EMFileMenu> list);
    boolean update(EMFileMenu et);
    void updateBatch(List<EMFileMenu> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMFileMenu get(String key);
    EMFileMenu getDraft(EMFileMenu et);
    boolean checkKey(EMFileMenu et);
    boolean save(EMFileMenu et);
    void saveBatch(List<EMFileMenu> list);
    Page<EMFileMenu> searchDefault(EMFileMenuSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMFileMenu> getEmfilemenuByIds(List<String> ids);
    List<EMFileMenu> getEmfilemenuByEntities(List<EMFileMenu> entities);
}


