package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMWO_OSCCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMWO_OSCCheckValueLogicImpl implements IEMWO_OSCCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwo_oscservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService getEmwo_oscService() {
        return this.emwo_oscservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMWO_OSC et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emwo_osccheckvaluedefault", et);
            kieSession.setGlobal("emwo_oscservice", emwo_oscservice);
            kieSession.setGlobal("iBzSysEmwo_oscDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwo_osccheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[新建时检查值]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
