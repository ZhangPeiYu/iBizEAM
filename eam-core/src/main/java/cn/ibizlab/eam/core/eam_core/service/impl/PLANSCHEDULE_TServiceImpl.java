package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_T;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_TSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_TService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.PLANSCHEDULE_TMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划_定时] 服务对象接口实现
 */
@Slf4j
@Service("PLANSCHEDULE_TServiceImpl")
public class PLANSCHEDULE_TServiceImpl extends ServiceImpl<PLANSCHEDULE_TMapper, PLANSCHEDULE_T> implements IPLANSCHEDULE_TService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(PLANSCHEDULE_T et) {
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleTid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<PLANSCHEDULE_T> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(PLANSCHEDULE_T et) {
        planscheduleService.update(planschedule_tInheritMapping.toPlanschedule(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("planschedule_tid", et.getPlanscheduleTid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getPlanscheduleTid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<PLANSCHEDULE_T> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        planscheduleService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public PLANSCHEDULE_T get(String key) {
        PLANSCHEDULE_T et = getById(key);
        if(et == null){
            et = new PLANSCHEDULE_T();
            et.setPlanscheduleTid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public PLANSCHEDULE_T getDraft(PLANSCHEDULE_T et) {
        return et;
    }

    @Override
    public boolean checkKey(PLANSCHEDULE_T et) {
        return (!ObjectUtils.isEmpty(et.getPlanscheduleTid())) && (!Objects.isNull(this.getById(et.getPlanscheduleTid())));
    }
    @Override
    @Transactional
    public boolean save(PLANSCHEDULE_T et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(PLANSCHEDULE_T et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<PLANSCHEDULE_T> list) {
        List<PLANSCHEDULE_T> create = new ArrayList<>();
        List<PLANSCHEDULE_T> update = new ArrayList<>();
        for (PLANSCHEDULE_T et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleTid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleTid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<PLANSCHEDULE_T> list) {
        List<PLANSCHEDULE_T> create = new ArrayList<>();
        List<PLANSCHEDULE_T> update = new ArrayList<>();
        for (PLANSCHEDULE_T et : list) {
            if (ObjectUtils.isEmpty(et.getPlanscheduleTid()) || ObjectUtils.isEmpty(getById(et.getPlanscheduleTid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 数据集
     */
    @Override
    public Page<PLANSCHEDULE_T> searchDefault(PLANSCHEDULE_TSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<PLANSCHEDULE_T> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<PLANSCHEDULE_T>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }






    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.PLANSCHEDULE_TInheritMapping planschedule_tInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULEService planscheduleService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(PLANSCHEDULE_T et){
        if(ObjectUtils.isEmpty(et.getPlanscheduleTid()))
            et.setPlanscheduleTid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE planschedule =planschedule_tInheritMapping.toPlanschedule(et);
        planschedule.set("scheduletype","1");
        planscheduleService.create(planschedule);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<PLANSCHEDULE_T> getPlanscheduleTByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<PLANSCHEDULE_T> getPlanscheduleTByEntities(List<PLANSCHEDULE_T> entities) {
        List ids =new ArrayList();
        for(PLANSCHEDULE_T entity : entities){
            Serializable id=entity.getPlanscheduleTid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IPLANSCHEDULE_TService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



