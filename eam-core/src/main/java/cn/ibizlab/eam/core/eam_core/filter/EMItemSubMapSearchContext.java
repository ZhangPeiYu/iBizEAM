package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItemSubMap;
/**
 * 关系型数据实体[EMItemSubMap] 查询条件对象
 */
@Slf4j
@Data
public class EMItemSubMapSearchContext extends QueryWrapperContext<EMItemSubMap> {

	private String n_emitemsubmapname_like;//[物品替换件名称]
	public void setN_emitemsubmapname_like(String n_emitemsubmapname_like) {
        this.n_emitemsubmapname_like = n_emitemsubmapname_like;
        if(!ObjectUtils.isEmpty(this.n_emitemsubmapname_like)){
            this.getSearchCond().like("emitemsubmapname", n_emitemsubmapname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_subitemname_eq;//[替换物品]
	public void setN_subitemname_eq(String n_subitemname_eq) {
        this.n_subitemname_eq = n_subitemname_eq;
        if(!ObjectUtils.isEmpty(this.n_subitemname_eq)){
            this.getSearchCond().eq("subitemname", n_subitemname_eq);
        }
    }
	private String n_subitemname_like;//[替换物品]
	public void setN_subitemname_like(String n_subitemname_like) {
        this.n_subitemname_like = n_subitemname_like;
        if(!ObjectUtils.isEmpty(this.n_subitemname_like)){
            this.getSearchCond().like("subitemname", n_subitemname_like);
        }
    }
	private String n_itemname_eq;//[物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_itemid_eq;//[物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_subitemid_eq;//[替换物品]
	public void setN_subitemid_eq(String n_subitemid_eq) {
        this.n_subitemid_eq = n_subitemid_eq;
        if(!ObjectUtils.isEmpty(this.n_subitemid_eq)){
            this.getSearchCond().eq("subitemid", n_subitemid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emitemsubmapname", query)
            );
		 }
	}
}



