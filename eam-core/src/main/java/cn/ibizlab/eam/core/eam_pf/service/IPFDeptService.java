package cn.ibizlab.eam.core.eam_pf.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_pf.domain.PFDept;
import cn.ibizlab.eam.core.eam_pf.filter.PFDeptSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[PFDept] 服务对象接口
 */
public interface IPFDeptService extends IService<PFDept> {

    boolean create(PFDept et);
    void createBatch(List<PFDept> list);
    boolean update(PFDept et);
    void updateBatch(List<PFDept> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    PFDept get(String key);
    PFDept getDraft(PFDept et);
    boolean checkKey(PFDept et);
    boolean save(PFDept et);
    void saveBatch(List<PFDept> list);
    Page<PFDept> searchDefault(PFDeptSearchContext context);
    Page<PFDept> searchTopDept(PFDeptSearchContext context);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<PFDept> getPfdeptByIds(List<String> ids);
    List<PFDept> getPfdeptByEntities(List<PFDept> entities);
}


