package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMCab;
/**
 * 关系型数据实体[EMCab] 查询条件对象
 */
@Slf4j
@Data
public class EMCabSearchContext extends QueryWrapperContext<EMCab> {

	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_emcabname_like;//[货架名称]
	public void setN_emcabname_like(String n_emcabname_like) {
        this.n_emcabname_like = n_emcabname_like;
        if(!ObjectUtils.isEmpty(this.n_emcabname_like)){
            this.getSearchCond().like("emcabname", n_emcabname_like);
        }
    }
	private String n_emstorepartname_eq;//[库位]
	public void setN_emstorepartname_eq(String n_emstorepartname_eq) {
        this.n_emstorepartname_eq = n_emstorepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_emstorepartname_eq)){
            this.getSearchCond().eq("emstorepartname", n_emstorepartname_eq);
        }
    }
	private String n_emstorepartname_like;//[库位]
	public void setN_emstorepartname_like(String n_emstorepartname_like) {
        this.n_emstorepartname_like = n_emstorepartname_like;
        if(!ObjectUtils.isEmpty(this.n_emstorepartname_like)){
            this.getSearchCond().like("emstorepartname", n_emstorepartname_like);
        }
    }
	private String n_emstorename_eq;//[仓库]
	public void setN_emstorename_eq(String n_emstorename_eq) {
        this.n_emstorename_eq = n_emstorename_eq;
        if(!ObjectUtils.isEmpty(this.n_emstorename_eq)){
            this.getSearchCond().eq("emstorename", n_emstorename_eq);
        }
    }
	private String n_emstorename_like;//[仓库]
	public void setN_emstorename_like(String n_emstorename_like) {
        this.n_emstorename_like = n_emstorename_like;
        if(!ObjectUtils.isEmpty(this.n_emstorename_like)){
            this.getSearchCond().like("emstorename", n_emstorename_like);
        }
    }
	private String n_emstorepartid_eq;//[库位]
	public void setN_emstorepartid_eq(String n_emstorepartid_eq) {
        this.n_emstorepartid_eq = n_emstorepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_emstorepartid_eq)){
            this.getSearchCond().eq("emstorepartid", n_emstorepartid_eq);
        }
    }
	private String n_emstoreid_eq;//[仓库]
	public void setN_emstoreid_eq(String n_emstoreid_eq) {
        this.n_emstoreid_eq = n_emstoreid_eq;
        if(!ObjectUtils.isEmpty(this.n_emstoreid_eq)){
            this.getSearchCond().eq("emstoreid", n_emstoreid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emcabname", query)
            );
		 }
	}
}



