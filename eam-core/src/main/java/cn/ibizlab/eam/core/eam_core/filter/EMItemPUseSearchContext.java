package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMItemPUse;
/**
 * 关系型数据实体[EMItemPUse] 查询条件对象
 */
@Slf4j
@Data
public class EMItemPUseSearchContext extends QueryWrapperContext<EMItemPUse> {

	private String n_emitempusename_like;//[领料单名称]
	public void setN_emitempusename_like(String n_emitempusename_like) {
        this.n_emitempusename_like = n_emitempusename_like;
        if(!ObjectUtils.isEmpty(this.n_emitempusename_like)){
            this.getSearchCond().like("emitempusename", n_emitempusename_like);
        }
    }
	private String n_itempuseinfo_like;//[领料单信息]
	public void setN_itempuseinfo_like(String n_itempuseinfo_like) {
        this.n_itempuseinfo_like = n_itempuseinfo_like;
        if(!ObjectUtils.isEmpty(this.n_itempuseinfo_like)){
            this.getSearchCond().like("itempuseinfo", n_itempuseinfo_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private Integer n_pusestate_eq;//[领料状态]
	public void setN_pusestate_eq(Integer n_pusestate_eq) {
        this.n_pusestate_eq = n_pusestate_eq;
        if(!ObjectUtils.isEmpty(this.n_pusestate_eq)){
            this.getSearchCond().eq("pusestate", n_pusestate_eq);
        }
    }
	private String n_sapcbzx_eq;//[sap成本中心]
	public void setN_sapcbzx_eq(String n_sapcbzx_eq) {
        this.n_sapcbzx_eq = n_sapcbzx_eq;
        if(!ObjectUtils.isEmpty(this.n_sapcbzx_eq)){
            this.getSearchCond().eq("sapcbzx", n_sapcbzx_eq);
        }
    }
	private String n_sapllyt_eq;//[sap领料用途]
	public void setN_sapllyt_eq(String n_sapllyt_eq) {
        this.n_sapllyt_eq = n_sapllyt_eq;
        if(!ObjectUtils.isEmpty(this.n_sapllyt_eq)){
            this.getSearchCond().eq("sapllyt", n_sapllyt_eq);
        }
    }
	private String n_useto_eq;//[用途]
	public void setN_useto_eq(String n_useto_eq) {
        this.n_useto_eq = n_useto_eq;
        if(!ObjectUtils.isEmpty(this.n_useto_eq)){
            this.getSearchCond().eq("useto", n_useto_eq);
        }
    }
	private String n_wfstep_eq;//[流程步骤]
	public void setN_wfstep_eq(String n_wfstep_eq) {
        this.n_wfstep_eq = n_wfstep_eq;
        if(!ObjectUtils.isEmpty(this.n_wfstep_eq)){
            this.getSearchCond().eq("wfstep", n_wfstep_eq);
        }
    }
	private String n_pusetype_eq;//[领料分类]
	public void setN_pusetype_eq(String n_pusetype_eq) {
        this.n_pusetype_eq = n_pusetype_eq;
        if(!ObjectUtils.isEmpty(this.n_pusetype_eq)){
            this.getSearchCond().eq("pusetype", n_pusetype_eq);
        }
    }
	private String n_storename_eq;//[仓库]
	public void setN_storename_eq(String n_storename_eq) {
        this.n_storename_eq = n_storename_eq;
        if(!ObjectUtils.isEmpty(this.n_storename_eq)){
            this.getSearchCond().eq("storename", n_storename_eq);
        }
    }
	private String n_storename_like;//[仓库]
	public void setN_storename_like(String n_storename_like) {
        this.n_storename_like = n_storename_like;
        if(!ObjectUtils.isEmpty(this.n_storename_like)){
            this.getSearchCond().like("storename", n_storename_like);
        }
    }
	private String n_objname_eq;//[领料位置]
	public void setN_objname_eq(String n_objname_eq) {
        this.n_objname_eq = n_objname_eq;
        if(!ObjectUtils.isEmpty(this.n_objname_eq)){
            this.getSearchCond().eq("objname", n_objname_eq);
        }
    }
	private String n_objname_like;//[领料位置]
	public void setN_objname_like(String n_objname_like) {
        this.n_objname_like = n_objname_like;
        if(!ObjectUtils.isEmpty(this.n_objname_like)){
            this.getSearchCond().like("objname", n_objname_like);
        }
    }
	private String n_teamname_eq;//[领料班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[领料班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_labservicename_eq;//[建议供应商]
	public void setN_labservicename_eq(String n_labservicename_eq) {
        this.n_labservicename_eq = n_labservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_labservicename_eq)){
            this.getSearchCond().eq("labservicename", n_labservicename_eq);
        }
    }
	private String n_labservicename_like;//[建议供应商]
	public void setN_labservicename_like(String n_labservicename_like) {
        this.n_labservicename_like = n_labservicename_like;
        if(!ObjectUtils.isEmpty(this.n_labservicename_like)){
            this.getSearchCond().like("labservicename", n_labservicename_like);
        }
    }
	private String n_itemname_eq;//[领料物品]
	public void setN_itemname_eq(String n_itemname_eq) {
        this.n_itemname_eq = n_itemname_eq;
        if(!ObjectUtils.isEmpty(this.n_itemname_eq)){
            this.getSearchCond().eq("itemname", n_itemname_eq);
        }
    }
	private String n_itemname_like;//[领料物品]
	public void setN_itemname_like(String n_itemname_like) {
        this.n_itemname_like = n_itemname_like;
        if(!ObjectUtils.isEmpty(this.n_itemname_like)){
            this.getSearchCond().like("itemname", n_itemname_like);
        }
    }
	private String n_mservicename_eq;//[制造商]
	public void setN_mservicename_eq(String n_mservicename_eq) {
        this.n_mservicename_eq = n_mservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_mservicename_eq)){
            this.getSearchCond().eq("mservicename", n_mservicename_eq);
        }
    }
	private String n_mservicename_like;//[制造商]
	public void setN_mservicename_like(String n_mservicename_like) {
        this.n_mservicename_like = n_mservicename_like;
        if(!ObjectUtils.isEmpty(this.n_mservicename_like)){
            this.getSearchCond().like("mservicename", n_mservicename_like);
        }
    }
	private String n_storepartname_eq;//[库位]
	public void setN_storepartname_eq(String n_storepartname_eq) {
        this.n_storepartname_eq = n_storepartname_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartname_eq)){
            this.getSearchCond().eq("storepartname", n_storepartname_eq);
        }
    }
	private String n_storepartname_like;//[库位]
	public void setN_storepartname_like(String n_storepartname_like) {
        this.n_storepartname_like = n_storepartname_like;
        if(!ObjectUtils.isEmpty(this.n_storepartname_like)){
            this.getSearchCond().like("storepartname", n_storepartname_like);
        }
    }
	private String n_equipname_eq;//[领料设备]
	public void setN_equipname_eq(String n_equipname_eq) {
        this.n_equipname_eq = n_equipname_eq;
        if(!ObjectUtils.isEmpty(this.n_equipname_eq)){
            this.getSearchCond().eq("equipname", n_equipname_eq);
        }
    }
	private String n_equipname_like;//[领料设备]
	public void setN_equipname_like(String n_equipname_like) {
        this.n_equipname_like = n_equipname_like;
        if(!ObjectUtils.isEmpty(this.n_equipname_like)){
            this.getSearchCond().like("equipname", n_equipname_like);
        }
    }
	private String n_purplanname_eq;//[采购计划]
	public void setN_purplanname_eq(String n_purplanname_eq) {
        this.n_purplanname_eq = n_purplanname_eq;
        if(!ObjectUtils.isEmpty(this.n_purplanname_eq)){
            this.getSearchCond().eq("purplanname", n_purplanname_eq);
        }
    }
	private String n_purplanname_like;//[采购计划]
	public void setN_purplanname_like(String n_purplanname_like) {
        this.n_purplanname_like = n_purplanname_like;
        if(!ObjectUtils.isEmpty(this.n_purplanname_like)){
            this.getSearchCond().like("purplanname", n_purplanname_like);
        }
    }
	private String n_woname_eq;//[领料工单]
	public void setN_woname_eq(String n_woname_eq) {
        this.n_woname_eq = n_woname_eq;
        if(!ObjectUtils.isEmpty(this.n_woname_eq)){
            this.getSearchCond().eq("woname", n_woname_eq);
        }
    }
	private String n_woname_like;//[领料工单]
	public void setN_woname_like(String n_woname_like) {
        this.n_woname_like = n_woname_like;
        if(!ObjectUtils.isEmpty(this.n_woname_like)){
            this.getSearchCond().like("woname", n_woname_like);
        }
    }
	private String n_storeid_eq;//[仓库]
	public void setN_storeid_eq(String n_storeid_eq) {
        this.n_storeid_eq = n_storeid_eq;
        if(!ObjectUtils.isEmpty(this.n_storeid_eq)){
            this.getSearchCond().eq("storeid", n_storeid_eq);
        }
    }
	private String n_itemid_eq;//[领料物品]
	public void setN_itemid_eq(String n_itemid_eq) {
        this.n_itemid_eq = n_itemid_eq;
        if(!ObjectUtils.isEmpty(this.n_itemid_eq)){
            this.getSearchCond().eq("itemid", n_itemid_eq);
        }
    }
	private String n_teamid_eq;//[领料班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }
	private String n_equipid_eq;//[领料设备]
	public void setN_equipid_eq(String n_equipid_eq) {
        this.n_equipid_eq = n_equipid_eq;
        if(!ObjectUtils.isEmpty(this.n_equipid_eq)){
            this.getSearchCond().eq("equipid", n_equipid_eq);
        }
    }
	private String n_labserviceid_eq;//[建议供应商]
	public void setN_labserviceid_eq(String n_labserviceid_eq) {
        this.n_labserviceid_eq = n_labserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_labserviceid_eq)){
            this.getSearchCond().eq("labserviceid", n_labserviceid_eq);
        }
    }
	private String n_woid_eq;//[领料工单]
	public void setN_woid_eq(String n_woid_eq) {
        this.n_woid_eq = n_woid_eq;
        if(!ObjectUtils.isEmpty(this.n_woid_eq)){
            this.getSearchCond().eq("woid", n_woid_eq);
        }
    }
	private String n_mserviceid_eq;//[制造商]
	public void setN_mserviceid_eq(String n_mserviceid_eq) {
        this.n_mserviceid_eq = n_mserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_mserviceid_eq)){
            this.getSearchCond().eq("mserviceid", n_mserviceid_eq);
        }
    }
	private String n_objid_eq;//[领料位置]
	public void setN_objid_eq(String n_objid_eq) {
        this.n_objid_eq = n_objid_eq;
        if(!ObjectUtils.isEmpty(this.n_objid_eq)){
            this.getSearchCond().eq("objid", n_objid_eq);
        }
    }
	private String n_purplanid_eq;//[采购计划]
	public void setN_purplanid_eq(String n_purplanid_eq) {
        this.n_purplanid_eq = n_purplanid_eq;
        if(!ObjectUtils.isEmpty(this.n_purplanid_eq)){
            this.getSearchCond().eq("purplanid", n_purplanid_eq);
        }
    }
	private String n_storepartid_eq;//[库位]
	public void setN_storepartid_eq(String n_storepartid_eq) {
        this.n_storepartid_eq = n_storepartid_eq;
        if(!ObjectUtils.isEmpty(this.n_storepartid_eq)){
            this.getSearchCond().eq("storepartid", n_storepartid_eq);
        }
    }
	private String n_aempid_eq;//[申请人]
	public void setN_aempid_eq(String n_aempid_eq) {
        this.n_aempid_eq = n_aempid_eq;
        if(!ObjectUtils.isEmpty(this.n_aempid_eq)){
            this.getSearchCond().eq("aempid", n_aempid_eq);
        }
    }
	private String n_aempname_eq;//[申请人]
	public void setN_aempname_eq(String n_aempname_eq) {
        this.n_aempname_eq = n_aempname_eq;
        if(!ObjectUtils.isEmpty(this.n_aempname_eq)){
            this.getSearchCond().eq("aempname", n_aempname_eq);
        }
    }
	private String n_aempname_like;//[申请人]
	public void setN_aempname_like(String n_aempname_like) {
        this.n_aempname_like = n_aempname_like;
        if(!ObjectUtils.isEmpty(this.n_aempname_like)){
            this.getSearchCond().like("aempname", n_aempname_like);
        }
    }
	private String n_sempid_eq;//[发料人]
	public void setN_sempid_eq(String n_sempid_eq) {
        this.n_sempid_eq = n_sempid_eq;
        if(!ObjectUtils.isEmpty(this.n_sempid_eq)){
            this.getSearchCond().eq("sempid", n_sempid_eq);
        }
    }
	private String n_sempname_eq;//[发料人]
	public void setN_sempname_eq(String n_sempname_eq) {
        this.n_sempname_eq = n_sempname_eq;
        if(!ObjectUtils.isEmpty(this.n_sempname_eq)){
            this.getSearchCond().eq("sempname", n_sempname_eq);
        }
    }
	private String n_sempname_like;//[发料人]
	public void setN_sempname_like(String n_sempname_like) {
        this.n_sempname_like = n_sempname_like;
        if(!ObjectUtils.isEmpty(this.n_sempname_like)){
            this.getSearchCond().like("sempname", n_sempname_like);
        }
    }
	private String n_empid_eq;//[领料人]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_empname_eq;//[领料人]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[领料人]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_apprempid_eq;//[批准人]
	public void setN_apprempid_eq(String n_apprempid_eq) {
        this.n_apprempid_eq = n_apprempid_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempid_eq)){
            this.getSearchCond().eq("apprempid", n_apprempid_eq);
        }
    }
	private String n_apprempname_eq;//[批准人]
	public void setN_apprempname_eq(String n_apprempname_eq) {
        this.n_apprempname_eq = n_apprempname_eq;
        if(!ObjectUtils.isEmpty(this.n_apprempname_eq)){
            this.getSearchCond().eq("apprempname", n_apprempname_eq);
        }
    }
	private String n_apprempname_like;//[批准人]
	public void setN_apprempname_like(String n_apprempname_like) {
        this.n_apprempname_like = n_apprempname_like;
        if(!ObjectUtils.isEmpty(this.n_apprempname_like)){
            this.getSearchCond().like("apprempname", n_apprempname_like);
        }
    }
	private String n_deptid_eq;//[领料部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_deptname_eq;//[领料部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[领料部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("itempuseinfo", query)
            );
		 }
	}
}



