package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWPListCost;
import cn.ibizlab.eam.core.eam_core.filter.EMWPListCostSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWPListCost] 服务对象接口
 */
public interface IEMWPListCostService extends IService<EMWPListCost> {

    boolean create(EMWPListCost et);
    void createBatch(List<EMWPListCost> list);
    boolean update(EMWPListCost et);
    void updateBatch(List<EMWPListCost> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWPListCost get(String key);
    EMWPListCost getDraft(EMWPListCost et);
    boolean checkKey(EMWPListCost et);
    EMWPListCost confirm(EMWPListCost et);
    boolean confirmBatch(List<EMWPListCost> etList);
    EMWPListCost fillItem(EMWPListCost et);
    boolean save(EMWPListCost et);
    void saveBatch(List<EMWPListCost> list);
    Page<EMWPListCost> searchCostByItem(EMWPListCostSearchContext context);
    Page<EMWPListCost> searchDefault(EMWPListCostSearchContext context);
    List<EMWPListCost> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    List<EMWPListCost> selectByLabserviceid(String emserviceid);
    void removeByLabserviceid(String emserviceid);
    List<EMWPListCost> selectByWplistid(String emwplistid);
    void removeByWplistid(String emwplistid);
    List<EMWPListCost> selectByUnitid(String pfunitid);
    void removeByUnitid(String pfunitid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWPListCost> getEmwplistcostByIds(List<String> ids);
    List<EMWPListCost> getEmwplistcostByEntities(List<EMWPListCost> entities);
}


