

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQCheck;
import cn.ibizlab.eam.core.eam_core.domain.EMEQAH;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQCheckInheritMapping {

    @Mappings({
        @Mapping(source ="emeqcheckid",target = "emeqahid"),
        @Mapping(source ="emeqcheckname",target = "emeqahname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQAH toEmeqah(EMEQCheck minorEntity);

    @Mappings({
        @Mapping(source ="emeqahid" ,target = "emeqcheckid"),
        @Mapping(source ="emeqahname" ,target = "emeqcheckname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    EMEQCheck toEmeqcheck(EMEQAH majorEntity);

    List<EMEQAH> toEmeqah(List<EMEQCheck> minorEntities);

    List<EMEQCheck> toEmeqcheck(List<EMEQAH> majorEntities);

}


