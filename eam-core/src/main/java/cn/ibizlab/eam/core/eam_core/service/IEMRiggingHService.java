package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMRiggingH;
import cn.ibizlab.eam.core.eam_core.filter.EMRiggingHSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMRiggingH] 服务对象接口
 */
public interface IEMRiggingHService extends IService<EMRiggingH> {

    boolean create(EMRiggingH et);
    void createBatch(List<EMRiggingH> list);
    boolean update(EMRiggingH et);
    void updateBatch(List<EMRiggingH> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMRiggingH get(String key);
    EMRiggingH getDraft(EMRiggingH et);
    boolean checkKey(EMRiggingH et);
    boolean save(EMRiggingH et);
    void saveBatch(List<EMRiggingH> list);
    Page<EMRiggingH> searchDefault(EMRiggingHSearchContext context);
    List<EMRiggingH> selectByEmitempuseid(String emitempuseid);
    void removeByEmitempuseid(String emitempuseid);
    List<EMRiggingH> selectByEmriggingid(String emriggingid);
    void removeByEmriggingid(String emriggingid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMRiggingH> getEmrigginghByIds(List<String> ids);
    List<EMRiggingH> getEmrigginghByEntities(List<EMRiggingH> entities);
}


