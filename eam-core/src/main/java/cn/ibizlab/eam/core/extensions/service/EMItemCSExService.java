package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.*;
import cn.ibizlab.eam.core.eam_core.service.impl.EMItemCSServiceImpl;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Primary;


/**
 * 实体[库间调整单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMItemCSExService")
public class EMItemCSExService extends EMItemCSServiceImpl {

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * @Auther 张耀阳
     * [Confirm:库存调整单确认] 行为扩展
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMItemCS confirm(EMItemCS et) {
        // 调整单确认
        // 首先判断需要调整的记录存不存在
        et = this.get(et.getEmitemcsid());
        if(et == null){
            throw new RuntimeException("未找到需要调整的单号，无法完成操作");
        }
        //确认后 修改对应的 调整表和产品表和对应库存标识
        //产品
        EMItem emItem = emitemService.get(et.getItemid());
        //修改产品里的最新存储位置
        emItem.setStoreid(et.getStoreid());
        emItem.setStorepartid(et.getStorepartid());
        emitemService.update(emItem);

        //库存
        EMStock emStock = emstockService.get(et.getStockid());
        //修改对应的库存标识
        emStock.setItemid(emItem.getEmitemid());
        emStock.setStoreid(et.getStoreid());
        emStock.setStorepartid(et.getStoreid());
        emstockService.update(emStock);

        //确认状态，即TRADESTATE从10(未确认)变为20(已确认)
        et.setTradestate(Integer.valueOf(StaticDict.EMTRADESTATE.ITEM_20.getValue()));
        Aops.getSelf(this).update(et);
        return super.confirm(et);
    }
}

