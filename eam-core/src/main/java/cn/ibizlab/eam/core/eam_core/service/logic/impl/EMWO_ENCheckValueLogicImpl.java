package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMWO_ENCheckValueLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_EN;

/**
 * 关系型数据实体[CheckValue] 对象
 */
@Slf4j
@Service
public class EMWO_ENCheckValueLogicImpl implements IEMWO_ENCheckValueLogic {

    @Autowired
    private KieContainer kieContainer;

    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwo_enservice;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService getEmwo_enService() {
        return this.emwo_enservice;
    }


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMWO_EN et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emwo_encheckvaluedefault", et);
            kieSession.setGlobal("emwo_enservice", emwo_enservice);
            kieSession.setGlobal("iBzSysEmwo_enDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emwo_encheckvalue");

        } catch (Exception e) {
            throw new RuntimeException("执行[新建时检查值]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
