package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMItem;
import cn.ibizlab.eam.core.eam_core.filter.EMItemSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMItem] 服务对象接口
 */
public interface IEMItemService extends IService<EMItem> {

    boolean create(EMItem et);
    void createBatch(List<EMItem> list);
    boolean update(EMItem et);
    void updateBatch(List<EMItem> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMItem get(String key);
    EMItem getDraft(EMItem et);
    boolean checkKey(EMItem et);
    boolean save(EMItem et);
    void saveBatch(List<EMItem> list);
    Page<EMItem> searchDefault(EMItemSearchContext context);
    Page<EMItem> searchItemTypeTree(EMItemSearchContext context);
    List<EMItem> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMItem> selectByEmcabid(String emcabid);
    void removeByEmcabid(String emcabid);
    List<EMItem> selectByItemtypeid(String emitemtypeid);
    void removeByItemtypeid(String emitemtypeid);
    List<EMItem> selectByLabserviceid(String emserviceid);
    void removeByLabserviceid(String emserviceid);
    List<EMItem> selectByMserviceid(String emserviceid);
    void removeByMserviceid(String emserviceid);
    List<EMItem> selectByStorepartid(String emstorepartid);
    void removeByStorepartid(String emstorepartid);
    List<EMItem> selectByStoreid(String emstoreid);
    void removeByStoreid(String emstoreid);
    List<EMItem> selectByEmpid(String pfempid);
    void removeByEmpid(String pfempid);
    List<EMItem> selectByLastaempid(String pfempid);
    void removeByLastaempid(String pfempid);
    List<EMItem> selectBySempid(String pfempid);
    void removeBySempid(String pfempid);
    List<EMItem> selectByUnitid(String pfunitid);
    void removeByUnitid(String pfunitid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMItem> getEmitemByIds(List<String> ids);
    List<EMItem> getEmitemByEntities(List<EMItem> entities);
}


