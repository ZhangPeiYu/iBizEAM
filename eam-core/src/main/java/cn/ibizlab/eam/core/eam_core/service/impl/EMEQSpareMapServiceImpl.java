package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareMap;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareMapSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMEQSpareMapService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMEQSpareMapMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[备件包引用] 服务对象接口实现
 */
@Slf4j
@Service("EMEQSpareMapServiceImpl")
public class EMEQSpareMapServiceImpl extends ServiceImpl<EMEQSpareMapMapper, EMEQSpareMap> implements IEMEQSpareMapService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSpareService emeqspareService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMEQSpareMap et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqsparemapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMEQSpareMap> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMEQSpareMap et) {
        fillParentData(et);
        emobjmapService.update(emeqsparemapInheritMapping.toEmobjmap(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emeqsparemapid", et.getEmeqsparemapid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmeqsparemapid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMEQSpareMap> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emobjmapService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMEQSpareMap get(String key) {
        EMEQSpareMap et = getById(key);
        if(et == null){
            et = new EMEQSpareMap();
            et.setEmeqsparemapid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMEQSpareMap getDraft(EMEQSpareMap et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMEQSpareMap et) {
        return (!ObjectUtils.isEmpty(et.getEmeqsparemapid())) && (!Objects.isNull(this.getById(et.getEmeqsparemapid())));
    }
    @Override
    @Transactional
    public boolean save(EMEQSpareMap et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMEQSpareMap et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMEQSpareMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSpareMap> create = new ArrayList<>();
        List<EMEQSpareMap> update = new ArrayList<>();
        for (EMEQSpareMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqsparemapid()) || ObjectUtils.isEmpty(getById(et.getEmeqsparemapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMEQSpareMap> list) {
        list.forEach(item->fillParentData(item));
        List<EMEQSpareMap> create = new ArrayList<>();
        List<EMEQSpareMap> update = new ArrayList<>();
        for (EMEQSpareMap et : list) {
            if (ObjectUtils.isEmpty(et.getEmeqsparemapid()) || ObjectUtils.isEmpty(getById(et.getEmeqsparemapid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMEQSpareMap> selectByEqspareid(String emeqspareid) {
        return baseMapper.selectByEqspareid(emeqspareid);
    }
    @Override
    public void removeByEqspareid(String emeqspareid) {
        this.remove(new QueryWrapper<EMEQSpareMap>().eq("eqspareid",emeqspareid));
    }

	@Override
    public List<EMEQSpareMap> selectByRefobjid(String emobjectid) {
        return baseMapper.selectByRefobjid(emobjectid);
    }
    @Override
    public void removeByRefobjid(String emobjectid) {
        this.remove(new QueryWrapper<EMEQSpareMap>().eq("refobjid",emobjectid));
    }


    /**
     * 查询集合 ByEQ
     */
    @Override
    public Page<EMEQSpareMap> searchByEQ(EMEQSpareMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSpareMap> pages=baseMapper.searchByEQ(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSpareMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMEQSpareMap> searchDefault(EMEQSpareMapSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMEQSpareMap> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMEQSpareMap>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMEQSpareMap et){
        //实体关系[DER1N_EMEQSPAREMAP_EMEQSPARE_EQSPAREID]
        if(!ObjectUtils.isEmpty(et.getEqspareid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEQSpare eqspare=et.getEqspare();
            if(ObjectUtils.isEmpty(eqspare)){
                cn.ibizlab.eam.core.eam_core.domain.EMEQSpare majorEntity=emeqspareService.get(et.getEqspareid());
                et.setEqspare(majorEntity);
                eqspare=majorEntity;
            }
            et.setEqsparename(eqspare.getEmeqsparename());
        }
        //实体关系[DER1N_EMEQSPAREMAP_EMOBJECT_REFOBJID]
        if(!ObjectUtils.isEmpty(et.getRefobjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject refobj=et.getRefobj();
            if(ObjectUtils.isEmpty(refobj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getRefobjid());
                et.setRefobj(majorEntity);
                refobj=majorEntity;
            }
            et.setRefobjname(refobj.getEmobjectname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMEQSpareMapInheritMapping emeqsparemapInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjMapService emobjmapService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMEQSpareMap et){
        if(ObjectUtils.isEmpty(et.getEmeqsparemapid()))
            et.setEmeqsparemapid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMObjMap emobjmap =emeqsparemapInheritMapping.toEmobjmap(et);
        emobjmap.set("emobjmaptype","SPAREMAP");
        emobjmapService.create(emobjmap);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMEQSpareMap> getEmeqsparemapByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMEQSpareMap> getEmeqsparemapByEntities(List<EMEQSpareMap> entities) {
        List ids =new ArrayList();
        for(EMEQSpareMap entity : entities){
            Serializable id=entity.getEmeqsparemapid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMEQSpareMapService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



