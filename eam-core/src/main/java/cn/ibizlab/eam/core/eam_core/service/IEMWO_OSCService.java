package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMWO_OSC;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_OSCSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMWO_OSC] 服务对象接口
 */
public interface IEMWO_OSCService extends IService<EMWO_OSC> {

    boolean create(EMWO_OSC et);
    void createBatch(List<EMWO_OSC> list);
    boolean update(EMWO_OSC et);
    void updateBatch(List<EMWO_OSC> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMWO_OSC get(String key);
    EMWO_OSC getDraft(EMWO_OSC et);
    EMWO_OSC acceptance(EMWO_OSC et);
    boolean acceptanceBatch(List<EMWO_OSC> etList);
    boolean checkKey(EMWO_OSC et);
    EMWO_OSC formUpdateByEmequipid(EMWO_OSC et);
    boolean save(EMWO_OSC et);
    void saveBatch(List<EMWO_OSC> list);
    EMWO_OSC submit(EMWO_OSC et);
    EMWO_OSC unAcceptance(EMWO_OSC et);
    Page<EMWO_OSC> searchCalendar(EMWO_OSCSearchContext context);
    Page<EMWO_OSC> searchConfirmed(EMWO_OSCSearchContext context);
    Page<EMWO_OSC> searchDefault(EMWO_OSCSearchContext context);
    Page<EMWO_OSC> searchDraft(EMWO_OSCSearchContext context);
    Page<EMWO_OSC> searchToConfirm(EMWO_OSCSearchContext context);
    List<EMWO_OSC> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMWO_OSC> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMWO_OSC> selectByDpid(String emobjectid);
    void removeByDpid(String emobjectid);
    List<EMWO_OSC> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMWO_OSC> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMWO_OSC> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMWO_OSC> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMWO_OSC> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMWO_OSC> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMWO_OSC> selectByWooriid(String emwooriid);
    void removeByWooriid(String emwooriid);
    List<EMWO_OSC> selectByWopid(String emwoid);
    void removeByWopid(String emwoid);
    List<EMWO_OSC> selectByRdeptid(String pfdeptid);
    void removeByRdeptid(String pfdeptid);
    List<EMWO_OSC> selectByMpersonid(String pfempid);
    void removeByMpersonid(String pfempid);
    List<EMWO_OSC> selectByRecvpersonid(String pfempid);
    void removeByRecvpersonid(String pfempid);
    List<EMWO_OSC> selectByRempid(String pfempid);
    void removeByRempid(String pfempid);
    List<EMWO_OSC> selectByWpersonid(String pfempid);
    void removeByWpersonid(String pfempid);
    List<EMWO_OSC> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMWO_OSC> getEmwoOscByIds(List<String> ids);
    List<EMWO_OSC> getEmwoOscByEntities(List<EMWO_OSC> entities);
}


