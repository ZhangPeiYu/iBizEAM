package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMServiceMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[服务商] 服务对象接口实现
 */
@Slf4j
@Service("EMServiceServiceImpl")
public class EMServiceServiceImpl extends ServiceImpl<EMServiceMapper, EMService> implements IEMServiceService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMApplyService emapplyService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMAssetService emassetService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMBiddingService embiddingService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMBidinquiryService embidinquiryService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQAHService emeqahService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQCheckService emeqcheckService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService emeqdebugService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQKeepService emeqkeepService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQLCTTIResService emeqlcttiresService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQMaintanceService emeqmaintanceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEQSetupService emeqsetupService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanTemplService emplantemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanService emplanService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPOService empoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMProductService emproductService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMProjectTrackService emprojecttrackService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResServiceService emresserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService emserviceevlService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceHistService emservicehistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_DPService emwoDpService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_ENService emwoEnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_INNERService emwoInnerService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_OSCService emwoOscService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService emwoPtService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListCostService emwplistcostService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWPListService emwplistService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFContractService pfcontractService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMService et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmserviceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMService> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMService et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emserviceid", et.getEmserviceid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmserviceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMService> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMService get(String key) {
        EMService et = getById(key);
        if(et == null){
            et = new EMService();
            et.setEmserviceid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMService getDraft(EMService et) {
        return et;
    }

    @Override
    public boolean checkKey(EMService et) {
        return (!ObjectUtils.isEmpty(et.getEmserviceid())) && (!Objects.isNull(this.getById(et.getEmserviceid())));
    }
    @Override
    @Transactional
    public EMService confirm(EMService et) {
        //自定义代码
        return et;
    }

    @Override
    @Transactional
    public boolean confirmBatch(List<EMService> etList) {
        for(EMService et : etList) {
            confirm(et);
        }
        return true;
    }

    @Override
    @Transactional
    public EMService genId(EMService et) {
         return et ;
    }

    @Override
    @Transactional
    public boolean save(EMService et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMService et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMService> list) {
        List<EMService> create = new ArrayList<>();
        List<EMService> update = new ArrayList<>();
        for (EMService et : list) {
            if (ObjectUtils.isEmpty(et.getEmserviceid()) || ObjectUtils.isEmpty(getById(et.getEmserviceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMService> list) {
        List<EMService> create = new ArrayList<>();
        List<EMService> update = new ArrayList<>();
        for (EMService et : list) {
            if (ObjectUtils.isEmpty(et.getEmserviceid()) || ObjectUtils.isEmpty(getById(et.getEmserviceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }

    @Override
    @Transactional
    public EMService unconfirmed(EMService et) {
         return et ;
    }



    /**
     * 查询集合 已审
     */
    @Override
    public Page<EMService> searchConfirmed(EMServiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMService> pages=baseMapper.searchConfirmed(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMService>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMService> searchDefault(EMServiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMService> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMService>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 未提交
     */
    @Override
    public Page<EMService> searchDraft(EMServiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMService> pages=baseMapper.searchDraft(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMService>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 待审
     */
    @Override
    public Page<EMService> searchToConfirm(EMServiceSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMService> pages=baseMapper.searchToConfirm(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMService>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMService> getEmserviceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMService> getEmserviceByEntities(List<EMService> entities) {
        List ids =new ArrayList();
        for(EMService entity : entities){
            Serializable id=entity.getEmserviceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMServiceService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



