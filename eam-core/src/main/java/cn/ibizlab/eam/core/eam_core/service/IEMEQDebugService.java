package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQDebug;
import cn.ibizlab.eam.core.eam_core.filter.EMEQDebugSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQDebug] 服务对象接口
 */
public interface IEMEQDebugService extends IService<EMEQDebug> {

    boolean create(EMEQDebug et);
    void createBatch(List<EMEQDebug> list);
    boolean update(EMEQDebug et);
    void updateBatch(List<EMEQDebug> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQDebug get(String key);
    EMEQDebug getDraft(EMEQDebug et);
    boolean checkKey(EMEQDebug et);
    boolean save(EMEQDebug et);
    void saveBatch(List<EMEQDebug> list);
    Page<EMEQDebug> searchCalendar(EMEQDebugSearchContext context);
    Page<EMEQDebug> searchDefault(EMEQDebugSearchContext context);
    List<EMEQDebug> selectByAcclassid(String emacclassid);
    void removeByAcclassid(String emacclassid);
    List<EMEQDebug> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEQDebug> selectByObjid(String emobjectid);
    void removeByObjid(String emobjectid);
    List<EMEQDebug> selectByRfoacid(String emrfoacid);
    void removeByRfoacid(String emrfoacid);
    List<EMEQDebug> selectByRfocaid(String emrfocaid);
    void removeByRfocaid(String emrfocaid);
    List<EMEQDebug> selectByRfodeid(String emrfodeid);
    void removeByRfodeid(String emrfodeid);
    List<EMEQDebug> selectByRfomoid(String emrfomoid);
    void removeByRfomoid(String emrfomoid);
    List<EMEQDebug> selectByRserviceid(String emserviceid);
    void removeByRserviceid(String emserviceid);
    List<EMEQDebug> selectByWoid(String emwoid);
    void removeByWoid(String emwoid);
    List<EMEQDebug> selectByRteamid(String pfteamid);
    void removeByRteamid(String pfteamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQDebug> getEmeqdebugByIds(List<String> ids);
    List<EMEQDebug> getEmeqdebugByEntities(List<EMEQDebug> entities);
}


