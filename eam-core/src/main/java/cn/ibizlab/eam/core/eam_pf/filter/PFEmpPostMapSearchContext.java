package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmpPostMap;
/**
 * 关系型数据实体[PFEmpPostMap] 查询条件对象
 */
@Slf4j
@Data
public class PFEmpPostMapSearchContext extends QueryWrapperContext<PFEmpPostMap> {

	private String n_empname_eq;//[职员]
	public void setN_empname_eq(String n_empname_eq) {
        this.n_empname_eq = n_empname_eq;
        if(!ObjectUtils.isEmpty(this.n_empname_eq)){
            this.getSearchCond().eq("empname", n_empname_eq);
        }
    }
	private String n_empname_like;//[职员]
	public void setN_empname_like(String n_empname_like) {
        this.n_empname_like = n_empname_like;
        if(!ObjectUtils.isEmpty(this.n_empname_like)){
            this.getSearchCond().like("empname", n_empname_like);
        }
    }
	private String n_deptid_eq;//[部门]
	public void setN_deptid_eq(String n_deptid_eq) {
        this.n_deptid_eq = n_deptid_eq;
        if(!ObjectUtils.isEmpty(this.n_deptid_eq)){
            this.getSearchCond().eq("deptid", n_deptid_eq);
        }
    }
	private String n_deptname_eq;//[部门]
	public void setN_deptname_eq(String n_deptname_eq) {
        this.n_deptname_eq = n_deptname_eq;
        if(!ObjectUtils.isEmpty(this.n_deptname_eq)){
            this.getSearchCond().eq("deptname", n_deptname_eq);
        }
    }
	private String n_deptname_like;//[部门]
	public void setN_deptname_like(String n_deptname_like) {
        this.n_deptname_like = n_deptname_like;
        if(!ObjectUtils.isEmpty(this.n_deptname_like)){
            this.getSearchCond().like("deptname", n_deptname_like);
        }
    }
	private String n_empid_eq;//[职员]
	public void setN_empid_eq(String n_empid_eq) {
        this.n_empid_eq = n_empid_eq;
        if(!ObjectUtils.isEmpty(this.n_empid_eq)){
            this.getSearchCond().eq("empid", n_empid_eq);
        }
    }
	private String n_pfemppostmapname_like;//[人事关系名称]
	public void setN_pfemppostmapname_like(String n_pfemppostmapname_like) {
        this.n_pfemppostmapname_like = n_pfemppostmapname_like;
        if(!ObjectUtils.isEmpty(this.n_pfemppostmapname_like)){
            this.getSearchCond().like("pfemppostmapname", n_pfemppostmapname_like);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_teamname_eq;//[班组]
	public void setN_teamname_eq(String n_teamname_eq) {
        this.n_teamname_eq = n_teamname_eq;
        if(!ObjectUtils.isEmpty(this.n_teamname_eq)){
            this.getSearchCond().eq("teamname", n_teamname_eq);
        }
    }
	private String n_teamname_like;//[班组]
	public void setN_teamname_like(String n_teamname_like) {
        this.n_teamname_like = n_teamname_like;
        if(!ObjectUtils.isEmpty(this.n_teamname_like)){
            this.getSearchCond().like("teamname", n_teamname_like);
        }
    }
	private String n_postname_eq;//[岗位]
	public void setN_postname_eq(String n_postname_eq) {
        this.n_postname_eq = n_postname_eq;
        if(!ObjectUtils.isEmpty(this.n_postname_eq)){
            this.getSearchCond().eq("postname", n_postname_eq);
        }
    }
	private String n_postname_like;//[岗位]
	public void setN_postname_like(String n_postname_like) {
        this.n_postname_like = n_postname_like;
        if(!ObjectUtils.isEmpty(this.n_postname_like)){
            this.getSearchCond().like("postname", n_postname_like);
        }
    }
	private String n_teamid_eq;//[班组]
	public void setN_teamid_eq(String n_teamid_eq) {
        this.n_teamid_eq = n_teamid_eq;
        if(!ObjectUtils.isEmpty(this.n_teamid_eq)){
            this.getSearchCond().eq("teamid", n_teamid_eq);
        }
    }
	private String n_postid_eq;//[岗位]
	public void setN_postid_eq(String n_postid_eq) {
        this.n_postid_eq = n_postid_eq;
        if(!ObjectUtils.isEmpty(this.n_postid_eq)){
            this.getSearchCond().eq("postid", n_postid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfemppostmapname", query)
            );
		 }
	}
}



