package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMStore;
import cn.ibizlab.eam.core.eam_core.filter.EMStoreSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMStoreService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMStoreMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[仓库] 服务对象接口实现
 */
@Slf4j
@Service("EMStoreServiceImpl")
public class EMStoreServiceImpl extends ServiceImpl<EMStoreMapper, EMStore> implements IEMStoreService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMCabService emcabService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemCSService emitemcsService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPLService emitemplService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPRtnService emitemprtnService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemPUseService emitempuseService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemRInService emitemrinService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemROutService emitemroutService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemTradeService emitemtradeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMItemService emitemService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStockService emstockService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMStorePartService emstorepartService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFEmpService pfempService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMStore et) {
        fillParentData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmstoreid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMStore> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMStore et) {
        fillParentData(et);
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emstoreid", et.getEmstoreid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmstoreid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMStore> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMStore get(String key) {
        EMStore et = getById(key);
        if(et == null){
            et = new EMStore();
            et.setEmstoreid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMStore getDraft(EMStore et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMStore et) {
        return (!ObjectUtils.isEmpty(et.getEmstoreid())) && (!Objects.isNull(this.getById(et.getEmstoreid())));
    }
    @Override
    @Transactional
    public boolean save(EMStore et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMStore et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMStore> list) {
        list.forEach(item->fillParentData(item));
        List<EMStore> create = new ArrayList<>();
        List<EMStore> update = new ArrayList<>();
        for (EMStore et : list) {
            if (ObjectUtils.isEmpty(et.getEmstoreid()) || ObjectUtils.isEmpty(getById(et.getEmstoreid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMStore> list) {
        list.forEach(item->fillParentData(item));
        List<EMStore> create = new ArrayList<>();
        List<EMStore> update = new ArrayList<>();
        for (EMStore et : list) {
            if (ObjectUtils.isEmpty(et.getEmstoreid()) || ObjectUtils.isEmpty(getById(et.getEmstoreid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMStore> selectByEmpid(String pfempid) {
        return baseMapper.selectByEmpid(pfempid);
    }
    @Override
    public void removeByEmpid(String pfempid) {
        this.remove(new QueryWrapper<EMStore>().eq("empid",pfempid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMStore> searchDefault(EMStoreSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMStore> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMStore>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMStore et){
        //实体关系[DER1N_EMSTORE_PFEMP_EMPID]
        if(!ObjectUtils.isEmpty(et.getEmpid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFEmp pfempid=et.getPfempid();
            if(ObjectUtils.isEmpty(pfempid)){
                cn.ibizlab.eam.core.eam_pf.domain.PFEmp majorEntity=pfempService.get(et.getEmpid());
                et.setPfempid(majorEntity);
                pfempid=majorEntity;
            }
            et.setEmpname(pfempid.getEmpinfo());
        }
    }




    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMStore> getEmstoreByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMStore> getEmstoreByEntities(List<EMStore> entities) {
        List ids =new ArrayList();
        for(EMStore entity : entities){
            Serializable id=entity.getEmstoreid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMStoreService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



