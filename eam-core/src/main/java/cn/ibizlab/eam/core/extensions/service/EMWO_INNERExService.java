package cn.ibizlab.eam.core.extensions.service;

import cn.ibizlab.eam.core.eam_core.domain.*;
import cn.ibizlab.eam.core.eam_core.service.impl.EMWO_INNERServiceImpl;
import cn.ibizlab.eam.core.util.helper.AddAHhelper;
import cn.ibizlab.eam.core.util.helper.Aops;
import cn.ibizlab.eam.util.dict.StaticDict;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

/**
 * 实体[内部工单] 自定义服务对象
 */
@Slf4j
@Primary
@Service("EMWO_INNERExService")
public class EMWO_INNERExService extends EMWO_INNERServiceImpl {

    @Autowired
    private AddAHhelper addAHhelper;

    @Override
    protected Class currentModelClass() {
        return com.baomidou.mybatisplus.core.toolkit.ReflectionKit.getSuperClassGenericType(this.getClass().getSuperclass(), 1);
    }

    /**
     * 验收通过
     * @param et
     * @return
     */
    @Override
    @Transactional
    public EMWO_INNER acceptance(EMWO_INNER et) {
        et = this.get(et.getEmwoInnerid());
        EMWO emwo = emwoService.get(et.getEmwoInnerid());

        if (emwo == null) {
            throw new RuntimeException("工单不存在，无法完成操作");
        }
        if (et == null) {
            throw new RuntimeException("内部工单不存在，无法完成操作");
        }
        addAHhelper.genRecord(emwo);
        // 如果是轮胎安装工单和钢丝绳更换工单，修改指定数据
        addAHhelper.orderType(et);

        et.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        Aops.getSelf(this).update(et);

        emwo.setWostate(Integer.valueOf(StaticDict.EMWOSTATE.ITEM_30.getValue()));
        emwoService.update(emwo);
        return super.acceptance(et);
    }

    @Override
    public boolean create(EMWO_INNER et) {
        Calendar calendar = Calendar.getInstance();
        // 起始时间
        if (et.getRegionbegindate() == null) {
            et.setRegionbegindate(et.getWodate());
        }
        // 结束时间=起始时间+安排工时(activeLengths)
        if (et.getRegionenddate() == null && et.getActivelengths() != null) {
            long activelengths = et.getActivelengths().longValue();
            et.setRegionenddate(new Timestamp(et.getRegionbegindate().getTime() + activelengths * 60 * 60 * 1000));
        }
        // 实际执行人如果为空，设置为责任人
        if (et.getRempid() != null && et.getWpersonid() == null) {
            et.setWpersonid(et.getRempid());
            et.setWpersonname(et.getRempname());
        }
        // 指派接收人如果为空，设置为责任人
        if (et.getRempid() != null && et.getRecvpersonid() == null) {
            et.setRecvpersonid(et.getRempid());
            et.setRecvpersonname(et.getRempname());
        }
        // 制定时间
        if (et.getMdate() == null) {
            et.setMdate(new Timestamp(calendar.getTimeInMillis()));
        }
        // 过期时间=制定时间+10天
        if (et.getMdate() != null && et.getExpiredate() == null) {
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) + 10);
            et.setExpiredate(new Timestamp(calendar.getTimeInMillis()));
        }
        return super.create(et);
}
}

