

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMItemTypeInheritMapping {

    @Mappings({
        @Mapping(source ="emitemtypeid",target = "emobjectid"),
        @Mapping(source ="emitemtypename",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="itemtypecode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMItemType minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emitemtypeid"),
        @Mapping(source ="emobjectname" ,target = "emitemtypename"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "itemtypecode"),
    })
    EMItemType toEmitemtype(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMItemType> minorEntities);

    List<EMItemType> toEmitemtype(List<EMObject> majorEntities);

}


