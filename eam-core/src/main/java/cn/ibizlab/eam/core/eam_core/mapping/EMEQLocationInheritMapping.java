

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMEQLocationInheritMapping {

    @Mappings({
        @Mapping(source ="emeqlocationid",target = "emobjectid"),
        @Mapping(source ="emeqlocationname",target = "emobjectname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="eqlocationcode",target = "objectcode"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObject toEmobject(EMEQLocation minorEntity);

    @Mappings({
        @Mapping(source ="emobjectid" ,target = "emeqlocationid"),
        @Mapping(source ="emobjectname" ,target = "emeqlocationname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objectcode",target = "eqlocationcode"),
    })
    EMEQLocation toEmeqlocation(EMObject majorEntity);

    List<EMObject> toEmobject(List<EMEQLocation> minorEntities);

    List<EMEQLocation> toEmeqlocation(List<EMObject> majorEntities);

}


