package cn.ibizlab.eam.core.eam_core.service.logic.impl;

import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import cn.ibizlab.eam.core.eam_core.service.logic.IEMItemROutFormUpdateBySTOREIDLogic;
import cn.ibizlab.eam.core.eam_core.domain.EMItemROut;

/**
 * 关系型数据实体[FormUpdateBySTOREID] 对象
 */
@Slf4j
@Service
public class EMItemROutFormUpdateBySTOREIDLogicImpl implements IEMItemROutFormUpdateBySTOREIDLogic {

    @Autowired
    private KieContainer kieContainer;


    @Autowired
    private cn.ibizlab.eam.core.eam_core.service.IEMItemROutService iBzSysDefaultService;

    public cn.ibizlab.eam.core.eam_core.service.IEMItemROutService getIBzSysDefaultService() {
        return this.iBzSysDefaultService;
    }

    @Override
    public void execute(EMItemROut et) {

        KieSession kieSession = null;
        try {
            kieSession = kieContainer.newKieSession();
            kieSession.insert(et); 
            kieSession.setGlobal("emitemroutformupdatebystoreiddefault", et);
            kieSession.setGlobal("iBzSysEmitemroutDefaultService", iBzSysDefaultService);
            kieSession.setGlobal("curuser", cn.ibizlab.eam.util.security.AuthenticationUser.getAuthenticationUser());
            kieSession.startProcess("cn.ibizlab.eam.core.eam_core.service.logic.emitemroutformupdatebystoreid");

        } catch (Exception e) {
            throw new RuntimeException("执行[仓库库位]处理逻辑发生异常" + e);
        } finally {
            if(kieSession != null) {
                kieSession.destroy();
            }
        }
    }
}
