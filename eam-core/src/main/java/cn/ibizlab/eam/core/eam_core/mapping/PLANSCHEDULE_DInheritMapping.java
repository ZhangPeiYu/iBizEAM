

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_D;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface PLANSCHEDULE_DInheritMapping {

    @Mappings({
        @Mapping(source ="planscheduleDid",target = "planscheduleid"),
        @Mapping(source ="planscheduleDname",target = "planschedulename"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE toPlanschedule(PLANSCHEDULE_D minorEntity);

    @Mappings({
        @Mapping(source ="planscheduleid" ,target = "planscheduleDid"),
        @Mapping(source ="planschedulename" ,target = "planscheduleDname"),
        @Mapping(target ="focusNull",ignore = true),
    })
    PLANSCHEDULE_D toPlanscheduleD(PLANSCHEDULE majorEntity);

    List<PLANSCHEDULE> toPlanschedule(List<PLANSCHEDULE_D> minorEntities);

    List<PLANSCHEDULE_D> toPlanscheduleD(List<PLANSCHEDULE> majorEntities);

}


