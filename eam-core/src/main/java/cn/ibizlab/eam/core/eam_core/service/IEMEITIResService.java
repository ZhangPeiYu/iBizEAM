package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEITIRes;
import cn.ibizlab.eam.core.eam_core.filter.EMEITIResSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEITIRes] 服务对象接口
 */
public interface IEMEITIResService extends IService<EMEITIRes> {

    boolean create(EMEITIRes et);
    void createBatch(List<EMEITIRes> list);
    boolean update(EMEITIRes et);
    void updateBatch(List<EMEITIRes> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEITIRes get(String key);
    EMEITIRes getDraft(EMEITIRes et);
    boolean checkKey(EMEITIRes et);
    boolean save(EMEITIRes et);
    void saveBatch(List<EMEITIRes> list);
    Page<EMEITIRes> searchDefault(EMEITIResSearchContext context);
    List<EMEITIRes> selectByEqlocationid(String emeqlocationid);
    void removeByEqlocationid(String emeqlocationid);
    List<EMEITIRes> selectByEquipid(String emequipid);
    void removeByEquipid(String emequipid);
    List<EMEITIRes> selectByItempuseid(String emitempuseid);
    void removeByItempuseid(String emitempuseid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEITIRes> getEmeitiresByIds(List<String> ids);
    List<EMEITIRes> getEmeitiresByEntities(List<EMEITIRes> entities);
}


