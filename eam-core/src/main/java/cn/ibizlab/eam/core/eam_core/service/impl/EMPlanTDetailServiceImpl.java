package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMPlanTDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMPlanTDetailSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMPlanTDetailService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMPlanTDetailMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[计划模板步骤] 服务对象接口实现
 */
@Slf4j
@Service("EMPlanTDetailServiceImpl")
public class EMPlanTDetailServiceImpl extends ServiceImpl<EMPlanTDetailMapper, EMPlanTDetail> implements IEMPlanTDetailService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMPlanTemplService emplantemplService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMPlanTDetail et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplantdetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMPlanTDetail> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMPlanTDetail et) {
        fillParentData(et);
        emresrefobjService.update(emplantdetailInheritMapping.toEmresrefobj(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emplantdetailid", et.getEmplantdetailid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmplantdetailid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMPlanTDetail> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emresrefobjService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMPlanTDetail get(String key) {
        EMPlanTDetail et = getById(key);
        if(et == null){
            et = new EMPlanTDetail();
            et.setEmplantdetailid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMPlanTDetail getDraft(EMPlanTDetail et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMPlanTDetail et) {
        return (!ObjectUtils.isEmpty(et.getEmplantdetailid())) && (!Objects.isNull(this.getById(et.getEmplantdetailid())));
    }
    @Override
    @Transactional
    public boolean save(EMPlanTDetail et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMPlanTDetail et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMPlanTDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlanTDetail> create = new ArrayList<>();
        List<EMPlanTDetail> update = new ArrayList<>();
        for (EMPlanTDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmplantdetailid()) || ObjectUtils.isEmpty(getById(et.getEmplantdetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMPlanTDetail> list) {
        list.forEach(item->fillParentData(item));
        List<EMPlanTDetail> create = new ArrayList<>();
        List<EMPlanTDetail> update = new ArrayList<>();
        for (EMPlanTDetail et : list) {
            if (ObjectUtils.isEmpty(et.getEmplantdetailid()) || ObjectUtils.isEmpty(getById(et.getEmplantdetailid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMPlanTDetail> selectByPlantemplid(String emplantemplid) {
        return baseMapper.selectByPlantemplid(emplantemplid);
    }
    @Override
    public void removeByPlantemplid(String emplantemplid) {
        this.remove(new QueryWrapper<EMPlanTDetail>().eq("plantemplid",emplantemplid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMPlanTDetail> searchDefault(EMPlanTDetailSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMPlanTDetail> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMPlanTDetail>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMPlanTDetail et){
        //实体关系[DER1N_EMPLANTDETAIL_EMPLANTEMPL_PLANTEMPLID]
        if(!ObjectUtils.isEmpty(et.getPlantemplid())){
            cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl plantempl=et.getPlantempl();
            if(ObjectUtils.isEmpty(plantempl)){
                cn.ibizlab.eam.core.eam_core.domain.EMPlanTempl majorEntity=emplantemplService.get(et.getPlantemplid());
                et.setPlantempl(majorEntity);
                plantempl=majorEntity;
            }
            et.setPlantemplname(plantempl.getEmplantemplname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMPlanTDetailInheritMapping emplantdetailInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMPlanTDetail et){
        if(ObjectUtils.isEmpty(et.getEmplantdetailid()))
            et.setEmplantdetailid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMResRefObj emresrefobj =emplantdetailInheritMapping.toEmresrefobj(et);
        emresrefobj.set("emresrefobjtype","PLANTDETAIL");
        emresrefobjService.create(emresrefobj);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMPlanTDetail> getEmplantdetailByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMPlanTDetail> getEmplantdetailByEntities(List<EMPlanTDetail> entities) {
        List ids =new ArrayList();
        for(EMPlanTDetail entity : entities){
            Serializable id=entity.getEmplantdetailid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMPlanTDetailService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



