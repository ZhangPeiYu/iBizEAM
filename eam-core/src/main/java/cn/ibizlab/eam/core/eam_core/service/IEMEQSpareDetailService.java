package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEQSpareDetail;
import cn.ibizlab.eam.core.eam_core.filter.EMEQSpareDetailSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEQSpareDetail] 服务对象接口
 */
public interface IEMEQSpareDetailService extends IService<EMEQSpareDetail> {

    boolean create(EMEQSpareDetail et);
    void createBatch(List<EMEQSpareDetail> list);
    boolean update(EMEQSpareDetail et);
    void updateBatch(List<EMEQSpareDetail> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEQSpareDetail get(String key);
    EMEQSpareDetail getDraft(EMEQSpareDetail et);
    boolean checkKey(EMEQSpareDetail et);
    boolean save(EMEQSpareDetail et);
    void saveBatch(List<EMEQSpareDetail> list);
    Page<EMEQSpareDetail> searchDefault(EMEQSpareDetailSearchContext context);
    List<EMEQSpareDetail> selectByEqspareid(String emeqspareid);
    void removeByEqspareid(String emeqspareid);
    List<EMEQSpareDetail> selectByItemid(String emitemid);
    void removeByItemid(String emitemid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEQSpareDetail> getEmeqsparedetailByIds(List<String> ids);
    List<EMEQSpareDetail> getEmeqsparedetailByEntities(List<EMEQSpareDetail> entities);
}


