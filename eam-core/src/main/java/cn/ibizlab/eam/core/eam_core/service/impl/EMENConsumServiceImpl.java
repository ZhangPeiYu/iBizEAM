package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMENConsum;
import cn.ibizlab.eam.core.eam_core.filter.EMENConsumSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMENConsumService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMENConsumMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[能耗] 服务对象接口实现
 */
@Slf4j
@Service("EMENConsumServiceImpl")
public class EMENConsumServiceImpl extends ServiceImpl<EMENConsumMapper, EMENConsum> implements IEMENConsumService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMENService emenService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMENConsum et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmenconsumid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMENConsum> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMENConsum et) {
        fillParentData(et);
        emdprctService.update(emenconsumInheritMapping.toEmdprct(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emenconsumid", et.getEmenconsumid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmenconsumid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMENConsum> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emdprctService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMENConsum get(String key) {
        EMENConsum et = getById(key);
        if(et == null){
            et = new EMENConsum();
            et.setEmenconsumid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMENConsum getDraft(EMENConsum et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMENConsum et) {
        return (!ObjectUtils.isEmpty(et.getEmenconsumid())) && (!Objects.isNull(this.getById(et.getEmenconsumid())));
    }
    @Override
    @Transactional
    public boolean save(EMENConsum et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMENConsum et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMENConsum> list) {
        list.forEach(item->fillParentData(item));
        List<EMENConsum> create = new ArrayList<>();
        List<EMENConsum> update = new ArrayList<>();
        for (EMENConsum et : list) {
            if (ObjectUtils.isEmpty(et.getEmenconsumid()) || ObjectUtils.isEmpty(getById(et.getEmenconsumid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMENConsum> list) {
        list.forEach(item->fillParentData(item));
        List<EMENConsum> create = new ArrayList<>();
        List<EMENConsum> update = new ArrayList<>();
        for (EMENConsum et : list) {
            if (ObjectUtils.isEmpty(et.getEmenconsumid()) || ObjectUtils.isEmpty(getById(et.getEmenconsumid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMENConsum> selectByEnid(String emenid) {
        return baseMapper.selectByEnid(emenid);
    }
    @Override
    public void removeByEnid(String emenid) {
        this.remove(new QueryWrapper<EMENConsum>().eq("enid",emenid));
    }

	@Override
    public List<EMENConsum> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMENConsum>().eq("equipid",emequipid));
    }

	@Override
    public List<EMENConsum> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMENConsum>().eq("objid",emobjectid));
    }

	@Override
    public List<EMENConsum> selectByWoid(String emwoid) {
        return baseMapper.selectByWoid(emwoid);
    }
    @Override
    public void removeByWoid(String emwoid) {
        this.remove(new QueryWrapper<EMENConsum>().eq("woid",emwoid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMENConsum> searchDefault(EMENConsumSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMENConsum> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMENConsum>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }

    /**
     * 查询集合 年度设备能耗
     */
    @Override
    public Page<Map> searchEqEnByYear(EMENConsumSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map> pages=baseMapper.searchEqEnByYear(context.getPages(),context,context.getSelectCond());
        return new PageImpl<Map>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMENConsum et){
        //实体关系[DER1N_EMENCONSUM_EMEN_ENID]
        if(!ObjectUtils.isEmpty(et.getEnid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEN en=et.getEn();
            if(ObjectUtils.isEmpty(en)){
                cn.ibizlab.eam.core.eam_core.domain.EMEN majorEntity=emenService.get(et.getEnid());
                et.setEn(majorEntity);
                en=majorEntity;
            }
            et.setEnprice(en.getPrice());
            et.setItemmtypename(en.getItemmtypename());
            et.setItemname(en.getItemname());
            et.setUnitname(en.getUnitname());
            et.setItemprice(en.getItemprice());
            et.setEnergytypeid(en.getEnergytypeid());
            et.setEnname(en.getEmenname());
            et.setItemtypeid(en.getItemtypeid());
            et.setItembtypeid(en.getItembtypeid());
            et.setItembtypename(en.getItembtypename());
            et.setItemid(en.getItemid());
            et.setItemmtypeid(en.getItemmtypeid());
        }
        //实体关系[DER1N_EMENCONSUM_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipcode(equip.getEquipcode());
            et.setEquipname(equip.getEquipinfo());
            et.setSname(equip.getSname());
        }
        //实体关系[DER1N_EMENCONSUM_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMENCONSUM_EMWO_WOID]
        if(!ObjectUtils.isEmpty(et.getWoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wo=et.getWo();
            if(ObjectUtils.isEmpty(wo)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWoid());
                et.setWo(majorEntity);
                wo=majorEntity;
            }
            et.setWoname(wo.getEmwoname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMENConsumInheritMapping emenconsumInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMDPRCTService emdprctService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMENConsum et){
        if(ObjectUtils.isEmpty(et.getEmenconsumid()))
            et.setEmenconsumid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMDPRCT emdprct =emenconsumInheritMapping.toEmdprct(et);
        emdprct.set("emdprcttype","ENCONSUM");
        emdprctService.create(emdprct);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMENConsum> getEmenconsumByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMENConsum> getEmenconsumByEntities(List<EMENConsum> entities) {
        List ids =new ArrayList();
        for(EMENConsum entity : entities){
            Serializable id=entity.getEmenconsumid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMENConsumService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



