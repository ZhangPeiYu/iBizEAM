

package cn.ibizlab.eam.core.eam_core.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDRWGMap;
import cn.ibizlab.eam.core.eam_core.domain.EMObjMap;
import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface EMDRWGMapInheritMapping {

    @Mappings({
        @Mapping(source ="emdrwgmapid",target = "emobjmapid"),
        @Mapping(source ="emdrwgmapname",target = "emobjmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="description",target = "description"),
        @Mapping(source ="drwgid",target = "objid"),
        @Mapping(source ="refobjid",target = "objpid"),
        @Mapping(source ="orgid",target = "orgid"),
    })
    EMObjMap toEmobjmap(EMDRWGMap minorEntity);

    @Mappings({
        @Mapping(source ="emobjmapid" ,target = "emdrwgmapid"),
        @Mapping(source ="emobjmapname" ,target = "emdrwgmapname"),
        @Mapping(target ="focusNull",ignore = true),
        @Mapping(source ="objid",target = "drwgid"),
        @Mapping(source ="objpid",target = "refobjid"),
    })
    EMDRWGMap toEmdrwgmap(EMObjMap majorEntity);

    List<EMObjMap> toEmobjmap(List<EMDRWGMap> minorEntities);

    List<EMDRWGMap> toEmdrwgmap(List<EMObjMap> majorEntities);

}


