package cn.ibizlab.eam.core.eam_core.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthlyDetail;
/**
 * 关系型数据实体[EMMonthlyDetail] 查询条件对象
 */
@Slf4j
@Data
public class EMMonthlyDetailSearchContext extends QueryWrapperContext<EMMonthlyDetail> {

	private String n_execution_eq;//[完成情况]
	public void setN_execution_eq(String n_execution_eq) {
        this.n_execution_eq = n_execution_eq;
        if(!ObjectUtils.isEmpty(this.n_execution_eq)){
            this.getSearchCond().eq("execution", n_execution_eq);
        }
    }
	private String n_emmonthlydetailname_like;//[项目名称]
	public void setN_emmonthlydetailname_like(String n_emmonthlydetailname_like) {
        this.n_emmonthlydetailname_like = n_emmonthlydetailname_like;
        if(!ObjectUtils.isEmpty(this.n_emmonthlydetailname_like)){
            this.getSearchCond().like("emmonthlydetailname", n_emmonthlydetailname_like);
        }
    }
	private String n_emmonthlyname_eq;//[维修中心月度计划]
	public void setN_emmonthlyname_eq(String n_emmonthlyname_eq) {
        this.n_emmonthlyname_eq = n_emmonthlyname_eq;
        if(!ObjectUtils.isEmpty(this.n_emmonthlyname_eq)){
            this.getSearchCond().eq("emmonthlyname", n_emmonthlyname_eq);
        }
    }
	private String n_emmonthlyname_like;//[维修中心月度计划]
	public void setN_emmonthlyname_like(String n_emmonthlyname_like) {
        this.n_emmonthlyname_like = n_emmonthlyname_like;
        if(!ObjectUtils.isEmpty(this.n_emmonthlyname_like)){
            this.getSearchCond().like("emmonthlyname", n_emmonthlyname_like);
        }
    }
	private String n_emmonthlyid_eq;//[维修中心月度计划]
	public void setN_emmonthlyid_eq(String n_emmonthlyid_eq) {
        this.n_emmonthlyid_eq = n_emmonthlyid_eq;
        if(!ObjectUtils.isEmpty(this.n_emmonthlyid_eq)){
            this.getSearchCond().eq("emmonthlyid", n_emmonthlyid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("emmonthlydetailname", query)
            );
		 }
	}
}



