package cn.ibizlab.eam.core.eam_core.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.eam.core.eam_core.domain.EMEICamDrop;
import cn.ibizlab.eam.core.eam_core.filter.EMEICamDropSearchContext;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[EMEICamDrop] 服务对象接口
 */
public interface IEMEICamDropService extends IService<EMEICamDrop> {

    boolean create(EMEICamDrop et);
    void createBatch(List<EMEICamDrop> list);
    boolean update(EMEICamDrop et);
    void updateBatch(List<EMEICamDrop> list);
    boolean remove(String key);
    void removeBatch(Collection<String> idList);
    EMEICamDrop get(String key);
    EMEICamDrop getDraft(EMEICamDrop et);
    boolean checkKey(EMEICamDrop et);
    boolean save(EMEICamDrop et);
    void saveBatch(List<EMEICamDrop> list);
    Page<EMEICamDrop> searchDefault(EMEICamDropSearchContext context);
    List<EMEICamDrop> selectByEiobjid(String emeicamid);
    void removeByEiobjid(String emeicamid);
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);

    List<EMEICamDrop> getEmeicamdropByIds(List<String> ids);
    List<EMEICamDrop> getEmeicamdropByEntities(List<EMEICamDrop> entities);
}


