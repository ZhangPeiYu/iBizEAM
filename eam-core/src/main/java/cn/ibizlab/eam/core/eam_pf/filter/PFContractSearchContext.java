package cn.ibizlab.eam.core.eam_pf.filter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.fastjson.annotation.JSONField;

import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;


import cn.ibizlab.eam.util.filter.QueryWrapperContext;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.ibizlab.eam.core.eam_pf.domain.PFContract;
/**
 * 关系型数据实体[PFContract] 查询条件对象
 */
@Slf4j
@Data
public class PFContractSearchContext extends QueryWrapperContext<PFContract> {

	private String n_contractobjgroup_eq;//[合同对方分组]
	public void setN_contractobjgroup_eq(String n_contractobjgroup_eq) {
        this.n_contractobjgroup_eq = n_contractobjgroup_eq;
        if(!ObjectUtils.isEmpty(this.n_contractobjgroup_eq)){
            this.getSearchCond().eq("contractobjgroup", n_contractobjgroup_eq);
        }
    }
	private String n_orgid_eq;//[组织]
	public void setN_orgid_eq(String n_orgid_eq) {
        this.n_orgid_eq = n_orgid_eq;
        if(!ObjectUtils.isEmpty(this.n_orgid_eq)){
            this.getSearchCond().eq("orgid", n_orgid_eq);
        }
    }
	private String n_pfcontractname_like;//[合同名称]
	public void setN_pfcontractname_like(String n_pfcontractname_like) {
        this.n_pfcontractname_like = n_pfcontractname_like;
        if(!ObjectUtils.isEmpty(this.n_pfcontractname_like)){
            this.getSearchCond().like("pfcontractname", n_pfcontractname_like);
        }
    }
	private String n_apprstate_eq;//[审批状态]
	public void setN_apprstate_eq(String n_apprstate_eq) {
        this.n_apprstate_eq = n_apprstate_eq;
        if(!ObjectUtils.isEmpty(this.n_apprstate_eq)){
            this.getSearchCond().eq("apprstate", n_apprstate_eq);
        }
    }
	private String n_performstate_eq;//[履约情况]
	public void setN_performstate_eq(String n_performstate_eq) {
        this.n_performstate_eq = n_performstate_eq;
        if(!ObjectUtils.isEmpty(this.n_performstate_eq)){
            this.getSearchCond().eq("performstate", n_performstate_eq);
        }
    }
	private String n_contracttypeid_eq;//[合同类型]
	public void setN_contracttypeid_eq(String n_contracttypeid_eq) {
        this.n_contracttypeid_eq = n_contracttypeid_eq;
        if(!ObjectUtils.isEmpty(this.n_contracttypeid_eq)){
            this.getSearchCond().eq("contracttypeid", n_contracttypeid_eq);
        }
    }
	private String n_contractinfo_like;//[合同信息]
	public void setN_contractinfo_like(String n_contractinfo_like) {
        this.n_contractinfo_like = n_contractinfo_like;
        if(!ObjectUtils.isEmpty(this.n_contractinfo_like)){
            this.getSearchCond().like("contractinfo", n_contractinfo_like);
        }
    }
	private String n_emservicename_eq;//[服务商]
	public void setN_emservicename_eq(String n_emservicename_eq) {
        this.n_emservicename_eq = n_emservicename_eq;
        if(!ObjectUtils.isEmpty(this.n_emservicename_eq)){
            this.getSearchCond().eq("emservicename", n_emservicename_eq);
        }
    }
	private String n_emservicename_like;//[服务商]
	public void setN_emservicename_like(String n_emservicename_like) {
        this.n_emservicename_like = n_emservicename_like;
        if(!ObjectUtils.isEmpty(this.n_emservicename_like)){
            this.getSearchCond().like("emservicename", n_emservicename_like);
        }
    }
	private String n_emserviceid_eq;//[服务商]
	public void setN_emserviceid_eq(String n_emserviceid_eq) {
        this.n_emserviceid_eq = n_emserviceid_eq;
        if(!ObjectUtils.isEmpty(this.n_emserviceid_eq)){
            this.getSearchCond().eq("emserviceid", n_emserviceid_eq);
        }
    }

    /**
	 * 启用快速搜索
	 */
    @Override
	public void setQuery(String query)
	{
		 this.query=query;
		 if(!StringUtils.isEmpty(query)){
            this.getSearchCond().and( wrapper ->
                     wrapper.like("pfcontractname", query)
            );
		 }
	}
}



