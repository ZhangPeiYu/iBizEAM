package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.SEQUENCE;
import cn.ibizlab.eam.core.eam_core.filter.SEQUENCESearchContext;
import cn.ibizlab.eam.core.eam_core.service.ISEQUENCEService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.SEQUENCEMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[序列号] 服务对象接口实现
 */
@Slf4j
@Service("SEQUENCEServiceImpl")
public class SEQUENCEServiceImpl extends ServiceImpl<SEQUENCEMapper, SEQUENCE> implements ISEQUENCEService {


    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(SEQUENCE et) {
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getSequenceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<SEQUENCE> list) {
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(SEQUENCE et) {
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("sequenceid", et.getSequenceid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getSequenceid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<SEQUENCE> list) {
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public SEQUENCE get(String key) {
        SEQUENCE et = getById(key);
        if(et == null){
            et = new SEQUENCE();
            et.setSequenceid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public SEQUENCE getDraft(SEQUENCE et) {
        return et;
    }

    @Override
    public boolean checkKey(SEQUENCE et) {
        return (!ObjectUtils.isEmpty(et.getSequenceid())) && (!Objects.isNull(this.getById(et.getSequenceid())));
    }
    @Override
    @Transactional
    public boolean save(SEQUENCE et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(SEQUENCE et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<SEQUENCE> list) {
        List<SEQUENCE> create = new ArrayList<>();
        List<SEQUENCE> update = new ArrayList<>();
        for (SEQUENCE et : list) {
            if (ObjectUtils.isEmpty(et.getSequenceid()) || ObjectUtils.isEmpty(getById(et.getSequenceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<SEQUENCE> list) {
        List<SEQUENCE> create = new ArrayList<>();
        List<SEQUENCE> update = new ArrayList<>();
        for (SEQUENCE et : list) {
            if (ObjectUtils.isEmpty(et.getSequenceid()) || ObjectUtils.isEmpty(getById(et.getSequenceid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }



    /**
     * 查询集合 数据集
     */
    @Override
    public Page<SEQUENCE> searchDefault(SEQUENCESearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<SEQUENCE> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<SEQUENCE>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }







    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<SEQUENCE> getSequenceByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<SEQUENCE> getSequenceByEntities(List<SEQUENCE> entities) {
        List ids =new ArrayList();
        for(SEQUENCE entity : entities){
            Serializable id=entity.getSequenceid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public ISEQUENCEService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



