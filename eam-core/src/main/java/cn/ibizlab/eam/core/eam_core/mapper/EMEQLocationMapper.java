package cn.ibizlab.eam.core.eam_core.mapper;

import java.util.List;
import org.apache.ibatis.annotations.*;
import java.util.Map;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import java.util.Map;
import org.apache.ibatis.annotations.Select;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLocation;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLocationSearchContext;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import java.io.Serializable;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.alibaba.fastjson.JSONObject;

public interface EMEQLocationMapper extends BaseMapper<EMEQLocation> {

    Page<EMEQLocation> searchDefault(IPage page, @Param("srf") EMEQLocationSearchContext context, @Param("ew") Wrapper<EMEQLocation> wrapper);
    Page<EMEQLocation> searchSub(IPage page, @Param("srf") EMEQLocationSearchContext context, @Param("ew") Wrapper<EMEQLocation> wrapper);
    @Override
    EMEQLocation selectById(Serializable id);
    @Override
    int insert(EMEQLocation entity);
    @Override
    int updateById(@Param(Constants.ENTITY) EMEQLocation entity);
    @Override
    int update(@Param(Constants.ENTITY) EMEQLocation entity, @Param("ew") Wrapper<EMEQLocation> updateWrapper);
    @Override
    int deleteById(Serializable id);
    /**
    * 自定义查询SQL
    * @param sql
    * @return
    */
    @Select("${sql}")
    List<JSONObject> selectBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义更新SQL
    * @param sql
    * @return
    */
    @Update("${sql}")
    boolean updateBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义插入SQL
    * @param sql
    * @return
    */
    @Insert("${sql}")
    boolean insertBySQL(@Param("sql") String sql, @Param("et")Map param);

    /**
    * 自定义删除SQL
    * @param sql
    * @return
    */
    @Delete("${sql}")
    boolean deleteBySQL(@Param("sql") String sql, @Param("et")Map param);

    List<EMEQLocation> selectByMajorequipid(@Param("emequipid") Serializable emequipid);

}
