package cn.ibizlab.eam.core.eam_core.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.math.BigInteger;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import cn.ibizlab.eam.util.errors.BadRequestAlertException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.context.annotation.Lazy;
import cn.ibizlab.eam.core.eam_core.domain.EMWO_PT;
import cn.ibizlab.eam.core.eam_core.filter.EMWO_PTSearchContext;
import cn.ibizlab.eam.core.eam_core.service.IEMWO_PTService;

import cn.ibizlab.eam.util.helper.CachedBeanCopier;
import cn.ibizlab.eam.util.helper.DEFieldCacheMap;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ibizlab.eam.core.eam_core.mapper.EMWO_PTMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.StringUtils;

/**
 * 实体[执勤工单] 服务对象接口实现
 */
@Slf4j
@Service("EMWO_PTServiceImpl")
public class EMWO_PTServiceImpl extends ServiceImpl<EMWO_PTMapper, EMWO_PT> implements IEMWO_PTService {

    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMACClassService emacclassService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMEquipService emequipService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMObjectService emobjectService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOACService emrfoacService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService emrfocaService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFODEService emrfodeService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMRFOMOService emrfomoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMServiceService emserviceService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOORIService emwooriService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMWOService emwoService;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_pf.service.IPFTeamService pfteamService;

    protected int batchSize = 500;

    @Override
    @Transactional
    public boolean create(EMWO_PT et) {
        fillParentData(et);
        createIndexMajorEntityData(et);
        if(!this.retBool(this.baseMapper.insert(et))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwoPtid()), et);
        return true;
    }

    @Override
    @Transactional
    public void createBatch(List<EMWO_PT> list) {
        list.forEach(item->fillParentData(item));
        this.saveBatch(list, batchSize);
    }

    @Override
    @Transactional
    public boolean update(EMWO_PT et) {
        fillParentData(et);
        emresrefobjService.update(emwo_ptInheritMapping.toEmresrefobj(et));
        if(!update(et, (Wrapper) et.getUpdateWrapper(true).eq("emwo_ptid", et.getEmwoPtid()))) {
            return false;
        }
        CachedBeanCopier.copy(get(et.getEmwoPtid()), et);
        return true;
    }

    @Override
    @Transactional
    public void updateBatch(List<EMWO_PT> list) {
        list.forEach(item->fillParentData(item));
        updateBatchById(list, batchSize);
    }

    @Override
    @Transactional
    public boolean remove(String key) {
        emresrefobjService.remove(key);
        boolean result = removeById(key);
        return result ;
    }

    @Override
    @Transactional
    public void removeBatch(Collection<String> idList) {
        removeByIds(idList);
    }

    @Override
    @Transactional
    public EMWO_PT get(String key) {
        EMWO_PT et = getById(key);
        if(et == null){
            et = new EMWO_PT();
            et.setEmwoPtid(key);
        }
        else {
        }
        return et;
    }

    @Override
    public EMWO_PT getDraft(EMWO_PT et) {
        fillParentData(et);
        return et;
    }

    @Override
    public boolean checkKey(EMWO_PT et) {
        return (!ObjectUtils.isEmpty(et.getEmwoPtid())) && (!Objects.isNull(this.getById(et.getEmwoPtid())));
    }
    @Override
    @Transactional
    public boolean save(EMWO_PT et) {
        if(!saveOrUpdate(et)) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional
    public boolean saveOrUpdate(EMWO_PT et) {
        if (null == et) {
            return false;
        } else {
            return checkKey(et) ? getProxyService().update(et) : getProxyService().create(et);
        }
    }

    @Override
    @Transactional
    public boolean saveBatch(Collection<EMWO_PT> list) {
        list.forEach(item->fillParentData(item));
        List<EMWO_PT> create = new ArrayList<>();
        List<EMWO_PT> update = new ArrayList<>();
        for (EMWO_PT et : list) {
            if (ObjectUtils.isEmpty(et.getEmwoPtid()) || ObjectUtils.isEmpty(getById(et.getEmwoPtid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
        return true;
    }

    @Override
    @Transactional
    public void saveBatch(List<EMWO_PT> list) {
        list.forEach(item->fillParentData(item));
        List<EMWO_PT> create = new ArrayList<>();
        List<EMWO_PT> update = new ArrayList<>();
        for (EMWO_PT et : list) {
            if (ObjectUtils.isEmpty(et.getEmwoPtid()) || ObjectUtils.isEmpty(getById(et.getEmwoPtid()))) {
                create.add(et);
            } else {
                update.add(et);
            }
        }
        if (create.size() > 0) {
            getProxyService().createBatch(create);
        }
        if (update.size() > 0) {
            getProxyService().updateBatch(update);
        }
    }


	@Override
    public List<EMWO_PT> selectByAcclassid(String emacclassid) {
        return baseMapper.selectByAcclassid(emacclassid);
    }
    @Override
    public void removeByAcclassid(String emacclassid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("acclassid",emacclassid));
    }

	@Override
    public List<EMWO_PT> selectByEquipid(String emequipid) {
        return baseMapper.selectByEquipid(emequipid);
    }
    @Override
    public void removeByEquipid(String emequipid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("equipid",emequipid));
    }

	@Override
    public List<EMWO_PT> selectByDpid(String emobjectid) {
        return baseMapper.selectByDpid(emobjectid);
    }
    @Override
    public void removeByDpid(String emobjectid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("dpid",emobjectid));
    }

	@Override
    public List<EMWO_PT> selectByObjid(String emobjectid) {
        return baseMapper.selectByObjid(emobjectid);
    }
    @Override
    public void removeByObjid(String emobjectid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("objid",emobjectid));
    }

	@Override
    public List<EMWO_PT> selectByRfoacid(String emrfoacid) {
        return baseMapper.selectByRfoacid(emrfoacid);
    }
    @Override
    public void removeByRfoacid(String emrfoacid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rfoacid",emrfoacid));
    }

	@Override
    public List<EMWO_PT> selectByRfocaid(String emrfocaid) {
        return baseMapper.selectByRfocaid(emrfocaid);
    }
    @Override
    public void removeByRfocaid(String emrfocaid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rfocaid",emrfocaid));
    }

	@Override
    public List<EMWO_PT> selectByRfodeid(String emrfodeid) {
        return baseMapper.selectByRfodeid(emrfodeid);
    }
    @Override
    public void removeByRfodeid(String emrfodeid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rfodeid",emrfodeid));
    }

	@Override
    public List<EMWO_PT> selectByRfomoid(String emrfomoid) {
        return baseMapper.selectByRfomoid(emrfomoid);
    }
    @Override
    public void removeByRfomoid(String emrfomoid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rfomoid",emrfomoid));
    }

	@Override
    public List<EMWO_PT> selectByRserviceid(String emserviceid) {
        return baseMapper.selectByRserviceid(emserviceid);
    }
    @Override
    public void removeByRserviceid(String emserviceid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rserviceid",emserviceid));
    }

	@Override
    public List<EMWO_PT> selectByWooriid(String emwooriid) {
        return baseMapper.selectByWooriid(emwooriid);
    }
    @Override
    public void removeByWooriid(String emwooriid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("wooriid",emwooriid));
    }

	@Override
    public List<EMWO_PT> selectByWopid(String emwoid) {
        return baseMapper.selectByWopid(emwoid);
    }
    @Override
    public void removeByWopid(String emwoid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("wopid",emwoid));
    }

	@Override
    public List<EMWO_PT> selectByRteamid(String pfteamid) {
        return baseMapper.selectByRteamid(pfteamid);
    }
    @Override
    public void removeByRteamid(String pfteamid) {
        this.remove(new QueryWrapper<EMWO_PT>().eq("rteamid",pfteamid));
    }


    /**
     * 查询集合 DEFAULT
     */
    @Override
    public Page<EMWO_PT> searchDefault(EMWO_PTSearchContext context) {
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<EMWO_PT> pages=baseMapper.searchDefault(context.getPages(),context,context.getSelectCond());
        return new PageImpl<EMWO_PT>(pages.getRecords(), context.getPageable(), pages.getTotal());
    }



    /**
     * 为当前实体填充父数据（外键值文本、外键值附加数据）
     * @param et
     */
    private void fillParentData(EMWO_PT et){
        //实体关系[DER1N_EMWO_PT_EMACCLASS_ACCLASSID]
        if(!ObjectUtils.isEmpty(et.getAcclassid())){
            cn.ibizlab.eam.core.eam_core.domain.EMACClass acclass=et.getAcclass();
            if(ObjectUtils.isEmpty(acclass)){
                cn.ibizlab.eam.core.eam_core.domain.EMACClass majorEntity=emacclassService.get(et.getAcclassid());
                et.setAcclass(majorEntity);
                acclass=majorEntity;
            }
            et.setAcclassname(acclass.getEmacclassname());
        }
        //实体关系[DER1N_EMWO_PT_EMEQUIP_EQUIPID]
        if(!ObjectUtils.isEmpty(et.getEquipid())){
            cn.ibizlab.eam.core.eam_core.domain.EMEquip equip=et.getEquip();
            if(ObjectUtils.isEmpty(equip)){
                cn.ibizlab.eam.core.eam_core.domain.EMEquip majorEntity=emequipService.get(et.getEquipid());
                et.setEquip(majorEntity);
                equip=majorEntity;
            }
            et.setEquipname(equip.getEquipinfo());
        }
        //实体关系[DER1N_EMWO_PT_EMOBJECT_DPID]
        if(!ObjectUtils.isEmpty(et.getDpid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject dp=et.getDp();
            if(ObjectUtils.isEmpty(dp)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getDpid());
                et.setDp(majorEntity);
                dp=majorEntity;
            }
            et.setDptype(dp.getEmobjecttype());
            et.setDpname(dp.getEmobjectname());
        }
        //实体关系[DER1N_EMWO_PT_EMOBJECT_OBJID]
        if(!ObjectUtils.isEmpty(et.getObjid())){
            cn.ibizlab.eam.core.eam_core.domain.EMObject obj=et.getObj();
            if(ObjectUtils.isEmpty(obj)){
                cn.ibizlab.eam.core.eam_core.domain.EMObject majorEntity=emobjectService.get(et.getObjid());
                et.setObj(majorEntity);
                obj=majorEntity;
            }
            et.setObjname(obj.getEmobjectname());
        }
        //实体关系[DER1N_EMWO_PT_EMRFOAC_RFOACID]
        if(!ObjectUtils.isEmpty(et.getRfoacid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOAC rfoac=et.getRfoac();
            if(ObjectUtils.isEmpty(rfoac)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOAC majorEntity=emrfoacService.get(et.getRfoacid());
                et.setRfoac(majorEntity);
                rfoac=majorEntity;
            }
            et.setRfoacname(rfoac.getEmrfoacname());
        }
        //实体关系[DER1N_EMWO_PT_EMRFOCA_RFOCAID]
        if(!ObjectUtils.isEmpty(et.getRfocaid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOCA rfoca=et.getRfoca();
            if(ObjectUtils.isEmpty(rfoca)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOCA majorEntity=emrfocaService.get(et.getRfocaid());
                et.setRfoca(majorEntity);
                rfoca=majorEntity;
            }
            et.setRfocaname(rfoca.getEmrfocaname());
        }
        //实体关系[DER1N_EMWO_PT_EMRFODE_RFODEID]
        if(!ObjectUtils.isEmpty(et.getRfodeid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFODE rfode=et.getRfode();
            if(ObjectUtils.isEmpty(rfode)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFODE majorEntity=emrfodeService.get(et.getRfodeid());
                et.setRfode(majorEntity);
                rfode=majorEntity;
            }
            et.setRfodename(rfode.getEmrfodename());
        }
        //实体关系[DER1N_EMWO_PT_EMRFOMO_RFOMOID]
        if(!ObjectUtils.isEmpty(et.getRfomoid())){
            cn.ibizlab.eam.core.eam_core.domain.EMRFOMO rfomo=et.getRfomo();
            if(ObjectUtils.isEmpty(rfomo)){
                cn.ibizlab.eam.core.eam_core.domain.EMRFOMO majorEntity=emrfomoService.get(et.getRfomoid());
                et.setRfomo(majorEntity);
                rfomo=majorEntity;
            }
            et.setRfomoname(rfomo.getEmrfomoname());
        }
        //实体关系[DER1N_EMWO_PT_EMSERVICE_RSERVICEID]
        if(!ObjectUtils.isEmpty(et.getRserviceid())){
            cn.ibizlab.eam.core.eam_core.domain.EMService rservice=et.getRservice();
            if(ObjectUtils.isEmpty(rservice)){
                cn.ibizlab.eam.core.eam_core.domain.EMService majorEntity=emserviceService.get(et.getRserviceid());
                et.setRservice(majorEntity);
                rservice=majorEntity;
            }
            et.setRservicename(rservice.getEmservicename());
        }
        //实体关系[DER1N_EMWO_PT_EMWOORI_WOORIID]
        if(!ObjectUtils.isEmpty(et.getWooriid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWOORI woori=et.getWoori();
            if(ObjectUtils.isEmpty(woori)){
                cn.ibizlab.eam.core.eam_core.domain.EMWOORI majorEntity=emwooriService.get(et.getWooriid());
                et.setWoori(majorEntity);
                woori=majorEntity;
            }
            et.setWooriname(woori.getEmwooriname());
            et.setWooritype(woori.getEmwooritype());
        }
        //实体关系[DER1N_EMWO_PT_EMWO_WOPID]
        if(!ObjectUtils.isEmpty(et.getWopid())){
            cn.ibizlab.eam.core.eam_core.domain.EMWO wop=et.getWop();
            if(ObjectUtils.isEmpty(wop)){
                cn.ibizlab.eam.core.eam_core.domain.EMWO majorEntity=emwoService.get(et.getWopid());
                et.setWop(majorEntity);
                wop=majorEntity;
            }
            et.setWopname(wop.getEmwoname());
        }
        //实体关系[DER1N_EMWO_PT_PFTEAM_RTEAMID]
        if(!ObjectUtils.isEmpty(et.getRteamid())){
            cn.ibizlab.eam.core.eam_pf.domain.PFTeam rteam=et.getRteam();
            if(ObjectUtils.isEmpty(rteam)){
                cn.ibizlab.eam.core.eam_pf.domain.PFTeam majorEntity=pfteamService.get(et.getRteamid());
                et.setRteam(majorEntity);
                rteam=majorEntity;
            }
            et.setRteamname(rteam.getPfteamname());
        }
    }



    @Autowired
    cn.ibizlab.eam.core.eam_core.mapping.EMWO_PTInheritMapping emwo_ptInheritMapping;
    @Autowired
    @Lazy
    protected cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService emresrefobjService;

    /**
     * 创建索引主实体数据
     * @param et
     */
    private void createIndexMajorEntityData(EMWO_PT et){
        if(ObjectUtils.isEmpty(et.getEmwoPtid()))
            et.setEmwoPtid((String)et.getDefaultKey(true));
        cn.ibizlab.eam.core.eam_core.domain.EMResRefObj emresrefobj =emwo_ptInheritMapping.toEmresrefobj(et);
        emresrefobj.set("emresrefobjtype","WO_PT");
        emresrefobjService.create(emresrefobj);
    }

    @Override
    public List<JSONObject> select(String sql, Map param){
        return this.baseMapper.selectBySQL(sql,param);
    }

    @Override
    @Transactional
    public boolean execute(String sql , Map param){
        if (sql == null || sql.isEmpty()) {
            return false;
        }
        if (sql.toLowerCase().trim().startsWith("insert")) {
            return this.baseMapper.insertBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("update")) {
            return this.baseMapper.updateBySQL(sql,param);
        }
        if (sql.toLowerCase().trim().startsWith("delete")) {
            return this.baseMapper.deleteBySQL(sql,param);
        }
        log.warn("暂未支持的SQL语法");
        return true;
    }

    @Override
    public List<EMWO_PT> getEmwoPtByIds(List<String> ids) {
         return this.listByIds(ids);
    }

    @Override
    public List<EMWO_PT> getEmwoPtByEntities(List<EMWO_PT> entities) {
        List ids =new ArrayList();
        for(EMWO_PT entity : entities){
            Serializable id=entity.getEmwoPtid();
            if(!ObjectUtils.isEmpty(id)){
                ids.add(id);
            }
        }
        if(ids.size()>0) {
            return this.listByIds(ids);
        }
        else {
            return entities;
        }
    }


    public IEMWO_PTService getProxyService() {
        return cn.ibizlab.eam.util.security.SpringContextHolder.getBean(this.getClass());
    }
}



