package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEICellReturn;
import cn.ibizlab.eam.core.eam_core.service.IEMEICellReturnService;
import cn.ibizlab.eam.core.eam_core.filter.EMEICellReturnSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对讲机归还记录" })
@RestController("WebApi-emeicellreturn")
@RequestMapping("")
public class EMEICellReturnResource {

    @Autowired
    public IEMEICellReturnService emeicellreturnService;

    @Autowired
    @Lazy
    public EMEICellReturnMapping emeicellreturnMapping;

    @PreAuthorize("hasPermission(this.emeicellreturnMapping.toDomain(#emeicellreturndto),'eam-EMEICellReturn-Create')")
    @ApiOperation(value = "新建对讲机归还记录", tags = {"对讲机归还记录" },  notes = "新建对讲机归还记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellreturns")
    public ResponseEntity<EMEICellReturnDTO> create(@Validated @RequestBody EMEICellReturnDTO emeicellreturndto) {
        EMEICellReturn domain = emeicellreturnMapping.toDomain(emeicellreturndto);
		emeicellreturnService.create(domain);
        EMEICellReturnDTO dto = emeicellreturnMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellreturnMapping.toDomain(#emeicellreturndtos),'eam-EMEICellReturn-Create')")
    @ApiOperation(value = "批量新建对讲机归还记录", tags = {"对讲机归还记录" },  notes = "批量新建对讲机归还记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellreturns/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEICellReturnDTO> emeicellreturndtos) {
        emeicellreturnService.createBatch(emeicellreturnMapping.toDomain(emeicellreturndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeicellreturn" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeicellreturnService.get(#emeicellreturn_id),'eam-EMEICellReturn-Update')")
    @ApiOperation(value = "更新对讲机归还记录", tags = {"对讲机归还记录" },  notes = "更新对讲机归还记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellreturns/{emeicellreturn_id}")
    public ResponseEntity<EMEICellReturnDTO> update(@PathVariable("emeicellreturn_id") String emeicellreturn_id, @RequestBody EMEICellReturnDTO emeicellreturndto) {
		EMEICellReturn domain  = emeicellreturnMapping.toDomain(emeicellreturndto);
        domain .setEmeicellreturnid(emeicellreturn_id);
		emeicellreturnService.update(domain );
		EMEICellReturnDTO dto = emeicellreturnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeicellreturnService.getEmeicellreturnByEntities(this.emeicellreturnMapping.toDomain(#emeicellreturndtos)),'eam-EMEICellReturn-Update')")
    @ApiOperation(value = "批量更新对讲机归还记录", tags = {"对讲机归还记录" },  notes = "批量更新对讲机归还记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeicellreturns/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEICellReturnDTO> emeicellreturndtos) {
        emeicellreturnService.updateBatch(emeicellreturnMapping.toDomain(emeicellreturndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeicellreturnService.get(#emeicellreturn_id),'eam-EMEICellReturn-Remove')")
    @ApiOperation(value = "删除对讲机归还记录", tags = {"对讲机归还记录" },  notes = "删除对讲机归还记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellreturns/{emeicellreturn_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeicellreturn_id") String emeicellreturn_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeicellreturnService.remove(emeicellreturn_id));
    }

    @PreAuthorize("hasPermission(this.emeicellreturnService.getEmeicellreturnByIds(#ids),'eam-EMEICellReturn-Remove')")
    @ApiOperation(value = "批量删除对讲机归还记录", tags = {"对讲机归还记录" },  notes = "批量删除对讲机归还记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeicellreturns/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeicellreturnService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeicellreturnMapping.toDomain(returnObject.body),'eam-EMEICellReturn-Get')")
    @ApiOperation(value = "获取对讲机归还记录", tags = {"对讲机归还记录" },  notes = "获取对讲机归还记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellreturns/{emeicellreturn_id}")
    public ResponseEntity<EMEICellReturnDTO> get(@PathVariable("emeicellreturn_id") String emeicellreturn_id) {
        EMEICellReturn domain = emeicellreturnService.get(emeicellreturn_id);
        EMEICellReturnDTO dto = emeicellreturnMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对讲机归还记录草稿", tags = {"对讲机归还记录" },  notes = "获取对讲机归还记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeicellreturns/getdraft")
    public ResponseEntity<EMEICellReturnDTO> getDraft(EMEICellReturnDTO dto) {
        EMEICellReturn domain = emeicellreturnMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeicellreturnMapping.toDto(emeicellreturnService.getDraft(domain)));
    }

    @ApiOperation(value = "检查对讲机归还记录", tags = {"对讲机归还记录" },  notes = "检查对讲机归还记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellreturns/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEICellReturnDTO emeicellreturndto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeicellreturnService.checkKey(emeicellreturnMapping.toDomain(emeicellreturndto)));
    }

    @PreAuthorize("hasPermission(this.emeicellreturnMapping.toDomain(#emeicellreturndto),'eam-EMEICellReturn-Save')")
    @ApiOperation(value = "保存对讲机归还记录", tags = {"对讲机归还记录" },  notes = "保存对讲机归还记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellreturns/save")
    public ResponseEntity<EMEICellReturnDTO> save(@RequestBody EMEICellReturnDTO emeicellreturndto) {
        EMEICellReturn domain = emeicellreturnMapping.toDomain(emeicellreturndto);
        emeicellreturnService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeicellreturnMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeicellreturnMapping.toDomain(#emeicellreturndtos),'eam-EMEICellReturn-Save')")
    @ApiOperation(value = "批量保存对讲机归还记录", tags = {"对讲机归还记录" },  notes = "批量保存对讲机归还记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeicellreturns/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEICellReturnDTO> emeicellreturndtos) {
        emeicellreturnService.saveBatch(emeicellreturnMapping.toDomain(emeicellreturndtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellReturn-searchDefault-all') and hasPermission(#context,'eam-EMEICellReturn-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对讲机归还记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeicellreturns/fetchdefault")
	public ResponseEntity<List<EMEICellReturnDTO>> fetchDefault(EMEICellReturnSearchContext context) {
        Page<EMEICellReturn> domains = emeicellreturnService.searchDefault(context) ;
        List<EMEICellReturnDTO> list = emeicellreturnMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEICellReturn-searchDefault-all') and hasPermission(#context,'eam-EMEICellReturn-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对讲机归还记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeicellreturns/searchdefault")
	public ResponseEntity<Page<EMEICellReturnDTO>> searchDefault(@RequestBody EMEICellReturnSearchContext context) {
        Page<EMEICellReturn> domains = emeicellreturnService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeicellreturnMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

