package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItemType;
import cn.ibizlab.eam.core.eam_core.service.IEMItemTypeService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemTypeSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物品类型" })
@RestController("WebApi-emitemtype")
@RequestMapping("")
public class EMItemTypeResource {

    @Autowired
    public IEMItemTypeService emitemtypeService;

    @Autowired
    @Lazy
    public EMItemTypeMapping emitemtypeMapping;

    @PreAuthorize("hasPermission(this.emitemtypeMapping.toDomain(#emitemtypedto),'eam-EMItemType-Create')")
    @ApiOperation(value = "新建物品类型", tags = {"物品类型" },  notes = "新建物品类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtypes")
    public ResponseEntity<EMItemTypeDTO> create(@Validated @RequestBody EMItemTypeDTO emitemtypedto) {
        EMItemType domain = emitemtypeMapping.toDomain(emitemtypedto);
		emitemtypeService.create(domain);
        EMItemTypeDTO dto = emitemtypeMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtypeMapping.toDomain(#emitemtypedtos),'eam-EMItemType-Create')")
    @ApiOperation(value = "批量新建物品类型", tags = {"物品类型" },  notes = "批量新建物品类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtypes/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemTypeDTO> emitemtypedtos) {
        emitemtypeService.createBatch(emitemtypeMapping.toDomain(emitemtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitemtype" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemtypeService.get(#emitemtype_id),'eam-EMItemType-Update')")
    @ApiOperation(value = "更新物品类型", tags = {"物品类型" },  notes = "更新物品类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemtypes/{emitemtype_id}")
    public ResponseEntity<EMItemTypeDTO> update(@PathVariable("emitemtype_id") String emitemtype_id, @RequestBody EMItemTypeDTO emitemtypedto) {
		EMItemType domain  = emitemtypeMapping.toDomain(emitemtypedto);
        domain .setEmitemtypeid(emitemtype_id);
		emitemtypeService.update(domain );
		EMItemTypeDTO dto = emitemtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemtypeService.getEmitemtypeByEntities(this.emitemtypeMapping.toDomain(#emitemtypedtos)),'eam-EMItemType-Update')")
    @ApiOperation(value = "批量更新物品类型", tags = {"物品类型" },  notes = "批量更新物品类型")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitemtypes/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemTypeDTO> emitemtypedtos) {
        emitemtypeService.updateBatch(emitemtypeMapping.toDomain(emitemtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemtypeService.get(#emitemtype_id),'eam-EMItemType-Remove')")
    @ApiOperation(value = "删除物品类型", tags = {"物品类型" },  notes = "删除物品类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemtypes/{emitemtype_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitemtype_id") String emitemtype_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemtypeService.remove(emitemtype_id));
    }

    @PreAuthorize("hasPermission(this.emitemtypeService.getEmitemtypeByIds(#ids),'eam-EMItemType-Remove')")
    @ApiOperation(value = "批量删除物品类型", tags = {"物品类型" },  notes = "批量删除物品类型")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitemtypes/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemtypeService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemtypeMapping.toDomain(returnObject.body),'eam-EMItemType-Get')")
    @ApiOperation(value = "获取物品类型", tags = {"物品类型" },  notes = "获取物品类型")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemtypes/{emitemtype_id}")
    public ResponseEntity<EMItemTypeDTO> get(@PathVariable("emitemtype_id") String emitemtype_id) {
        EMItemType domain = emitemtypeService.get(emitemtype_id);
        EMItemTypeDTO dto = emitemtypeMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物品类型草稿", tags = {"物品类型" },  notes = "获取物品类型草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitemtypes/getdraft")
    public ResponseEntity<EMItemTypeDTO> getDraft(EMItemTypeDTO dto) {
        EMItemType domain = emitemtypeMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtypeMapping.toDto(emitemtypeService.getDraft(domain)));
    }

    @ApiOperation(value = "检查物品类型", tags = {"物品类型" },  notes = "检查物品类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtypes/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemTypeDTO emitemtypedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemtypeService.checkKey(emitemtypeMapping.toDomain(emitemtypedto)));
    }

    @PreAuthorize("hasPermission(this.emitemtypeMapping.toDomain(#emitemtypedto),'eam-EMItemType-Save')")
    @ApiOperation(value = "保存物品类型", tags = {"物品类型" },  notes = "保存物品类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtypes/save")
    public ResponseEntity<EMItemTypeDTO> save(@RequestBody EMItemTypeDTO emitemtypedto) {
        EMItemType domain = emitemtypeMapping.toDomain(emitemtypedto);
        emitemtypeService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemtypeMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemtypeMapping.toDomain(#emitemtypedtos),'eam-EMItemType-Save')")
    @ApiOperation(value = "批量保存物品类型", tags = {"物品类型" },  notes = "批量保存物品类型")
	@RequestMapping(method = RequestMethod.POST, value = "/emitemtypes/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemTypeDTO> emitemtypedtos) {
        emitemtypeService.saveBatch(emitemtypeMapping.toDomain(emitemtypedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchChildAll-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "获取ChildAll", tags = {"物品类型" } ,notes = "获取ChildAll")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtypes/fetchchildall")
	public ResponseEntity<List<EMItemTypeDTO>> fetchChildAll(EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchChildAll(context) ;
        List<EMItemTypeDTO> list = emitemtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchChildAll-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "查询ChildAll", tags = {"物品类型" } ,notes = "查询ChildAll")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtypes/searchchildall")
	public ResponseEntity<Page<EMItemTypeDTO>> searchChildAll(@RequestBody EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchChildAll(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchDefault-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"物品类型" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtypes/fetchdefault")
	public ResponseEntity<List<EMItemTypeDTO>> fetchDefault(EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchDefault(context) ;
        List<EMItemTypeDTO> list = emitemtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchDefault-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"物品类型" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtypes/searchdefault")
	public ResponseEntity<Page<EMItemTypeDTO>> searchDefault(@RequestBody EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchRoot-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "获取上级类型查询", tags = {"物品类型" } ,notes = "获取上级类型查询")
    @RequestMapping(method= RequestMethod.GET , value="/emitemtypes/fetchroot")
	public ResponseEntity<List<EMItemTypeDTO>> fetchRoot(EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchRoot(context) ;
        List<EMItemTypeDTO> list = emitemtypeMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItemType-searchRoot-all') and hasPermission(#context,'eam-EMItemType-Get')")
	@ApiOperation(value = "查询上级类型查询", tags = {"物品类型" } ,notes = "查询上级类型查询")
    @RequestMapping(method= RequestMethod.POST , value="/emitemtypes/searchroot")
	public ResponseEntity<Page<EMItemTypeDTO>> searchRoot(@RequestBody EMItemTypeSearchContext context) {
        Page<EMItemType> domains = emitemtypeService.searchRoot(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemtypeMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

