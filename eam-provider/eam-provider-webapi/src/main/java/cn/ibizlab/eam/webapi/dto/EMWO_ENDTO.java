package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMWO_ENDTO]
 */
@Data
@ApiModel("能耗登记工单")
public class EMWO_ENDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [WOTEAM]
     *
     */
    @JSONField(name = "woteam")
    @JsonProperty("woteam")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单组")
    private String woteam;

    /**
     * 属性 [WFSTATE]
     *
     */
    @JSONField(name = "wfstate")
    @JsonProperty("wfstate")
    @ApiModelProperty("工作流状态")
    private Integer wfstate;

    /**
     * 属性 [REGIONBEGINDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionbegindate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionbegindate")
    @ApiModelProperty("实际抄表时间")
    private Timestamp regionbegindate;

    /**
     * 属性 [EXPIREDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "expiredate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("expiredate")
    @ApiModelProperty("过期日期")
    private Timestamp expiredate;

    /**
     * 属性 [WFSTEP]
     *
     */
    @JSONField(name = "wfstep")
    @JsonProperty("wfstep")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("流程步骤")
    private String wfstep;

    /**
     * 属性 [PREFEE]
     *
     */
    @JSONField(name = "prefee")
    @JsonProperty("prefee")
    @ApiModelProperty("预算(￥)")
    private Double prefee;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [CONTENT]
     *
     */
    @JSONField(name = "content")
    @JsonProperty("content")
    @Size(min = 0, max = 1048576, message = "内容长度必须小于等于[1048576]")
    @ApiModelProperty("详细内容")
    private String content;

    /**
     * 属性 [PRIORITY]
     *
     */
    @JSONField(name = "priority")
    @JsonProperty("priority")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("优先级")
    private String priority;

    /**
     * 属性 [MDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "mdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("mdate")
    @ApiModelProperty("制定时间")
    private Timestamp mdate;

    /**
     * 属性 [EMWO_ENNAME]
     *
     */
    @JSONField(name = "emwo_enname")
    @JsonProperty("emwo_enname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单名称")
    private String emwoEnname;

    /**
     * 属性 [VAL]
     *
     */
    @JSONField(name = "val")
    @JsonProperty("val")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("值")
    private String val;

    /**
     * 属性 [REGIONENDDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "regionenddate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("regionenddate")
    @ApiModelProperty("结束时间")
    private Timestamp regionenddate;

    /**
     * 属性 [WODATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "wodate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("wodate")
    @NotNull(message = "[执行日期]不允许为空!")
    @ApiModelProperty("执行日期")
    private Timestamp wodate;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [ACTIVELENGTHS]
     *
     */
    @JSONField(name = "activelengths")
    @JsonProperty("activelengths")
    @ApiModelProperty("持续时间(H)")
    private Double activelengths;

    /**
     * 属性 [VRATE]
     *
     */
    @JSONField(name = "vrate")
    @JsonProperty("vrate")
    @ApiModelProperty("倍率")
    private Double vrate;

    /**
     * 属性 [CURVAL]
     *
     */
    @JSONField(name = "curval")
    @JsonProperty("curval")
    @ApiModelProperty("本次记录值")
    private Double curval;

    /**
     * 属性 [WOSTATE]
     *
     */
    @JSONField(name = "wostate")
    @JsonProperty("wostate")
    @ApiModelProperty("工单状态")
    private Integer wostate;

    /**
     * 属性 [WORKLENGTH]
     *
     */
    @JSONField(name = "worklength")
    @JsonProperty("worklength")
    @ApiModelProperty("实际工时(分)")
    private Double worklength;

    /**
     * 属性 [WOGROUP]
     *
     */
    @JSONField(name = "wogroup")
    @JsonProperty("wogroup")
    @NotBlank(message = "[工单分组]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("工单分组")
    private String wogroup;

    /**
     * 属性 [EMWOTYPE]
     *
     */
    @JSONField(name = "emwotype")
    @JsonProperty("emwotype")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单分组")
    private String emwotype;

    /**
     * 属性 [EMWO_ENID]
     *
     */
    @JSONField(name = "emwo_enid")
    @JsonProperty("emwo_enid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("工单编号")
    private String emwoEnid;

    /**
     * 属性 [LASTVAL]
     *
     */
    @JSONField(name = "lastval")
    @JsonProperty("lastval")
    @ApiModelProperty("上次记录值")
    private Double lastval;

    /**
     * 属性 [WFINSTANCEID]
     *
     */
    @JSONField(name = "wfinstanceid")
    @JsonProperty("wfinstanceid")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工作流实例")
    private String wfinstanceid;

    /**
     * 属性 [WOTEAM_SHOW]
     *
     */
    @JSONField(name = "woteam_show")
    @JsonProperty("woteam_show")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单组")
    private String woteamShow;

    /**
     * 属性 [BDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    @ApiModelProperty("上次采集时间")
    private Timestamp bdate;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [WOTYPE]
     *
     */
    @JSONField(name = "wotype")
    @JsonProperty("wotype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("工单类型")
    private String wotype;

    /**
     * 属性 [EQSTOPLENGTH]
     *
     */
    @JSONField(name = "eqstoplength")
    @JsonProperty("eqstoplength")
    @ApiModelProperty("停运时间(分)")
    private Double eqstoplength;

    /**
     * 属性 [WRESULT]
     *
     */
    @JSONField(name = "wresult")
    @JsonProperty("wresult")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("执行结果")
    private String wresult;

    /**
     * 属性 [NVAL]
     *
     */
    @JSONField(name = "nval")
    @JsonProperty("nval")
    @ApiModelProperty("能耗值")
    private Double nval;

    /**
     * 属性 [ARCHIVE]
     *
     */
    @JSONField(name = "archive")
    @JsonProperty("archive")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("归档")
    private String archive;

    /**
     * 属性 [WODESC]
     *
     */
    @JSONField(name = "wodesc")
    @JsonProperty("wodesc")
    @Size(min = 0, max = 1000, message = "内容长度必须小于等于[1000]")
    @ApiModelProperty("工单内容")
    private String wodesc;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [RFOMONAME]
     *
     */
    @JSONField(name = "rfomoname")
    @JsonProperty("rfomoname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("模式")
    private String rfomoname;

    /**
     * 属性 [RFOACNAME]
     *
     */
    @JSONField(name = "rfoacname")
    @JsonProperty("rfoacname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("方案")
    private String rfoacname;

    /**
     * 属性 [RTEAMNAME]
     *
     */
    @JSONField(name = "rteamname")
    @JsonProperty("rteamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任班组")
    private String rteamname;

    /**
     * 属性 [RFOCANAME]
     *
     */
    @JSONField(name = "rfocaname")
    @JsonProperty("rfocaname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("原因")
    private String rfocaname;

    /**
     * 属性 [RSERVICENAME]
     *
     */
    @JSONField(name = "rservicename")
    @JsonProperty("rservicename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("服务商")
    private String rservicename;

    /**
     * 属性 [DPTYPE]
     *
     */
    @JSONField(name = "dptype")
    @JsonProperty("dptype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("测点类型")
    private String dptype;

    /**
     * 属性 [OBJNAME]
     *
     */
    @JSONField(name = "objname")
    @JsonProperty("objname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("位置")
    private String objname;

    /**
     * 属性 [WOPNAME_SHOW]
     *
     */
    @JSONField(name = "wopname_show")
    @JsonProperty("wopname_show")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("上级工单")
    private String wopnameShow;

    /**
     * 属性 [WOPNAME]
     *
     */
    @JSONField(name = "wopname")
    @JsonProperty("wopname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("上级工单")
    private String wopname;

    /**
     * 属性 [ACCLASSNAME]
     *
     */
    @JSONField(name = "acclassname")
    @JsonProperty("acclassname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("总帐科目")
    private String acclassname;

    /**
     * 属性 [RFODENAME]
     *
     */
    @JSONField(name = "rfodename")
    @JsonProperty("rfodename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("现象")
    private String rfodename;

    /**
     * 属性 [DPNAME]
     *
     */
    @JSONField(name = "dpname")
    @JsonProperty("dpname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("能源")
    private String dpname;

    /**
     * 属性 [WOORINAME]
     *
     */
    @JSONField(name = "wooriname")
    @JsonProperty("wooriname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("工单来源")
    private String wooriname;

    /**
     * 属性 [WOORITYPE]
     *
     */
    @JSONField(name = "wooritype")
    @JsonProperty("wooritype")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("来源类型")
    private String wooritype;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备")
    private String equipname;

    /**
     * 属性 [DPID]
     *
     */
    @JSONField(name = "dpid")
    @JsonProperty("dpid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("能源")
    private String dpid;

    /**
     * 属性 [OBJID]
     *
     */
    @JSONField(name = "objid")
    @JsonProperty("objid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("位置")
    private String objid;

    /**
     * 属性 [RSERVICEID]
     *
     */
    @JSONField(name = "rserviceid")
    @JsonProperty("rserviceid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("服务商")
    private String rserviceid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipid;

    /**
     * 属性 [WOORIID]
     *
     */
    @JSONField(name = "wooriid")
    @JsonProperty("wooriid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("工单来源")
    private String wooriid;

    /**
     * 属性 [ACCLASSID]
     *
     */
    @JSONField(name = "acclassid")
    @JsonProperty("acclassid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("总帐科目")
    private String acclassid;

    /**
     * 属性 [RFOMOID]
     *
     */
    @JSONField(name = "rfomoid")
    @JsonProperty("rfomoid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("模式")
    private String rfomoid;

    /**
     * 属性 [RTEAMID]
     *
     */
    @JSONField(name = "rteamid")
    @JsonProperty("rteamid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任班组")
    private String rteamid;

    /**
     * 属性 [RFOCAID]
     *
     */
    @JSONField(name = "rfocaid")
    @JsonProperty("rfocaid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("原因")
    private String rfocaid;

    /**
     * 属性 [RFODEID]
     *
     */
    @JSONField(name = "rfodeid")
    @JsonProperty("rfodeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("现象")
    private String rfodeid;

    /**
     * 属性 [WOPID]
     *
     */
    @JSONField(name = "wopid")
    @JsonProperty("wopid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("上级工单")
    private String wopid;

    /**
     * 属性 [RFOACID]
     *
     */
    @JSONField(name = "rfoacid")
    @JsonProperty("rfoacid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("方案")
    private String rfoacid;

    /**
     * 属性 [MPERSONID]
     *
     */
    @JSONField(name = "mpersonid")
    @JsonProperty("mpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("制定人")
    private String mpersonid;

    /**
     * 属性 [MPERSONNAME]
     *
     */
    @JSONField(name = "mpersonname")
    @JsonProperty("mpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("制定人")
    private String mpersonname;

    /**
     * 属性 [RDEPTID]
     *
     */
    @JSONField(name = "rdeptid")
    @JsonProperty("rdeptid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任部门")
    private String rdeptid;

    /**
     * 属性 [RDEPTNAME]
     *
     */
    @JSONField(name = "rdeptname")
    @JsonProperty("rdeptname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任部门")
    private String rdeptname;

    /**
     * 属性 [WPERSONID]
     *
     */
    @JSONField(name = "wpersonid")
    @JsonProperty("wpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("抄表人")
    private String wpersonid;

    /**
     * 属性 [WPERSONNAME]
     *
     */
    @JSONField(name = "wpersonname")
    @JsonProperty("wpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("抄表人")
    private String wpersonname;

    /**
     * 属性 [REMPID]
     *
     */
    @JSONField(name = "rempid")
    @JsonProperty("rempid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("责任人")
    private String rempid;

    /**
     * 属性 [REMPNAME]
     *
     */
    @JSONField(name = "rempname")
    @JsonProperty("rempname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("责任人")
    private String rempname;

    /**
     * 属性 [RECVPERSONID]
     *
     */
    @JSONField(name = "recvpersonid")
    @JsonProperty("recvpersonid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("指派抄表人")
    private String recvpersonid;

    /**
     * 属性 [RECVPERSONNAME]
     *
     */
    @JSONField(name = "recvpersonname")
    @JsonProperty("recvpersonname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("指派抄表人")
    private String recvpersonname;


    /**
     * 设置 [WFSTATE]
     */
    public void setWfstate(Integer  wfstate){
        this.wfstate = wfstate ;
        this.modify("wfstate",wfstate);
    }

    /**
     * 设置 [REGIONBEGINDATE]
     */
    public void setRegionbegindate(Timestamp  regionbegindate){
        this.regionbegindate = regionbegindate ;
        this.modify("regionbegindate",regionbegindate);
    }

    /**
     * 设置 [EXPIREDATE]
     */
    public void setExpiredate(Timestamp  expiredate){
        this.expiredate = expiredate ;
        this.modify("expiredate",expiredate);
    }

    /**
     * 设置 [WFSTEP]
     */
    public void setWfstep(String  wfstep){
        this.wfstep = wfstep ;
        this.modify("wfstep",wfstep);
    }

    /**
     * 设置 [PREFEE]
     */
    public void setPrefee(Double  prefee){
        this.prefee = prefee ;
        this.modify("prefee",prefee);
    }

    /**
     * 设置 [CONTENT]
     */
    public void setContent(String  content){
        this.content = content ;
        this.modify("content",content);
    }

    /**
     * 设置 [PRIORITY]
     */
    public void setPriority(String  priority){
        this.priority = priority ;
        this.modify("priority",priority);
    }

    /**
     * 设置 [MDATE]
     */
    public void setMdate(Timestamp  mdate){
        this.mdate = mdate ;
        this.modify("mdate",mdate);
    }

    /**
     * 设置 [EMWO_ENNAME]
     */
    public void setEmwoEnname(String  emwoEnname){
        this.emwoEnname = emwoEnname ;
        this.modify("emwo_enname",emwoEnname);
    }

    /**
     * 设置 [VAL]
     */
    public void setVal(String  val){
        this.val = val ;
        this.modify("val",val);
    }

    /**
     * 设置 [REGIONENDDATE]
     */
    public void setRegionenddate(Timestamp  regionenddate){
        this.regionenddate = regionenddate ;
        this.modify("regionenddate",regionenddate);
    }

    /**
     * 设置 [WODATE]
     */
    public void setWodate(Timestamp  wodate){
        this.wodate = wodate ;
        this.modify("wodate",wodate);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ACTIVELENGTHS]
     */
    public void setActivelengths(Double  activelengths){
        this.activelengths = activelengths ;
        this.modify("activelengths",activelengths);
    }

    /**
     * 设置 [VRATE]
     */
    public void setVrate(Double  vrate){
        this.vrate = vrate ;
        this.modify("vrate",vrate);
    }

    /**
     * 设置 [CURVAL]
     */
    public void setCurval(Double  curval){
        this.curval = curval ;
        this.modify("curval",curval);
    }

    /**
     * 设置 [WOSTATE]
     */
    public void setWostate(Integer  wostate){
        this.wostate = wostate ;
        this.modify("wostate",wostate);
    }

    /**
     * 设置 [WORKLENGTH]
     */
    public void setWorklength(Double  worklength){
        this.worklength = worklength ;
        this.modify("worklength",worklength);
    }

    /**
     * 设置 [WOGROUP]
     */
    public void setWogroup(String  wogroup){
        this.wogroup = wogroup ;
        this.modify("wogroup",wogroup);
    }

    /**
     * 设置 [LASTVAL]
     */
    public void setLastval(Double  lastval){
        this.lastval = lastval ;
        this.modify("lastval",lastval);
    }

    /**
     * 设置 [WFINSTANCEID]
     */
    public void setWfinstanceid(String  wfinstanceid){
        this.wfinstanceid = wfinstanceid ;
        this.modify("wfinstanceid",wfinstanceid);
    }

    /**
     * 设置 [BDATE]
     */
    public void setBdate(Timestamp  bdate){
        this.bdate = bdate ;
        this.modify("bdate",bdate);
    }

    /**
     * 设置 [WOTYPE]
     */
    public void setWotype(String  wotype){
        this.wotype = wotype ;
        this.modify("wotype",wotype);
    }

    /**
     * 设置 [EQSTOPLENGTH]
     */
    public void setEqstoplength(Double  eqstoplength){
        this.eqstoplength = eqstoplength ;
        this.modify("eqstoplength",eqstoplength);
    }

    /**
     * 设置 [WRESULT]
     */
    public void setWresult(String  wresult){
        this.wresult = wresult ;
        this.modify("wresult",wresult);
    }

    /**
     * 设置 [NVAL]
     */
    public void setNval(Double  nval){
        this.nval = nval ;
        this.modify("nval",nval);
    }

    /**
     * 设置 [ARCHIVE]
     */
    public void setArchive(String  archive){
        this.archive = archive ;
        this.modify("archive",archive);
    }

    /**
     * 设置 [WODESC]
     */
    public void setWodesc(String  wodesc){
        this.wodesc = wodesc ;
        this.modify("wodesc",wodesc);
    }

    /**
     * 设置 [DPID]
     */
    public void setDpid(String  dpid){
        this.dpid = dpid ;
        this.modify("dpid",dpid);
    }

    /**
     * 设置 [OBJID]
     */
    public void setObjid(String  objid){
        this.objid = objid ;
        this.modify("objid",objid);
    }

    /**
     * 设置 [RSERVICEID]
     */
    public void setRserviceid(String  rserviceid){
        this.rserviceid = rserviceid ;
        this.modify("rserviceid",rserviceid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }

    /**
     * 设置 [WOORIID]
     */
    public void setWooriid(String  wooriid){
        this.wooriid = wooriid ;
        this.modify("wooriid",wooriid);
    }

    /**
     * 设置 [ACCLASSID]
     */
    public void setAcclassid(String  acclassid){
        this.acclassid = acclassid ;
        this.modify("acclassid",acclassid);
    }

    /**
     * 设置 [RFOMOID]
     */
    public void setRfomoid(String  rfomoid){
        this.rfomoid = rfomoid ;
        this.modify("rfomoid",rfomoid);
    }

    /**
     * 设置 [RTEAMID]
     */
    public void setRteamid(String  rteamid){
        this.rteamid = rteamid ;
        this.modify("rteamid",rteamid);
    }

    /**
     * 设置 [RFOCAID]
     */
    public void setRfocaid(String  rfocaid){
        this.rfocaid = rfocaid ;
        this.modify("rfocaid",rfocaid);
    }

    /**
     * 设置 [RFODEID]
     */
    public void setRfodeid(String  rfodeid){
        this.rfodeid = rfodeid ;
        this.modify("rfodeid",rfodeid);
    }

    /**
     * 设置 [WOPID]
     */
    public void setWopid(String  wopid){
        this.wopid = wopid ;
        this.modify("wopid",wopid);
    }

    /**
     * 设置 [RFOACID]
     */
    public void setRfoacid(String  rfoacid){
        this.rfoacid = rfoacid ;
        this.modify("rfoacid",rfoacid);
    }

    /**
     * 设置 [MPERSONID]
     */
    public void setMpersonid(String  mpersonid){
        this.mpersonid = mpersonid ;
        this.modify("mpersonid",mpersonid);
    }

    /**
     * 设置 [RDEPTID]
     */
    public void setRdeptid(String  rdeptid){
        this.rdeptid = rdeptid ;
        this.modify("rdeptid",rdeptid);
    }

    /**
     * 设置 [WPERSONID]
     */
    public void setWpersonid(String  wpersonid){
        this.wpersonid = wpersonid ;
        this.modify("wpersonid",wpersonid);
    }

    /**
     * 设置 [REMPID]
     */
    public void setRempid(String  rempid){
        this.rempid = rempid ;
        this.modify("rempid",rempid);
    }

    /**
     * 设置 [RECVPERSONID]
     */
    public void setRecvpersonid(String  recvpersonid){
        this.recvpersonid = recvpersonid ;
        this.modify("recvpersonid",recvpersonid);
    }


}


