package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFEmpPostMap;
import cn.ibizlab.eam.webapi.dto.PFEmpPostMapDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiPFEmpPostMapMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface PFEmpPostMapMapping extends MappingBase<PFEmpPostMapDTO, PFEmpPostMap> {


}

