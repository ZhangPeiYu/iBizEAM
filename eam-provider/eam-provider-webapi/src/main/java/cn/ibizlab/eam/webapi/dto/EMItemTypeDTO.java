package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMItemTypeDTO]
 */
@Data
@ApiModel("物品类型")
public class EMItemTypeDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [EMITEMTYPEID]
     *
     */
    @JSONField(name = "emitemtypeid")
    @JsonProperty("emitemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品类型标识")
    private String emitemtypeid;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [ITEMTYPECODE]
     *
     */
    @JSONField(name = "itemtypecode")
    @JsonProperty("itemtypecode")
    @NotBlank(message = "[物品类型代码]不允许为空!")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品类型代码")
    private String itemtypecode;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [EMITEMTYPENAME]
     *
     */
    @JSONField(name = "emitemtypename")
    @JsonProperty("emitemtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品类型名称")
    private String emitemtypename;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [ITEMTYPEINFO]
     *
     */
    @JSONField(name = "itemtypeinfo")
    @JsonProperty("itemtypeinfo")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品类型信息")
    private String itemtypeinfo;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [ITEMTYPEPCODE]
     *
     */
    @JSONField(name = "itemtypepcode")
    @JsonProperty("itemtypepcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("上级类型代码")
    private String itemtypepcode;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("一级类")
    private String itembtypename;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("二级类")
    private String itemmtypename;

    /**
     * 属性 [ITEMTYPEPNAME]
     *
     */
    @JSONField(name = "itemtypepname")
    @JsonProperty("itemtypepname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("上级类型")
    private String itemtypepname;

    /**
     * 属性 [ITEMTYPEPID]
     *
     */
    @JSONField(name = "itemtypepid")
    @JsonProperty("itemtypepid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("上级类型")
    private String itemtypepid;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("二级类")
    private String itemmtypeid;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("一级类")
    private String itembtypeid;


    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [ITEMTYPECODE]
     */
    public void setItemtypecode(String  itemtypecode){
        this.itemtypecode = itemtypecode ;
        this.modify("itemtypecode",itemtypecode);
    }

    /**
     * 设置 [EMITEMTYPENAME]
     */
    public void setEmitemtypename(String  emitemtypename){
        this.emitemtypename = emitemtypename ;
        this.modify("emitemtypename",emitemtypename);
    }

    /**
     * 设置 [ITEMTYPEPID]
     */
    public void setItemtypepid(String  itemtypepid){
        this.itemtypepid = itemtypepid ;
        this.modify("itemtypepid",itemtypepid);
    }

    /**
     * 设置 [ITEMMTYPEID]
     */
    public void setItemmtypeid(String  itemmtypeid){
        this.itemmtypeid = itemmtypeid ;
        this.modify("itemmtypeid",itemmtypeid);
    }

    /**
     * 设置 [ITEMBTYPEID]
     */
    public void setItembtypeid(String  itembtypeid){
        this.itembtypeid = itembtypeid ;
        this.modify("itembtypeid",itembtypeid);
    }


}


