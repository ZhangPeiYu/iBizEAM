package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_W;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_WService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_WSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划_按周" })
@RestController("WebApi-planschedule_w")
@RequestMapping("")
public class PLANSCHEDULE_WResource {

    @Autowired
    public IPLANSCHEDULE_WService planschedule_wService;

    @Autowired
    @Lazy
    public PLANSCHEDULE_WMapping planschedule_wMapping;

    @PreAuthorize("hasPermission(this.planschedule_wMapping.toDomain(#planschedule_wdto),'eam-PLANSCHEDULE_W-Create')")
    @ApiOperation(value = "新建计划_按周", tags = {"计划_按周" },  notes = "新建计划_按周")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ws")
    public ResponseEntity<PLANSCHEDULE_WDTO> create(@Validated @RequestBody PLANSCHEDULE_WDTO planschedule_wdto) {
        PLANSCHEDULE_W domain = planschedule_wMapping.toDomain(planschedule_wdto);
		planschedule_wService.create(domain);
        PLANSCHEDULE_WDTO dto = planschedule_wMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_wMapping.toDomain(#planschedule_wdtos),'eam-PLANSCHEDULE_W-Create')")
    @ApiOperation(value = "批量新建计划_按周", tags = {"计划_按周" },  notes = "批量新建计划_按周")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ws/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULE_WDTO> planschedule_wdtos) {
        planschedule_wService.createBatch(planschedule_wMapping.toDomain(planschedule_wdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule_w" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planschedule_wService.get(#planschedule_w_id),'eam-PLANSCHEDULE_W-Update')")
    @ApiOperation(value = "更新计划_按周", tags = {"计划_按周" },  notes = "更新计划_按周")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ws/{planschedule_w_id}")
    public ResponseEntity<PLANSCHEDULE_WDTO> update(@PathVariable("planschedule_w_id") String planschedule_w_id, @RequestBody PLANSCHEDULE_WDTO planschedule_wdto) {
		PLANSCHEDULE_W domain  = planschedule_wMapping.toDomain(planschedule_wdto);
        domain .setPlanscheduleWid(planschedule_w_id);
		planschedule_wService.update(domain );
		PLANSCHEDULE_WDTO dto = planschedule_wMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_wService.getPlanscheduleWByEntities(this.planschedule_wMapping.toDomain(#planschedule_wdtos)),'eam-PLANSCHEDULE_W-Update')")
    @ApiOperation(value = "批量更新计划_按周", tags = {"计划_按周" },  notes = "批量更新计划_按周")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ws/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULE_WDTO> planschedule_wdtos) {
        planschedule_wService.updateBatch(planschedule_wMapping.toDomain(planschedule_wdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planschedule_wService.get(#planschedule_w_id),'eam-PLANSCHEDULE_W-Remove')")
    @ApiOperation(value = "删除计划_按周", tags = {"计划_按周" },  notes = "删除计划_按周")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ws/{planschedule_w_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_w_id") String planschedule_w_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planschedule_wService.remove(planschedule_w_id));
    }

    @PreAuthorize("hasPermission(this.planschedule_wService.getPlanscheduleWByIds(#ids),'eam-PLANSCHEDULE_W-Remove')")
    @ApiOperation(value = "批量删除计划_按周", tags = {"计划_按周" },  notes = "批量删除计划_按周")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ws/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planschedule_wService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planschedule_wMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE_W-Get')")
    @ApiOperation(value = "获取计划_按周", tags = {"计划_按周" },  notes = "获取计划_按周")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ws/{planschedule_w_id}")
    public ResponseEntity<PLANSCHEDULE_WDTO> get(@PathVariable("planschedule_w_id") String planschedule_w_id) {
        PLANSCHEDULE_W domain = planschedule_wService.get(planschedule_w_id);
        PLANSCHEDULE_WDTO dto = planschedule_wMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划_按周草稿", tags = {"计划_按周" },  notes = "获取计划_按周草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ws/getdraft")
    public ResponseEntity<PLANSCHEDULE_WDTO> getDraft(PLANSCHEDULE_WDTO dto) {
        PLANSCHEDULE_W domain = planschedule_wMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_wMapping.toDto(planschedule_wService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划_按周", tags = {"计划_按周" },  notes = "检查计划_按周")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ws/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULE_WDTO planschedule_wdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planschedule_wService.checkKey(planschedule_wMapping.toDomain(planschedule_wdto)));
    }

    @PreAuthorize("hasPermission(this.planschedule_wMapping.toDomain(#planschedule_wdto),'eam-PLANSCHEDULE_W-Save')")
    @ApiOperation(value = "保存计划_按周", tags = {"计划_按周" },  notes = "保存计划_按周")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ws/save")
    public ResponseEntity<PLANSCHEDULE_WDTO> save(@RequestBody PLANSCHEDULE_WDTO planschedule_wdto) {
        PLANSCHEDULE_W domain = planschedule_wMapping.toDomain(planschedule_wdto);
        planschedule_wService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_wMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planschedule_wMapping.toDomain(#planschedule_wdtos),'eam-PLANSCHEDULE_W-Save')")
    @ApiOperation(value = "批量保存计划_按周", tags = {"计划_按周" },  notes = "批量保存计划_按周")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ws/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULE_WDTO> planschedule_wdtos) {
        planschedule_wService.saveBatch(planschedule_wMapping.toDomain(planschedule_wdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_W-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_W-Get')")
	@ApiOperation(value = "获取数据集", tags = {"计划_按周" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedule_ws/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULE_WDTO>> fetchDefault(PLANSCHEDULE_WSearchContext context) {
        Page<PLANSCHEDULE_W> domains = planschedule_wService.searchDefault(context) ;
        List<PLANSCHEDULE_WDTO> list = planschedule_wMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_W-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_W-Get')")
	@ApiOperation(value = "查询数据集", tags = {"计划_按周" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedule_ws/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULE_WDTO>> searchDefault(@RequestBody PLANSCHEDULE_WSearchContext context) {
        Page<PLANSCHEDULE_W> domains = planschedule_wService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planschedule_wMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

