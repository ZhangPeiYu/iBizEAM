package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssessMent;
import cn.ibizlab.eam.core.eam_core.service.IEMAssessMentService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssessMentSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划及项目进程考核" })
@RestController("WebApi-emassessment")
@RequestMapping("")
public class EMAssessMentResource {

    @Autowired
    public IEMAssessMentService emassessmentService;

    @Autowired
    @Lazy
    public EMAssessMentMapping emassessmentMapping;

    @PreAuthorize("hasPermission(this.emassessmentMapping.toDomain(#emassessmentdto),'eam-EMAssessMent-Create')")
    @ApiOperation(value = "新建计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "新建计划及项目进程考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessments")
    public ResponseEntity<EMAssessMentDTO> create(@Validated @RequestBody EMAssessMentDTO emassessmentdto) {
        EMAssessMent domain = emassessmentMapping.toDomain(emassessmentdto);
		emassessmentService.create(domain);
        EMAssessMentDTO dto = emassessmentMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmentMapping.toDomain(#emassessmentdtos),'eam-EMAssessMent-Create')")
    @ApiOperation(value = "批量新建计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "批量新建计划及项目进程考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessments/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssessMentDTO> emassessmentdtos) {
        emassessmentService.createBatch(emassessmentMapping.toDomain(emassessmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassessment" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassessmentService.get(#emassessment_id),'eam-EMAssessMent-Update')")
    @ApiOperation(value = "更新计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "更新计划及项目进程考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessments/{emassessment_id}")
    public ResponseEntity<EMAssessMentDTO> update(@PathVariable("emassessment_id") String emassessment_id, @RequestBody EMAssessMentDTO emassessmentdto) {
		EMAssessMent domain  = emassessmentMapping.toDomain(emassessmentdto);
        domain .setEmassessmentid(emassessment_id);
		emassessmentService.update(domain );
		EMAssessMentDTO dto = emassessmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassessmentService.getEmassessmentByEntities(this.emassessmentMapping.toDomain(#emassessmentdtos)),'eam-EMAssessMent-Update')")
    @ApiOperation(value = "批量更新计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "批量更新计划及项目进程考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassessments/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssessMentDTO> emassessmentdtos) {
        emassessmentService.updateBatch(emassessmentMapping.toDomain(emassessmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassessmentService.get(#emassessment_id),'eam-EMAssessMent-Remove')")
    @ApiOperation(value = "删除计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "删除计划及项目进程考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessments/{emassessment_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassessment_id") String emassessment_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassessmentService.remove(emassessment_id));
    }

    @PreAuthorize("hasPermission(this.emassessmentService.getEmassessmentByIds(#ids),'eam-EMAssessMent-Remove')")
    @ApiOperation(value = "批量删除计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "批量删除计划及项目进程考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassessments/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassessmentService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassessmentMapping.toDomain(returnObject.body),'eam-EMAssessMent-Get')")
    @ApiOperation(value = "获取计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "获取计划及项目进程考核")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessments/{emassessment_id}")
    public ResponseEntity<EMAssessMentDTO> get(@PathVariable("emassessment_id") String emassessment_id) {
        EMAssessMent domain = emassessmentService.get(emassessment_id);
        EMAssessMentDTO dto = emassessmentMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划及项目进程考核草稿", tags = {"计划及项目进程考核" },  notes = "获取计划及项目进程考核草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassessments/getdraft")
    public ResponseEntity<EMAssessMentDTO> getDraft(EMAssessMentDTO dto) {
        EMAssessMent domain = emassessmentMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmentMapping.toDto(emassessmentService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "检查计划及项目进程考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessments/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssessMentDTO emassessmentdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassessmentService.checkKey(emassessmentMapping.toDomain(emassessmentdto)));
    }

    @PreAuthorize("hasPermission(this.emassessmentMapping.toDomain(#emassessmentdto),'eam-EMAssessMent-Save')")
    @ApiOperation(value = "保存计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "保存计划及项目进程考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessments/save")
    public ResponseEntity<EMAssessMentDTO> save(@RequestBody EMAssessMentDTO emassessmentdto) {
        EMAssessMent domain = emassessmentMapping.toDomain(emassessmentdto);
        emassessmentService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassessmentMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassessmentMapping.toDomain(#emassessmentdtos),'eam-EMAssessMent-Save')")
    @ApiOperation(value = "批量保存计划及项目进程考核", tags = {"计划及项目进程考核" },  notes = "批量保存计划及项目进程考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emassessments/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssessMentDTO> emassessmentdtos) {
        emassessmentService.saveBatch(emassessmentMapping.toDomain(emassessmentdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMent-searchDefault-all') and hasPermission(#context,'eam-EMAssessMent-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计划及项目进程考核" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassessments/fetchdefault")
	public ResponseEntity<List<EMAssessMentDTO>> fetchDefault(EMAssessMentSearchContext context) {
        Page<EMAssessMent> domains = emassessmentService.searchDefault(context) ;
        List<EMAssessMentDTO> list = emassessmentMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssessMent-searchDefault-all') and hasPermission(#context,'eam-EMAssessMent-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计划及项目进程考核" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassessments/searchdefault")
	public ResponseEntity<Page<EMAssessMentDTO>> searchDefault(@RequestBody EMAssessMentSearchContext context) {
        Page<EMAssessMent> domains = emassessmentService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassessmentMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

