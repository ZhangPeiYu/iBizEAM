package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_pf.domain.PFUnit;
import cn.ibizlab.eam.core.eam_pf.service.IPFUnitService;
import cn.ibizlab.eam.core.eam_pf.filter.PFUnitSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计量单位" })
@RestController("WebApi-pfunit")
@RequestMapping("")
public class PFUnitResource {

    @Autowired
    public IPFUnitService pfunitService;

    @Autowired
    @Lazy
    public PFUnitMapping pfunitMapping;

    @PreAuthorize("hasPermission(this.pfunitMapping.toDomain(#pfunitdto),'eam-PFUnit-Create')")
    @ApiOperation(value = "新建计量单位", tags = {"计量单位" },  notes = "新建计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfunits")
    public ResponseEntity<PFUnitDTO> create(@Validated @RequestBody PFUnitDTO pfunitdto) {
        PFUnit domain = pfunitMapping.toDomain(pfunitdto);
		pfunitService.create(domain);
        PFUnitDTO dto = pfunitMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfunitMapping.toDomain(#pfunitdtos),'eam-PFUnit-Create')")
    @ApiOperation(value = "批量新建计量单位", tags = {"计量单位" },  notes = "批量新建计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfunits/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PFUnitDTO> pfunitdtos) {
        pfunitService.createBatch(pfunitMapping.toDomain(pfunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "pfunit" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.pfunitService.get(#pfunit_id),'eam-PFUnit-Update')")
    @ApiOperation(value = "更新计量单位", tags = {"计量单位" },  notes = "更新计量单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfunits/{pfunit_id}")
    public ResponseEntity<PFUnitDTO> update(@PathVariable("pfunit_id") String pfunit_id, @RequestBody PFUnitDTO pfunitdto) {
		PFUnit domain  = pfunitMapping.toDomain(pfunitdto);
        domain .setPfunitid(pfunit_id);
		pfunitService.update(domain );
		PFUnitDTO dto = pfunitMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.pfunitService.getPfunitByEntities(this.pfunitMapping.toDomain(#pfunitdtos)),'eam-PFUnit-Update')")
    @ApiOperation(value = "批量更新计量单位", tags = {"计量单位" },  notes = "批量更新计量单位")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfunits/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PFUnitDTO> pfunitdtos) {
        pfunitService.updateBatch(pfunitMapping.toDomain(pfunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.pfunitService.get(#pfunit_id),'eam-PFUnit-Remove')")
    @ApiOperation(value = "删除计量单位", tags = {"计量单位" },  notes = "删除计量单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfunits/{pfunit_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("pfunit_id") String pfunit_id) {
         return ResponseEntity.status(HttpStatus.OK).body(pfunitService.remove(pfunit_id));
    }

    @PreAuthorize("hasPermission(this.pfunitService.getPfunitByIds(#ids),'eam-PFUnit-Remove')")
    @ApiOperation(value = "批量删除计量单位", tags = {"计量单位" },  notes = "批量删除计量单位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfunits/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        pfunitService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.pfunitMapping.toDomain(returnObject.body),'eam-PFUnit-Get')")
    @ApiOperation(value = "获取计量单位", tags = {"计量单位" },  notes = "获取计量单位")
	@RequestMapping(method = RequestMethod.GET, value = "/pfunits/{pfunit_id}")
    public ResponseEntity<PFUnitDTO> get(@PathVariable("pfunit_id") String pfunit_id) {
        PFUnit domain = pfunitService.get(pfunit_id);
        PFUnitDTO dto = pfunitMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计量单位草稿", tags = {"计量单位" },  notes = "获取计量单位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/pfunits/getdraft")
    public ResponseEntity<PFUnitDTO> getDraft(PFUnitDTO dto) {
        PFUnit domain = pfunitMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(pfunitMapping.toDto(pfunitService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计量单位", tags = {"计量单位" },  notes = "检查计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfunits/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PFUnitDTO pfunitdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(pfunitService.checkKey(pfunitMapping.toDomain(pfunitdto)));
    }

    @PreAuthorize("hasPermission(this.pfunitMapping.toDomain(#pfunitdto),'eam-PFUnit-Save')")
    @ApiOperation(value = "保存计量单位", tags = {"计量单位" },  notes = "保存计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfunits/save")
    public ResponseEntity<PFUnitDTO> save(@RequestBody PFUnitDTO pfunitdto) {
        PFUnit domain = pfunitMapping.toDomain(pfunitdto);
        pfunitService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(pfunitMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.pfunitMapping.toDomain(#pfunitdtos),'eam-PFUnit-Save')")
    @ApiOperation(value = "批量保存计量单位", tags = {"计量单位" },  notes = "批量保存计量单位")
	@RequestMapping(method = RequestMethod.POST, value = "/pfunits/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PFUnitDTO> pfunitdtos) {
        pfunitService.saveBatch(pfunitMapping.toDomain(pfunitdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFUnit-searchDefault-all') and hasPermission(#context,'eam-PFUnit-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"计量单位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfunits/fetchdefault")
	public ResponseEntity<List<PFUnitDTO>> fetchDefault(PFUnitSearchContext context) {
        Page<PFUnit> domains = pfunitService.searchDefault(context) ;
        List<PFUnitDTO> list = pfunitMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PFUnit-searchDefault-all') and hasPermission(#context,'eam-PFUnit-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"计量单位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfunits/searchdefault")
	public ResponseEntity<Page<PFUnitDTO>> searchDefault(@RequestBody PFUnitSearchContext context) {
        Page<PFUnit> domains = pfunitService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(pfunitMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

