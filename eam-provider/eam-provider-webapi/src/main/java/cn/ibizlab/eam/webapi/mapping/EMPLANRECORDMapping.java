package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMPLANRECORD;
import cn.ibizlab.eam.webapi.dto.EMPLANRECORDDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMPLANRECORDMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMPLANRECORDMapping extends MappingBase<EMPLANRECORDDTO, EMPLANRECORD> {


}

