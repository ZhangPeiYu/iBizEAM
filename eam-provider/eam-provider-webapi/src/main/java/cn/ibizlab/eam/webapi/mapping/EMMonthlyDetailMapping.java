package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMMonthlyDetail;
import cn.ibizlab.eam.webapi.dto.EMMonthlyDetailDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMMonthlyDetailMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMMonthlyDetailMapping extends MappingBase<EMMonthlyDetailDTO, EMMonthlyDetail> {


}

