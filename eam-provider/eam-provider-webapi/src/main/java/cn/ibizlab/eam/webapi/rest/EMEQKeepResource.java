package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQKeep;
import cn.ibizlab.eam.core.eam_core.service.IEMEQKeepService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQKeepSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"维护保养" })
@RestController("WebApi-emeqkeep")
@RequestMapping("")
public class EMEQKeepResource {

    @Autowired
    public IEMEQKeepService emeqkeepService;

    @Autowired
    @Lazy
    public EMEQKeepMapping emeqkeepMapping;

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "新建维护保养", tags = {"维护保养" },  notes = "新建维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkeeps")
    public ResponseEntity<EMEQKeepDTO> create(@Validated @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
		emeqkeepService.create(domain);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "批量新建维护保养", tags = {"维护保养" },  notes = "批量新建维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkeeps/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        emeqkeepService.createBatch(emeqkeepMapping.toDomain(emeqkeepdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkeep" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "更新维护保养", tags = {"维护保养" },  notes = "更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> update(@PathVariable("emeqkeep_id") String emeqkeep_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
		EMEQKeep domain  = emeqkeepMapping.toDomain(emeqkeepdto);
        domain .setEmeqkeepid(emeqkeep_id);
		emeqkeepService.update(domain );
		EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByEntities(this.emeqkeepMapping.toDomain(#emeqkeepdtos)),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "批量更新维护保养", tags = {"维护保养" },  notes = "批量更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqkeeps/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        emeqkeepService.updateBatch(emeqkeepMapping.toDomain(emeqkeepdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "删除维护保养", tags = {"维护保养" },  notes = "删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqkeep_id") String emeqkeep_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.remove(emeqkeep_id));
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByIds(#ids),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "批量删除维护保养", tags = {"维护保养" },  notes = "批量删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqkeeps/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqkeepService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkeepMapping.toDomain(returnObject.body),'eam-EMEQKeep-Get')")
    @ApiOperation(value = "获取维护保养", tags = {"维护保养" },  notes = "获取维护保养")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> get(@PathVariable("emeqkeep_id") String emeqkeep_id) {
        EMEQKeep domain = emeqkeepService.get(emeqkeep_id);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取维护保养草稿", tags = {"维护保养" },  notes = "获取维护保养草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqkeeps/getdraft")
    public ResponseEntity<EMEQKeepDTO> getDraft(EMEQKeepDTO dto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(emeqkeepService.getDraft(domain)));
    }

    @ApiOperation(value = "检查维护保养", tags = {"维护保养" },  notes = "检查维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkeeps/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQKeepDTO emeqkeepdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.checkKey(emeqkeepMapping.toDomain(emeqkeepdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "保存维护保养", tags = {"维护保养" },  notes = "保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkeeps/save")
    public ResponseEntity<EMEQKeepDTO> save(@RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        emeqkeepService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "批量保存维护保养", tags = {"维护保养" },  notes = "批量保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqkeeps/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        emeqkeepService.saveBatch(emeqkeepMapping.toDomain(emeqkeepdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"维护保养" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emeqkeeps/fetchcalendar")
	public ResponseEntity<List<EMEQKeepDTO>> fetchCalendar(EMEQKeepSearchContext context) {
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"维护保养" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emeqkeeps/searchcalendar")
	public ResponseEntity<Page<EMEQKeepDTO>> searchCalendar(@RequestBody EMEQKeepSearchContext context) {
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"维护保养" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqkeeps/fetchdefault")
	public ResponseEntity<List<EMEQKeepDTO>> fetchDefault(EMEQKeepSearchContext context) {
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"维护保养" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqkeeps/searchdefault")
	public ResponseEntity<Page<EMEQKeepDTO>> searchDefault(@RequestBody EMEQKeepSearchContext context) {
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "根据设备档案建立维护保养", tags = {"维护保养" },  notes = "根据设备档案建立维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqkeeps")
    public ResponseEntity<EMEQKeepDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
		emeqkeepService.create(domain);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "根据设备档案批量建立维护保养", tags = {"维护保养" },  notes = "根据设备档案批量建立维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqkeepService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkeep" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "根据设备档案更新维护保养", tags = {"维护保养" },  notes = "根据设备档案更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqkeepid(emeqkeep_id);
		emeqkeepService.update(domain);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByEntities(this.emeqkeepMapping.toDomain(#emeqkeepdtos)),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "根据设备档案批量更新维护保养", tags = {"维护保养" },  notes = "根据设备档案批量更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqkeepService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "根据设备档案删除维护保养", tags = {"维护保养" },  notes = "根据设备档案删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.remove(emeqkeep_id));
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByIds(#ids),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "根据设备档案批量删除维护保养", tags = {"维护保养" },  notes = "根据设备档案批量删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqkeepService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkeepMapping.toDomain(returnObject.body),'eam-EMEQKeep-Get')")
    @ApiOperation(value = "根据设备档案获取维护保养", tags = {"维护保养" },  notes = "根据设备档案获取维护保养")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id) {
        EMEQKeep domain = emeqkeepService.get(emeqkeep_id);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取维护保养草稿", tags = {"维护保养" },  notes = "根据设备档案获取维护保养草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqkeeps/getdraft")
    public ResponseEntity<EMEQKeepDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQKeepDTO dto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(emeqkeepService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查维护保养", tags = {"维护保养" },  notes = "根据设备档案检查维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqkeeps/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.checkKey(emeqkeepMapping.toDomain(emeqkeepdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "根据设备档案保存维护保养", tags = {"维护保养" },  notes = "根据设备档案保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqkeeps/save")
    public ResponseEntity<EMEQKeepDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
        emeqkeepService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "根据设备档案批量保存维护保养", tags = {"维护保养" },  notes = "根据设备档案批量保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqkeeps/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqkeepService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"维护保养" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqkeeps/fetchcalendar")
	public ResponseEntity<List<EMEQKeepDTO>> fetchEMEQKeepCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"维护保养" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqkeeps/searchcalendar")
	public ResponseEntity<Page<EMEQKeepDTO>> searchEMEQKeepCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"维护保养" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqkeeps/fetchdefault")
	public ResponseEntity<List<EMEQKeepDTO>> fetchEMEQKeepDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"维护保养" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqkeeps/searchdefault")
	public ResponseEntity<Page<EMEQKeepDTO>> searchEMEQKeepDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "根据班组设备档案建立维护保养", tags = {"维护保养" },  notes = "根据班组设备档案建立维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps")
    public ResponseEntity<EMEQKeepDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
		emeqkeepService.create(domain);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立维护保养", tags = {"维护保养" },  notes = "根据班组设备档案批量建立维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqkeepService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqkeep" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "根据班组设备档案更新维护保养", tags = {"维护保养" },  notes = "根据班组设备档案更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqkeepid(emeqkeep_id);
		emeqkeepService.update(domain);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByEntities(this.emeqkeepMapping.toDomain(#emeqkeepdtos)),'eam-EMEQKeep-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新维护保养", tags = {"维护保养" },  notes = "根据班组设备档案批量更新维护保养")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqkeepService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.get(#emeqkeep_id),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "根据班组设备档案删除维护保养", tags = {"维护保养" },  notes = "根据班组设备档案删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.remove(emeqkeep_id));
    }

    @PreAuthorize("hasPermission(this.emeqkeepService.getEmeqkeepByIds(#ids),'eam-EMEQKeep-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除维护保养", tags = {"维护保养" },  notes = "根据班组设备档案批量删除维护保养")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqkeepService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqkeepMapping.toDomain(returnObject.body),'eam-EMEQKeep-Get')")
    @ApiOperation(value = "根据班组设备档案获取维护保养", tags = {"维护保养" },  notes = "根据班组设备档案获取维护保养")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/{emeqkeep_id}")
    public ResponseEntity<EMEQKeepDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqkeep_id") String emeqkeep_id) {
        EMEQKeep domain = emeqkeepService.get(emeqkeep_id);
        EMEQKeepDTO dto = emeqkeepMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取维护保养草稿", tags = {"维护保养" },  notes = "根据班组设备档案获取维护保养草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/getdraft")
    public ResponseEntity<EMEQKeepDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQKeepDTO dto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(emeqkeepService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查维护保养", tags = {"维护保养" },  notes = "根据班组设备档案检查维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqkeepService.checkKey(emeqkeepMapping.toDomain(emeqkeepdto)));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdto),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "根据班组设备档案保存维护保养", tags = {"维护保养" },  notes = "根据班组设备档案保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/save")
    public ResponseEntity<EMEQKeepDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepDTO emeqkeepdto) {
        EMEQKeep domain = emeqkeepMapping.toDomain(emeqkeepdto);
        domain.setEquipid(emequip_id);
        emeqkeepService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqkeepMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqkeepMapping.toDomain(#emeqkeepdtos),'eam-EMEQKeep-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存维护保养", tags = {"维护保养" },  notes = "根据班组设备档案批量保存维护保养")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQKeepDTO> emeqkeepdtos) {
        List<EMEQKeep> domainlist=emeqkeepMapping.toDomain(emeqkeepdtos);
        for(EMEQKeep domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqkeepService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"维护保养" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/fetchcalendar")
	public ResponseEntity<List<EMEQKeepDTO>> fetchEMEQKeepCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchCalendar-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"维护保养" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/searchcalendar")
	public ResponseEntity<Page<EMEQKeepDTO>> searchEMEQKeepCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"维护保养" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/fetchdefault")
	public ResponseEntity<List<EMEQKeepDTO>> fetchEMEQKeepDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
        List<EMEQKeepDTO> list = emeqkeepMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQKeep-searchDefault-all') and hasPermission(#context,'eam-EMEQKeep-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"维护保养" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqkeeps/searchdefault")
	public ResponseEntity<Page<EMEQKeepDTO>> searchEMEQKeepDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQKeepSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQKeep> domains = emeqkeepService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqkeepMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

