package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMItem;
import cn.ibizlab.eam.core.eam_core.service.IEMItemService;
import cn.ibizlab.eam.core.eam_core.filter.EMItemSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"物品" })
@RestController("WebApi-emitem")
@RequestMapping("")
public class EMItemResource {

    @Autowired
    public IEMItemService emitemService;

    @Autowired
    @Lazy
    public EMItemMapping emitemMapping;

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Create')")
    @ApiOperation(value = "新建物品", tags = {"物品" },  notes = "新建物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems")
    public ResponseEntity<EMItemDTO> create(@Validated @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
		emitemService.create(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Create')")
    @ApiOperation(value = "批量新建物品", tags = {"物品" },  notes = "批量新建物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMItemDTO> emitemdtos) {
        emitemService.createBatch(emitemMapping.toDomain(emitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Update')")
    @ApiOperation(value = "更新物品", tags = {"物品" },  notes = "更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> update(@PathVariable("emitem_id") String emitem_id, @RequestBody EMItemDTO emitemdto) {
		EMItem domain  = emitemMapping.toDomain(emitemdto);
        domain .setEmitemid(emitem_id);
		emitemService.update(domain );
		EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByEntities(this.emitemMapping.toDomain(#emitemdtos)),'eam-EMItem-Update')")
    @ApiOperation(value = "批量更新物品", tags = {"物品" },  notes = "批量更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emitems/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMItemDTO> emitemdtos) {
        emitemService.updateBatch(emitemMapping.toDomain(emitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Remove')")
    @ApiOperation(value = "删除物品", tags = {"物品" },  notes = "删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/{emitem_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emitem_id") String emitem_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emitemService.remove(emitem_id));
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByIds(#ids),'eam-EMItem-Remove')")
    @ApiOperation(value = "批量删除物品", tags = {"物品" },  notes = "批量删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emitems/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemMapping.toDomain(returnObject.body),'eam-EMItem-Get')")
    @ApiOperation(value = "获取物品", tags = {"物品" },  notes = "获取物品")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> get(@PathVariable("emitem_id") String emitem_id) {
        EMItem domain = emitemService.get(emitem_id);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取物品草稿", tags = {"物品" },  notes = "获取物品草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emitems/getdraft")
    public ResponseEntity<EMItemDTO> getDraft(EMItemDTO dto) {
        EMItem domain = emitemMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(emitemService.getDraft(domain)));
    }

    @ApiOperation(value = "检查物品", tags = {"物品" },  notes = "检查物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMItemDTO emitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemService.checkKey(emitemMapping.toDomain(emitemdto)));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Save')")
    @ApiOperation(value = "保存物品", tags = {"物品" },  notes = "保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/save")
    public ResponseEntity<EMItemDTO> save(@RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        emitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Save')")
    @ApiOperation(value = "批量保存物品", tags = {"物品" },  notes = "批量保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emitems/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMItemDTO> emitemdtos) {
        emitemService.saveBatch(emitemMapping.toDomain(emitemdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"物品" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/fetchdefault")
	public ResponseEntity<List<EMItemDTO>> fetchDefault(EMItemSearchContext context) {
        Page<EMItem> domains = emitemService.searchDefault(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"物品" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/searchdefault")
	public ResponseEntity<Page<EMItemDTO>> searchDefault(@RequestBody EMItemSearchContext context) {
        Page<EMItem> domains = emitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "获取类型树物品", tags = {"物品" } ,notes = "获取类型树物品")
    @RequestMapping(method= RequestMethod.GET , value="/emitems/fetchitemtypetree")
	public ResponseEntity<List<EMItemDTO>> fetchItemTypeTree(EMItemSearchContext context) {
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "查询类型树物品", tags = {"物品" } ,notes = "查询类型树物品")
    @RequestMapping(method= RequestMethod.POST , value="/emitems/searchitemtypetree")
	public ResponseEntity<Page<EMItemDTO>> searchItemTypeTree(@RequestBody EMItemSearchContext context) {
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Create')")
    @ApiOperation(value = "根据服务商建立物品", tags = {"物品" },  notes = "根据服务商建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems")
    public ResponseEntity<EMItemDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setMserviceid(emservice_id);
		emitemService.create(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Create')")
    @ApiOperation(value = "根据服务商批量建立物品", tags = {"物品" },  notes = "根据服务商批量建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setMserviceid(emservice_id);
        }
        emitemService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Update')")
    @ApiOperation(value = "根据服务商更新物品", tags = {"物品" },  notes = "根据服务商更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setMserviceid(emservice_id);
        domain.setEmitemid(emitem_id);
		emitemService.update(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByEntities(this.emitemMapping.toDomain(#emitemdtos)),'eam-EMItem-Update')")
    @ApiOperation(value = "根据服务商批量更新物品", tags = {"物品" },  notes = "根据服务商批量更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emitems/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setMserviceid(emservice_id);
        }
        emitemService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据服务商删除物品", tags = {"物品" },  notes = "根据服务商删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/{emitem_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemService.remove(emitem_id));
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByIds(#ids),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据服务商批量删除物品", tags = {"物品" },  notes = "根据服务商批量删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emitems/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemMapping.toDomain(returnObject.body),'eam-EMItem-Get')")
    @ApiOperation(value = "根据服务商获取物品", tags = {"物品" },  notes = "根据服务商获取物品")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emitem_id") String emitem_id) {
        EMItem domain = emitemService.get(emitem_id);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取物品草稿", tags = {"物品" },  notes = "根据服务商获取物品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emitems/getdraft")
    public ResponseEntity<EMItemDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id, EMItemDTO dto) {
        EMItem domain = emitemMapping.toDomain(dto);
        domain.setMserviceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(emitemService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查物品", tags = {"物品" },  notes = "根据服务商检查物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemDTO emitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemService.checkKey(emitemMapping.toDomain(emitemdto)));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Save')")
    @ApiOperation(value = "根据服务商保存物品", tags = {"物品" },  notes = "根据服务商保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/save")
    public ResponseEntity<EMItemDTO> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setMserviceid(emservice_id);
        emitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Save')")
    @ApiOperation(value = "根据服务商批量保存物品", tags = {"物品" },  notes = "根据服务商批量保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emitems/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
             domain.setMserviceid(emservice_id);
        }
        emitemService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"物品" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/fetchdefault")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMItemSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"物品" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/searchdefault")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据服务商获取类型树物品", tags = {"物品" } ,notes = "根据服务商获取类型树物品")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emitems/fetchitemtypetree")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemItemTypeTreeByEMService(@PathVariable("emservice_id") String emservice_id,EMItemSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据服务商查询类型树物品", tags = {"物品" } ,notes = "根据服务商查询类型树物品")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emitems/searchitemtypetree")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemItemTypeTreeByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMItemSearchContext context) {
        context.setN_mserviceid_eq(emservice_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库库位建立物品", tags = {"物品" },  notes = "根据仓库库位建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems")
    public ResponseEntity<EMItemDTO> createByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
		emitemService.create(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库库位批量建立物品", tags = {"物品" },  notes = "根据仓库库位批量建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> createBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitemService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库库位更新物品", tags = {"物品" },  notes = "根据仓库库位更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> updateByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmitemid(emitem_id);
		emitemService.update(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByEntities(this.emitemMapping.toDomain(#emitemdtos)),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库库位批量更新物品", tags = {"物品" },  notes = "根据仓库库位批量更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> updateBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitemService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库库位删除物品", tags = {"物品" },  notes = "根据仓库库位删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<Boolean> removeByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemService.remove(emitem_id));
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByIds(#ids),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库库位批量删除物品", tags = {"物品" },  notes = "根据仓库库位批量删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> removeBatchByEMStorePart(@RequestBody List<String> ids) {
        emitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemMapping.toDomain(returnObject.body),'eam-EMItem-Get')")
    @ApiOperation(value = "根据仓库库位获取物品", tags = {"物品" },  notes = "根据仓库库位获取物品")
	@RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> getByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMItem domain = emitemService.get(emitem_id);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库库位获取物品草稿", tags = {"物品" },  notes = "根据仓库库位获取物品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstoreparts/{emstorepart_id}/emitems/getdraft")
    public ResponseEntity<EMItemDTO> getDraftByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, EMItemDTO dto) {
        EMItem domain = emitemMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(emitemService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库库位检查物品", tags = {"物品" },  notes = "根据仓库库位检查物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemService.checkKey(emitemMapping.toDomain(emitemdto)));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库库位保存物品", tags = {"物品" },  notes = "根据仓库库位保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/save")
    public ResponseEntity<EMItemDTO> saveByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
        emitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库库位批量保存物品", tags = {"物品" },  notes = "根据仓库库位批量保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstoreparts/{emstorepart_id}/emitems/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emitemService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库库位获取DEFAULT", tags = {"物品" } ,notes = "根据仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/fetchdefault")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库库位查询DEFAULT", tags = {"物品" } ,notes = "根据仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/searchdefault")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemDefaultByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库库位获取类型树物品", tags = {"物品" } ,notes = "根据仓库库位获取类型树物品")
    @RequestMapping(method= RequestMethod.GET , value="/emstoreparts/{emstorepart_id}/emitems/fetchitemtypetree")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemItemTypeTreeByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id,EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库库位查询类型树物品", tags = {"物品" } ,notes = "根据仓库库位查询类型树物品")
    @RequestMapping(method= RequestMethod.POST , value="/emstoreparts/{emstorepart_id}/emitems/searchitemtypetree")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemItemTypeTreeByEMStorePart(@PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库建立物品", tags = {"物品" },  notes = "根据仓库建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems")
    public ResponseEntity<EMItemDTO> createByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStoreid(emstore_id);
		emitemService.create(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库批量建立物品", tags = {"物品" },  notes = "根据仓库批量建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/batch")
    public ResponseEntity<Boolean> createBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emitemService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库更新物品", tags = {"物品" },  notes = "根据仓库更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> updateByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStoreid(emstore_id);
        domain.setEmitemid(emitem_id);
		emitemService.update(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByEntities(this.emitemMapping.toDomain(#emitemdtos)),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库批量更新物品", tags = {"物品" },  notes = "根据仓库批量更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emitems/batch")
    public ResponseEntity<Boolean> updateBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStoreid(emstore_id);
        }
        emitemService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库删除物品", tags = {"物品" },  notes = "根据仓库删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/{emitem_id}")
    public ResponseEntity<Boolean> removeByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemService.remove(emitem_id));
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByIds(#ids),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库批量删除物品", tags = {"物品" },  notes = "根据仓库批量删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emitems/batch")
    public ResponseEntity<Boolean> removeBatchByEMStore(@RequestBody List<String> ids) {
        emitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemMapping.toDomain(returnObject.body),'eam-EMItem-Get')")
    @ApiOperation(value = "根据仓库获取物品", tags = {"物品" },  notes = "根据仓库获取物品")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> getByEMStore(@PathVariable("emstore_id") String emstore_id, @PathVariable("emitem_id") String emitem_id) {
        EMItem domain = emitemService.get(emitem_id);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库获取物品草稿", tags = {"物品" },  notes = "根据仓库获取物品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emitems/getdraft")
    public ResponseEntity<EMItemDTO> getDraftByEMStore(@PathVariable("emstore_id") String emstore_id, EMItemDTO dto) {
        EMItem domain = emitemMapping.toDomain(dto);
        domain.setStoreid(emstore_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(emitemService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库检查物品", tags = {"物品" },  notes = "根据仓库检查物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemDTO emitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemService.checkKey(emitemMapping.toDomain(emitemdto)));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库保存物品", tags = {"物品" },  notes = "根据仓库保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/save")
    public ResponseEntity<EMItemDTO> saveByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStoreid(emstore_id);
        emitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库批量保存物品", tags = {"物品" },  notes = "根据仓库批量保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emitems/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
             domain.setStoreid(emstore_id);
        }
        emitemService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库获取DEFAULT", tags = {"物品" } ,notes = "根据仓库获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/fetchdefault")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemDefaultByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库查询DEFAULT", tags = {"物品" } ,notes = "根据仓库查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/searchdefault")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemDefaultByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库获取类型树物品", tags = {"物品" } ,notes = "根据仓库获取类型树物品")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emitems/fetchitemtypetree")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemItemTypeTreeByEMStore(@PathVariable("emstore_id") String emstore_id,EMItemSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库查询类型树物品", tags = {"物品" } ,notes = "根据仓库查询类型树物品")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emitems/searchitemtypetree")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemItemTypeTreeByEMStore(@PathVariable("emstore_id") String emstore_id, @RequestBody EMItemSearchContext context) {
        context.setN_storeid_eq(emstore_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库仓库库位建立物品", tags = {"物品" },  notes = "根据仓库仓库库位建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems")
    public ResponseEntity<EMItemDTO> createByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
		emitemService.create(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Create')")
    @ApiOperation(value = "根据仓库仓库库位批量建立物品", tags = {"物品" },  notes = "根据仓库仓库库位批量建立物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> createBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitemService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emitem" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库仓库库位更新物品", tags = {"物品" },  notes = "根据仓库仓库库位更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> updateByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
        domain.setEmitemid(emitem_id);
		emitemService.update(domain);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByEntities(this.emitemMapping.toDomain(#emitemdtos)),'eam-EMItem-Update')")
    @ApiOperation(value = "根据仓库仓库库位批量更新物品", tags = {"物品" },  notes = "根据仓库仓库库位批量更新物品")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> updateBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
            domain.setStorepartid(emstorepart_id);
        }
        emitemService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emitemService.get(#emitem_id),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库仓库库位删除物品", tags = {"物品" },  notes = "根据仓库仓库库位删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<Boolean> removeByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emitemService.remove(emitem_id));
    }

    @PreAuthorize("hasPermission(this.emitemService.getEmitemByIds(#ids),'eam-EMItem-Remove')")
    @ApiOperation(value = "根据仓库仓库库位批量删除物品", tags = {"物品" },  notes = "根据仓库仓库库位批量删除物品")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/batch")
    public ResponseEntity<Boolean> removeBatchByEMStoreEMStorePart(@RequestBody List<String> ids) {
        emitemService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emitemMapping.toDomain(returnObject.body),'eam-EMItem-Get')")
    @ApiOperation(value = "根据仓库仓库库位获取物品", tags = {"物品" },  notes = "根据仓库仓库库位获取物品")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/{emitem_id}")
    public ResponseEntity<EMItemDTO> getByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @PathVariable("emitem_id") String emitem_id) {
        EMItem domain = emitemService.get(emitem_id);
        EMItemDTO dto = emitemMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据仓库仓库库位获取物品草稿", tags = {"物品" },  notes = "根据仓库仓库库位获取物品草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/getdraft")
    public ResponseEntity<EMItemDTO> getDraftByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, EMItemDTO dto) {
        EMItem domain = emitemMapping.toDomain(dto);
        domain.setStorepartid(emstorepart_id);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(emitemService.getDraft(domain)));
    }

    @ApiOperation(value = "根据仓库仓库库位检查物品", tags = {"物品" },  notes = "根据仓库仓库库位检查物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emitemService.checkKey(emitemMapping.toDomain(emitemdto)));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdto),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库仓库库位保存物品", tags = {"物品" },  notes = "根据仓库仓库库位保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/save")
    public ResponseEntity<EMItemDTO> saveByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemDTO emitemdto) {
        EMItem domain = emitemMapping.toDomain(emitemdto);
        domain.setStorepartid(emstorepart_id);
        emitemService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emitemMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emitemMapping.toDomain(#emitemdtos),'eam-EMItem-Save')")
    @ApiOperation(value = "根据仓库仓库库位批量保存物品", tags = {"物品" },  notes = "根据仓库仓库库位批量保存物品")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody List<EMItemDTO> emitemdtos) {
        List<EMItem> domainlist=emitemMapping.toDomain(emitemdtos);
        for(EMItem domain:domainlist){
             domain.setStorepartid(emstorepart_id);
        }
        emitemService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取DEFAULT", tags = {"物品" } ,notes = "根据仓库仓库库位获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/fetchdefault")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchDefault-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询DEFAULT", tags = {"物品" } ,notes = "根据仓库仓库库位查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/searchdefault")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemDefaultByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库仓库库位获取类型树物品", tags = {"物品" } ,notes = "根据仓库仓库库位获取类型树物品")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/fetchitemtypetree")
	public ResponseEntity<List<EMItemDTO>> fetchEMItemItemTypeTreeByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id,EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
        List<EMItemDTO> list = emitemMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMItem-searchItemTypeTree-all') and hasPermission(#context,'eam-EMItem-Get')")
	@ApiOperation(value = "根据仓库仓库库位查询类型树物品", tags = {"物品" } ,notes = "根据仓库仓库库位查询类型树物品")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/{emstore_id}/emstoreparts/{emstorepart_id}/emitems/searchitemtypetree")
	public ResponseEntity<Page<EMItemDTO>> searchEMItemItemTypeTreeByEMStoreEMStorePart(@PathVariable("emstore_id") String emstore_id, @PathVariable("emstorepart_id") String emstorepart_id, @RequestBody EMItemSearchContext context) {
        context.setN_storepartid_eq(emstorepart_id);
        Page<EMItem> domains = emitemService.searchItemTypeTree(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emitemMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

