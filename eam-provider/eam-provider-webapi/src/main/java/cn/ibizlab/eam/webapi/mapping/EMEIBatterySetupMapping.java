package cn.ibizlab.eam.webapi.mapping;

import org.mapstruct.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEIBatterySetup;
import cn.ibizlab.eam.webapi.dto.EMEIBatterySetupDTO;
import cn.ibizlab.eam.util.domain.MappingBase;

@Mapper(componentModel = "spring", uses = {}, implementationName = "WebApiEMEIBatterySetupMapping",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface EMEIBatterySetupMapping extends MappingBase<EMEIBatterySetupDTO, EMEIBatterySetup> {


}

