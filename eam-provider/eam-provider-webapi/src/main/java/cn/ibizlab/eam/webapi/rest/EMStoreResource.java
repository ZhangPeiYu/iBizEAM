package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMStore;
import cn.ibizlab.eam.core.eam_core.service.IEMStoreService;
import cn.ibizlab.eam.core.eam_core.filter.EMStoreSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"仓库" })
@RestController("WebApi-emstore")
@RequestMapping("")
public class EMStoreResource {

    @Autowired
    public IEMStoreService emstoreService;

    @Autowired
    @Lazy
    public EMStoreMapping emstoreMapping;

    @PreAuthorize("hasPermission(this.emstoreMapping.toDomain(#emstoredto),'eam-EMStore-Create')")
    @ApiOperation(value = "新建仓库", tags = {"仓库" },  notes = "新建仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores")
    public ResponseEntity<EMStoreDTO> create(@Validated @RequestBody EMStoreDTO emstoredto) {
        EMStore domain = emstoreMapping.toDomain(emstoredto);
		emstoreService.create(domain);
        EMStoreDTO dto = emstoreMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstoreMapping.toDomain(#emstoredtos),'eam-EMStore-Create')")
    @ApiOperation(value = "批量新建仓库", tags = {"仓库" },  notes = "批量新建仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMStoreDTO> emstoredtos) {
        emstoreService.createBatch(emstoreMapping.toDomain(emstoredtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emstore" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emstoreService.get(#emstore_id),'eam-EMStore-Update')")
    @ApiOperation(value = "更新仓库", tags = {"仓库" },  notes = "更新仓库")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/{emstore_id}")
    public ResponseEntity<EMStoreDTO> update(@PathVariable("emstore_id") String emstore_id, @RequestBody EMStoreDTO emstoredto) {
		EMStore domain  = emstoreMapping.toDomain(emstoredto);
        domain .setEmstoreid(emstore_id);
		emstoreService.update(domain );
		EMStoreDTO dto = emstoreMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emstoreService.getEmstoreByEntities(this.emstoreMapping.toDomain(#emstoredtos)),'eam-EMStore-Update')")
    @ApiOperation(value = "批量更新仓库", tags = {"仓库" },  notes = "批量更新仓库")
	@RequestMapping(method = RequestMethod.PUT, value = "/emstores/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMStoreDTO> emstoredtos) {
        emstoreService.updateBatch(emstoreMapping.toDomain(emstoredtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emstoreService.get(#emstore_id),'eam-EMStore-Remove')")
    @ApiOperation(value = "删除仓库", tags = {"仓库" },  notes = "删除仓库")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/{emstore_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emstore_id") String emstore_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emstoreService.remove(emstore_id));
    }

    @PreAuthorize("hasPermission(this.emstoreService.getEmstoreByIds(#ids),'eam-EMStore-Remove')")
    @ApiOperation(value = "批量删除仓库", tags = {"仓库" },  notes = "批量删除仓库")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emstores/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emstoreService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emstoreMapping.toDomain(returnObject.body),'eam-EMStore-Get')")
    @ApiOperation(value = "获取仓库", tags = {"仓库" },  notes = "获取仓库")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/{emstore_id}")
    public ResponseEntity<EMStoreDTO> get(@PathVariable("emstore_id") String emstore_id) {
        EMStore domain = emstoreService.get(emstore_id);
        EMStoreDTO dto = emstoreMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取仓库草稿", tags = {"仓库" },  notes = "获取仓库草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emstores/getdraft")
    public ResponseEntity<EMStoreDTO> getDraft(EMStoreDTO dto) {
        EMStore domain = emstoreMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emstoreMapping.toDto(emstoreService.getDraft(domain)));
    }

    @ApiOperation(value = "检查仓库", tags = {"仓库" },  notes = "检查仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMStoreDTO emstoredto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emstoreService.checkKey(emstoreMapping.toDomain(emstoredto)));
    }

    @PreAuthorize("hasPermission(this.emstoreMapping.toDomain(#emstoredto),'eam-EMStore-Save')")
    @ApiOperation(value = "保存仓库", tags = {"仓库" },  notes = "保存仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/save")
    public ResponseEntity<EMStoreDTO> save(@RequestBody EMStoreDTO emstoredto) {
        EMStore domain = emstoreMapping.toDomain(emstoredto);
        emstoreService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emstoreMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emstoreMapping.toDomain(#emstoredtos),'eam-EMStore-Save')")
    @ApiOperation(value = "批量保存仓库", tags = {"仓库" },  notes = "批量保存仓库")
	@RequestMapping(method = RequestMethod.POST, value = "/emstores/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMStoreDTO> emstoredtos) {
        emstoreService.saveBatch(emstoreMapping.toDomain(emstoredtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStore-searchDefault-all') and hasPermission(#context,'eam-EMStore-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"仓库" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emstores/fetchdefault")
	public ResponseEntity<List<EMStoreDTO>> fetchDefault(EMStoreSearchContext context) {
        Page<EMStore> domains = emstoreService.searchDefault(context) ;
        List<EMStoreDTO> list = emstoreMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMStore-searchDefault-all') and hasPermission(#context,'eam-EMStore-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"仓库" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emstores/searchdefault")
	public ResponseEntity<Page<EMStoreDTO>> searchDefault(@RequestBody EMStoreSearchContext context) {
        Page<EMStore> domains = emstoreService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emstoreMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

