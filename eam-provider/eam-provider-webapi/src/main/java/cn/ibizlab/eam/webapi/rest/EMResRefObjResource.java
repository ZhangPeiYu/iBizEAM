package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMResRefObj;
import cn.ibizlab.eam.core.eam_core.service.IEMResRefObjService;
import cn.ibizlab.eam.core.eam_core.filter.EMResRefObjSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资源引用对象" })
@RestController("WebApi-emresrefobj")
@RequestMapping("")
public class EMResRefObjResource {

    @Autowired
    public IEMResRefObjService emresrefobjService;

    @Autowired
    @Lazy
    public EMResRefObjMapping emresrefobjMapping;

    @PreAuthorize("hasPermission(this.emresrefobjMapping.toDomain(#emresrefobjdto),'eam-EMResRefObj-Create')")
    @ApiOperation(value = "新建资源引用对象", tags = {"资源引用对象" },  notes = "新建资源引用对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emresrefobjs")
    public ResponseEntity<EMResRefObjDTO> create(@Validated @RequestBody EMResRefObjDTO emresrefobjdto) {
        EMResRefObj domain = emresrefobjMapping.toDomain(emresrefobjdto);
		emresrefobjService.create(domain);
        EMResRefObjDTO dto = emresrefobjMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresrefobjMapping.toDomain(#emresrefobjdtos),'eam-EMResRefObj-Create')")
    @ApiOperation(value = "批量新建资源引用对象", tags = {"资源引用对象" },  notes = "批量新建资源引用对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emresrefobjs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMResRefObjDTO> emresrefobjdtos) {
        emresrefobjService.createBatch(emresrefobjMapping.toDomain(emresrefobjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emresrefobj" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emresrefobjService.get(#emresrefobj_id),'eam-EMResRefObj-Update')")
    @ApiOperation(value = "更新资源引用对象", tags = {"资源引用对象" },  notes = "更新资源引用对象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresrefobjs/{emresrefobj_id}")
    public ResponseEntity<EMResRefObjDTO> update(@PathVariable("emresrefobj_id") String emresrefobj_id, @RequestBody EMResRefObjDTO emresrefobjdto) {
		EMResRefObj domain  = emresrefobjMapping.toDomain(emresrefobjdto);
        domain .setEmresrefobjid(emresrefobj_id);
		emresrefobjService.update(domain );
		EMResRefObjDTO dto = emresrefobjMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emresrefobjService.getEmresrefobjByEntities(this.emresrefobjMapping.toDomain(#emresrefobjdtos)),'eam-EMResRefObj-Update')")
    @ApiOperation(value = "批量更新资源引用对象", tags = {"资源引用对象" },  notes = "批量更新资源引用对象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emresrefobjs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMResRefObjDTO> emresrefobjdtos) {
        emresrefobjService.updateBatch(emresrefobjMapping.toDomain(emresrefobjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emresrefobjService.get(#emresrefobj_id),'eam-EMResRefObj-Remove')")
    @ApiOperation(value = "删除资源引用对象", tags = {"资源引用对象" },  notes = "删除资源引用对象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresrefobjs/{emresrefobj_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emresrefobj_id") String emresrefobj_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emresrefobjService.remove(emresrefobj_id));
    }

    @PreAuthorize("hasPermission(this.emresrefobjService.getEmresrefobjByIds(#ids),'eam-EMResRefObj-Remove')")
    @ApiOperation(value = "批量删除资源引用对象", tags = {"资源引用对象" },  notes = "批量删除资源引用对象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emresrefobjs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emresrefobjService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emresrefobjMapping.toDomain(returnObject.body),'eam-EMResRefObj-Get')")
    @ApiOperation(value = "获取资源引用对象", tags = {"资源引用对象" },  notes = "获取资源引用对象")
	@RequestMapping(method = RequestMethod.GET, value = "/emresrefobjs/{emresrefobj_id}")
    public ResponseEntity<EMResRefObjDTO> get(@PathVariable("emresrefobj_id") String emresrefobj_id) {
        EMResRefObj domain = emresrefobjService.get(emresrefobj_id);
        EMResRefObjDTO dto = emresrefobjMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资源引用对象草稿", tags = {"资源引用对象" },  notes = "获取资源引用对象草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emresrefobjs/getdraft")
    public ResponseEntity<EMResRefObjDTO> getDraft(EMResRefObjDTO dto) {
        EMResRefObj domain = emresrefobjMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emresrefobjMapping.toDto(emresrefobjService.getDraft(domain)));
    }

    @ApiOperation(value = "检查资源引用对象", tags = {"资源引用对象" },  notes = "检查资源引用对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emresrefobjs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMResRefObjDTO emresrefobjdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emresrefobjService.checkKey(emresrefobjMapping.toDomain(emresrefobjdto)));
    }

    @PreAuthorize("hasPermission(this.emresrefobjMapping.toDomain(#emresrefobjdto),'eam-EMResRefObj-Save')")
    @ApiOperation(value = "保存资源引用对象", tags = {"资源引用对象" },  notes = "保存资源引用对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emresrefobjs/save")
    public ResponseEntity<EMResRefObjDTO> save(@RequestBody EMResRefObjDTO emresrefobjdto) {
        EMResRefObj domain = emresrefobjMapping.toDomain(emresrefobjdto);
        emresrefobjService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emresrefobjMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emresrefobjMapping.toDomain(#emresrefobjdtos),'eam-EMResRefObj-Save')")
    @ApiOperation(value = "批量保存资源引用对象", tags = {"资源引用对象" },  notes = "批量保存资源引用对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emresrefobjs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMResRefObjDTO> emresrefobjdtos) {
        emresrefobjService.saveBatch(emresrefobjMapping.toDomain(emresrefobjdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResRefObj-searchDefault-all') and hasPermission(#context,'eam-EMResRefObj-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"资源引用对象" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emresrefobjs/fetchdefault")
	public ResponseEntity<List<EMResRefObjDTO>> fetchDefault(EMResRefObjSearchContext context) {
        Page<EMResRefObj> domains = emresrefobjService.searchDefault(context) ;
        List<EMResRefObjDTO> list = emresrefobjMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResRefObj-searchDefault-all') and hasPermission(#context,'eam-EMResRefObj-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"资源引用对象" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emresrefobjs/searchdefault")
	public ResponseEntity<Page<EMResRefObjDTO>> searchDefault(@RequestBody EMResRefObjSearchContext context) {
        Page<EMResRefObj> domains = emresrefobjService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresrefobjMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResRefObj-searchIndexDER-all') and hasPermission(#context,'eam-EMResRefObj-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"资源引用对象" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emresrefobjs/fetchindexder")
	public ResponseEntity<List<EMResRefObjDTO>> fetchIndexDER(EMResRefObjSearchContext context) {
        Page<EMResRefObj> domains = emresrefobjService.searchIndexDER(context) ;
        List<EMResRefObjDTO> list = emresrefobjMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMResRefObj-searchIndexDER-all') and hasPermission(#context,'eam-EMResRefObj-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"资源引用对象" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emresrefobjs/searchindexder")
	public ResponseEntity<Page<EMResRefObjDTO>> searchIndexDER(@RequestBody EMResRefObjSearchContext context) {
        Page<EMResRefObj> domains = emresrefobjService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emresrefobjMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

