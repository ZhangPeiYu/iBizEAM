package cn.ibizlab.eam.webapi.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.math.BigInteger;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.math.BigDecimal;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.alibaba.fastjson.annotation.JSONField;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import cn.ibizlab.eam.util.domain.DTOBase;
import cn.ibizlab.eam.util.domain.DTOClient;
import lombok.Data;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 服务DTO对象[EMResItemDTO]
 */
@Data
@ApiModel("物品资源")
public class EMResItemDTO extends DTOBase implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 属性 [PRICE]
     *
     */
    @JSONField(name = "price")
    @JsonProperty("price")
    @ApiModelProperty("单价")
    private Double price;

    /**
     * 属性 [AMOUNT]
     *
     */
    @JSONField(name = "amount")
    @JsonProperty("amount")
    @NotNull(message = "[总金额]不允许为空!")
    @ApiModelProperty("总金额")
    private Double amount;

    /**
     * 属性 [EMRESITEMNAME]
     *
     */
    @JSONField(name = "emresitemname")
    @JsonProperty("emresitemname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品资源名称")
    private String emresitemname;

    /**
     * 属性 [DESCRIPTION]
     *
     */
    @JSONField(name = "description")
    @JsonProperty("description")
    @Size(min = 0, max = 2000, message = "内容长度必须小于等于[2000]")
    @ApiModelProperty("描述")
    private String description;

    /**
     * 属性 [UPDATEMAN]
     *
     */
    @JSONField(name = "updateman")
    @JsonProperty("updateman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("更新人")
    private String updateman;

    /**
     * 属性 [UPDATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "updatedate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("updatedate")
    @ApiModelProperty("更新时间")
    private Timestamp updatedate;

    /**
     * 属性 [PNUM]
     *
     */
    @JSONField(name = "pnum")
    @JsonProperty("pnum")
    @ApiModelProperty("安排用量")
    private Double pnum;

    /**
     * 属性 [BDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "bdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("bdate")
    @ApiModelProperty("开始时间")
    private Timestamp bdate;

    /**
     * 属性 [SNUM]
     *
     */
    @JSONField(name = "snum")
    @JsonProperty("snum")
    @ApiModelProperty("实际用量")
    private Double snum;

    /**
     * 属性 [CREATEMAN]
     *
     */
    @JSONField(name = "createman")
    @JsonProperty("createman")
    @Size(min = 0, max = 60, message = "内容长度必须小于等于[60]")
    @ApiModelProperty("建立人")
    private String createman;

    /**
     * 属性 [EMRESITEMID]
     *
     */
    @JSONField(name = "emresitemid")
    @JsonProperty("emresitemid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品资源标识")
    private String emresitemid;

    /**
     * 属性 [ORGID]
     *
     */
    @JSONField(name = "orgid")
    @JsonProperty("orgid")
    @Size(min = 0, max = 40, message = "内容长度必须小于等于[40]")
    @ApiModelProperty("组织")
    private String orgid;

    /**
     * 属性 [EDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "edate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("edate")
    @ApiModelProperty("结束时间")
    private Timestamp edate;

    /**
     * 属性 [ENABLE]
     *
     */
    @JSONField(name = "enable")
    @JsonProperty("enable")
    @ApiModelProperty("逻辑有效标志")
    private Integer enable;

    /**
     * 属性 [DATAFROM]
     *
     */
    @JSONField(name = "datafrom")
    @JsonProperty("datafrom")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("数据来源")
    private String datafrom;

    /**
     * 属性 [CREATEDATE]
     *
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale = "zh" , timezone="GMT+8")
    @JSONField(name = "createdate" , format="yyyy-MM-dd HH:mm:ss")
    @JsonProperty("createdate")
    @ApiModelProperty("建立时间")
    private Timestamp createdate;

    /**
     * 属性 [RESREFOBJNAME]
     *
     */
    @JSONField(name = "resrefobjname")
    @JsonProperty("resrefobjname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("引用对象")
    private String resrefobjname;

    /**
     * 属性 [ITEMBTYPENAME]
     *
     */
    @JSONField(name = "itembtypename")
    @JsonProperty("itembtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品大类")
    private String itembtypename;

    /**
     * 属性 [SNAME]
     *
     */
    @JSONField(name = "sname")
    @JsonProperty("sname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备统计")
    private String sname;

    /**
     * 属性 [EQUIPCODE]
     *
     */
    @JSONField(name = "equipcode")
    @JsonProperty("equipcode")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipcode;

    /**
     * 属性 [ITEMBTYPEID]
     *
     */
    @JSONField(name = "itembtypeid")
    @JsonProperty("itembtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品大类")
    private String itembtypeid;

    /**
     * 属性 [RESNAME_SHOW]
     *
     */
    @JSONField(name = "resname_show")
    @JsonProperty("resname_show")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品")
    private String resnameShow;

    /**
     * 属性 [ITEMMTYPENAME]
     *
     */
    @JSONField(name = "itemmtypename")
    @JsonProperty("itemmtypename")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品二级类")
    private String itemmtypename;

    /**
     * 属性 [UNITNAME]
     *
     */
    @JSONField(name = "unitname")
    @JsonProperty("unitname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("单位")
    private String unitname;

    /**
     * 属性 [ITEMMTYPEID]
     *
     */
    @JSONField(name = "itemmtypeid")
    @JsonProperty("itemmtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品二级类")
    private String itemmtypeid;

    /**
     * 属性 [EQUIPNAME]
     *
     */
    @JSONField(name = "equipname")
    @JsonProperty("equipname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("设备")
    private String equipname;

    /**
     * 属性 [RESNAME]
     *
     */
    @JSONField(name = "resname")
    @JsonProperty("resname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("物品")
    private String resname;

    /**
     * 属性 [ITEMTYPEID]
     *
     */
    @JSONField(name = "itemtypeid")
    @JsonProperty("itemtypeid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品类型")
    private String itemtypeid;

    /**
     * 属性 [TEAMNAME]
     *
     */
    @JSONField(name = "teamname")
    @JsonProperty("teamname")
    @Size(min = 0, max = 200, message = "内容长度必须小于等于[200]")
    @ApiModelProperty("班组")
    private String teamname;

    /**
     * 属性 [EQENABLE]
     *
     */
    @JSONField(name = "eqenable")
    @JsonProperty("eqenable")
    @ApiModelProperty("设备逻辑有效")
    private Integer eqenable;

    /**
     * 属性 [RESID]
     *
     */
    @JSONField(name = "resid")
    @JsonProperty("resid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("物品")
    private String resid;

    /**
     * 属性 [RESREFOBJID]
     *
     */
    @JSONField(name = "resrefobjid")
    @JsonProperty("resrefobjid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("引用对象")
    private String resrefobjid;

    /**
     * 属性 [EQUIPID]
     *
     */
    @JSONField(name = "equipid")
    @JsonProperty("equipid")
    @Size(min = 0, max = 100, message = "内容长度必须小于等于[100]")
    @ApiModelProperty("设备")
    private String equipid;


    /**
     * 设置 [PRICE]
     */
    public void setPrice(Double  price){
        this.price = price ;
        this.modify("price",price);
    }

    /**
     * 设置 [AMOUNT]
     */
    public void setAmount(Double  amount){
        this.amount = amount ;
        this.modify("amount",amount);
    }

    /**
     * 设置 [EMRESITEMNAME]
     */
    public void setEmresitemname(String  emresitemname){
        this.emresitemname = emresitemname ;
        this.modify("emresitemname",emresitemname);
    }

    /**
     * 设置 [DESCRIPTION]
     */
    public void setDescription(String  description){
        this.description = description ;
        this.modify("description",description);
    }

    /**
     * 设置 [PNUM]
     */
    public void setPnum(Double  pnum){
        this.pnum = pnum ;
        this.modify("pnum",pnum);
    }

    /**
     * 设置 [BDATE]
     */
    public void setBdate(Timestamp  bdate){
        this.bdate = bdate ;
        this.modify("bdate",bdate);
    }

    /**
     * 设置 [SNUM]
     */
    public void setSnum(Double  snum){
        this.snum = snum ;
        this.modify("snum",snum);
    }

    /**
     * 设置 [EDATE]
     */
    public void setEdate(Timestamp  edate){
        this.edate = edate ;
        this.modify("edate",edate);
    }

    /**
     * 设置 [DATAFROM]
     */
    public void setDatafrom(String  datafrom){
        this.datafrom = datafrom ;
        this.modify("datafrom",datafrom);
    }

    /**
     * 设置 [RESID]
     */
    public void setResid(String  resid){
        this.resid = resid ;
        this.modify("resid",resid);
    }

    /**
     * 设置 [RESREFOBJID]
     */
    public void setResrefobjid(String  resrefobjid){
        this.resrefobjid = resrefobjid ;
        this.modify("resrefobjid",resrefobjid);
    }

    /**
     * 设置 [EQUIPID]
     */
    public void setEquipid(String  equipid){
        this.equipid = equipid ;
        this.modify("equipid",equipid);
    }


}


