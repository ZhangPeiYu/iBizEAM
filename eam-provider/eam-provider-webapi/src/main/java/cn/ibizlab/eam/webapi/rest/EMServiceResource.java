package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMService;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务商" })
@RestController("WebApi-emservice")
@RequestMapping("")
public class EMServiceResource {

    @Autowired
    public IEMServiceService emserviceService;

    @Autowired
    @Lazy
    public EMServiceMapping emserviceMapping;

    @PreAuthorize("hasPermission(this.emserviceMapping.toDomain(#emservicedto),'eam-EMService-Create')")
    @ApiOperation(value = "新建服务商", tags = {"服务商" },  notes = "新建服务商")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices")
    public ResponseEntity<EMServiceDTO> create(@Validated @RequestBody EMServiceDTO emservicedto) {
        EMService domain = emserviceMapping.toDomain(emservicedto);
		emserviceService.create(domain);
        EMServiceDTO dto = emserviceMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceMapping.toDomain(#emservicedtos),'eam-EMService-Create')")
    @ApiOperation(value = "批量新建服务商", tags = {"服务商" },  notes = "批量新建服务商")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMServiceDTO> emservicedtos) {
        emserviceService.createBatch(emserviceMapping.toDomain(emservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emservice" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emserviceService.get(#emservice_id),'eam-EMService-Update')")
    @ApiOperation(value = "更新服务商", tags = {"服务商" },  notes = "更新服务商")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}")
    public ResponseEntity<EMServiceDTO> update(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceDTO emservicedto) {
		EMService domain  = emserviceMapping.toDomain(emservicedto);
        domain .setEmserviceid(emservice_id);
		emserviceService.update(domain );
		EMServiceDTO dto = emserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceService.getEmserviceByEntities(this.emserviceMapping.toDomain(#emservicedtos)),'eam-EMService-Update')")
    @ApiOperation(value = "批量更新服务商", tags = {"服务商" },  notes = "批量更新服务商")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMServiceDTO> emservicedtos) {
        emserviceService.updateBatch(emserviceMapping.toDomain(emservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emserviceService.get(#emservice_id),'eam-EMService-Remove')")
    @ApiOperation(value = "删除服务商", tags = {"服务商" },  notes = "删除服务商")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emservice_id") String emservice_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emserviceService.remove(emservice_id));
    }

    @PreAuthorize("hasPermission(this.emserviceService.getEmserviceByIds(#ids),'eam-EMService-Remove')")
    @ApiOperation(value = "批量删除服务商", tags = {"服务商" },  notes = "批量删除服务商")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emserviceService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emserviceMapping.toDomain(returnObject.body),'eam-EMService-Get')")
    @ApiOperation(value = "获取服务商", tags = {"服务商" },  notes = "获取服务商")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}")
    public ResponseEntity<EMServiceDTO> get(@PathVariable("emservice_id") String emservice_id) {
        EMService domain = emserviceService.get(emservice_id);
        EMServiceDTO dto = emserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务商草稿", tags = {"服务商" },  notes = "获取服务商草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/getdraft")
    public ResponseEntity<EMServiceDTO> getDraft(EMServiceDTO dto) {
        EMService domain = emserviceMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceMapping.toDto(emserviceService.getDraft(domain)));
    }

    @ApiOperation(value = "检查服务商", tags = {"服务商" },  notes = "检查服务商")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMServiceDTO emservicedto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emserviceService.checkKey(emserviceMapping.toDomain(emservicedto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-Confirm-all')")
    @ApiOperation(value = "审核通过", tags = {"服务商" },  notes = "审核通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/confirm")
    public ResponseEntity<EMServiceDTO> confirm(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceDTO emservicedto) {
        EMService domain = emserviceMapping.toDomain(emservicedto);
        domain.setEmserviceid(emservice_id);
        domain = emserviceService.confirm(domain);
        emservicedto = emserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emservicedto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-Confirm-all')")
    @ApiOperation(value = "批量处理[审核通过]", tags = {"服务商" },  notes = "批量处理[审核通过]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMServiceDTO> emservicedtos) {
        List<EMService> domains = emserviceMapping.toDomain(emservicedtos);
        boolean result = emserviceService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-GenId-all')")
    @ApiOperation(value = "生成id", tags = {"服务商" },  notes = "生成id")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/genid")
    public ResponseEntity<EMServiceDTO> genId(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceDTO emservicedto) {
        EMService domain = emserviceMapping.toDomain(emservicedto);
        domain.setEmserviceid(emservice_id);
        domain = emserviceService.genId(domain);
        emservicedto = emserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emservicedto);
    }

    @PreAuthorize("hasPermission(this.emserviceMapping.toDomain(#emservicedto),'eam-EMService-Save')")
    @ApiOperation(value = "保存服务商", tags = {"服务商" },  notes = "保存服务商")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/save")
    public ResponseEntity<EMServiceDTO> save(@RequestBody EMServiceDTO emservicedto) {
        EMService domain = emserviceMapping.toDomain(emservicedto);
        emserviceService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emserviceMapping.toDomain(#emservicedtos),'eam-EMService-Save')")
    @ApiOperation(value = "批量保存服务商", tags = {"服务商" },  notes = "批量保存服务商")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMServiceDTO> emservicedtos) {
        emserviceService.saveBatch(emserviceMapping.toDomain(emservicedtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-Unconfirmed-all')")
    @ApiOperation(value = "审核未通过", tags = {"服务商" },  notes = "审核未通过")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/unconfirmed")
    public ResponseEntity<EMServiceDTO> unconfirmed(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceDTO emservicedto) {
        EMService domain = emserviceMapping.toDomain(emservicedto);
        domain.setEmserviceid(emservice_id);
        domain = emserviceService.unconfirmed(domain);
        emservicedto = emserviceMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emservicedto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchConfirmed-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "获取已审", tags = {"服务商" } ,notes = "获取已审")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/fetchconfirmed")
	public ResponseEntity<List<EMServiceDTO>> fetchConfirmed(EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchConfirmed(context) ;
        List<EMServiceDTO> list = emserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchConfirmed-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "查询已审", tags = {"服务商" } ,notes = "查询已审")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/searchconfirmed")
	public ResponseEntity<Page<EMServiceDTO>> searchConfirmed(@RequestBody EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchDefault-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务商" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/fetchdefault")
	public ResponseEntity<List<EMServiceDTO>> fetchDefault(EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchDefault(context) ;
        List<EMServiceDTO> list = emserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchDefault-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务商" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/searchdefault")
	public ResponseEntity<Page<EMServiceDTO>> searchDefault(@RequestBody EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchDraft-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "获取未提交", tags = {"服务商" } ,notes = "获取未提交")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/fetchdraft")
	public ResponseEntity<List<EMServiceDTO>> fetchDraft(EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchDraft(context) ;
        List<EMServiceDTO> list = emserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchDraft-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "查询未提交", tags = {"服务商" } ,notes = "查询未提交")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/searchdraft")
	public ResponseEntity<Page<EMServiceDTO>> searchDraft(@RequestBody EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchToConfirm-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "获取待审", tags = {"服务商" } ,notes = "获取待审")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/fetchtoconfirm")
	public ResponseEntity<List<EMServiceDTO>> fetchToConfirm(EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchToConfirm(context) ;
        List<EMServiceDTO> list = emserviceMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMService-searchToConfirm-all') and hasPermission(#context,'eam-EMService-Get')")
	@ApiOperation(value = "查询待审", tags = {"服务商" } ,notes = "查询待审")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/searchtoconfirm")
	public ResponseEntity<Page<EMServiceDTO>> searchToConfirm(@RequestBody EMServiceSearchContext context) {
        Page<EMService> domains = emserviceService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

