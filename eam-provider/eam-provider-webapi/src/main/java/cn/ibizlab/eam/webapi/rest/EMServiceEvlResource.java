package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMServiceEvl;
import cn.ibizlab.eam.core.eam_core.service.IEMServiceEvlService;
import cn.ibizlab.eam.core.eam_core.filter.EMServiceEvlSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"服务商评估" })
@RestController("WebApi-emserviceevl")
@RequestMapping("")
public class EMServiceEvlResource {

    @Autowired
    public IEMServiceEvlService emserviceevlService;

    @Autowired
    @Lazy
    public EMServiceEvlMapping emserviceevlMapping;

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "新建服务商评估", tags = {"服务商评估" },  notes = "新建服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls")
    public ResponseEntity<EMServiceEvlDTO> create(@Validated @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
		emserviceevlService.create(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "批量新建服务商评估", tags = {"服务商评估" },  notes = "批量新建服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.createBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emserviceevl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "更新服务商评估", tags = {"服务商评估" },  notes = "更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> update(@PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
		EMServiceEvl domain  = emserviceevlMapping.toDomain(emserviceevldto);
        domain .setEmserviceevlid(emserviceevl_id);
		emserviceevlService.update(domain );
		EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByEntities(this.emserviceevlMapping.toDomain(#emserviceevldtos)),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "批量更新服务商评估", tags = {"服务商评估" },  notes = "批量更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.updateBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "删除服务商评估", tags = {"服务商评估" },  notes = "删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emserviceevl_id") String emserviceevl_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.remove(emserviceevl_id));
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByIds(#ids),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "批量删除服务商评估", tags = {"服务商评估" },  notes = "批量删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emserviceevls/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emserviceevlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emserviceevlMapping.toDomain(returnObject.body),'eam-EMServiceEvl-Get')")
    @ApiOperation(value = "获取服务商评估", tags = {"服务商评估" },  notes = "获取服务商评估")
	@RequestMapping(method = RequestMethod.GET, value = "/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> get(@PathVariable("emserviceevl_id") String emserviceevl_id) {
        EMServiceEvl domain = emserviceevlService.get(emserviceevl_id);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取服务商评估草稿", tags = {"服务商评估" },  notes = "获取服务商评估草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emserviceevls/getdraft")
    public ResponseEntity<EMServiceEvlDTO> getDraft(EMServiceEvlDTO dto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(emserviceevlService.getDraft(domain)));
    }

    @ApiOperation(value = "检查服务商评估", tags = {"服务商评估" },  notes = "检查服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMServiceEvlDTO emserviceevldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.checkKey(emserviceevlMapping.toDomain(emserviceevldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Confirm-all')")
    @ApiOperation(value = "确认", tags = {"服务商评估" },  notes = "确认")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/{emserviceevl_id}/confirm")
    public ResponseEntity<EMServiceEvlDTO> confirm(@PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setEmserviceevlid(emserviceevl_id);
        domain = emserviceevlService.confirm(domain);
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Confirm-all')")
    @ApiOperation(value = "批量处理[确认]", tags = {"服务商评估" },  notes = "批量处理[确认]")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/confirmbatch")
    public ResponseEntity<Boolean> confirmBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domains = emserviceevlMapping.toDomain(emserviceevldtos);
        boolean result = emserviceevlService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Rejected-all')")
    @ApiOperation(value = "驳回", tags = {"服务商评估" },  notes = "驳回")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/{emserviceevl_id}/rejected")
    public ResponseEntity<EMServiceEvlDTO> rejected(@PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setEmserviceevlid(emserviceevl_id);
        domain = emserviceevlService.rejected(domain);
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "保存服务商评估", tags = {"服务商评估" },  notes = "保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/save")
    public ResponseEntity<EMServiceEvlDTO> save(@RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        emserviceevlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "批量保存服务商评估", tags = {"服务商评估" },  notes = "批量保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        emserviceevlService.saveBatch(emserviceevlMapping.toDomain(emserviceevldtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Submit-all')")
    @ApiOperation(value = "提交", tags = {"服务商评估" },  notes = "提交")
	@RequestMapping(method = RequestMethod.POST, value = "/emserviceevls/{emserviceevl_id}/submit")
    public ResponseEntity<EMServiceEvlDTO> submit(@PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setEmserviceevlid(emserviceevl_id);
        domain = emserviceevlService.submit(domain);
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchConfirmed-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "获取已完成", tags = {"服务商评估" } ,notes = "获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchconfirmed")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchConfirmed(EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchConfirmed(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchConfirmed-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "查询已完成", tags = {"服务商评估" } ,notes = "查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchconfirmed")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchConfirmed(@RequestBody EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"服务商评估" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchdefault")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchDefault(EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"服务商评估" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchdefault")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchDefault(@RequestBody EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDraft-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "获取草稿", tags = {"服务商评估" } ,notes = "获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchdraft")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchDraft(EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDraft(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDraft-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "查询草稿", tags = {"服务商评估" } ,notes = "查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchdraft")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchDraft(@RequestBody EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取评估前五", tags = {"服务商评估" } ,notes = "获取评估前五")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchevaluatetop5")
	public ResponseEntity<List<Map>> fetchEvaluateTop5(EMServiceEvlSearchContext context) {
        Page<Map> domains = emserviceevlService.searchEvaluateTop5(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询评估前五", tags = {"服务商评估" } ,notes = "查询评估前五")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchevaluatetop5")
	public ResponseEntity<Page<Map>> searchEvaluateTop5(@RequestBody EMServiceEvlSearchContext context) {
        Page<Map> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

	@ApiOperation(value = "获取OverallEVL", tags = {"服务商评估" } ,notes = "获取OverallEVL")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchoverallevl")
	public ResponseEntity<List<Map>> fetchOverallEVL(EMServiceEvlSearchContext context) {
        Page<Map> domains = emserviceevlService.searchOverallEVL(context) ;
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "查询OverallEVL", tags = {"服务商评估" } ,notes = "查询OverallEVL")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchoverallevl")
	public ResponseEntity<Page<Map>> searchOverallEVL(@RequestBody EMServiceEvlSearchContext context) {
        Page<Map> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchToConfirm-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "获取待确认", tags = {"服务商评估" } ,notes = "获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emserviceevls/fetchtoconfirm")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchToConfirm(EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchToConfirm(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchToConfirm-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "查询待确认", tags = {"服务商评估" } ,notes = "查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emserviceevls/searchtoconfirm")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchToConfirm(@RequestBody EMServiceEvlSearchContext context) {
        Page<EMServiceEvl> domains = emserviceevlService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "根据服务商建立服务商评估", tags = {"服务商评估" },  notes = "根据服务商建立服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls")
    public ResponseEntity<EMServiceEvlDTO> createByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
		emserviceevlService.create(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Create')")
    @ApiOperation(value = "根据服务商批量建立服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量建立服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> createBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
            domain.setServiceid(emservice_id);
        }
        emserviceevlService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emserviceevl" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "根据服务商更新服务商评估", tags = {"服务商评估" },  notes = "根据服务商更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> updateByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        domain.setEmserviceevlid(emserviceevl_id);
		emserviceevlService.update(domain);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByEntities(this.emserviceevlMapping.toDomain(#emserviceevldtos)),'eam-EMServiceEvl-Update')")
    @ApiOperation(value = "根据服务商批量更新服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量更新服务商评估")
	@RequestMapping(method = RequestMethod.PUT, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> updateBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
            domain.setServiceid(emservice_id);
        }
        emserviceevlService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.get(#emserviceevl_id),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "根据服务商删除服务商评估", tags = {"服务商评估" },  notes = "根据服务商删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<Boolean> removeByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.remove(emserviceevl_id));
    }

    @PreAuthorize("hasPermission(this.emserviceevlService.getEmserviceevlByIds(#ids),'eam-EMServiceEvl-Remove')")
    @ApiOperation(value = "根据服务商批量删除服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量删除服务商评估")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emservices/{emservice_id}/emserviceevls/batch")
    public ResponseEntity<Boolean> removeBatchByEMService(@RequestBody List<String> ids) {
        emserviceevlService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emserviceevlMapping.toDomain(returnObject.body),'eam-EMServiceEvl-Get')")
    @ApiOperation(value = "根据服务商获取服务商评估", tags = {"服务商评估" },  notes = "根据服务商获取服务商评估")
	@RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}")
    public ResponseEntity<EMServiceEvlDTO> getByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id) {
        EMServiceEvl domain = emserviceevlService.get(emserviceevl_id);
        EMServiceEvlDTO dto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据服务商获取服务商评估草稿", tags = {"服务商评估" },  notes = "根据服务商获取服务商评估草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emservices/{emservice_id}/emserviceevls/getdraft")
    public ResponseEntity<EMServiceEvlDTO> getDraftByEMService(@PathVariable("emservice_id") String emservice_id, EMServiceEvlDTO dto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(dto);
        domain.setServiceid(emservice_id);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(emserviceevlService.getDraft(domain)));
    }

    @ApiOperation(value = "根据服务商检查服务商评估", tags = {"服务商评估" },  notes = "根据服务商检查服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emserviceevlService.checkKey(emserviceevlMapping.toDomain(emserviceevldto)));
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Confirm-all')")
    @ApiOperation(value = "根据服务商服务商评估", tags = {"服务商评估" },  notes = "根据服务商服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}/confirm")
    public ResponseEntity<EMServiceEvlDTO> confirmByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        domain = emserviceevlService.confirm(domain) ;
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }
    @ApiOperation(value = "批量处理[根据服务商服务商评估]", tags = {"服务商评估" },  notes = "批量处理[根据服务商服务商评估]")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/confirmbatch")
    public ResponseEntity<Boolean> confirmByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domains = emserviceevlMapping.toDomain(emserviceevldtos);
        boolean result = emserviceevlService.confirmBatch(domains);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Rejected-all')")
    @ApiOperation(value = "根据服务商服务商评估", tags = {"服务商评估" },  notes = "根据服务商服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}/rejected")
    public ResponseEntity<EMServiceEvlDTO> rejectedByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        domain = emserviceevlService.rejected(domain) ;
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }
    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldto),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "根据服务商保存服务商评估", tags = {"服务商评估" },  notes = "根据服务商保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/save")
    public ResponseEntity<EMServiceEvlDTO> saveByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        emserviceevlService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevlMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emserviceevlMapping.toDomain(#emserviceevldtos),'eam-EMServiceEvl-Save')")
    @ApiOperation(value = "根据服务商批量保存服务商评估", tags = {"服务商评估" },  notes = "根据服务商批量保存服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody List<EMServiceEvlDTO> emserviceevldtos) {
        List<EMServiceEvl> domainlist=emserviceevlMapping.toDomain(emserviceevldtos);
        for(EMServiceEvl domain:domainlist){
             domain.setServiceid(emservice_id);
        }
        emserviceevlService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-Submit-all')")
    @ApiOperation(value = "根据服务商服务商评估", tags = {"服务商评估" },  notes = "根据服务商服务商评估")
	@RequestMapping(method = RequestMethod.POST, value = "/emservices/{emservice_id}/emserviceevls/{emserviceevl_id}/submit")
    public ResponseEntity<EMServiceEvlDTO> submitByEMService(@PathVariable("emservice_id") String emservice_id, @PathVariable("emserviceevl_id") String emserviceevl_id, @RequestBody EMServiceEvlDTO emserviceevldto) {
        EMServiceEvl domain = emserviceevlMapping.toDomain(emserviceevldto);
        domain.setServiceid(emservice_id);
        domain = emserviceevlService.submit(domain) ;
        emserviceevldto = emserviceevlMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emserviceevldto);
    }
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchConfirmed-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商获取已完成", tags = {"服务商评估" } ,notes = "根据服务商获取已完成")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchconfirmed")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchEMServiceEvlConfirmedByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchConfirmed(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchConfirmed-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商查询已完成", tags = {"服务商评估" } ,notes = "根据服务商查询已完成")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchconfirmed")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchEMServiceEvlConfirmedByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchConfirmed(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商获取DEFAULT", tags = {"服务商评估" } ,notes = "根据服务商获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchdefault")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchEMServiceEvlDefaultByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDefault-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商查询DEFAULT", tags = {"服务商评估" } ,notes = "根据服务商查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchdefault")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchEMServiceEvlDefaultByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDraft-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商获取草稿", tags = {"服务商评估" } ,notes = "根据服务商获取草稿")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchdraft")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchEMServiceEvlDraftByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDraft(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchDraft-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商查询草稿", tags = {"服务商评估" } ,notes = "根据服务商查询草稿")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchdraft")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchEMServiceEvlDraftByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchDraft(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商获取评估前五", tags = {"服务商评估" } ,notes = "根据服务商获取评估前五")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchevaluatetop5")
	public ResponseEntity<List<Map>> fetchEMServiceEvlEvaluateTop5ByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<Map> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商查询评估前五", tags = {"服务商评估" } ,notes = "根据服务商查询评估前五")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchevaluatetop5")
	public ResponseEntity<Page<Map>> searchEMServiceEvlEvaluateTop5ByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<Map> domains = emserviceevlService.searchEvaluateTop5(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
	@ApiOperation(value = "根据服务商获取OverallEVL", tags = {"服务商评估" } ,notes = "根据服务商获取OverallEVL")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchoverallevl")
	public ResponseEntity<List<Map>> fetchEMServiceEvlOverallEVLByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<Map> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(domains.getContent());
	}

	@ApiOperation(value = "根据服务商查询OverallEVL", tags = {"服务商评估" } ,notes = "根据服务商查询OverallEVL")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchoverallevl")
	public ResponseEntity<Page<Map>> searchEMServiceEvlOverallEVLByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<Map> domains = emserviceevlService.searchOverallEVL(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(domains.getContent(), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchToConfirm-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商获取待确认", tags = {"服务商评估" } ,notes = "根据服务商获取待确认")
    @RequestMapping(method= RequestMethod.GET , value="/emservices/{emservice_id}/emserviceevls/fetchtoconfirm")
	public ResponseEntity<List<EMServiceEvlDTO>> fetchEMServiceEvlToConfirmByEMService(@PathVariable("emservice_id") String emservice_id,EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchToConfirm(context) ;
        List<EMServiceEvlDTO> list = emserviceevlMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMServiceEvl-searchToConfirm-all') and hasPermission(#context,'eam-EMServiceEvl-Get')")
	@ApiOperation(value = "根据服务商查询待确认", tags = {"服务商评估" } ,notes = "根据服务商查询待确认")
    @RequestMapping(method= RequestMethod.POST , value="/emservices/{emservice_id}/emserviceevls/searchtoconfirm")
	public ResponseEntity<Page<EMServiceEvlDTO>> searchEMServiceEvlToConfirmByEMService(@PathVariable("emservice_id") String emservice_id, @RequestBody EMServiceEvlSearchContext context) {
        context.setN_serviceid_eq(emservice_id);
        Page<EMServiceEvl> domains = emserviceevlService.searchToConfirm(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emserviceevlMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

