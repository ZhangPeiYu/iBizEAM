package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMRFOCA;
import cn.ibizlab.eam.core.eam_core.service.IEMRFOCAService;
import cn.ibizlab.eam.core.eam_core.filter.EMRFOCASearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"原因" })
@RestController("WebApi-emrfoca")
@RequestMapping("")
public class EMRFOCAResource {

    @Autowired
    public IEMRFOCAService emrfocaService;

    @Autowired
    @Lazy
    public EMRFOCAMapping emrfocaMapping;

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "新建原因", tags = {"原因" },  notes = "新建原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfocas")
    public ResponseEntity<EMRFOCADTO> create(@Validated @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
		emrfocaService.create(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "批量新建原因", tags = {"原因" },  notes = "批量新建原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfocas/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMRFOCADTO> emrfocadtos) {
        emrfocaService.createBatch(emrfocaMapping.toDomain(emrfocadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoca" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "更新原因", tags = {"原因" },  notes = "更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> update(@PathVariable("emrfoca_id") String emrfoca_id, @RequestBody EMRFOCADTO emrfocadto) {
		EMRFOCA domain  = emrfocaMapping.toDomain(emrfocadto);
        domain .setEmrfocaid(emrfoca_id);
		emrfocaService.update(domain );
		EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByEntities(this.emrfocaMapping.toDomain(#emrfocadtos)),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "批量更新原因", tags = {"原因" },  notes = "批量更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfocas/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMRFOCADTO> emrfocadtos) {
        emrfocaService.updateBatch(emrfocaMapping.toDomain(emrfocadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "删除原因", tags = {"原因" },  notes = "删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfocas/{emrfoca_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emrfoca_id") String emrfoca_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emrfocaService.remove(emrfoca_id));
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByIds(#ids),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "批量删除原因", tags = {"原因" },  notes = "批量删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfocas/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emrfocaService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfocaMapping.toDomain(returnObject.body),'eam-EMRFOCA-Get')")
    @ApiOperation(value = "获取原因", tags = {"原因" },  notes = "获取原因")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> get(@PathVariable("emrfoca_id") String emrfoca_id) {
        EMRFOCA domain = emrfocaService.get(emrfoca_id);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取原因草稿", tags = {"原因" },  notes = "获取原因草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfocas/getdraft")
    public ResponseEntity<EMRFOCADTO> getDraft(EMRFOCADTO dto) {
        EMRFOCA domain = emrfocaMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(emrfocaService.getDraft(domain)));
    }

    @ApiOperation(value = "检查原因", tags = {"原因" },  notes = "检查原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfocas/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMRFOCADTO emrfocadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfocaService.checkKey(emrfocaMapping.toDomain(emrfocadto)));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "保存原因", tags = {"原因" },  notes = "保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfocas/save")
    public ResponseEntity<EMRFOCADTO> save(@RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        emrfocaService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "批量保存原因", tags = {"原因" },  notes = "批量保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfocas/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMRFOCADTO> emrfocadtos) {
        emrfocaService.saveBatch(emrfocaMapping.toDomain(emrfocadtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"原因" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfocas/fetchdefault")
	public ResponseEntity<List<EMRFOCADTO>> fetchDefault(EMRFOCASearchContext context) {
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
        List<EMRFOCADTO> list = emrfocaMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"原因" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfocas/searchdefault")
	public ResponseEntity<Page<EMRFOCADTO>> searchDefault(@RequestBody EMRFOCASearchContext context) {
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfocaMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据现象建立原因", tags = {"原因" },  notes = "根据现象建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfocas")
    public ResponseEntity<EMRFOCADTO> createByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfodeid(emrfode_id);
		emrfocaService.create(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据现象批量建立原因", tags = {"原因" },  notes = "根据现象批量建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfocas/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfocaService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoca" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据现象更新原因", tags = {"原因" },  notes = "根据现象更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> updateByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoca_id") String emrfoca_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfodeid(emrfode_id);
        domain.setEmrfocaid(emrfoca_id);
		emrfocaService.update(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByEntities(this.emrfocaMapping.toDomain(#emrfocadtos)),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据现象批量更新原因", tags = {"原因" },  notes = "根据现象批量更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfocas/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfodeid(emrfode_id);
        }
        emrfocaService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据现象删除原因", tags = {"原因" },  notes = "根据现象删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<Boolean> removeByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoca_id") String emrfoca_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfocaService.remove(emrfoca_id));
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByIds(#ids),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据现象批量删除原因", tags = {"原因" },  notes = "根据现象批量删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfocas/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODE(@RequestBody List<String> ids) {
        emrfocaService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfocaMapping.toDomain(returnObject.body),'eam-EMRFOCA-Get')")
    @ApiOperation(value = "根据现象获取原因", tags = {"原因" },  notes = "根据现象获取原因")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> getByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfoca_id") String emrfoca_id) {
        EMRFOCA domain = emrfocaService.get(emrfoca_id);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象获取原因草稿", tags = {"原因" },  notes = "根据现象获取原因草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfocas/getdraft")
    public ResponseEntity<EMRFOCADTO> getDraftByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, EMRFOCADTO dto) {
        EMRFOCA domain = emrfocaMapping.toDomain(dto);
        domain.setRfodeid(emrfode_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(emrfocaService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象检查原因", tags = {"原因" },  notes = "根据现象检查原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfocas/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOCADTO emrfocadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfocaService.checkKey(emrfocaMapping.toDomain(emrfocadto)));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据现象保存原因", tags = {"原因" },  notes = "根据现象保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfocas/save")
    public ResponseEntity<EMRFOCADTO> saveByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfodeid(emrfode_id);
        emrfocaService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据现象批量保存原因", tags = {"原因" },  notes = "根据现象批量保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfocas/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
             domain.setRfodeid(emrfode_id);
        }
        emrfocaService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据现象获取DEFAULT", tags = {"原因" } ,notes = "根据现象获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfocas/fetchdefault")
	public ResponseEntity<List<EMRFOCADTO>> fetchEMRFOCADefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id,EMRFOCASearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
        List<EMRFOCADTO> list = emrfocaMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据现象查询DEFAULT", tags = {"原因" } ,notes = "根据现象查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfocas/searchdefault")
	public ResponseEntity<Page<EMRFOCADTO>> searchEMRFOCADefaultByEMRFODE(@PathVariable("emrfode_id") String emrfode_id, @RequestBody EMRFOCASearchContext context) {
        context.setN_rfodeid_eq(emrfode_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfocaMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据模式建立原因", tags = {"原因" },  notes = "根据模式建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfocas")
    public ResponseEntity<EMRFOCADTO> createByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
		emrfocaService.create(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据模式批量建立原因", tags = {"原因" },  notes = "根据模式批量建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> createBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoca" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据模式更新原因", tags = {"原因" },  notes = "根据模式更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> updateByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
        domain.setEmrfocaid(emrfoca_id);
		emrfocaService.update(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByEntities(this.emrfocaMapping.toDomain(#emrfocadtos)),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据模式批量更新原因", tags = {"原因" },  notes = "根据模式批量更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据模式删除原因", tags = {"原因" },  notes = "根据模式删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<Boolean> removeByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfocaService.remove(emrfoca_id));
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByIds(#ids),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据模式批量删除原因", tags = {"原因" },  notes = "根据模式批量删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFOMO(@RequestBody List<String> ids) {
        emrfocaService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfocaMapping.toDomain(returnObject.body),'eam-EMRFOCA-Get')")
    @ApiOperation(value = "根据模式获取原因", tags = {"原因" },  notes = "根据模式获取原因")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> getByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id) {
        EMRFOCA domain = emrfocaService.get(emrfoca_id);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据模式获取原因草稿", tags = {"原因" },  notes = "根据模式获取原因草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfomos/{emrfomo_id}/emrfocas/getdraft")
    public ResponseEntity<EMRFOCADTO> getDraftByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, EMRFOCADTO dto) {
        EMRFOCA domain = emrfocaMapping.toDomain(dto);
        domain.setRfomoid(emrfomo_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(emrfocaService.getDraft(domain)));
    }

    @ApiOperation(value = "根据模式检查原因", tags = {"原因" },  notes = "根据模式检查原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfocas/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfocaService.checkKey(emrfocaMapping.toDomain(emrfocadto)));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据模式保存原因", tags = {"原因" },  notes = "根据模式保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfocas/save")
    public ResponseEntity<EMRFOCADTO> saveByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
        emrfocaService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据模式批量保存原因", tags = {"原因" },  notes = "根据模式批量保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfomos/{emrfomo_id}/emrfocas/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
             domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据模式获取DEFAULT", tags = {"原因" } ,notes = "根据模式获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfomos/{emrfomo_id}/emrfocas/fetchdefault")
	public ResponseEntity<List<EMRFOCADTO>> fetchEMRFOCADefaultByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id,EMRFOCASearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
        List<EMRFOCADTO> list = emrfocaMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据模式查询DEFAULT", tags = {"原因" } ,notes = "根据模式查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfomos/{emrfomo_id}/emrfocas/searchdefault")
	public ResponseEntity<Page<EMRFOCADTO>> searchEMRFOCADefaultByEMRFOMO(@PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCASearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfocaMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据现象模式建立原因", tags = {"原因" },  notes = "根据现象模式建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas")
    public ResponseEntity<EMRFOCADTO> createByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
		emrfocaService.create(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Create')")
    @ApiOperation(value = "根据现象模式批量建立原因", tags = {"原因" },  notes = "根据现象模式批量建立原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> createBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emrfoca" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据现象模式更新原因", tags = {"原因" },  notes = "根据现象模式更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> updateByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
        domain.setEmrfocaid(emrfoca_id);
		emrfocaService.update(domain);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByEntities(this.emrfocaMapping.toDomain(#emrfocadtos)),'eam-EMRFOCA-Update')")
    @ApiOperation(value = "根据现象模式批量更新原因", tags = {"原因" },  notes = "根据现象模式批量更新原因")
	@RequestMapping(method = RequestMethod.PUT, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> updateBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
            domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emrfocaService.get(#emrfoca_id),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据现象模式删除原因", tags = {"原因" },  notes = "根据现象模式删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<Boolean> removeByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emrfocaService.remove(emrfoca_id));
    }

    @PreAuthorize("hasPermission(this.emrfocaService.getEmrfocaByIds(#ids),'eam-EMRFOCA-Remove')")
    @ApiOperation(value = "根据现象模式批量删除原因", tags = {"原因" },  notes = "根据现象模式批量删除原因")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/batch")
    public ResponseEntity<Boolean> removeBatchByEMRFODEEMRFOMO(@RequestBody List<String> ids) {
        emrfocaService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emrfocaMapping.toDomain(returnObject.body),'eam-EMRFOCA-Get')")
    @ApiOperation(value = "根据现象模式获取原因", tags = {"原因" },  notes = "根据现象模式获取原因")
	@RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/{emrfoca_id}")
    public ResponseEntity<EMRFOCADTO> getByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @PathVariable("emrfoca_id") String emrfoca_id) {
        EMRFOCA domain = emrfocaService.get(emrfoca_id);
        EMRFOCADTO dto = emrfocaMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据现象模式获取原因草稿", tags = {"原因" },  notes = "根据现象模式获取原因草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/getdraft")
    public ResponseEntity<EMRFOCADTO> getDraftByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, EMRFOCADTO dto) {
        EMRFOCA domain = emrfocaMapping.toDomain(dto);
        domain.setRfomoid(emrfomo_id);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(emrfocaService.getDraft(domain)));
    }

    @ApiOperation(value = "根据现象模式检查原因", tags = {"原因" },  notes = "根据现象模式检查原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emrfocaService.checkKey(emrfocaMapping.toDomain(emrfocadto)));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadto),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据现象模式保存原因", tags = {"原因" },  notes = "根据现象模式保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/save")
    public ResponseEntity<EMRFOCADTO> saveByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCADTO emrfocadto) {
        EMRFOCA domain = emrfocaMapping.toDomain(emrfocadto);
        domain.setRfomoid(emrfomo_id);
        emrfocaService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emrfocaMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emrfocaMapping.toDomain(#emrfocadtos),'eam-EMRFOCA-Save')")
    @ApiOperation(value = "根据现象模式批量保存原因", tags = {"原因" },  notes = "根据现象模式批量保存原因")
	@RequestMapping(method = RequestMethod.POST, value = "/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody List<EMRFOCADTO> emrfocadtos) {
        List<EMRFOCA> domainlist=emrfocaMapping.toDomain(emrfocadtos);
        for(EMRFOCA domain:domainlist){
             domain.setRfomoid(emrfomo_id);
        }
        emrfocaService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据现象模式获取DEFAULT", tags = {"原因" } ,notes = "根据现象模式获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/fetchdefault")
	public ResponseEntity<List<EMRFOCADTO>> fetchEMRFOCADefaultByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id,EMRFOCASearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
        List<EMRFOCADTO> list = emrfocaMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMRFOCA-searchDefault-all') and hasPermission(#context,'eam-EMRFOCA-Get')")
	@ApiOperation(value = "根据现象模式查询DEFAULT", tags = {"原因" } ,notes = "根据现象模式查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emrfodes/{emrfode_id}/emrfomos/{emrfomo_id}/emrfocas/searchdefault")
	public ResponseEntity<Page<EMRFOCADTO>> searchEMRFOCADefaultByEMRFODEEMRFOMO(@PathVariable("emrfode_id") String emrfode_id, @PathVariable("emrfomo_id") String emrfomo_id, @RequestBody EMRFOCASearchContext context) {
        context.setN_rfomoid_eq(emrfomo_id);
        Page<EMRFOCA> domains = emrfocaService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emrfocaMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

