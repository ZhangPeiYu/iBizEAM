package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQDebug;
import cn.ibizlab.eam.core.eam_core.service.IEMEQDebugService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQDebugSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"事故记录" })
@RestController("WebApi-emeqdebug")
@RequestMapping("")
public class EMEQDebugResource {

    @Autowired
    public IEMEQDebugService emeqdebugService;

    @Autowired
    @Lazy
    public EMEQDebugMapping emeqdebugMapping;

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "新建事故记录", tags = {"事故记录" },  notes = "新建事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqdebugs")
    public ResponseEntity<EMEQDebugDTO> create(@Validated @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
		emeqdebugService.create(domain);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "批量新建事故记录", tags = {"事故记录" },  notes = "批量新建事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqdebugs/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        emeqdebugService.createBatch(emeqdebugMapping.toDomain(emeqdebugdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqdebug" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "更新事故记录", tags = {"事故记录" },  notes = "更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> update(@PathVariable("emeqdebug_id") String emeqdebug_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
		EMEQDebug domain  = emeqdebugMapping.toDomain(emeqdebugdto);
        domain .setEmeqdebugid(emeqdebug_id);
		emeqdebugService.update(domain );
		EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByEntities(this.emeqdebugMapping.toDomain(#emeqdebugdtos)),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "批量更新事故记录", tags = {"事故记录" },  notes = "批量更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqdebugs/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        emeqdebugService.updateBatch(emeqdebugMapping.toDomain(emeqdebugdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "删除事故记录", tags = {"事故记录" },  notes = "删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqdebug_id") String emeqdebug_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.remove(emeqdebug_id));
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByIds(#ids),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "批量删除事故记录", tags = {"事故记录" },  notes = "批量删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqdebugs/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqdebugService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqdebugMapping.toDomain(returnObject.body),'eam-EMEQDebug-Get')")
    @ApiOperation(value = "获取事故记录", tags = {"事故记录" },  notes = "获取事故记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> get(@PathVariable("emeqdebug_id") String emeqdebug_id) {
        EMEQDebug domain = emeqdebugService.get(emeqdebug_id);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取事故记录草稿", tags = {"事故记录" },  notes = "获取事故记录草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqdebugs/getdraft")
    public ResponseEntity<EMEQDebugDTO> getDraft(EMEQDebugDTO dto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(emeqdebugService.getDraft(domain)));
    }

    @ApiOperation(value = "检查事故记录", tags = {"事故记录" },  notes = "检查事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqdebugs/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQDebugDTO emeqdebugdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.checkKey(emeqdebugMapping.toDomain(emeqdebugdto)));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "保存事故记录", tags = {"事故记录" },  notes = "保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqdebugs/save")
    public ResponseEntity<EMEQDebugDTO> save(@RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        emeqdebugService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "批量保存事故记录", tags = {"事故记录" },  notes = "批量保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqdebugs/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        emeqdebugService.saveBatch(emeqdebugMapping.toDomain(emeqdebugdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "获取日历查询", tags = {"事故记录" } ,notes = "获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emeqdebugs/fetchcalendar")
	public ResponseEntity<List<EMEQDebugDTO>> fetchCalendar(EMEQDebugSearchContext context) {
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "查询日历查询", tags = {"事故记录" } ,notes = "查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emeqdebugs/searchcalendar")
	public ResponseEntity<Page<EMEQDebugDTO>> searchCalendar(@RequestBody EMEQDebugSearchContext context) {
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"事故记录" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqdebugs/fetchdefault")
	public ResponseEntity<List<EMEQDebugDTO>> fetchDefault(EMEQDebugSearchContext context) {
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"事故记录" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqdebugs/searchdefault")
	public ResponseEntity<Page<EMEQDebugDTO>> searchDefault(@RequestBody EMEQDebugSearchContext context) {
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "根据设备档案建立事故记录", tags = {"事故记录" },  notes = "根据设备档案建立事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqdebugs")
    public ResponseEntity<EMEQDebugDTO> createByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
		emeqdebugService.create(domain);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "根据设备档案批量建立事故记录", tags = {"事故记录" },  notes = "根据设备档案批量建立事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> createBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqdebugService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqdebug" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "根据设备档案更新事故记录", tags = {"事故记录" },  notes = "根据设备档案更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> updateByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqdebugid(emeqdebug_id);
		emeqdebugService.update(domain);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByEntities(this.emeqdebugMapping.toDomain(#emeqdebugdtos)),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "根据设备档案批量更新事故记录", tags = {"事故记录" },  notes = "根据设备档案批量更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> updateBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqdebugService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "根据设备档案删除事故记录", tags = {"事故记录" },  notes = "根据设备档案删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<Boolean> removeByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.remove(emeqdebug_id));
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByIds(#ids),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "根据设备档案批量删除事故记录", tags = {"事故记录" },  notes = "根据设备档案批量删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> removeBatchByEMEquip(@RequestBody List<String> ids) {
        emeqdebugService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqdebugMapping.toDomain(returnObject.body),'eam-EMEQDebug-Get')")
    @ApiOperation(value = "根据设备档案获取事故记录", tags = {"事故记录" },  notes = "根据设备档案获取事故记录")
	@RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> getByEMEquip(@PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id) {
        EMEQDebug domain = emeqdebugService.get(emeqdebug_id);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据设备档案获取事故记录草稿", tags = {"事故记录" },  notes = "根据设备档案获取事故记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/emequips/{emequip_id}/emeqdebugs/getdraft")
    public ResponseEntity<EMEQDebugDTO> getDraftByEMEquip(@PathVariable("emequip_id") String emequip_id, EMEQDebugDTO dto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(emeqdebugService.getDraft(domain)));
    }

    @ApiOperation(value = "根据设备档案检查事故记录", tags = {"事故记录" },  notes = "根据设备档案检查事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqdebugs/checkkey")
    public ResponseEntity<Boolean> checkKeyByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.checkKey(emeqdebugMapping.toDomain(emeqdebugdto)));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "根据设备档案保存事故记录", tags = {"事故记录" },  notes = "根据设备档案保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqdebugs/save")
    public ResponseEntity<EMEQDebugDTO> saveByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
        emeqdebugService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "根据设备档案批量保存事故记录", tags = {"事故记录" },  notes = "根据设备档案批量保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/emequips/{emequip_id}/emeqdebugs/savebatch")
    public ResponseEntity<Boolean> saveBatchByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqdebugService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据设备档案获取日历查询", tags = {"事故记录" } ,notes = "根据设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqdebugs/fetchcalendar")
	public ResponseEntity<List<EMEQDebugDTO>> fetchEMEQDebugCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据设备档案查询日历查询", tags = {"事故记录" } ,notes = "根据设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqdebugs/searchcalendar")
	public ResponseEntity<Page<EMEQDebugDTO>> searchEMEQDebugCalendarByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据设备档案获取DEFAULT", tags = {"事故记录" } ,notes = "根据设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emequips/{emequip_id}/emeqdebugs/fetchdefault")
	public ResponseEntity<List<EMEQDebugDTO>> fetchEMEQDebugDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id,EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据设备档案查询DEFAULT", tags = {"事故记录" } ,notes = "根据设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emequips/{emequip_id}/emeqdebugs/searchdefault")
	public ResponseEntity<Page<EMEQDebugDTO>> searchEMEQDebugDefaultByEMEquip(@PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "根据班组设备档案建立事故记录", tags = {"事故记录" },  notes = "根据班组设备档案建立事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs")
    public ResponseEntity<EMEQDebugDTO> createByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
		emeqdebugService.create(domain);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Create')")
    @ApiOperation(value = "根据班组设备档案批量建立事故记录", tags = {"事故记录" },  notes = "根据班组设备档案批量建立事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> createBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqdebugService.createBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqdebug" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "根据班组设备档案更新事故记录", tags = {"事故记录" },  notes = "根据班组设备档案更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> updateByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
        domain.setEmeqdebugid(emeqdebug_id);
		emeqdebugService.update(domain);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByEntities(this.emeqdebugMapping.toDomain(#emeqdebugdtos)),'eam-EMEQDebug-Update')")
    @ApiOperation(value = "根据班组设备档案批量更新事故记录", tags = {"事故记录" },  notes = "根据班组设备档案批量更新事故记录")
	@RequestMapping(method = RequestMethod.PUT, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> updateBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
            domain.setEquipid(emequip_id);
        }
        emeqdebugService.updateBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.get(#emeqdebug_id),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "根据班组设备档案删除事故记录", tags = {"事故记录" },  notes = "根据班组设备档案删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<Boolean> removeByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id) {
		return ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.remove(emeqdebug_id));
    }

    @PreAuthorize("hasPermission(this.emeqdebugService.getEmeqdebugByIds(#ids),'eam-EMEQDebug-Remove')")
    @ApiOperation(value = "根据班组设备档案批量删除事故记录", tags = {"事故记录" },  notes = "根据班组设备档案批量删除事故记录")
	@RequestMapping(method = RequestMethod.DELETE, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/batch")
    public ResponseEntity<Boolean> removeBatchByPFTeamEMEquip(@RequestBody List<String> ids) {
        emeqdebugService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqdebugMapping.toDomain(returnObject.body),'eam-EMEQDebug-Get')")
    @ApiOperation(value = "根据班组设备档案获取事故记录", tags = {"事故记录" },  notes = "根据班组设备档案获取事故记录")
	@RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/{emeqdebug_id}")
    public ResponseEntity<EMEQDebugDTO> getByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @PathVariable("emeqdebug_id") String emeqdebug_id) {
        EMEQDebug domain = emeqdebugService.get(emeqdebug_id);
        EMEQDebugDTO dto = emeqdebugMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "根据班组设备档案获取事故记录草稿", tags = {"事故记录" },  notes = "根据班组设备档案获取事故记录草稿")
    @RequestMapping(method = RequestMethod.GET, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/getdraft")
    public ResponseEntity<EMEQDebugDTO> getDraftByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, EMEQDebugDTO dto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(dto);
        domain.setEquipid(emequip_id);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(emeqdebugService.getDraft(domain)));
    }

    @ApiOperation(value = "根据班组设备档案检查事故记录", tags = {"事故记录" },  notes = "根据班组设备档案检查事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/checkkey")
    public ResponseEntity<Boolean> checkKeyByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqdebugService.checkKey(emeqdebugMapping.toDomain(emeqdebugdto)));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdto),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "根据班组设备档案保存事故记录", tags = {"事故记录" },  notes = "根据班组设备档案保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/save")
    public ResponseEntity<EMEQDebugDTO> saveByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugDTO emeqdebugdto) {
        EMEQDebug domain = emeqdebugMapping.toDomain(emeqdebugdto);
        domain.setEquipid(emequip_id);
        emeqdebugService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqdebugMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqdebugMapping.toDomain(#emeqdebugdtos),'eam-EMEQDebug-Save')")
    @ApiOperation(value = "根据班组设备档案批量保存事故记录", tags = {"事故记录" },  notes = "根据班组设备档案批量保存事故记录")
	@RequestMapping(method = RequestMethod.POST, value = "/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/savebatch")
    public ResponseEntity<Boolean> saveBatchByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody List<EMEQDebugDTO> emeqdebugdtos) {
        List<EMEQDebug> domainlist=emeqdebugMapping.toDomain(emeqdebugdtos);
        for(EMEQDebug domain:domainlist){
             domain.setEquipid(emequip_id);
        }
        emeqdebugService.saveBatch(domainlist);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据班组设备档案获取日历查询", tags = {"事故记录" } ,notes = "根据班组设备档案获取日历查询")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/fetchcalendar")
	public ResponseEntity<List<EMEQDebugDTO>> fetchEMEQDebugCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchCalendar-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据班组设备档案查询日历查询", tags = {"事故记录" } ,notes = "根据班组设备档案查询日历查询")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/searchcalendar")
	public ResponseEntity<Page<EMEQDebugDTO>> searchEMEQDebugCalendarByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchCalendar(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据班组设备档案获取DEFAULT", tags = {"事故记录" } ,notes = "根据班组设备档案获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/fetchdefault")
	public ResponseEntity<List<EMEQDebugDTO>> fetchEMEQDebugDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id,EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
        List<EMEQDebugDTO> list = emeqdebugMapping.toDto(domains.getContent());
	    return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQDebug-searchDefault-all') and hasPermission(#context,'eam-EMEQDebug-Get')")
	@ApiOperation(value = "根据班组设备档案查询DEFAULT", tags = {"事故记录" } ,notes = "根据班组设备档案查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/pfteams/{pfteam_id}/emequips/{emequip_id}/emeqdebugs/searchdefault")
	public ResponseEntity<Page<EMEQDebugDTO>> searchEMEQDebugDefaultByPFTeamEMEquip(@PathVariable("pfteam_id") String pfteam_id, @PathVariable("emequip_id") String emequip_id, @RequestBody EMEQDebugSearchContext context) {
        context.setN_equipid_eq(emequip_id);
        Page<EMEQDebug> domains = emeqdebugService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqdebugMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}
}

