package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_T;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_TService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_TSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"计划_定时" })
@RestController("WebApi-planschedule_t")
@RequestMapping("")
public class PLANSCHEDULE_TResource {

    @Autowired
    public IPLANSCHEDULE_TService planschedule_tService;

    @Autowired
    @Lazy
    public PLANSCHEDULE_TMapping planschedule_tMapping;

    @PreAuthorize("hasPermission(this.planschedule_tMapping.toDomain(#planschedule_tdto),'eam-PLANSCHEDULE_T-Create')")
    @ApiOperation(value = "新建计划_定时", tags = {"计划_定时" },  notes = "新建计划_定时")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ts")
    public ResponseEntity<PLANSCHEDULE_TDTO> create(@Validated @RequestBody PLANSCHEDULE_TDTO planschedule_tdto) {
        PLANSCHEDULE_T domain = planschedule_tMapping.toDomain(planschedule_tdto);
		planschedule_tService.create(domain);
        PLANSCHEDULE_TDTO dto = planschedule_tMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_tMapping.toDomain(#planschedule_tdtos),'eam-PLANSCHEDULE_T-Create')")
    @ApiOperation(value = "批量新建计划_定时", tags = {"计划_定时" },  notes = "批量新建计划_定时")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ts/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULE_TDTO> planschedule_tdtos) {
        planschedule_tService.createBatch(planschedule_tMapping.toDomain(planschedule_tdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule_t" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planschedule_tService.get(#planschedule_t_id),'eam-PLANSCHEDULE_T-Update')")
    @ApiOperation(value = "更新计划_定时", tags = {"计划_定时" },  notes = "更新计划_定时")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ts/{planschedule_t_id}")
    public ResponseEntity<PLANSCHEDULE_TDTO> update(@PathVariable("planschedule_t_id") String planschedule_t_id, @RequestBody PLANSCHEDULE_TDTO planschedule_tdto) {
		PLANSCHEDULE_T domain  = planschedule_tMapping.toDomain(planschedule_tdto);
        domain .setPlanscheduleTid(planschedule_t_id);
		planschedule_tService.update(domain );
		PLANSCHEDULE_TDTO dto = planschedule_tMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_tService.getPlanscheduleTByEntities(this.planschedule_tMapping.toDomain(#planschedule_tdtos)),'eam-PLANSCHEDULE_T-Update')")
    @ApiOperation(value = "批量更新计划_定时", tags = {"计划_定时" },  notes = "批量更新计划_定时")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_ts/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULE_TDTO> planschedule_tdtos) {
        planschedule_tService.updateBatch(planschedule_tMapping.toDomain(planschedule_tdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planschedule_tService.get(#planschedule_t_id),'eam-PLANSCHEDULE_T-Remove')")
    @ApiOperation(value = "删除计划_定时", tags = {"计划_定时" },  notes = "删除计划_定时")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ts/{planschedule_t_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_t_id") String planschedule_t_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planschedule_tService.remove(planschedule_t_id));
    }

    @PreAuthorize("hasPermission(this.planschedule_tService.getPlanscheduleTByIds(#ids),'eam-PLANSCHEDULE_T-Remove')")
    @ApiOperation(value = "批量删除计划_定时", tags = {"计划_定时" },  notes = "批量删除计划_定时")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_ts/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planschedule_tService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planschedule_tMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE_T-Get')")
    @ApiOperation(value = "获取计划_定时", tags = {"计划_定时" },  notes = "获取计划_定时")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ts/{planschedule_t_id}")
    public ResponseEntity<PLANSCHEDULE_TDTO> get(@PathVariable("planschedule_t_id") String planschedule_t_id) {
        PLANSCHEDULE_T domain = planschedule_tService.get(planschedule_t_id);
        PLANSCHEDULE_TDTO dto = planschedule_tMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取计划_定时草稿", tags = {"计划_定时" },  notes = "获取计划_定时草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_ts/getdraft")
    public ResponseEntity<PLANSCHEDULE_TDTO> getDraft(PLANSCHEDULE_TDTO dto) {
        PLANSCHEDULE_T domain = planschedule_tMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_tMapping.toDto(planschedule_tService.getDraft(domain)));
    }

    @ApiOperation(value = "检查计划_定时", tags = {"计划_定时" },  notes = "检查计划_定时")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ts/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULE_TDTO planschedule_tdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planschedule_tService.checkKey(planschedule_tMapping.toDomain(planschedule_tdto)));
    }

    @PreAuthorize("hasPermission(this.planschedule_tMapping.toDomain(#planschedule_tdto),'eam-PLANSCHEDULE_T-Save')")
    @ApiOperation(value = "保存计划_定时", tags = {"计划_定时" },  notes = "保存计划_定时")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ts/save")
    public ResponseEntity<PLANSCHEDULE_TDTO> save(@RequestBody PLANSCHEDULE_TDTO planschedule_tdto) {
        PLANSCHEDULE_T domain = planschedule_tMapping.toDomain(planschedule_tdto);
        planschedule_tService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_tMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planschedule_tMapping.toDomain(#planschedule_tdtos),'eam-PLANSCHEDULE_T-Save')")
    @ApiOperation(value = "批量保存计划_定时", tags = {"计划_定时" },  notes = "批量保存计划_定时")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_ts/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULE_TDTO> planschedule_tdtos) {
        planschedule_tService.saveBatch(planschedule_tMapping.toDomain(planschedule_tdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_T-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_T-Get')")
	@ApiOperation(value = "获取数据集", tags = {"计划_定时" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedule_ts/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULE_TDTO>> fetchDefault(PLANSCHEDULE_TSearchContext context) {
        Page<PLANSCHEDULE_T> domains = planschedule_tService.searchDefault(context) ;
        List<PLANSCHEDULE_TDTO> list = planschedule_tMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_T-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_T-Get')")
	@ApiOperation(value = "查询数据集", tags = {"计划_定时" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedule_ts/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULE_TDTO>> searchDefault(@RequestBody PLANSCHEDULE_TSearchContext context) {
        Page<PLANSCHEDULE_T> domains = planschedule_tService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planschedule_tMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

