package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMBerth;
import cn.ibizlab.eam.core.eam_core.service.IEMBerthService;
import cn.ibizlab.eam.core.eam_core.filter.EMBerthSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"泊位" })
@RestController("WebApi-emberth")
@RequestMapping("")
public class EMBerthResource {

    @Autowired
    public IEMBerthService emberthService;

    @Autowired
    @Lazy
    public EMBerthMapping emberthMapping;

    @PreAuthorize("hasPermission(this.emberthMapping.toDomain(#emberthdto),'eam-EMBerth-Create')")
    @ApiOperation(value = "新建泊位", tags = {"泊位" },  notes = "新建泊位")
	@RequestMapping(method = RequestMethod.POST, value = "/emberths")
    public ResponseEntity<EMBerthDTO> create(@Validated @RequestBody EMBerthDTO emberthdto) {
        EMBerth domain = emberthMapping.toDomain(emberthdto);
		emberthService.create(domain);
        EMBerthDTO dto = emberthMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emberthMapping.toDomain(#emberthdtos),'eam-EMBerth-Create')")
    @ApiOperation(value = "批量新建泊位", tags = {"泊位" },  notes = "批量新建泊位")
	@RequestMapping(method = RequestMethod.POST, value = "/emberths/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMBerthDTO> emberthdtos) {
        emberthService.createBatch(emberthMapping.toDomain(emberthdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emberth" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emberthService.get(#emberth_id),'eam-EMBerth-Update')")
    @ApiOperation(value = "更新泊位", tags = {"泊位" },  notes = "更新泊位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emberths/{emberth_id}")
    public ResponseEntity<EMBerthDTO> update(@PathVariable("emberth_id") String emberth_id, @RequestBody EMBerthDTO emberthdto) {
		EMBerth domain  = emberthMapping.toDomain(emberthdto);
        domain .setEmberthid(emberth_id);
		emberthService.update(domain );
		EMBerthDTO dto = emberthMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emberthService.getEmberthByEntities(this.emberthMapping.toDomain(#emberthdtos)),'eam-EMBerth-Update')")
    @ApiOperation(value = "批量更新泊位", tags = {"泊位" },  notes = "批量更新泊位")
	@RequestMapping(method = RequestMethod.PUT, value = "/emberths/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMBerthDTO> emberthdtos) {
        emberthService.updateBatch(emberthMapping.toDomain(emberthdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emberthService.get(#emberth_id),'eam-EMBerth-Remove')")
    @ApiOperation(value = "删除泊位", tags = {"泊位" },  notes = "删除泊位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emberths/{emberth_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emberth_id") String emberth_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emberthService.remove(emberth_id));
    }

    @PreAuthorize("hasPermission(this.emberthService.getEmberthByIds(#ids),'eam-EMBerth-Remove')")
    @ApiOperation(value = "批量删除泊位", tags = {"泊位" },  notes = "批量删除泊位")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emberths/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emberthService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emberthMapping.toDomain(returnObject.body),'eam-EMBerth-Get')")
    @ApiOperation(value = "获取泊位", tags = {"泊位" },  notes = "获取泊位")
	@RequestMapping(method = RequestMethod.GET, value = "/emberths/{emberth_id}")
    public ResponseEntity<EMBerthDTO> get(@PathVariable("emberth_id") String emberth_id) {
        EMBerth domain = emberthService.get(emberth_id);
        EMBerthDTO dto = emberthMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取泊位草稿", tags = {"泊位" },  notes = "获取泊位草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emberths/getdraft")
    public ResponseEntity<EMBerthDTO> getDraft(EMBerthDTO dto) {
        EMBerth domain = emberthMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emberthMapping.toDto(emberthService.getDraft(domain)));
    }

    @ApiOperation(value = "检查泊位", tags = {"泊位" },  notes = "检查泊位")
	@RequestMapping(method = RequestMethod.POST, value = "/emberths/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMBerthDTO emberthdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emberthService.checkKey(emberthMapping.toDomain(emberthdto)));
    }

    @PreAuthorize("hasPermission(this.emberthMapping.toDomain(#emberthdto),'eam-EMBerth-Save')")
    @ApiOperation(value = "保存泊位", tags = {"泊位" },  notes = "保存泊位")
	@RequestMapping(method = RequestMethod.POST, value = "/emberths/save")
    public ResponseEntity<EMBerthDTO> save(@RequestBody EMBerthDTO emberthdto) {
        EMBerth domain = emberthMapping.toDomain(emberthdto);
        emberthService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emberthMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emberthMapping.toDomain(#emberthdtos),'eam-EMBerth-Save')")
    @ApiOperation(value = "批量保存泊位", tags = {"泊位" },  notes = "批量保存泊位")
	@RequestMapping(method = RequestMethod.POST, value = "/emberths/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMBerthDTO> emberthdtos) {
        emberthService.saveBatch(emberthMapping.toDomain(emberthdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBerth-searchDefault-all') and hasPermission(#context,'eam-EMBerth-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"泊位" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emberths/fetchdefault")
	public ResponseEntity<List<EMBerthDTO>> fetchDefault(EMBerthSearchContext context) {
        Page<EMBerth> domains = emberthService.searchDefault(context) ;
        List<EMBerthDTO> list = emberthMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMBerth-searchDefault-all') and hasPermission(#context,'eam-EMBerth-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"泊位" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emberths/searchdefault")
	public ResponseEntity<Page<EMBerthDTO>> searchDefault(@RequestBody EMBerthSearchContext context) {
        Page<EMBerth> domains = emberthService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emberthMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

