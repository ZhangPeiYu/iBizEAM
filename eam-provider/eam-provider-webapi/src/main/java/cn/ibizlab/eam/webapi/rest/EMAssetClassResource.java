package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMAssetClass;
import cn.ibizlab.eam.core.eam_core.service.IEMAssetClassService;
import cn.ibizlab.eam.core.eam_core.filter.EMAssetClassSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"资产类别" })
@RestController("WebApi-emassetclass")
@RequestMapping("")
public class EMAssetClassResource {

    @Autowired
    public IEMAssetClassService emassetclassService;

    @Autowired
    @Lazy
    public EMAssetClassMapping emassetclassMapping;

    @PreAuthorize("hasPermission(this.emassetclassMapping.toDomain(#emassetclassdto),'eam-EMAssetClass-Create')")
    @ApiOperation(value = "新建资产类别", tags = {"资产类别" },  notes = "新建资产类别")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclasses")
    public ResponseEntity<EMAssetClassDTO> create(@Validated @RequestBody EMAssetClassDTO emassetclassdto) {
        EMAssetClass domain = emassetclassMapping.toDomain(emassetclassdto);
		emassetclassService.create(domain);
        EMAssetClassDTO dto = emassetclassMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclassMapping.toDomain(#emassetclassdtos),'eam-EMAssetClass-Create')")
    @ApiOperation(value = "批量新建资产类别", tags = {"资产类别" },  notes = "批量新建资产类别")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclasses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMAssetClassDTO> emassetclassdtos) {
        emassetclassService.createBatch(emassetclassMapping.toDomain(emassetclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emassetclass" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emassetclassService.get(#emassetclass_id),'eam-EMAssetClass-Update')")
    @ApiOperation(value = "更新资产类别", tags = {"资产类别" },  notes = "更新资产类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassetclasses/{emassetclass_id}")
    public ResponseEntity<EMAssetClassDTO> update(@PathVariable("emassetclass_id") String emassetclass_id, @RequestBody EMAssetClassDTO emassetclassdto) {
		EMAssetClass domain  = emassetclassMapping.toDomain(emassetclassdto);
        domain .setEmassetclassid(emassetclass_id);
		emassetclassService.update(domain );
		EMAssetClassDTO dto = emassetclassMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emassetclassService.getEmassetclassByEntities(this.emassetclassMapping.toDomain(#emassetclassdtos)),'eam-EMAssetClass-Update')")
    @ApiOperation(value = "批量更新资产类别", tags = {"资产类别" },  notes = "批量更新资产类别")
	@RequestMapping(method = RequestMethod.PUT, value = "/emassetclasses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMAssetClassDTO> emassetclassdtos) {
        emassetclassService.updateBatch(emassetclassMapping.toDomain(emassetclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emassetclassService.get(#emassetclass_id),'eam-EMAssetClass-Remove')")
    @ApiOperation(value = "删除资产类别", tags = {"资产类别" },  notes = "删除资产类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassetclasses/{emassetclass_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emassetclass_id") String emassetclass_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emassetclassService.remove(emassetclass_id));
    }

    @PreAuthorize("hasPermission(this.emassetclassService.getEmassetclassByIds(#ids),'eam-EMAssetClass-Remove')")
    @ApiOperation(value = "批量删除资产类别", tags = {"资产类别" },  notes = "批量删除资产类别")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emassetclasses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emassetclassService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emassetclassMapping.toDomain(returnObject.body),'eam-EMAssetClass-Get')")
    @ApiOperation(value = "获取资产类别", tags = {"资产类别" },  notes = "获取资产类别")
	@RequestMapping(method = RequestMethod.GET, value = "/emassetclasses/{emassetclass_id}")
    public ResponseEntity<EMAssetClassDTO> get(@PathVariable("emassetclass_id") String emassetclass_id) {
        EMAssetClass domain = emassetclassService.get(emassetclass_id);
        EMAssetClassDTO dto = emassetclassMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取资产类别草稿", tags = {"资产类别" },  notes = "获取资产类别草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emassetclasses/getdraft")
    public ResponseEntity<EMAssetClassDTO> getDraft(EMAssetClassDTO dto) {
        EMAssetClass domain = emassetclassMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclassMapping.toDto(emassetclassService.getDraft(domain)));
    }

    @ApiOperation(value = "检查资产类别", tags = {"资产类别" },  notes = "检查资产类别")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclasses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMAssetClassDTO emassetclassdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emassetclassService.checkKey(emassetclassMapping.toDomain(emassetclassdto)));
    }

    @PreAuthorize("hasPermission(this.emassetclassMapping.toDomain(#emassetclassdto),'eam-EMAssetClass-Save')")
    @ApiOperation(value = "保存资产类别", tags = {"资产类别" },  notes = "保存资产类别")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclasses/save")
    public ResponseEntity<EMAssetClassDTO> save(@RequestBody EMAssetClassDTO emassetclassdto) {
        EMAssetClass domain = emassetclassMapping.toDomain(emassetclassdto);
        emassetclassService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emassetclassMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emassetclassMapping.toDomain(#emassetclassdtos),'eam-EMAssetClass-Save')")
    @ApiOperation(value = "批量保存资产类别", tags = {"资产类别" },  notes = "批量保存资产类别")
	@RequestMapping(method = RequestMethod.POST, value = "/emassetclasses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMAssetClassDTO> emassetclassdtos) {
        emassetclassService.saveBatch(emassetclassMapping.toDomain(emassetclassdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClass-searchDefault-all') and hasPermission(#context,'eam-EMAssetClass-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"资产类别" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emassetclasses/fetchdefault")
	public ResponseEntity<List<EMAssetClassDTO>> fetchDefault(EMAssetClassSearchContext context) {
        Page<EMAssetClass> domains = emassetclassService.searchDefault(context) ;
        List<EMAssetClassDTO> list = emassetclassMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMAssetClass-searchDefault-all') and hasPermission(#context,'eam-EMAssetClass-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"资产类别" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emassetclasses/searchdefault")
	public ResponseEntity<Page<EMAssetClassDTO>> searchDefault(@RequestBody EMAssetClassSearchContext context) {
        Page<EMAssetClass> domains = emassetclassService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emassetclassMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

