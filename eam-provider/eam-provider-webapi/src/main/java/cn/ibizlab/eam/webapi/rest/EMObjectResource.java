package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMObject;
import cn.ibizlab.eam.core.eam_core.service.IEMObjectService;
import cn.ibizlab.eam.core.eam_core.filter.EMObjectSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"对象" })
@RestController("WebApi-emobject")
@RequestMapping("")
public class EMObjectResource {

    @Autowired
    public IEMObjectService emobjectService;

    @Autowired
    @Lazy
    public EMObjectMapping emobjectMapping;

    @PreAuthorize("hasPermission(this.emobjectMapping.toDomain(#emobjectdto),'eam-EMObject-Create')")
    @ApiOperation(value = "新建对象", tags = {"对象" },  notes = "新建对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjects")
    public ResponseEntity<EMObjectDTO> create(@Validated @RequestBody EMObjectDTO emobjectdto) {
        EMObject domain = emobjectMapping.toDomain(emobjectdto);
		emobjectService.create(domain);
        EMObjectDTO dto = emobjectMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emobjectMapping.toDomain(#emobjectdtos),'eam-EMObject-Create')")
    @ApiOperation(value = "批量新建对象", tags = {"对象" },  notes = "批量新建对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjects/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMObjectDTO> emobjectdtos) {
        emobjectService.createBatch(emobjectMapping.toDomain(emobjectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emobject" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emobjectService.get(#emobject_id),'eam-EMObject-Update')")
    @ApiOperation(value = "更新对象", tags = {"对象" },  notes = "更新对象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emobjects/{emobject_id}")
    public ResponseEntity<EMObjectDTO> update(@PathVariable("emobject_id") String emobject_id, @RequestBody EMObjectDTO emobjectdto) {
		EMObject domain  = emobjectMapping.toDomain(emobjectdto);
        domain .setEmobjectid(emobject_id);
		emobjectService.update(domain );
		EMObjectDTO dto = emobjectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emobjectService.getEmobjectByEntities(this.emobjectMapping.toDomain(#emobjectdtos)),'eam-EMObject-Update')")
    @ApiOperation(value = "批量更新对象", tags = {"对象" },  notes = "批量更新对象")
	@RequestMapping(method = RequestMethod.PUT, value = "/emobjects/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMObjectDTO> emobjectdtos) {
        emobjectService.updateBatch(emobjectMapping.toDomain(emobjectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emobjectService.get(#emobject_id),'eam-EMObject-Remove')")
    @ApiOperation(value = "删除对象", tags = {"对象" },  notes = "删除对象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emobjects/{emobject_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emobject_id") String emobject_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emobjectService.remove(emobject_id));
    }

    @PreAuthorize("hasPermission(this.emobjectService.getEmobjectByIds(#ids),'eam-EMObject-Remove')")
    @ApiOperation(value = "批量删除对象", tags = {"对象" },  notes = "批量删除对象")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emobjects/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emobjectService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emobjectMapping.toDomain(returnObject.body),'eam-EMObject-Get')")
    @ApiOperation(value = "获取对象", tags = {"对象" },  notes = "获取对象")
	@RequestMapping(method = RequestMethod.GET, value = "/emobjects/{emobject_id}")
    public ResponseEntity<EMObjectDTO> get(@PathVariable("emobject_id") String emobject_id) {
        EMObject domain = emobjectService.get(emobject_id);
        EMObjectDTO dto = emobjectMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取对象草稿", tags = {"对象" },  notes = "获取对象草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emobjects/getdraft")
    public ResponseEntity<EMObjectDTO> getDraft(EMObjectDTO dto) {
        EMObject domain = emobjectMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emobjectMapping.toDto(emobjectService.getDraft(domain)));
    }

    @ApiOperation(value = "检查对象", tags = {"对象" },  notes = "检查对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjects/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMObjectDTO emobjectdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emobjectService.checkKey(emobjectMapping.toDomain(emobjectdto)));
    }

    @PreAuthorize("hasPermission(this.emobjectMapping.toDomain(#emobjectdto),'eam-EMObject-Save')")
    @ApiOperation(value = "保存对象", tags = {"对象" },  notes = "保存对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjects/save")
    public ResponseEntity<EMObjectDTO> save(@RequestBody EMObjectDTO emobjectdto) {
        EMObject domain = emobjectMapping.toDomain(emobjectdto);
        emobjectService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emobjectMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emobjectMapping.toDomain(#emobjectdtos),'eam-EMObject-Save')")
    @ApiOperation(value = "批量保存对象", tags = {"对象" },  notes = "批量保存对象")
	@RequestMapping(method = RequestMethod.POST, value = "/emobjects/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMObjectDTO> emobjectdtos) {
        emobjectService.saveBatch(emobjectMapping.toDomain(emobjectdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObject-searchDefault-all') and hasPermission(#context,'eam-EMObject-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"对象" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emobjects/fetchdefault")
	public ResponseEntity<List<EMObjectDTO>> fetchDefault(EMObjectSearchContext context) {
        Page<EMObject> domains = emobjectService.searchDefault(context) ;
        List<EMObjectDTO> list = emobjectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObject-searchDefault-all') and hasPermission(#context,'eam-EMObject-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"对象" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emobjects/searchdefault")
	public ResponseEntity<Page<EMObjectDTO>> searchDefault(@RequestBody EMObjectSearchContext context) {
        Page<EMObject> domains = emobjectService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObject-searchIndexDER-all') and hasPermission(#context,'eam-EMObject-Get')")
	@ApiOperation(value = "获取IndexDER", tags = {"对象" } ,notes = "获取IndexDER")
    @RequestMapping(method= RequestMethod.GET , value="/emobjects/fetchindexder")
	public ResponseEntity<List<EMObjectDTO>> fetchIndexDER(EMObjectSearchContext context) {
        Page<EMObject> domains = emobjectService.searchIndexDER(context) ;
        List<EMObjectDTO> list = emobjectMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMObject-searchIndexDER-all') and hasPermission(#context,'eam-EMObject-Get')")
	@ApiOperation(value = "查询IndexDER", tags = {"对象" } ,notes = "查询IndexDER")
    @RequestMapping(method= RequestMethod.POST , value="/emobjects/searchindexder")
	public ResponseEntity<Page<EMObjectDTO>> searchIndexDER(@RequestBody EMObjectSearchContext context) {
        Page<EMObject> domains = emobjectService.searchIndexDER(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emobjectMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

