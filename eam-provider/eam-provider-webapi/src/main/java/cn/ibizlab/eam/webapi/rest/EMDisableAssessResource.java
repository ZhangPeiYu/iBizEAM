package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMDisableAssess;
import cn.ibizlab.eam.core.eam_core.service.IEMDisableAssessService;
import cn.ibizlab.eam.core.eam_core.filter.EMDisableAssessSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"设备停用考核" })
@RestController("WebApi-emdisableassess")
@RequestMapping("")
public class EMDisableAssessResource {

    @Autowired
    public IEMDisableAssessService emdisableassessService;

    @Autowired
    @Lazy
    public EMDisableAssessMapping emdisableassessMapping;

    @PreAuthorize("hasPermission(this.emdisableassessMapping.toDomain(#emdisableassessdto),'eam-EMDisableAssess-Create')")
    @ApiOperation(value = "新建设备停用考核", tags = {"设备停用考核" },  notes = "新建设备停用考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emdisableassesses")
    public ResponseEntity<EMDisableAssessDTO> create(@Validated @RequestBody EMDisableAssessDTO emdisableassessdto) {
        EMDisableAssess domain = emdisableassessMapping.toDomain(emdisableassessdto);
		emdisableassessService.create(domain);
        EMDisableAssessDTO dto = emdisableassessMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdisableassessMapping.toDomain(#emdisableassessdtos),'eam-EMDisableAssess-Create')")
    @ApiOperation(value = "批量新建设备停用考核", tags = {"设备停用考核" },  notes = "批量新建设备停用考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emdisableassesses/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMDisableAssessDTO> emdisableassessdtos) {
        emdisableassessService.createBatch(emdisableassessMapping.toDomain(emdisableassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emdisableassess" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emdisableassessService.get(#emdisableassess_id),'eam-EMDisableAssess-Update')")
    @ApiOperation(value = "更新设备停用考核", tags = {"设备停用考核" },  notes = "更新设备停用考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdisableassesses/{emdisableassess_id}")
    public ResponseEntity<EMDisableAssessDTO> update(@PathVariable("emdisableassess_id") String emdisableassess_id, @RequestBody EMDisableAssessDTO emdisableassessdto) {
		EMDisableAssess domain  = emdisableassessMapping.toDomain(emdisableassessdto);
        domain .setEmdisableassessid(emdisableassess_id);
		emdisableassessService.update(domain );
		EMDisableAssessDTO dto = emdisableassessMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emdisableassessService.getEmdisableassessByEntities(this.emdisableassessMapping.toDomain(#emdisableassessdtos)),'eam-EMDisableAssess-Update')")
    @ApiOperation(value = "批量更新设备停用考核", tags = {"设备停用考核" },  notes = "批量更新设备停用考核")
	@RequestMapping(method = RequestMethod.PUT, value = "/emdisableassesses/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMDisableAssessDTO> emdisableassessdtos) {
        emdisableassessService.updateBatch(emdisableassessMapping.toDomain(emdisableassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emdisableassessService.get(#emdisableassess_id),'eam-EMDisableAssess-Remove')")
    @ApiOperation(value = "删除设备停用考核", tags = {"设备停用考核" },  notes = "删除设备停用考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdisableassesses/{emdisableassess_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emdisableassess_id") String emdisableassess_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emdisableassessService.remove(emdisableassess_id));
    }

    @PreAuthorize("hasPermission(this.emdisableassessService.getEmdisableassessByIds(#ids),'eam-EMDisableAssess-Remove')")
    @ApiOperation(value = "批量删除设备停用考核", tags = {"设备停用考核" },  notes = "批量删除设备停用考核")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emdisableassesses/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emdisableassessService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emdisableassessMapping.toDomain(returnObject.body),'eam-EMDisableAssess-Get')")
    @ApiOperation(value = "获取设备停用考核", tags = {"设备停用考核" },  notes = "获取设备停用考核")
	@RequestMapping(method = RequestMethod.GET, value = "/emdisableassesses/{emdisableassess_id}")
    public ResponseEntity<EMDisableAssessDTO> get(@PathVariable("emdisableassess_id") String emdisableassess_id) {
        EMDisableAssess domain = emdisableassessService.get(emdisableassess_id);
        EMDisableAssessDTO dto = emdisableassessMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取设备停用考核草稿", tags = {"设备停用考核" },  notes = "获取设备停用考核草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emdisableassesses/getdraft")
    public ResponseEntity<EMDisableAssessDTO> getDraft(EMDisableAssessDTO dto) {
        EMDisableAssess domain = emdisableassessMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emdisableassessMapping.toDto(emdisableassessService.getDraft(domain)));
    }

    @ApiOperation(value = "检查设备停用考核", tags = {"设备停用考核" },  notes = "检查设备停用考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emdisableassesses/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMDisableAssessDTO emdisableassessdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emdisableassessService.checkKey(emdisableassessMapping.toDomain(emdisableassessdto)));
    }

    @PreAuthorize("hasPermission(this.emdisableassessMapping.toDomain(#emdisableassessdto),'eam-EMDisableAssess-Save')")
    @ApiOperation(value = "保存设备停用考核", tags = {"设备停用考核" },  notes = "保存设备停用考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emdisableassesses/save")
    public ResponseEntity<EMDisableAssessDTO> save(@RequestBody EMDisableAssessDTO emdisableassessdto) {
        EMDisableAssess domain = emdisableassessMapping.toDomain(emdisableassessdto);
        emdisableassessService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emdisableassessMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emdisableassessMapping.toDomain(#emdisableassessdtos),'eam-EMDisableAssess-Save')")
    @ApiOperation(value = "批量保存设备停用考核", tags = {"设备停用考核" },  notes = "批量保存设备停用考核")
	@RequestMapping(method = RequestMethod.POST, value = "/emdisableassesses/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMDisableAssessDTO> emdisableassessdtos) {
        emdisableassessService.saveBatch(emdisableassessMapping.toDomain(emdisableassessdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDisableAssess-searchDefault-all') and hasPermission(#context,'eam-EMDisableAssess-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"设备停用考核" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emdisableassesses/fetchdefault")
	public ResponseEntity<List<EMDisableAssessDTO>> fetchDefault(EMDisableAssessSearchContext context) {
        Page<EMDisableAssess> domains = emdisableassessService.searchDefault(context) ;
        List<EMDisableAssessDTO> list = emdisableassessMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMDisableAssess-searchDefault-all') and hasPermission(#context,'eam-EMDisableAssess-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"设备停用考核" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emdisableassesses/searchdefault")
	public ResponseEntity<Page<EMDisableAssessDTO>> searchDefault(@RequestBody EMDisableAssessSearchContext context) {
        Page<EMDisableAssess> domains = emdisableassessService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emdisableassessMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

