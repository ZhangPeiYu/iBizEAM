package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.EMEQLCTTIRes;
import cn.ibizlab.eam.core.eam_core.service.IEMEQLCTTIResService;
import cn.ibizlab.eam.core.eam_core.filter.EMEQLCTTIResSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"轮胎位置" })
@RestController("WebApi-emeqlcttires")
@RequestMapping("")
public class EMEQLCTTIResResource {

    @Autowired
    public IEMEQLCTTIResService emeqlcttiresService;

    @Autowired
    @Lazy
    public EMEQLCTTIResMapping emeqlcttiresMapping;

    @PreAuthorize("hasPermission(this.emeqlcttiresMapping.toDomain(#emeqlcttiresdto),'eam-EMEQLCTTIRes-Create')")
    @ApiOperation(value = "新建轮胎位置", tags = {"轮胎位置" },  notes = "新建轮胎位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlcttires")
    public ResponseEntity<EMEQLCTTIResDTO> create(@Validated @RequestBody EMEQLCTTIResDTO emeqlcttiresdto) {
        EMEQLCTTIRes domain = emeqlcttiresMapping.toDomain(emeqlcttiresdto);
		emeqlcttiresService.create(domain);
        EMEQLCTTIResDTO dto = emeqlcttiresMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresMapping.toDomain(#emeqlcttiresdtos),'eam-EMEQLCTTIRes-Create')")
    @ApiOperation(value = "批量新建轮胎位置", tags = {"轮胎位置" },  notes = "批量新建轮胎位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlcttires/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<EMEQLCTTIResDTO> emeqlcttiresdtos) {
        emeqlcttiresService.createBatch(emeqlcttiresMapping.toDomain(emeqlcttiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "emeqlcttires" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.emeqlcttiresService.get(#emeqlcttires_id),'eam-EMEQLCTTIRes-Update')")
    @ApiOperation(value = "更新轮胎位置", tags = {"轮胎位置" },  notes = "更新轮胎位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlcttires/{emeqlcttires_id}")
    public ResponseEntity<EMEQLCTTIResDTO> update(@PathVariable("emeqlcttires_id") String emeqlcttires_id, @RequestBody EMEQLCTTIResDTO emeqlcttiresdto) {
		EMEQLCTTIRes domain  = emeqlcttiresMapping.toDomain(emeqlcttiresdto);
        domain .setEmeqlocationid(emeqlcttires_id);
		emeqlcttiresService.update(domain );
		EMEQLCTTIResDTO dto = emeqlcttiresMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresService.getEmeqlcttiresByEntities(this.emeqlcttiresMapping.toDomain(#emeqlcttiresdtos)),'eam-EMEQLCTTIRes-Update')")
    @ApiOperation(value = "批量更新轮胎位置", tags = {"轮胎位置" },  notes = "批量更新轮胎位置")
	@RequestMapping(method = RequestMethod.PUT, value = "/emeqlcttires/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<EMEQLCTTIResDTO> emeqlcttiresdtos) {
        emeqlcttiresService.updateBatch(emeqlcttiresMapping.toDomain(emeqlcttiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresService.get(#emeqlcttires_id),'eam-EMEQLCTTIRes-Remove')")
    @ApiOperation(value = "删除轮胎位置", tags = {"轮胎位置" },  notes = "删除轮胎位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlcttires/{emeqlcttires_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("emeqlcttires_id") String emeqlcttires_id) {
         return ResponseEntity.status(HttpStatus.OK).body(emeqlcttiresService.remove(emeqlcttires_id));
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresService.getEmeqlcttiresByIds(#ids),'eam-EMEQLCTTIRes-Remove')")
    @ApiOperation(value = "批量删除轮胎位置", tags = {"轮胎位置" },  notes = "批量删除轮胎位置")
	@RequestMapping(method = RequestMethod.DELETE, value = "/emeqlcttires/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        emeqlcttiresService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.emeqlcttiresMapping.toDomain(returnObject.body),'eam-EMEQLCTTIRes-Get')")
    @ApiOperation(value = "获取轮胎位置", tags = {"轮胎位置" },  notes = "获取轮胎位置")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlcttires/{emeqlcttires_id}")
    public ResponseEntity<EMEQLCTTIResDTO> get(@PathVariable("emeqlcttires_id") String emeqlcttires_id) {
        EMEQLCTTIRes domain = emeqlcttiresService.get(emeqlcttires_id);
        EMEQLCTTIResDTO dto = emeqlcttiresMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取轮胎位置草稿", tags = {"轮胎位置" },  notes = "获取轮胎位置草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/emeqlcttires/getdraft")
    public ResponseEntity<EMEQLCTTIResDTO> getDraft(EMEQLCTTIResDTO dto) {
        EMEQLCTTIRes domain = emeqlcttiresMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlcttiresMapping.toDto(emeqlcttiresService.getDraft(domain)));
    }

    @ApiOperation(value = "检查轮胎位置", tags = {"轮胎位置" },  notes = "检查轮胎位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlcttires/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody EMEQLCTTIResDTO emeqlcttiresdto) {
        return  ResponseEntity.status(HttpStatus.OK).body(emeqlcttiresService.checkKey(emeqlcttiresMapping.toDomain(emeqlcttiresdto)));
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresMapping.toDomain(#emeqlcttiresdto),'eam-EMEQLCTTIRes-Save')")
    @ApiOperation(value = "保存轮胎位置", tags = {"轮胎位置" },  notes = "保存轮胎位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlcttires/save")
    public ResponseEntity<EMEQLCTTIResDTO> save(@RequestBody EMEQLCTTIResDTO emeqlcttiresdto) {
        EMEQLCTTIRes domain = emeqlcttiresMapping.toDomain(emeqlcttiresdto);
        emeqlcttiresService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(emeqlcttiresMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.emeqlcttiresMapping.toDomain(#emeqlcttiresdtos),'eam-EMEQLCTTIRes-Save')")
    @ApiOperation(value = "批量保存轮胎位置", tags = {"轮胎位置" },  notes = "批量保存轮胎位置")
	@RequestMapping(method = RequestMethod.POST, value = "/emeqlcttires/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<EMEQLCTTIResDTO> emeqlcttiresdtos) {
        emeqlcttiresService.saveBatch(emeqlcttiresMapping.toDomain(emeqlcttiresdtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTTIRes-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTTIRes-Get')")
	@ApiOperation(value = "获取DEFAULT", tags = {"轮胎位置" } ,notes = "获取DEFAULT")
    @RequestMapping(method= RequestMethod.GET , value="/emeqlcttires/fetchdefault")
	public ResponseEntity<List<EMEQLCTTIResDTO>> fetchDefault(EMEQLCTTIResSearchContext context) {
        Page<EMEQLCTTIRes> domains = emeqlcttiresService.searchDefault(context) ;
        List<EMEQLCTTIResDTO> list = emeqlcttiresMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-EMEQLCTTIRes-searchDefault-all') and hasPermission(#context,'eam-EMEQLCTTIRes-Get')")
	@ApiOperation(value = "查询DEFAULT", tags = {"轮胎位置" } ,notes = "查询DEFAULT")
    @RequestMapping(method= RequestMethod.POST , value="/emeqlcttires/searchdefault")
	public ResponseEntity<Page<EMEQLCTTIResDTO>> searchDefault(@RequestBody EMEQLCTTIResSearchContext context) {
        Page<EMEQLCTTIRes> domains = emeqlcttiresService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(emeqlcttiresMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

