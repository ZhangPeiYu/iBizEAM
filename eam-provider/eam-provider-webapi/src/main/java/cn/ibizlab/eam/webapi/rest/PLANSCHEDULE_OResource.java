package cn.ibizlab.eam.webapi.rest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSONObject;
import javax.servlet.ServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import cn.ibizlab.eam.webapi.dto.*;
import cn.ibizlab.eam.webapi.mapping.*;
import cn.ibizlab.eam.core.eam_core.domain.PLANSCHEDULE_O;
import cn.ibizlab.eam.core.eam_core.service.IPLANSCHEDULE_OService;
import cn.ibizlab.eam.core.eam_core.filter.PLANSCHEDULE_OSearchContext;
import cn.ibizlab.eam.util.annotation.VersionCheck;

@Slf4j
@Api(tags = {"自定义间隔天数" })
@RestController("WebApi-planschedule_o")
@RequestMapping("")
public class PLANSCHEDULE_OResource {

    @Autowired
    public IPLANSCHEDULE_OService planschedule_oService;

    @Autowired
    @Lazy
    public PLANSCHEDULE_OMapping planschedule_oMapping;

    @PreAuthorize("hasPermission(this.planschedule_oMapping.toDomain(#planschedule_odto),'eam-PLANSCHEDULE_O-Create')")
    @ApiOperation(value = "新建自定义间隔天数", tags = {"自定义间隔天数" },  notes = "新建自定义间隔天数")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_os")
    public ResponseEntity<PLANSCHEDULE_ODTO> create(@Validated @RequestBody PLANSCHEDULE_ODTO planschedule_odto) {
        PLANSCHEDULE_O domain = planschedule_oMapping.toDomain(planschedule_odto);
		planschedule_oService.create(domain);
        PLANSCHEDULE_ODTO dto = planschedule_oMapping.toDto(domain);
		return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_oMapping.toDomain(#planschedule_odtos),'eam-PLANSCHEDULE_O-Create')")
    @ApiOperation(value = "批量新建自定义间隔天数", tags = {"自定义间隔天数" },  notes = "批量新建自定义间隔天数")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_os/batch")
    public ResponseEntity<Boolean> createBatch(@RequestBody List<PLANSCHEDULE_ODTO> planschedule_odtos) {
        planschedule_oService.createBatch(planschedule_oMapping.toDomain(planschedule_odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @VersionCheck(entity = "planschedule_o" , versionfield = "updatedate")
    @PreAuthorize("hasPermission(this.planschedule_oService.get(#planschedule_o_id),'eam-PLANSCHEDULE_O-Update')")
    @ApiOperation(value = "更新自定义间隔天数", tags = {"自定义间隔天数" },  notes = "更新自定义间隔天数")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_os/{planschedule_o_id}")
    public ResponseEntity<PLANSCHEDULE_ODTO> update(@PathVariable("planschedule_o_id") String planschedule_o_id, @RequestBody PLANSCHEDULE_ODTO planschedule_odto) {
		PLANSCHEDULE_O domain  = planschedule_oMapping.toDomain(planschedule_odto);
        domain .setPlanscheduleOid(planschedule_o_id);
		planschedule_oService.update(domain );
		PLANSCHEDULE_ODTO dto = planschedule_oMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @PreAuthorize("hasPermission(this.planschedule_oService.getPlanscheduleOByEntities(this.planschedule_oMapping.toDomain(#planschedule_odtos)),'eam-PLANSCHEDULE_O-Update')")
    @ApiOperation(value = "批量更新自定义间隔天数", tags = {"自定义间隔天数" },  notes = "批量更新自定义间隔天数")
	@RequestMapping(method = RequestMethod.PUT, value = "/planschedule_os/batch")
    public ResponseEntity<Boolean> updateBatch(@RequestBody List<PLANSCHEDULE_ODTO> planschedule_odtos) {
        planschedule_oService.updateBatch(planschedule_oMapping.toDomain(planschedule_odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasPermission(this.planschedule_oService.get(#planschedule_o_id),'eam-PLANSCHEDULE_O-Remove')")
    @ApiOperation(value = "删除自定义间隔天数", tags = {"自定义间隔天数" },  notes = "删除自定义间隔天数")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_os/{planschedule_o_id}")
    public ResponseEntity<Boolean> remove(@PathVariable("planschedule_o_id") String planschedule_o_id) {
         return ResponseEntity.status(HttpStatus.OK).body(planschedule_oService.remove(planschedule_o_id));
    }

    @PreAuthorize("hasPermission(this.planschedule_oService.getPlanscheduleOByIds(#ids),'eam-PLANSCHEDULE_O-Remove')")
    @ApiOperation(value = "批量删除自定义间隔天数", tags = {"自定义间隔天数" },  notes = "批量删除自定义间隔天数")
	@RequestMapping(method = RequestMethod.DELETE, value = "/planschedule_os/batch")
    public ResponseEntity<Boolean> removeBatch(@RequestBody List<String> ids) {
        planschedule_oService.removeBatch(ids);
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostAuthorize("hasPermission(this.planschedule_oMapping.toDomain(returnObject.body),'eam-PLANSCHEDULE_O-Get')")
    @ApiOperation(value = "获取自定义间隔天数", tags = {"自定义间隔天数" },  notes = "获取自定义间隔天数")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_os/{planschedule_o_id}")
    public ResponseEntity<PLANSCHEDULE_ODTO> get(@PathVariable("planschedule_o_id") String planschedule_o_id) {
        PLANSCHEDULE_O domain = planschedule_oService.get(planschedule_o_id);
        PLANSCHEDULE_ODTO dto = planschedule_oMapping.toDto(domain);
        return ResponseEntity.status(HttpStatus.OK).body(dto);
    }

    @ApiOperation(value = "获取自定义间隔天数草稿", tags = {"自定义间隔天数" },  notes = "获取自定义间隔天数草稿")
	@RequestMapping(method = RequestMethod.GET, value = "/planschedule_os/getdraft")
    public ResponseEntity<PLANSCHEDULE_ODTO> getDraft(PLANSCHEDULE_ODTO dto) {
        PLANSCHEDULE_O domain = planschedule_oMapping.toDomain(dto);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_oMapping.toDto(planschedule_oService.getDraft(domain)));
    }

    @ApiOperation(value = "检查自定义间隔天数", tags = {"自定义间隔天数" },  notes = "检查自定义间隔天数")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_os/checkkey")
    public ResponseEntity<Boolean> checkKey(@RequestBody PLANSCHEDULE_ODTO planschedule_odto) {
        return  ResponseEntity.status(HttpStatus.OK).body(planschedule_oService.checkKey(planschedule_oMapping.toDomain(planschedule_odto)));
    }

    @PreAuthorize("hasPermission(this.planschedule_oMapping.toDomain(#planschedule_odto),'eam-PLANSCHEDULE_O-Save')")
    @ApiOperation(value = "保存自定义间隔天数", tags = {"自定义间隔天数" },  notes = "保存自定义间隔天数")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_os/save")
    public ResponseEntity<PLANSCHEDULE_ODTO> save(@RequestBody PLANSCHEDULE_ODTO planschedule_odto) {
        PLANSCHEDULE_O domain = planschedule_oMapping.toDomain(planschedule_odto);
        planschedule_oService.save(domain);
        return ResponseEntity.status(HttpStatus.OK).body(planschedule_oMapping.toDto(domain));
    }

    @PreAuthorize("hasPermission(this.planschedule_oMapping.toDomain(#planschedule_odtos),'eam-PLANSCHEDULE_O-Save')")
    @ApiOperation(value = "批量保存自定义间隔天数", tags = {"自定义间隔天数" },  notes = "批量保存自定义间隔天数")
	@RequestMapping(method = RequestMethod.POST, value = "/planschedule_os/savebatch")
    public ResponseEntity<Boolean> saveBatch(@RequestBody List<PLANSCHEDULE_ODTO> planschedule_odtos) {
        planschedule_oService.saveBatch(planschedule_oMapping.toDomain(planschedule_odtos));
        return  ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_O-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_O-Get')")
	@ApiOperation(value = "获取数据集", tags = {"自定义间隔天数" } ,notes = "获取数据集")
    @RequestMapping(method= RequestMethod.GET , value="/planschedule_os/fetchdefault")
	public ResponseEntity<List<PLANSCHEDULE_ODTO>> fetchDefault(PLANSCHEDULE_OSearchContext context) {
        Page<PLANSCHEDULE_O> domains = planschedule_oService.searchDefault(context) ;
        List<PLANSCHEDULE_ODTO> list = planschedule_oMapping.toDto(domains.getContent());
        return ResponseEntity.status(HttpStatus.OK)
                .header("x-page", String.valueOf(context.getPageable().getPageNumber()))
                .header("x-per-page", String.valueOf(context.getPageable().getPageSize()))
                .header("x-total", String.valueOf(domains.getTotalElements()))
                .body(list);
	}

    @PreAuthorize("hasAnyAuthority('ROLE_SUPERADMIN','eam-PLANSCHEDULE_O-searchDefault-all') and hasPermission(#context,'eam-PLANSCHEDULE_O-Get')")
	@ApiOperation(value = "查询数据集", tags = {"自定义间隔天数" } ,notes = "查询数据集")
    @RequestMapping(method= RequestMethod.POST , value="/planschedule_os/searchdefault")
	public ResponseEntity<Page<PLANSCHEDULE_ODTO>> searchDefault(@RequestBody PLANSCHEDULE_OSearchContext context) {
        Page<PLANSCHEDULE_O> domains = planschedule_oService.searchDefault(context) ;
	    return ResponseEntity.status(HttpStatus.OK)
                .body(new PageImpl(planschedule_oMapping.toDto(domains.getContent()), context.getPageable(), domains.getTotalElements()));
	}



}

